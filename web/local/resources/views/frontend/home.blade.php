<div class="row" ng-if="product_compare.length>0 && remove_compare!='1'">
	<div class="compare_box col-md-7 col-md-offset-3">
		<div class="comp-box " ng-repeat="comp_pro in product_compare">
		    	<img src="{{URL::asset('uploads')}}/<%comp_pro.all_img[0].image%>" width="43"> 
		    	<a href="comp_pro.slug"><%comp_pro.pro_name%></a>  
		    	<span class="com_cut" ng-click="remove_comp(comp_pro.id);" style="cursor:pointer">x</span>
		</div>
		<div class="comp-box " ng-repeat="comp_pro in getNumber(4-product_compare.length)">
		    	
		</div>
		<div class=" com-box_but " >
		<button class="btn btn-primary">Compare</button>
		</div>	
		<span class="com_cut_slas" ng-click="rem_comp_box();" style="cursor:pointer">x</span>	
	</div>
</div>
<div class="row img-bm-resp">
        	<div class="col-xs-12 img-bm">
            	<img src="{{URL::asset('frontend')}}/images/top-ban.jpg" alt="" class="img-responsive">           
                </div>
    	</div>
<div class="row">
        	<!--Middle Part Start-->
        	<div id="content1" class="col-xs-12">
          		<div class="row">
            		<div class="col-sm-9 col-xs-12 img_sli">
                           <!--  Angular Slideshow Start-->
                           <div>
								 <data-owl-carousel id="carousel_100" class="owl-carousel about-carousel sl_ban" data-options="{autoPlay: 5000, stopOnHover: true,  
        slideSpeed : 300, paginationSpeed : 400, singleItem : true}">
                        
                          <div owl-carousel-item="" ng-repeat="slid_data in slides">
                          
                                <div class=" clearfix">
                                
                                    <div class="image">
                                        <a href="javascript:void(0)"><img bn-lazy-src="<%slid_data.src%>" alt="" title="" class="img-responsive" border="0" /></a>                        </div>
                                    
                                    
                                </div>
                          </div>           
                   
          		  </data-owl-carousel>
  
								 
								</div>
                        <!--<div id="slides_control1">
						<div>
						  <carousel interval="myInterval">
							<slide ng-repeat="slide in slides" active="slide.active">
							  <img ng-src="<%slide.image%>">
							  <div class="carousel-caption">
								<>Slide <%$index+1%></h4>
							  </div>
							</slide>
						  </carousel>
						</div>
					  </div>-->
                            <!-- Slideshow End-->
            		</div>
                 
            		<div class="col-sm-3 col-xs-12  flip">
              			<div class="marketshop-banner ">
                			<div class="row">
                  				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 mrk_lft"> <a href="javascript:void(0)"><img title="sample-banner1" alt="sample-banner1" src="{{URL::asset('frontend')}}/images/banner/sp-small-banner-1.jpg"></a></div>
                  				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 mar-bar mrk_rgt"> <a href="javascript:void(0)"><img title="sample-banner" alt="sample-banner" src="{{URL::asset('frontend')}}/images/banner/sp-small-banner-2.jpg"></a></div>
                			</div>
              			</div>
            		</div>
          		</div>
                
                <div class="row img-bm1">
            		<div class="col-sm-9 img-bn_res">
                    	<div class="row advrt">
                    	<div class="col-sm-6"><a href="javascript:void(0)"><img src="{{URL::asset('frontend')}}/images/sm-01.jpg" alt="" class="img-responsive"></a></div>
                        <div class="col-sm-6"><a href="javascript:void(0)"><img src="{{URL::asset('frontend')}}/images/sm-02.jpg" alt="" class="img-responsive"></a></div>
                        </div>
            		</div>
            		<div class="col-sm-3 flip">
                    	<a href="javascript:void(0)"><img src="{{URL::asset('frontend')}}/images/sm-03.jpg" alt="" class="img-responsive"></a></div>                
                </div>
          		<!-- New Product Start-->
               
          		<h3 class="subtitle" >New Products</h3>          		
                        <data-owl-carousel id="carousel_1" class="owl-carousel pro new_pro about-carousel" >
                        
                          <div owl-carousel-item="" ng-repeat="(key, slid_data ) in home_products.new_pro" ng-init="slid_data.compare_pro='Compare'">                          
                                <div class="product-thumb clearfix">
                                <div class="button-group hidden-xs hidden-sm">
                                 <div class="add-to-links">
                                    <div class="row ico_data">
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <button type="button" data-toggle="tooltip" ng-init="favorite[key]='Favorite'" data-ng-click="fav(key)"  title="" class="btn btn-primary btn-block"  data-original-title="Add to Wish List">
                                          <i class="fa fa-heart"></i> <span><%favorite[key]%></span></button>
                                       </div>
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <button type="button" data-toggle="tooltip" title=""  ng-click="add_to_compare(slid_data)" ng-if="slid_data.compare_pro=='Compare'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Compare</span></button>
                                           <button type="button" data-toggle="tooltip" title=""  ng-if="slid_data.compare_pro=='Added'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Added</span></button>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12 vie_prodmod view_pro"> 
                                          <a href="<%slid_data.slug%>" data-toggle="modal" class="view_pro"><img src="{{URL::asset('frontend')}}/images/eye.png" alt="view" /></a>
                                          <button ui-sref="Modal.productmodel({id:slid_data.id})"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                                    <div class="image">
                                        <a href="javascript:void(0)"><img bn-lazy-src="{{URL::asset('uploads')}}/<%slid_data.all_img[0].image%>" alt="<%slid_data.pro_name%>" title="<%slid_data.pro_name%>" class="img-responsive" border="0" /></a> 
									</div>
                                    <div class="caption">
                                        <h4><a href="<%slid_data.slug%>"><%slid_data.pro_name%></a></h4>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <div class="rating">
                                                   <span class="fa fa-stack" ng-repeat="i in  getNumber(slid_data.avg_review)"><i class="fa fa-star fa-stack-2x"></i></span> 
                                                   <span class="fa fa-stack" ng-repeat="i in  getNumber(5-slid_data.avg_review)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
                                                 </div>
                                            </div>                            
                                            <div class="col-xs-12 col-md-6">
                                               <!-- <p class="price"> <%slid_data.price%>  </p>
												<p class="price"> <%slid_data.price%>  </p>-->
												   <p class="price"> 
												    <span class="price-old" ng-if="slid_data.prod_price !=''">Rs.<%slid_data.price%></span> 
												    <span class="price-new"  ng-if="slid_data.prod_price !=''">Rs.<%slid_data.prod_price%></span>
												    <span class="price-new"  ng-if="slid_data.prod_price ==''">Rs.<%slid_data.price%></span>
												   
												   </p>
                                            </div>
                                        </div>
										 <span class="saving" ng-if="slid_data.percent!=''"><%slid_data.percent%> % Off</span>  
                                    </div>
                                    
                                </div>
                          </div>           
                   
          		  </data-owl-carousel>
                
                
                
          		<!-- Popular Product Start-->
          		<h3 class="subtitle">Popular Products</h3>
                         <data-owl-carousel id="carousel_2" class="owl-carousel pro new_pro about-carousel" >
                          <div owl-carousel-item="" ng-repeat="(key,slid_data) in  home_products.popular"  ng-init="slid_data.compare_pro='Compare'">
                                <div class="product-thumb clearfix">
                                 <div class="button-group hidden-xs hidden-sm">
                                 <div class="add-to-links">
                                    <div class="row ico_data">
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <button type="button" data-toggle="tooltip" title="" ng-init="popular_fav[key]='Favorite'" data-ng-click="populr_fav(key)" class="btn btn-primary btn-block"  data-original-title="Add to Wish List">
                                          <i class="fa fa-heart"></i> <span><%popular_fav[key]%></span></button>
                                       </div>
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                         <button type="button" data-toggle="tooltip" title=""  ng-click="add_to_compare(slid_data)" ng-if="slid_data.compare_pro=='Compare'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Compare</span></button>
                                           <button type="button" data-toggle="tooltip" title=""  ng-if="slid_data.compare_pro=='Added'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Added</span></button>
                                       </div>
                                    </div>
                                    <div class="row"><div class="col-md-12 vie_prodmod view_pro"> 
                                               <a href="<%slid_data.slug%>" data-toggle="modal" class="view_pro"><img src="{{URL::asset('frontend')}}/images/eye.png" alt="view" /></a>
                                          <button ui-sref="Modal.productmodel({id:slid_data.id})"><i class="fa fa-eye" aria-hidden="true"></i></button>
										</div>
                                    </div>
                                 </div>
                              </div>
                                    <div class="image">
                                        <a href="javascript:void(0)"><img bn-lazy-src="{{URL::asset('uploads')}}/<%slid_data.all_img[0].image%>" alt="<%slid_data.pro_name%>" title="<%slid_data.pro_name%>" class="img-responsive" /></a>   
									</div>
                                    <div class="caption">
                                        <h4><a href="<%slid_data.slug%>"><%slid_data.pro_name%></a></h4>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <div class="rating"> 
												   <span class="fa fa-stack" ng-repeat="i in  getNumber(slid_data.avg_review)"><i class="fa fa-star fa-stack-2x"></i></span> 
                                                   <span class="fa fa-stack" ng-repeat="i in  getNumber(5-slid_data.avg_review)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span>  
													</div>
                                            </div>                            
                                            <div class="col-xs-12 col-md-6">
                                                    <span class="price-old" ng-if="slid_data.prod_price !=''">Rs.<%slid_data.price%></span> 
												    <span class="price-new"  ng-if="slid_data.prod_price !=''">Rs.<%slid_data.prod_price%></span>
												    <span class="price-new"  ng-if="slid_data.prod_price ==''">Rs.<%slid_data.price%></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                              
                            </div>
                 </data-owl-carousel>
            
         <!--  Featured Product End

           Categories Product Slider Start-->
          <div class="category-module" ng-repeat="ft_cat in home_products.feature_cat_upr" id="latest_category<%ft_cat.id%>">
          	<h3 class="subtitle"><%ft_cat.category.category_name%> </h3>
            	<div class="category-module-content">
              		<ul id="sub-cat" class="tabs">
                		<li  ng-repeat="subct in ft_cat.subcats" ng-class="{'active': isSetTab(ft_cat.id,subct.id)}" ><a href="javascript:void(0)"                          ng-click="setTab(ft_cat.id,subct.id)"><%subct.category_name%></a>
                		</li>
                	
              		</ul>
              		<div  ng-repeat="subct in ft_cat.subcats" id="tab-cat<%ft_cat.id%>_<%subct.id%>" ng-show="isSetTab(ft_cat.id,subct.id)" class="tab_content">
                		
                         <data-owl-carousel  id="carousel_<%ft_cat.id%>_<%subct.id%>" class="owl-carousel about-carousel" data-options="{itemsCustom : [[320, 2],[768, 3],[1200, 6],[1600, 6]]}">
                              <div class="latest_category_tabs" owl-carousel-item="" ng-repeat="(key,slid_data) in subct.products" ng-init="slid_data.compare_pro='Compare'">
                  			<div class="product-thumb slid_layou">
                             <div class="button-group hidden-xs hidden-sm">
                                 <div class="add-to-links">
                                    <div class="row ico_data">
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <button type="button" data-toggle="tooltip" title="" ng-init="electrn_fav_cat1[key]='Favorite'" data-ng-click="electron_favcat1(key)" class="btn btn-primary btn-block"  data-original-title="Add to Wish List">
                                          <i class="fa fa-heart"></i> <span><%electrn_fav_cat1[key]%></span></button>
                                       </div>
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                            <button type="button" data-toggle="tooltip" title=""  ng-click="add_to_compare(slid_data)" ng-if="slid_data.compare_pro=='Compare'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Compare</span></button>
                                           <button type="button" data-toggle="tooltip" title=""  ng-if="slid_data.compare_pro=='Added'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Added</span></button>
                                       </div>
                                    </div>
                                    <div class="row"><div class="col-md-12 vie_prodmod view_pro"> 
                                           <a href="<%slid_data.slug%>" data-toggle="modal" class="view_pro"><img src="{{URL::asset('frontend')}}/images/eye.png" alt="view" /></a> 
										 <button ui-sref="Modal.productmodel({id:slid_data.id})"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                    			<div class="image">
                               		<a href="javascript:void(0)"><img bn-lazy-src="{{URL::asset('uploads')}}/<%slid_data.all_img[0].image%>" alt="" title="<%slid_data.pro_name%>" class="img-responsive" /></a>                                </div>
                    			<div class="caption">
                      				<h4><a href="<%slid_data.slug%>"><%slid_data.pro_name%></a></h4>
                      				<div class="rating">
									 <span class="fa fa-stack" ng-repeat="i in  getNumber(slid_data.avg_review)"><i class="fa fa-star fa-stack-2x"></i></span> 
									 
                                                   <span class="fa fa-stack" ng-repeat="i in  getNumber(5-slid_data.avg_review)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> <span class="rev_c">(<%slid_data.count_review%>)</span>                                   </div>                                    
                      				 <p class="price"> 
		                                        <span class="price-old" ng-if="slid_data.prod_price !=''">Rs.<%slid_data.price%></span> 
						        <span class="price-new"  ng-if="slid_data.prod_price !=''">Rs.<%slid_data.prod_price%></span>
							<span class="price-new"  ng-if="slid_data.prod_price ==''">Rs.<%slid_data.price%></span>
		                                    	<span class="saving" ng-if="slid_data.percent!=''"><%slid_data.percent%> % Off</span>                                
					   </p>
                    			</div>
                    			
                  			</div>
                  			
                		</div>
                        </data-owl-carousel>
              		</div>
              		
            	</div>
				   
                 <!--<h5> <a class="view_1" href="javascript:void(0)"></a> Electronics</h5>-->
          	</div>
        
          <div class="marketshop-banner">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><a href="javascript:void(0)"><img title="Sample Banner" alt="Sample Banner 2" src="{{URL::asset('frontend')}}/images/banner/sun_shop.jpg"></a></div>
             
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				  <div class="row pi_mar">
					   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pi_marr">
					  <a href="javascript:void(0)"><img title="Sample Banner 3" alt="Sample Banner" src="{{URL::asset('frontend')}}/images/banner/shop_1.jpg"></a>					  </div>
					   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pi_marr">
					     <a href="javascript:void(0)"><img title="Sample Banner 3" alt="Sample Banner" src="{{URL::asset('frontend')}}/images/banner/shop_3.jpg"></a>				      </div>
				  </div>
				  <div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ad_res">
					  <a href="javascript:void(0)"><img title="Sample Banner 3" alt="Sample Banner" src="{{URL::asset('frontend')}}/images/banner/shop_2.jpg"></a>					  </div>
					   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ad_res">
					     <a href="javascript:void(0)"><img title="Sample Banner 3" alt="Sample Banner" src="{{URL::asset('frontend')}}/images/banner/shop_4.jpg"></a>						 </div>
				   </div>
			  </div>
            </div>
          </div>
         <!--  Banner End          
              Categories Product Slider Start-->
          <div class="category-module"  ng-repeat="ft_cat in home_products.feature_cat_lwr" id="latest_category<%ft_cat.id%>">
          	<h3 class="subtitle"><%ft_cat.category.category_name%> </h3>
            	<div class="category-module-content">
              		<ul id="sub-cat" class="tabs">
                		<li  ng-repeat="subct in ft_cat.subcats" ng-class="{'active': isSetTab(ft_cat.id,subct.id)}" ><a href="javascript:void(0)"                          ng-click="setTab(ft_cat.id,subct.id)"><%subct.category_name%></a></li>
              		</ul>
              		<div   ng-repeat="subct in ft_cat.subcats"  id="tab-cat<%ft_cat.id%>_<%subct.id%>" class="tab_content" ng-show="isSetTab(ft_cat.id,subct.id)" >
                		
                         <data-owl-carousel id="carousel_<%ft_cat.id%>_<%subct.id%>" class="owl-carousel about-carousel" data-options="{itemsCustom : [[320, 2],[768, 3],[1200, 6],[1600, 6]]}">
                              <div class="latest_category_tabs" owl-carousel-item=""  ng-repeat="(key,slid_data) in subct.products" ng-init="slid_data.compare_pro='Compare'">
                  			<div class="product-thumb slid_layou">
                             <div class="button-group hidden-xs hidden-sm">
                                 <div class="add-to-links">
                                    <div class="row ico_data">
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <button type="button" data-toggle="tooltip" title="" ng-init="electrn_fav_cat13[key]='Favorite'" data-ng-click="electron_favcat13(key)" class="btn btn-primary btn-block"  data-original-title="Add to Wish List">
                                          <i class="fa fa-heart"></i> <span><%electrn_fav_cat13[key]%></span></button>
                                       </div>
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <button type="button" data-toggle="tooltip" title=""  ng-click="add_to_compare(slid_data)" ng-if="slid_data.compare_pro=='Compare'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Compare</span></button>
                                           <button type="button" data-toggle="tooltip" title=""  ng-if="slid_data.compare_pro=='Added'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Added</span></button>
                                       </div>
                                    </div>
                                    <div class="row"><div class="col-md-12 vie_prodmod view_pro"> 
                                           <a href="<%slid_data.slug%>" data-toggle="modal" class="view_pro"><img src="{{URL::asset('frontend')}}/images/eye.png" alt="view" /></a> 
										 <button ui-sref="Modal.productmodel({id:slid_data.id})"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                    			<div class="image">
                               		<a href="javascript:void(0)"><img bn-lazy-src="{{URL::asset('uploads')}}/<%slid_data.all_img[0].image%>" alt="" title="<%slid_data.pro_name%>" class="img-responsive" /></a>                                </div>
                    			<div class="caption">
                      				<h4><a href="<%slid_data.slug%>"><%slid_data.pro_name%></a></h4>
                      				<div class="rating"> 
									 <span class="fa fa-stack" ng-repeat="i in  getNumber(slid_data.avg_review)"><i class="fa fa-star fa-stack-2x"></i></span> 
									 
                                    <span class="fa fa-stack" ng-repeat="i in  getNumber(5-slid_data.avg_review)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
									<span class="rev_c">(<%slid_data.count_review%>)</span>       
									</div>                                    
                      				<p class="price"> 
                                    	 
                                       <span class="price-old" ng-if="slid_data.prod_price !=''">Rs.<%slid_data.price%></span> 
										<span class="price-new"  ng-if="slid_data.prod_price !=''">Rs.<%slid_data.prod_price%></span>
										<span class="price-new"  ng-if="slid_data.prod_price ==''">Rs.<%slid_data.price%></span>
                                    	<span class="saving" ng-if="slid_data.percent!=''"><%slid_data.percent%> % Off</span>                                  </p>
                    			</div>
                    			
                  			</div>
                  			
                		</div>
                        </data-owl-carousel>
              		</div>
              		
            	</div>
				 
                <!--<h5><a class="view_3" href="javascript:void(0)"></a> Perfumes</h5>-->
          	</div>
        
          
         
        </div>
		
        <!--Middle Part End-->
      </div>


