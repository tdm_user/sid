<div class="container confirm_div" align="center">
	<div class="row" style="margin-top:23px;">
		<div class="col-md-12 col-sm-12 col-xs-12 order_ack">
			Thanks for your order
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 ack_msg">
			We will send you a confirmation email shortly.
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 c_order_div">
					<div class="col-md-12 col-sm-12 col-xs-12" style="color:#7a7a7a;font-size:15px;margin-top:24px;">
						Your order number is
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 confirmation_number">
						<% checkout_confirm[0].invoice_no %>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 mailing_address">
						If you have any queries please email us on <span style="color:#53b9c1;text-decoration: underline;">cs@shopindiscount.com</span>, or call us on (+977) 1 4780680 for any details regarding the order.
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 check_status">
						To check the status of your order online, sign in to your account and go to ‘Track Order’ page or check the link provided in the order confirmation email your received.
					</div>
		</div>
		
	</div>
</div>