<div class="row" ng-if="product_compare.length>0 && remove_compare!='1'">
	<div class="compare_box col-md-7 col-md-offset-3">
		<div class="comp-box " ng-repeat="comp_pro in product_compare">
		    	<img src="{{URL::asset('uploads')}}/<%comp_pro.all_img[0].image%>" width="43"> 
		    	<a href="comp_pro.slug"><%comp_pro.pro_name%></a>  
		    	<span class="com_cut" ng-click="remove_comp(comp_pro.id);" style="cursor:pointer">x</span>
		</div>
		<div class="comp-box " ng-repeat="comp_pro in getNumber(4-product_compare.length)">
		    	
		</div>
		<div class=" com-box_but " >
		<button class="btn btn-primary">Compare</button>
		</div>	
		<span class="com_cut_slas" ng-click="rem_comp_box();" style="cursor:pointer">x</span>	
	</div>
</div>
<div class="row list_pro">
            <!--Left Part Start -->
            <aside id="column-left" class="col-sm-2  hidden-xs hidden-sm">
               <div class="box-category">
                  <ul id="cat_accordion">
                    
                     <li>
                        <a href="javascript:void(0)" class="active">Brand</a> <span class="down"></span>                       
                        <ul>
                            <li>
                             <input type="text" id="sid_search1" placeholder="Enter Brand" class="form-control" ng-model="Brand" /> 
                            </li>
                           <li ng-repeat="subcategory_1 in category_content.brands| filter:Brand" ng-init="filter_brand[subcategory_1.id]=true">
                              <div class="checkbox">
                                 <label><input type="checkbox" ng-click="includebrand(subcategory_1.id)" ><%subcategory_1.brand_name%></label>
                              </div>
                           </li>
                          
                        </ul>
                        
                     </li>
                     <li ng-repeat="optgr in category_content.pro_options">
                        <a href="javascript:void(0)" class="active"><%optgr.option_name%></a> <span class="down"></span>                    
                        <ul>
                          <li>
                          <input type="text" id="sid_search_screen" placeholder="Enter <%optgr.option_name%>" class="form-control" ng-model="search[optgr.id]" />
                          </li>
                           <li ng-repeat="subcategory_1 in optgr.options | filter:search[optgr.id]">
                              <div class="checkbox">
                                 <label><input type="checkbox" ng-click="includeopt(subcategory_1.id)"  value=""><%subcategory_1.option_name%></label>
                              </div>
                           </li>
                          
                        </ul>
                     </li>
                     
                     <li>
                        <a href="javascript:void(0)" class="active">Price</a> <span class="down"></span>
                        <ul>
                           <li ng-repeat="subcategory_1 in sub_cat_price" ng-click="includeprice(subcategory_1.ids)"><a href="javascript:void(0)"><%subcategory_1.sub_price%></a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
               <div class="banner">
                 
                <a href="javascript:void(0)"><img src="{{URL::asset('frontend')}}/images/banner/cate_bana.jpg" alt="small banner1" class="img-responsive" /></a>
               </div>
            </aside>
            <!--Left Part End -->
            <!--Middle Part Start-->
            <div id="content" class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
               <h3 class="subtitle">Televisions<span class="more_cat"><a href="javascript:void(0)">(<%category_content.products.length%> Items)</a></span></h3>
               <!--Tabs Part Start-->
               <div class="panel panel-default">
                  <div class="panel-body product_sub">
                     <span class="short_c">Sort By:</span>
                     <ul class="nav nav-tabs short_c">
                        <li class="active" ng-click="order_by='-count_popularity'"><a  href="javascript:void(0)">Popularity</a></li>
                        <li ng-click="order_by='-avg_review'" ><a href="javascript:void(0)">Top ratings</a></li>
                        <li ng-click="order_by='-created_at'"><a href="javascript:void(0)">Newness</a></li>
                        <li ng-click="order_by='price'" ><a href="javascript:void(0)">Price Low to High </a></li>
                        <li ng-click="order_by='-price'" ><a href="javascript:void(0)">Price High to Low </a></li>
                        <li ng-click="order_by='-prod_price'" ><a href="javascript:void(0)">Discount: By Price </a></li>
                        <li ng-click="order_by='percent'"><a  href="javascript:void(0)">Discount: By Percent </a></li>
                     </ul>
                  </div>
               </div>
               <div class="tab-content tab_con">
                  <div class="tab-pane active" id="tab-1">
                       <div class="row products-category prd_new">
                         <div class="product-layout product-grid col-lg-2 col-md-3 col-sm-4 col-xs-12" ng-repeat="subcategory_1 in category_content.products | orderBy:order_by | filter:brandFilter | filter:optFilter| filter:priceFilter" ng-init="subcategory_1.compare_pro='Compare'">
                           <div class="product-thumb">
                              <div class="button-group hidden-xs hidden-sm">
                                 <div class="add-to-links">
                                    <div class="row ico_data">
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <button type="button" data-toggle="tooltip" title="" onclick="" class="btn btn-primary btn-block"  data-original-title="Add to Wish List">
                                          <i class="fa fa-heart"></i> <span>Favorite</span></button>
                                       </div>
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <button type="button" data-toggle="tooltip" title=""  ng-click="add_to_compare(subcategory_1)" ng-if="subcategory_1.compare_pro=='Compare'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Compare</span></button>
                                           <button type="button" data-toggle="tooltip" title=""  ng-if="subcategory_1.compare_pro=='Added'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Added</span></button>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12 vie_prodmod view_pro"> 
                                          <!-- <a href="#product-modal" data-toggle="modal" class="view_pro"><img src="images/eye.png" alt="view" /></a> -->
					<button ui-sref="Modal.productmodel({id:subcategory_1.id})"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="image">
                                 <a href="<%subcategory_1.slug%>"><img src="{{URL::asset('uploads')}}/<%subcategory_1.all_img[0].image%>" alt="<%subcategory_1.pro_name%>" title="<%subcategory_1.pro_name%>" class="img-responsive"></a>
                              </div>
                              <div>
                                 <div class="caption">
                                    <h4><a href="<%subcategory_1.slug%>"><% subcategory_1.pro_name %></a></h4>
                                    <div class="rating"> 
                                     <span class="fa fa-stack" ng-repeat="i in  getNumber(subcategory_1.avg_review)"><i class="fa fa-star fa-stack-2x"></i></span>
                <span class="fa fa-stack" ng-repeat="i in  getNumber(5-subcategory_1.avg_review)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
                <span class="rev_c">(<%subcategory_1.count_review%>)</span> 
                                                                        
                                    </div>
                                    <p class="price">                                      
                                         <span class="price-old" ng-if="subcategory_1.prod_price !=''">Rs.<%subcategory_1.price%></span> 
					 <span class="price-new"  ng-if="subcategory_1.prod_price !=''">Rs.<%subcategory_1.prod_price%></span>
					 <span class="price-new"  ng-if="subcategory_1.prod_price ==''">Rs.<%subcategory_1.price%></span>
                         	         <span class="saving" ng-if="subcategory_1.percent!=''"><%subcategory_1.percent%> % Off</span>                                  
                                    </p>
                                 </div>
                                 <div class="button-group dropdown" id="siml_pro" ng-if="subcategory_1.rela_pro.length > 0">
                                    <button class="btn btn-primary btn-block dropdown-toggle" type="button"  data-toggle="dropdown">
                                    <span class="tip" >See Similar Products</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                       <li role="presentation">
                                          <div class="row tool-con">
                                             <div class="col-md-12">
                                                <h5 ng-if="subcategory_1.rela_pro.length > 0"><%subcategory_1.rela_pro.length%> Similar Product (s) found</h5>
                                                <p class="tip_close">
                                                   <a href="javascript:void(0);" title="close"><img src="{{URL::asset('frontend')}}/images/close2.png" title="close"  alt="close"/></a>
                                                </p>
                                             </div>
                                          </div> 
                                            <data-owl-carousel id="carousel_6" class="owl-carousel product_carousel_tip too_cont about-carousel" data-options="{itemsCustom : [[320, 1],[768, 3],[1200,4],[1600, 4]]}">
                                            <div class="latest_category_tabs" owl-carousel-item="" ng-repeat="subcategory_rel in subcategory_1.rela_pro">
                                           <div class="product-thumb clearfix" >
                                                <div class="image">
                                                   <a href="javascript:void(0)"><img src="{{URL::asset('uploads')}}/<%subcategory_rel.all_img[0].image%>" alt="<%subcategory_rel.pro_name%>" title="<%subcategory_rel.pro_name%>" class="img-responsive" /></a>                        
                                                </div>
                                                <div class="caption">
                                                   <h4><a href="javascript:void(0)"><%subcategory_rel.pro_name%></a></h4>
                                                   <div class="row">
                                                      <div class="col-xs-6 col-md-6">
                                                         <span class="price-old" ng-if="subcategory_rel.prod_price !=''">Rs.<%subcategory_rel.price%></span> 
													
                                                      </div>
                                                       <div class="col-xs-6 col-md-6">
                                                       <span class="price-new"  ng-if="subcategory_rel.prod_price !=''">Rs.<%subcategory_rel.prod_price%></span>
						       <span class="price-new"  ng-if="subcategory_rel.prod_price ==''">Rs.<%subcategory_rel.price%></span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                            </div>
                                            </data-owl-carousel>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                         
                   
                       
					   <span class="clearfix visible-lg-block"></span>
                       </div>
                       
                      <div class="text-center row">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <p><a href="javascript:void(0)" class="btn btn-default load_cla" title="Load More"> Load more >> </a></p>
                           </div>
                       </div>
                      
                  </div>
				  
				  
				  
				  
                  
               </div>
               <!--Tabs Part End-->
               <div class="row text-center">
                  <div class="col-sm-12">
                     <div>
                        <div class="item"> <a href="javascript:void(0)"><img src="{{URL::asset('frontend')}}/images/banner/cate_bana2.jpg" alt="small banner" class="img-responsive" /></a> </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--Middle Part End -->
         </div>
 <script>
          /*---------------------------------------------------
        Categories Accordion
    ----------------------------------------------------- */
  jQuery(document).ready(function($){
	 
	  $('#cat_accordion').cutomAccordion({
        saveState: false,
        autoExpand: true
    });
	

		 $(".nav-tabs.short_c a").click(function(e){
        e.preventDefault();
        $(this).tab('show');
			});
	});

</script>


