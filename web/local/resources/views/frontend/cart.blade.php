<div class="row panel_spac">
<div class="col-lg-12 col-md-12">
<h3 class="subtitle">Shopping Cart </h3>
<div class="panel-group" id="accordion1">
<div class="panel panel-default">
 
  <div class="">
    <div class="panel-body">
        <div class="row text-center">
            
            <div class="col-sm-12 col-xs-12" >
                   
                   <div class="table-responsive" ng-if="cart_item.total_num > 0">
			  <table class="table  text-left cart_tb">
			     <thead>
			        <tr>
				   <th>#</th>
				   <th>Item Name</th>
				   <th>Image</th>
				   <th>Quantity</th>
				   <th>Price</th>
				   <th>Action</th>
				</tr>
			     </thead>
			     <tbody>
			           <tr ng-repeat="crt_itm in cart_item.cart_data">
			              <td ng-bind="crt_itm.id"></td>
			              <td ng-bind="crt_itm.name"></td>
			              <td ><img src="{{URL::asset('uploads/')}}/<%crt_itm.image%>" alt="<%crt_itm.name %>" width="100"/></td>
			              <td ><div class="form-group" ><input type="text" ng-model="crt_itm.quantity" size="2" class="form-control"/>
			              <span class="btn btn-primary qy_bu" ng-click="crt_itm.quantity=crt_itm.quantity+1">+</span>
			              <span class="btn btn-danger  qy_bu" ng-if="crt_itm.quantity > 1" ng-click="crt_itm.quantity=crt_itm.quantity-1">-</span>
			            
			              </div></td>
			              <td ><%crt_itm.price%> Rs.</td>
			              <td><i class="fa fa-edit" ng-click="update_cart(crt_itm);"></i><i class="fa fa-trash" ng-click="delete_item(crt_itm.id);" style="color:#f34948"></i></td>		              
			           </tr>
			     </tbody>
			     <tfoot>
			           <th>#</th>
				   <th>Item Name</th>
				   <th>Image</th>
				   <th>Quantity</th>
				   <th>Price</th>
				   <th>Action</th>
			     </tfoot>
			     
			  </table>
	        </div>
	        <div class="empty_cart"  ng-if="cart_item.total_num == 0"><p>Your Shopping Cart is empty.</p>
	        <a href="#/" class="btn btn-primary" >Continue Shopping</a>
	        </div>
	         <div class="col-md-6 col-sm-12 col-xs-12 pull-right" ng-if="cart_item.total_num > 0">
			    <table class="table  text-left cart_tb tol_pr">
			        <tr>
			          <td class="text-right">Total </td>
			          <td><%cart_item.total%> Rs.</td>
			        </tr>
			        
			    </table>
			
			<button type="button" class="btn btn-primary pull-right ">Checkout</button>
			<button type="button" class="btn btn-danger pull-right ck_btn" data-toggle="modal" data-target="#del_modal" >Empty Cart</button>
		
		  <!-- Modal -->
               <div class="modal fade" id="del_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Empty Cart</h4>
                          </div>
                          <div class="modal-body">
                           Are you sure you want to empty the shopping cart.
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>                           
                               
                               <button  class="btn btn-danger" data-dismiss="modal" ng-click="empty_cart();" >Empty Cart</button>
                           
                          </div>
                        </div>
                      </div>
                    </div>	 
	      </div>
            </div>
            
        </div>
        
    </div>
  </div>
</div>



</div>
</div>

</div> 
