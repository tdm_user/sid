<div ng-if="shipping_data.logged_in=='no'" class="container-fluid text-center">
    <h4> Please login then click checkout.</h4>
</div>
<div ng-if="shipping_data.logged_in=='yes'">
<div class="container-fluid">
       <div class="alert alert-success" ng-if="success">
            <p >
            <% success_flash %>
            </p>
        </div>
        <div class="alert alert-danger"  ng-if="error">
            <p>
            <% error %>
            </p>
        </div> 
	<div class="col-xs-12 col-md-12 col-lg-12 col-sm-12 top_heading">
		<h3>Select your shipping address</h3>
	</div>

	<div class="container-fluid main_address_div">
		<div class="col-md-12 col-xs-12 col-sm-12 address_box_radio" ng-repeat="add in shipping_data.addresses">
			<div class="col-md-6 col-sm-2 col-xs-12">
				 
					<div class="col-md-1 col-xs-12 col-sm-1">
						<input type="radio" class="form_control " name="address_radio" ng-model="checkship.address" value="<%add.id%>">
					</div>
					<div class="col-md-11 col-sm-11 col-xs-12 address">
						<label class="address_checkbox"><%add.ship_fname%> <%add.ship_lname%></label><br>
							<%add.ship_address%>,Near <%add.ship_landmark%><br>
							<%add.city%>, <%add.state%><br>
							<%add.country%>,<%add.ship_mobile%><br>
						         <button ui-sref="Modal.editAddress({id:add.id})" class="address_btn">Edit</button>
							<!---<a ui-sref="Modal.editAddress({id:add.id})" class="address_a">Edit</a>-->
					</div>
				
			</div>
		</div>
		
		<div class="col-md-6 col-xs-12 col-sm-6 address_box_add">
				<div class="col-md-12 col-xs-12 col-sm-12 btn-div">
					<button ui-sref="Modal.AddNewAddress" class="btn-style">+</button>
				</div>
				<div class="col-md-12 col-xs-12 col-sm-12 btn-name">Add New Address</div>

	
		</div>
	</div>

	<!------------------------------------Review  and confirm order------------------------------------------------>

	<div class="col-md-12 col-xs-12 col-sm-12 subtitle review_div">
			Review  and confirm your order <span class="review_no"> (<%shipping_data.total_num%> Items)</span>
	</div>

	<div class="col-md-12 col-xs-12 col-sm-12 main_review_div">
		
		<div class="col-md-12 col-xs-12 col-sm-12 review_seller">
			<span class="seller_name">Seller:</span>obiw
			
			<span class="review_icon"><img src="{{URL::asset('frontend')}}/images/chat.png" height="15px">&nbsp;&nbsp;Chat with Seller</span>
			
			<span class="review_icon"><img src="{{URL::asset('frontend')}}/images/user_n.png" height="15px">&nbsp;&nbsp;Follow Seller</span>
			
		</div>
		<div class="col-md-12 col-xs-12 col-sm-12 review_table table-responsive">
			<table class="table product-table-style">
				<tr class="table_heading">
					<td>Product Details</td>
					<td>Quantity</td>
					<td class="price_p">Price</td>
					<td class="shipping_p">Shipping Cost</td>
					<td class="action_p">Action</td>
				</tr>
				<tr ng-repeat="crtdata in shipping_data.cart_data">
					<td>
						<div class="col-md-12 col-sm-12 col-xs-12 padding_style">
							<div class="col-md-2 col-sm-2 col-xs-3 image_style">
								<img src="{{URL::asset('uploads')}}/<%crtdata.image%>" alt="<%crtdata.name %>" width="100"/>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-9">
								<div class="col-md-12 col-sm-12 col-xs-3">
									<%crtdata.name%>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="col-md-3 col-sm-3 col-xs-6">
										Capacity
									</div>
									<div class="col-md-9 col-sm-9 col-xs-6">
										16 GB
									</div>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="col-md-3 col-sm-3 col-xs-6">
										Color
									</div>
									<div class="col-md-9 col-sm-9 col-xs-6">
										Red
									</div>
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="col-md-12 col-sm-12 col-xs-12 input-group quantity">
							
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-number btn_style"  data-type="minus"  ng-if="crtdata.quantity > 1" ng-click="crtdata.quantity=crtdata.quantity-1">
									  <span class="glyphicon glyphicon-minus"></span>
								</button>
							</span>
							<input type="text" name="quant[1]" ng-model="crtdata.quantity" class="form-control input-number" value="1" min="1" max="10" height="26">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-number btn_style" data-type="plus" data-field="quant[1]" ng-click="crtdata.quantity=crtdata.quantity+1">
									<span class="glyphicon glyphicon-plus"></span>
								</button>
							</span>
						
						</div>
					</td>
					<td>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<span class="cart-data-style" ng-if="crtdata.prod_price!=''">Rs.<%crtdata.pri%></span>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<span class="cost" ng-if="crtdata.prod_price!=''">Rs.<%crtdata.prod_price%></span>
								<span class="cost" ng-if="crtdata.prod_price==''">Rs.<%crtdata.pri%></span>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<span class="offer" ng-if="crtdata.percent!=''"><%crtdata.percent%>% Off</span>
							</div>
						</div>
					</td>
					<td>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<select class="selectpicker select_custom">
							  <option>SID Standard Delivery</option>
							  <option>1</option>
							  <option>2</option>
							</select><br>
							<span class="delivery_cost"><b>Rs. 150</b></span><br>
							<span class="deliver">Delivery Time:</span><span style="delivery_time">1-3 days</span>
						</div>
					</td>
					<td>
						<div class="col-sm-12 col-md-12 col-xs-12">
							<div class="col-md-12 col-sm-12 col-xs-12 action _div">
								<i class="fa fa-heart fav_remove" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Favorite
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<i class="fa fa-trash-o fav_remove" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Remove
							</div>
						</div>
					</td>
				</tr>
				
			</table>
		</div>
				<!------------------------------------------End of top row------------------------------------------>

		<div class="col-xs-12 col-md-6 col-sm-6 col-xs-6" style="width:1303px;margin-top:24px;margin-bottom:24px;">
			<div class="col-xs-4 col-md-4 col-sm-4" style="width:732px;padding:0px;">
				<div class="row col-xs-12" >
					<div class="col-xs-2 store_promo">
						<span>Store Promotion:</span>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12 dropdown">
						<button class="btn btn-default dropdown-toggle get_seller_coupon" type="button" data-toggle="dropdown">Get Seller Coupon
						<span class="caret caret_color"></span></button>
						<ul class="dropdown-menu">
						  <li ng-repeat="coup in shipping_data.coupons">
						      <div class="col-md-6 col-sm-6 col-xs-6"> 
							      <p><%coup.description%></p> 
							      <span>Expires On <%coup.expire_date%></span>              
							  </div>
							   <div class="col-md-6 col-sm-6 col-xs-6">
							        <button class='btn btn-code'>
									    Coupon Code
										<%coup.coupon_name%>
									</button> 
							   </div>
						  </li>
						</ul>
					</div>
					<div class="col-md-5 col-xs-5 col-sm-5 input-group padding">
						
						
							<input type="text" class="form-control coupon_code" placeholder="Enter Coupon code Here" ng-model="shipping_data.coupon">
								
							<label type="button" class="btn apply-btn" ng-click="apply_coupon(shipping_data.coupon);">Apply</label>
						

					</div>

					<div class="col-sm-2 col-md-2 col-xs-12 seller-store">
						<span>Go to Seller Store ></span>
					</div>
				</div>

				<div class="col-xs-12 col-md-12 col-sm-12 cust_note_detail">
					<div class="col-md-2 col-sm-2 col-xs-12 cust_note">Customer Note:</div>
					<div class="col-md-10 col-sm-10 col-xs-12 padding_r">
						<input type="text" class="form-control cust_feild" ng-model="shipping_data.customer_note">
					</div>
				</div>
			</div>
			
			<div class="col-sm-4 col-md-4 col-xs-12 saving_div">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<span class="total_save">Total Savings:</span><span class="total_save_price"><b>Rs 1,647.00</b></span>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 cost_count">
				<div class="col-xs-12 col-sm-12 col-md-12 subtotal">
					<span>Subtotal:</span><span class="cost_font"><b>Rs 37,302.00</b></span>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 shipping_cost">
					<span>Shipping Cost:</span><span class="cost_font"><b>Rs 300.00</b></span>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 total">
					<span>Total:</span><span class="cost_font"><b>Rs <%shipping_data.total%></b></span>
				</div>
			</div>
		</div>
	</div>
	
</div>
<div class="container-fluid">
	<div class="col-md-6 col-sm-6 col-xs-12 padding_s">
		<a class="continue_shop">< Continue Shopping</a>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 tsaving_tammount">
		<span class="total_savings">All Total Savings:</span><span class="saving_ammount">Rs.0</span>
		<span class="line_style">|</span>
		<span class="all_total">All Total:</span><span class="total_ammount">Rs.<%shipping_data.total%></span><br>
		<button class="btn btn-primary" ng-click="continue_checkout(checkship);">Continue</button>
	</div>
</div>

	<!-------------------------------------------------------start products which can also bought-------------------------->
		<h3 class="subtitle" style="clear:both;">Customers who viewed this product also viewed</h3>

 <div class="category-module" id="latest_category">

<data-owl-carousel id="carousel_6" class="owl-carousel about-carousel" data-options="{itemsCustom : [[320, 2],[768, 3],[1200, 6],[1600, 6]]}">
  <div class="latest_category_tabs" owl-carousel-item="" ng-repeat="slid_data in e_product_slide_a_phon">
     <div class="product-thumb slid_layou">
    <div class="button-group hidden-xs hidden-sm">
     <div class="add-to-links">
        <div class="row ico_data">
           <div class="col-lg-6 col-md-6 col-sm-6">
              <button type="button" data-toggle="tooltip" title="" onClick="" class="btn btn-primary btn-block"  data-original-title="Add to Wish List">
              <i class="fa fa-heart"></i> <span>Favorite</span></button>
           </div>
           <div class="col-lg-6 col-md-6 col-sm-6">
              <button type="button" data-toggle="tooltip" title="" onClick="" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
              <img src="images/compare.png"><span>Compare</span></button>
           </div>
        </div>
        <div class="row">
        
        <div class="col-md-12 vie_prodmod view_pro"> 
                                          <!-- <a href="#product-modal" data-toggle="modal" class="view_pro"><img src="images/eye.png" alt="view" /></a> -->
				   <button ui-sref="Modal.productmodel"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                    </div>
        </div>
     </div>
  </div>
    <div class="image">
        <a href="javascript:void(0)"><img bn-lazy-src="<%slid_data.thum_img%>" alt="" title="Aspire Ultrabook Laptop" class="img-responsive" /></a>                                </div>
    <div class="caption">
        <h4><a href="javascript:void(0)"><%slid_data.title%></a></h4>
        <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> <span class="rev_c">(165)</span>                                   </div>                                    
        <p class="price"> 
            
            <span class="price-old"><%slid_data.d_price%></span>
            <span class="price-new"><%slid_data.price%></span>  
            <span class="saving" ng-if="slid_data.off_p"><%slid_data.off_p%></span>                                    </p>
    </div>
    
</div>
  </div>
</data-owl-carousel>	

</div>
</div>

