<!DOCTYPE html>
<html  ng-app="myApp">
<head>
<meta charset="UTF-8"/>
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="{{URL::asset('frontend/images/favicon.png')}}" rel="icon" />
<title>SID</title>
<meta name="description" content="Responsive and clean html template design for any kind of ecommerce webshop">
<!-- CSS Part Start-->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/webslidemenu.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/library/css/framework.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/style.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/global.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/responsive.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/ng-tiny-scrollbar.min.css') }}" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"> 
<link rel="stylesheet" href="{{ URL::asset('frontend/css/animate.min.css') }}">
</head>
<body ng-controller="MyCtrl">
    <div class="wrapper-wide">
        <div id="header">
         <!-- Top Bar Start-->
    <nav id="top" class="htop">
      		<div class="container">
        		<div class="row"> 
                <div class="col-xs-12">
                	<span class="drop-icon visible-sm visible-xs me_top"><i class="fa fa-align-justify"></i></span>
          			<div class="flip">
            			<div class="links-left">
                        	<div id="currency" class="btn-group">
             					<button class="btn-link"><img src="{{URL::asset('frontend/images/flags/nrs.png')}}" alt=""> <span> NRS <i class="fa fa-caret-down"></i></span></button>
              					<!-- data-toggle="dropdown"  <ul class="dropdown-menu">
                                    <li><button class="currency-select btn btn-link btn-block" type="button" name="USD"><img src="images/flags/gb.png"> US Dollar</button></li>
              					</ul>-->
            				</div>
							
								<ul>
									<li class="mobile"><i class="fa fa-phone"></i> Phone: (+977) 1 4780680</li>
									<li><a href="javascript:void(0)">Home Delivery</a></li>                               
								</ul>
							
            			</div>

          				<div id="top-links" class="left-top nav pull-right flip">
          					<div class="links">
            					<ul>
                                    <li><a href="javascript:void(0)">Advertise</a></li>
                                    <li><a href="javascript:void(0)">24x7 Customer Care</a></li> 
                                    <li><a href="javascript:void(0)">Track Order</a></li>
                                    <li><a href="javascript:void(0)">Deal of the Day</a></li>
                                    <li><a href="javascript:void(0)">Sell with Us</a></li>         
                                   <!-- <li><a href="#login-modal" data-toggle="modal">Log In</a></li>-->
                                    <button class="bt_lo" ui-sref="Modal.login" ng-if="auth_user==false" >Log In</button>
                                    <button class="bt_lo" ng-click="user_logout();" ng-if="auth_user">Log Out</button>
            					</ul>
            				</div>
          				</div>
          			</div>
                    </div>
        		</div>
      		</div>
    	</nav>
    <!-- Top Bar End-->
    <!-- Header Start-->
    <div class="row mob_lo">
    	<div class="col-xs-3">
        <div class="wsmenuexpandermain slideRight"><a id="navToggle" class="animated-arrow slideLeft" href="javascript:void(0)"><span></span></a></div>
        <div class="wsmenucontent overlapblackbg"></div> 
      
        </div>
        <div class="col-xs-6 res_are">
        
            <div id="logo"><a href="#/"><img class="img-responsive" src="{{URL::asset('frontend/images/logo_w.png')}}" title="MarketShop" alt="MarketShop" /></a></div>
          		
        </div>
        <div class="col-xs-3 right_pa">
        <ul>
        <li><a href="javascript:void(0)" ><img src="{{URL::asset('frontend/images/user.png')}}" title="user" /> </a></li>
        <li><a href="javascript:void(0)"> <img src="{{URL::asset('frontend/images/cart.png')}}" title="cart" /> </a></li>
        </ul>
        </div>
    </div>
    <header class="header-row">
		<div class="container mob_bg">
        	<div class="table-container">
          	<!-- Logo Start -->
          		<div class="col-table-cell col-lg-5 col-md-5 col-sm-4 col-xs-12 inner">
            		<div id="logo"><a href="#/"><img class="img-responsive" src="{{URL::asset('frontend/images/logo.png')}}" title="MarketShop" alt="MarketShop" /></a></div>
          		</div>
               
          	<!-- Logo End -->
          	<!-- Search Start-->
          	<div class="col-table-cell col-lg-6 col-md-6  col-sm-6  col-xs-12 ">            	
                    <div id="search" class="input-group mb_sea">
              		<input id="filter_name" type="text" name="search" value="" placeholder="What are you looking for today" class="form-control input-lg" />
              		<button type="button" class="button-search"><i class="fa fa-search"></i></button>
            	   </div>
                                    
		    <div class="input-group cate_sear">
               
                <input type="hidden" name="search_param" value="all" id="search_param">         
                <input type="text" class="form-control" name="x" placeholder="I'm looking for...">
                 <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-default dropdown-toggle categr" data-toggle="dropdown">
                    	<span id="search_concept">All Categories</span>&nbsp;&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>
                    </button>
                  <ul class="dropdown-menu drop category_lis" role="menu">
		      <li><a href="javascript:void(0)">All Categories</a></li>
                     <li ng-repeat="nav in navigation.menu" ><a href="javascript:void(0)"><% nav.category_name %></a></li>
                
                     
                    </ul>
                </div>
                <span class="input-group-btn">
                    <button class="btn btn-default sear" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        

                    
          	</div>
          	<!-- Search End-->
          	<!-- Mini Cart Start-->
          	<div class="col-table-cell col-lg-1 col-md-2 col-sm-2 col-xs-12 inner">
            	<div id="cart">
              		<button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="heading dropdown-toggle"> 
                            <!--<i class="fa fa-shopping-cart" aria-hidden="true"></i>--><i class="fa fa-shopping-cart" aria-hidden="true"></i><span id="cart-total">Cart <b ng-bind="minicart.total_num"></b></span></button>
              	<ul class="dropdown-menu" ng-if="minicart.total_num!=0">
                    <li>
                      <table class="table">
                        <tbody>
                          <tr ng-repeat="minicrt in minicart.cart_data">
                            <td class="text-center"><a href="javascript:void(0)"><img class="img-thumbnail" width ="50" title="<% minicrt.name%>" alt="<% minicrt.name%>" src="{{URL::asset('uploads/')}}/<%minicrt.image%>"></a></td>
                            <td class="text-left"><a href="javascript:void(0)"><% minicrt.name%></a></td>
                            <td class="text-right">x <%minicrt.quantity%></td>
                            <td class="text-right">Rs. <%minicrt.price%></td>
                            <td class="text-center"><button class="btn btn-danger btn-xs remove" title="Remove" ng-click="remove_item_minicart(minicrt.id)" type="button"><i class="fa fa-times"></i></button></td>
                          </tr>
                          <!--<tr>
                            <td class="text-center"><a href="javascript:void(0)"><img class="img-thumbnail" title="Aspire Ultrabook Laptop" alt="Aspire Ultrabook Laptop" src="images/product/samsung_tab_1-50x50.jpg"></a></td>
                            <td class="text-left"><a href="javascript:void(0)">Aspire Ultrabook Laptop</a></td>
                            <td class="text-right">x 1</td>
                            <td class="text-right">Rs.230.00</td>
                            <td class="text-center"><button class="btn btn-danger btn-xs remove" title="Remove" onClick="" type="button"><i class="fa fa-times"></i></button></td>
                          </tr>-->
                        </tbody>
                      </table>
                    </li>
                    <li>
                      <div>
                        <table class="table table-bordered">
                          <tbody>
                            <!--<tr>
                              <td class="text-right"><strong>Sub-Total</strong></td>
                              <td class="text-right">Rs.940.00</td>
                            </tr>
                            <tr>
                              <td class="text-right"><strong>Eco Tax (-2.00)</strong></td>
                              <td class="text-right">Rs.4.00</td>
                            </tr>
                            <tr>
                              <td class="text-right"><strong>VAT (20%)</strong></td>
                              <td class="text-right">Rs.188.00</td>
                            </tr>-->
                            <tr>
                              <td class="text-right"><strong>Total</strong></td>
                              <td class="text-right"  >Rs.<%minicart.total%></td>
                            </tr>
                          </tbody>
                        </table>
                        <p class="checkout"><a href="#/cart" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> View Cart</a>&nbsp;&nbsp;&nbsp;<a href="#/checkout" class="btn btn-primary"><i class="fa fa-share"></i> Checkout</a></p>
                      </div>
                    </li>
              	</ul>
            </div>
          </div>
          <!-- Mini Cart End-->
        </div>
      </div>
    </header>
    <!-- Header End-->
    <!-- Main Menu Start-->
 
    <script id="nav.html" type="text/ng-template">
         <a href="#/<%nav.slug%>" ng-if="nav.slug !='' "><%nav.category_name%><span  ng-if="nav.all_category.length>0">&rsaquo;</span></a>
         <a href="#/categorys/<%nav.id%>" ng-if="nav.slug =='' "><%nav.category_name%><span  ng-if="nav.all_category.length>0">&rsaquo;</span></a>
           <ul class="wsmenu-submenu-sub" ng-if="nav.all_category.length>0">
                <li ng-repeat="nav in nav.all_category" ng-include="'nav.html'">                    	
				</li>
								
			</ul>

    </script>
   
	<nav id="menu" class="navbar wsmenu slideLeft clearfix">
    	<div class="container">
        	<ul class="mobile-sub wsmenu-list">
                   <li><a href="javascript:void(0)">All Categories</a>
		       <ul class="wsmenu-submenu">
                  		  <li ng-repeat="nav in navigation.menu" ng-include="'nav.html'"></li>
					
			 </ul>                    
                  </li>
                 
                  <li ng-repeat="custom_nav in nav_menu.all_menu|filter:{position:'Header'}" >
                 	 <a href="<%custom_nav.url%>" ng-if="custom_nav.url != ''" ng-bind="custom_nav.menu_name"></a>
                	 <a href="#<%custom_nav.slug%>" ng-if="custom_nav.slug!= ''" ng-bind="custom_nav.menu_name"></a>
                	
                  	<div class="megamenu clearfix" ng-if="custom_nav.mega_menu=='1'">
                    	<ul class="col-lg-2 col-md-2 col-xs-12 col-sm-4 link-list" ng-repeat="mga_tit in custom_nav.mega_title_table">
                        	<li class="title"><b><%mga_tit.title%></b></li>
                         	<li ng-repeat="chil_nav in custom_nav.child_nav|filter:{mega_menu_id:mga_tit.id}">
                         		<a  href="<%chil_nav.url%>" ng-if="chil_nav.url != '' && chil_nav.menu_name!= ''"  ng-bind="chil_nav.menu_name"></a>
                         		<a  href="<%chil_nav.slug%>" ng-if="chil_nav.slug != ''  && chil_nav.menu_name!= ''"  ng-bind="chil_nav.menu_name"></a>
                         		<a  href="<%chil_nav.url%>" ng-if="chil_nav.url != '' && chil_nav.menu_name== ''"  ><img src="<% chil_nav.menu_image %>" alt="" /></a>
                         		<a  href="<%chil_nav.slug%>" ng-if="chil_nav.slug != ''  && chil_nav.menu_name == ''"  ><img src="<% chil_nav.menu_image %>" alt="" /></a>
                         	</li>                        	
                      	</ul>                           
                                                                                                    
                    </div>
                     <ul class="wsmenu-submenu" ng-if=" custom_nav.mega_menu != '1'">
                                <li ng-repeat="chil_nav in custom_nav.child_nav">
				     <a  href="<%chil_nav.url%>" ng-if="chil_nav.url != '' && chil_nav.menu_name!= ''"  ng-bind="chil_nav.menu_name"></a>
                         		<a  href="<%chil_nav.slug%>" ng-if="chil_nav.slug != ''  && chil_nav.menu_name!= ''"  ng-bind="chil_nav.menu_name"></a>
                         		<a  href="<%chil_nav.url%>" ng-if="chil_nav.url != '' && chil_nav.menu_name == ''"  ><img src="<% chil_nav.menu_image %>" alt="" /></a>
                         		<a  href="<%chil_nav.slug%>" ng-if="chil_nav.slug != ''  && chil_nav.menu_name == ''"  ><img src="<% chil_nav.menu_image %>" alt="" /></a>		   	
				</li>
								
			</ul>
                  </li>
				
                  <li class="custom-link-right"><a href="javascript:void(0)" target="_blank">Offers</a></li>                                    
			</ul>
    	</div>
	</nav>



    </div>
    <div class="container cnt_min">
	    
        <div ui-view="modal" autoscroll="false"></div>
        <div ng-view></div>
    </div>
    </div>
    <!--Footer Start-->
    <footer id="footer">
        <div class="fpart-first">

      <div class="container">

        <div class="row">

          <div class="column col-lg-2 col-md-2 col-sm-12 col-xs-12">

            <h5>Acount </h5>

            <ul>

              <li><a href="javascript:void(0)" >My Shoping Cart</a> </li>

			  <li><a href="javascript:void(0)">Wish List</a> </li>

            </ul>

          </div>

          <div class="column col-lg-4 col-md-4 col-sm-4 col-xs-12">

            <h5>Buying & Selling on ShopinDiscount.com</h5>

           <div>		   </div>

		   <div class="column col-lg-6 col-md-6 col-sm-6 col-xs-6">

			    <ul>
		
		              <li ng-repeat="(ky,chil_nav) in nav_men_array=(nav_menu.all_menu|filter:{position:'Footer'} : true )"  ng-if="ky < nav_men_array.length/2" >
		              <a  href="<%chil_nav.url%>" ng-if="chil_nav.url != '' && chil_nav.menu_name!= ''"  ng-bind="chil_nav.menu_name"></a>
                         		<a  href="<%chil_nav.slug%>" ng-if="chil_nav.slug != ''  && chil_nav.menu_name!= ''"  ng-bind="chil_nav.menu_name"></a>
                         		<a  href="<%chil_nav.url%>" ng-if="chil_nav.url != '' && chil_nav.menu_name== ''"  ><img src="<% chil_nav.menu_image %>" alt="" /></a>
                         		<a  href="<%chil_nav.slug%>" ng-if="chil_nav.slug != ''  && chil_nav.menu_name == ''"  ><img src="<% chil_nav.menu_image %>" alt="" /></a>
		              
		              </li>
		
		            </ul>

			</div>

			 <div class="column col-lg-6 col-md-6 col-sm-6 col-xs-6">

				    <ul >
		              <li ng-repeat="(ky,chil_nav) in nav_men_array=(nav_menu.all_menu|filter:{position:'Footer'} : true )"  ng-if="ky > nav_men_array.length/2 || ky == nav_men_array.length/2" >
		              <a  href="<%chil_nav.url%>" ng-if="chil_nav.url != '' && chil_nav.menu_name!= ''"  ng-bind="chil_nav.menu_name"></a>
                         		<a  href="<%chil_nav.slug%>" ng-if="chil_nav.slug != ''  && chil_nav.menu_name!= ''"  ng-bind="chil_nav.menu_name"></a>
                         		<a  href="<%chil_nav.url%>" ng-if="chil_nav.url != '' && chil_nav.menu_name== '' &&  chil_nav.menu_image!=''"  ><img src="<% chil_nav.menu_image %>" alt="" /></a>
                         		<a  href="<%chil_nav.slug%>" ng-if="chil_nav.slug != ''  && chil_nav.menu_name == ''  &&  chil_nav.menu_image!=''"  ><img src="<% chil_nav.menu_image %>" alt="" /></a>
		              
		              </li>
		            
		
		            </ul>

			</div>

          </div>

          <div class="column col-lg-3 col-md-3 col-sm-3 col-xs-12">

            <h5>Follow Us On</h5>

             <img src="{{URL::asset('frontend/images/socialicons/follo_us.jpg')}}" alt="Follow Us" title="Follow Us"  class="img_follo"/>          </div>

          

          <div class="column col-lg-3 col-md-3 col-sm-3 col-xs-12 foot_for">

            <h5>Sign Up for Our Newsletter</h5>
             
            <div class="form-group">
              <div class="alert alert-success" ng-if="news_ltr.success">
                    <p >
	            <% news_ltr.success %>
	            </p>
           </div>
            <div class="alert alert-danger"  ng-if="news_ltr.errors">
	            <ul>
	                <li ng-repeat ="er in news_ltr.errors"><% er %></li>
	         
	            </ul>
         </div>
            <input id="full_nam" type="text" required placeholder="Full Name" ng-model="news_ltr.name" name="fname" class="form-control" />

			 <input id="email" type="email" required placeholder="email" ng-model="news_ltr.email" name="email" class="form-control" />

			 <input id="mobile" type = "tel" required placeholder="Mobile No"  ng-model="news_ltr.mobile" name="mobile" class="form-control" />

                            <select class="form-control"  ng-model="news_ltr.occupation">

                                   <option value="">Occupation </option>

                                   <option value="Business Man">Business Man</option>

                                    <option value="Designer">Designer </option>

                                    <option value="Programmer">Programmer</option>

                                    <option value="Student">Student</option>

                                    <option value="Teacher">Teacher </option>

                                    <option value="Worker">Worker</option>

                            </select>

			

                         

                            <select id="cityId" class="form-control" name="city"  ng-model="news_ltr.city" required="required">

                                    <option value="">Select City</option>

                                   <option ng-repeat="city in city_newsletter" value="<%city%>"><%city%></option>



                           </select>

                         

                         

                         

                         

             <div class="form-group radio">            

                         

			<span> <input type="radio" class="gender_ca" id="gender" value="male" name="gender"  ng-model="news_ltr.gender"  />   Male</span>

			<span> <input type="radio" class="gender_ca" id="gender" value="Female" name="gender"  ng-model="news_ltr.gender"/>  Female </span> 

            </div>          

            

            </div>
         

            <input type="button" ng-click="news_ltr_subsc(news_ltr);" value="Subscribe" class="btn btn-primary">

          </div>

        </div>

      </div>

    </div>

<div class="fpart-second">

      <div class="container">

        <div id="powered" class="clearfix">

          <div class="powered_text pull-left flip">   

<p><a href="javascript:void(0)"  class="categ">Mobiles:</a> <a href="javascript:void(0)" class="sub_cat" title="mobile" > HTC Mobiles</a> / <a href="javascript:void(0)" class="sub_cat" title="mobile" >iPhones </a>/ <a href="javascript:void(0)" class="sub_cat" title="mobile" >LG Mobiles</a> /<a href="javascript:void(0)" class="sub_cat" title="mobile" > Karbonn Mobiles</a> / <a href="javascript:void(0)" class="sub_cat" title="mobile" >Intex Mobiles</a> / <a href="javascript:void(0)" class="sub_cat" title="mobile" >Micromax Mobiles</a> / <a href="javascript:void(0)" class="sub_cat" title="mobile" >Asus Mobiles </a>/ <a href="javascript:void(0)" class="sub_cat" title="mobile" >Samsung Mobiles </a>/ <a href="javascript:void(0)" class="sub_cat" title="mobile" >Lenovo Mobiles</a> / <a href="javascript:void(0)" class="sub_cat" title="mobile" >Oppo Mobiles</a> / <a href="javascript:void(0)" class="sub_cat" title="mobile" >Xolo Mobiles </a>/ <a href="javascript:void(0)" class="sub_cat" title="mobile" >Panasonic Mobiles</a> </p>

<p><a href="javascript:void(0)"  class="categ">Tablets:</a> <a href="javascript:void(0)" class="sub_cat" title="tablet" >Apple iPads</a> / <a href="javascript:void(0)" class="sub_cat" title="tablet" >Samsung Tablets</a> / <a href="javascript:void(0)" class="sub_cat" title="tablet" >Windows Tablets </a>/<a href="javascript:void(0)" class="sub_cat" title="tablet" > 3G Tablets</a> / <a href="javascript:void(0)" class="sub_cat" title="tablet" >Calling Tablets </a>/ <a href="javascript:void(0)" class="sub_cat" title="tablet" >Micromax Tablets</a> / <a href="javascript:void(0)" class="sub_cat" title="tablet" >Lenovo Tablets</a> / <a href="javascript:void(0)" class="sub_cat" title="tablet" >Asus Tablets</a> / <a href="javascript:void(0)" class="sub_cat" title="tablet" >Dell Tablets</a> /<a href="javascript:void(0)" class="sub_cat" title="tablet" > iBall Tablets </a>/ <a href="javascript:void(0)" class="sub_cat" title="tablet" >Ambrane Tablets</a></p>

<p><a href="javascript:void(0)"  class="categ">Mobiles Accessories:</a><a href="javascript:void(0)" class="sub_cat" title="mobile accessories" > Mobile Covers </a>/ <a href="javascript:void(0)" class="sub_cat" title="mobile accessories" >Power Banks</a> / <a href="javascript:void(0)" class="sub_cat" title="mobile accessories" >Samsung Power Banks</a> / <a href="javascript:void(0)" class="sub_cat" title="mobile accessories" >Ambrane Power Banks </a>/ <a href="javascript:void(0)" class="sub_cat" title="mobile accessories" >Intex Power Banks </a>/ <a href="javascript:void(0)" class="sub_cat" title="mobile accessories" >Sony Power Banks </a>/ <a href="javascript:void(0)" class="sub_cat" title="mobile accessories" >Lenovo Power Banks</a> / <a href="javascript:void(0)" class="sub_cat" title="mobile accessories" >PNY Power Banks</a> / <a href="javascript:void(0)" class="sub_cat" title="mobile accessories" >Portronics Power Banks</a></p>

<p><a href="javascript:void(0)"  class="categ">Computers:</a><a href="javascript:void(0)" class="sub_cat" title="computers" > Lenovo Laptops</a> / <a href="javascript:void(0)" class="sub_cat" title="computers" >Acer Laptops </a>/ <a href="javascript:void(0)" class="sub_cat" title="computers" >Apple Macbooks</a> / <a href="javascript:void(0)" class="sub_cat" title="computers" >Notebook </a>/ <a href="javascript:void(0)" class="sub_cat" title="computers" >Laptops </a>/ <a href="javascript:void(0)" class="sub_cat" title="computers" >External Hard Disks</a> / <a href="javascript:void(0)" class="sub_cat" title="computers" >Dell Laptops </a>/ <a href="javascript:void(0)" class="sub_cat" title="computers" >HP Laptops </a>/<a href="javascript:void(0)" class="sub_cat" title="computers" > Pen Drives </a></p>

<p><a href="javascript:void(0)"  class="categ">Camera:</a><a href="javascript:void(0)" class="sub_cat" title="camera" > DSLR Cameras </a>/<a href="javascript:void(0)" class="sub_cat" title="camera" > Canon Cameras </a>/ <a href="javascript:void(0)" class="sub_cat" title="camera" >Nikon Coolpix </a>/ <a href="javascript:void(0)" class="sub_cat" title="camera" >Nikon DSLR Cameras</a> /<a href="javascript:void(0)" class="sub_cat" title="camera" > Sony Cameras </a>/ <a href="javascript:void(0)" class="sub_cat" title="camera" >Digital Cameras </a>/ <a href="javascript:void(0)" class="sub_cat" title="camera" >Panasonic Cameras</a> /<a href="javascript:void(0)" class="sub_cat" title="camera" > Samsung Cameras</a> </p>

<p><a href="javascript:void(0)"  class="categ">Watches:</a> <a href="javascript:void(0)" class="sub_cat" title="watches" >Men's Watches </a>/ <a href="javascript:void(0)" class="sub_cat" title="watches" >Women's Watches </a>/ <a href="javascript:void(0)" class="sub_cat" title="watches" >Casio Watches </a>/ <a href="javascript:void(0)" class="sub_cat" title="watches" >Titan Watches</a> / <a href="javascript:void(0)" class="sub_cat" title="watches" >Fastrack Watches</a> /<a href="javascript:void(0)" class="sub_cat" title="watches" > Fossil watches</a> /<a href="javascript:void(0)" class="sub_cat" title="watches" > Casio Edifice </a>/<a href="javascript:void(0)" class="sub_cat" title="watches" > Tissot Watches </a></p>

<p><a href="javascript:void(0)"  class="categ">Fashion:</a><a href="javascript:void(0)" class="sub_cat" title="fashion" > Bags </a>/ <a href="javascript:void(0)" class="sub_cat" title="fashion" >Wildcraft </a>/<a href="javascript:void(0)" class="sub_cat" title="fashion" > American Tourister </a>/<a href="javascript:void(0)" class="sub_cat" title="fashion" > Skybags</a> /<a href="javascript:void(0)" class="sub_cat" title="fashion" > Fastrack Bags </a>/ <a href="javascript:void(0)" class="sub_cat" title="fashion" >Shoes </a>/ <a href="javascript:void(0)" class="sub_cat" title="fashion" >Boots</a> / <a href="javascript:void(0)" class="sub_cat" title="fashion" >Kurta</a> /<a href="javascript:void(0)" class="sub_cat" title="fashion" >Raymond</a> / <a href="javascript:void(0)" class="sub_cat" title="fashion" >Ray-Ban </a></p>

<p><a href="javascript:void(0)"  class="categ">Home & Kitchen:</a><a href="javascript:void(0)" class="sub_cat" title="home & kitchen" > Home Decor</a> /<a href="javascript:void(0)" class="sub_cat" title="home & kitchen" > Paintings </a>/<a href="javascript:void(0)" class="sub_cat" title="home & kitchen" > Clocks</a> /<a href="javascript:void(0)" class="sub_cat" title="home & kitchen" > Flasks & Thermos </a>/<a href="javascript:void(0)" class="sub_cat" title="home & kitchen" > Photo Frames</a> / <a href="javascript:void(0)" class="sub_cat" title="home & kitchen" >Tupperware Kitchenware </a>/<a href="javascript:void(0)" class="sub_cat" title="home & kitchen" > Hawkins </a></p><a href="javascript:void(0)" class="sub_cat" title="home & kitchen" >Kitchenware / Milton Kitchenware</a> </p>

<p><a href="javascript:void(0)"  class="categ">Appliances:</a> <a href="javascript:void(0)" class="sub_cat" title="appliances" >Air Conditioners </a>/ <a href="javascript:void(0)" class="sub_cat" title="appliances" >Air Coolers</a> / <a href="javascript:void(0)" class="sub_cat" title="appliances" >Refrigerators </a>/<a href="javascript:void(0)" class="sub_cat" title="appliances" > Fans</a> / <a href="javascript:void(0)" class="sub_cat" title="appliances" >Washing Machines </a>/ <a href="javascript:void(0)" class="sub_cat" title="appliances" >Inverters & Stabilizers</a> / <a href="javascript:void(0)" class="sub_cat" title="appliances" >Mircowaves</a></p>

<p><a href="javascript:void(0)"  class="categ">Home Furnishing:</a> <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Bed Linen </a>/ <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Bed Sheets</a> / <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Bath Linen</a> / <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Mattresses </a>/ <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Curtains </a>/ <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Cushion Covers</a> / <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Pillows </a>/<a href="javascript:void(0)" class="sub_cat" title="home furnishing" > Blankets </a>/ <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Carpets</a> / <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Kids Bedding</a> / <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Sleepwell Mattress </a>/ <a href="javascript:void(0)" class="sub_cat" title="home furnishing" >Kurlon Mattress</a> </p>

<p><a href="javascript:void(0)"  class="categ">Furniture:</a><a href="javascript:void(0)" class="sub_cat" title="furniture" > Living Room </a>/ <a href="javascript:void(0)" class="sub_cat" title="furniture" >Bedroom Furniture</a>/<a href="javascript:void(0)" class="sub_cat" title="furniture" > Chairs</a> / <a href="javascript:void(0)" class="sub_cat" title="furniture" >Dining Sets</a> / <a href="javascript:void(0)" class="sub_cat" title="furniture" >Dining Chairs</a> / <a href="javascript:void(0)" class="sub_cat" title="furniture" >Bean Bags</a> / <a href="javascript:void(0)" class="sub_cat" title="furniture" >Tables and Desks</a> / <a href="javascript:void(0)" class="sub_cat" title="furniture" >Shoe Storage </a>/ <a href="javascript:void(0)" class="sub_cat" title="storage cabinates toys & games" >Storage Cabinets Toys & Games:</a><a href="javascript:void(0)" class="sub_cat" title="storage cabinates toys & games" > Soft Toys</a> /<a href="javascript:void(0)" class="sub_cat" title="storage cabinates toys & games" > Dolls </a>/ <a href="javascript:void(0)" class="sub_cat" title="storage cabinates toys & games" >Musical Toys</a> /<a href="javascript:void(0)" class="sub_cat" title="storage cabinates toys & games" > Electric Scooters </a>/ <a href="javascript:void(0)" class="sub_cat" title="storage cabinates toys & games" >Skate Scooters</a> /<a href="javascript:void(0)" class="sub_cat" title="storage cabinates toys & games" > Barbie Dolls</a>/ <a href="javascript:void(0)" class="sub_cat" title="storage cabinates toys & games" >Barbie Games</a> / <a href="javascript:void(0)" class="sub_cat" title="storage cabinates toys & games" >Action Figures </a></p>

    

          </div>

        </div>

        <div class="bottom-row">          </div>

          <div> 

		  <label class="pay_ca"> Payment Method</label>

		  <a href="javascript:void(0)" > <img data-toggle="tooltip" src="{{URL::asset('frontend/images/payment/pay_1.jpg')}}" alt="paypal" title="VISA"></a>

                  

                  <a href="javascript:void(0)" > <img data-toggle="tooltip" src="{{URL::asset('frontend/images/payment/pay_2.jpg')}}" alt="american-express" title="Maestro">

                  </a> <a href="javascript:void(0)" > <img data-toggle="tooltip" src="{{URL::asset('frontend/images/payment/pay_3.jpg')}}" alt="2checkout" title="PayPal"></a> 

                  <a href="javascript:void(0)" > <img data-toggle="tooltip" src="{{URL::asset('frontend/images/payment/pay_4.jpg')}}" alt="maestro" title="Net Banking"></a> 

                  <a href="javascript:void(0)" > <img data-toggle="tooltip" src="{{URL::asset('frontend/images/payment/pay_5.jpg')}}" alt="discover" title="Cash on Delivery"></a> 

                 

          </div>

        </div>

		<div class="bootm_foo">

	     <p>Copyright © 2016 Shop in Discount by <a href="javascript:void(0)" >Webherns Pvt. Ltd</a></p>

	    </div>

      </div>


            <div id="back-top">
            <a data-toggle="tooltip" title="Back to Top" href="javascript:void(0)" class="backtotop">
            <i class="fa fa-chevron-up"></i></a>
        </div>
    </footer>
    
 
<!--Footer End-->

<!-- JS Part Start-->
<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/angular.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/library/js/framework.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery.easing-1.3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery.dcjqaccordion.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/webslidemenu.js') }}"></script>
<!-- JS Part End-->

<!--angular-js Part-->

<script type="text/javascript" src="{{ URL::asset('frontend/js/angular-route.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/angular-ui-router.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/angular-animate.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/angular-aria.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/angular-material.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/jk-carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/ng-tiny-scrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery.ez-plus.js') }}"></script>
 <script type="text/javascript" src="{{ URL::asset('frontend/js/angular-ezplus.js') }}"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.8/angular-sanitize.js"></script> 
<script type="text/javascript" src="{{ URL::asset('frontend/js/script.js') }}"></script>
 <script type="text/javascript" src="https://js.stripe.com/v2/" defer></script>
 <script>
                    $(document).ready(function () {
                        Stripe.setPublishableKey('{!! env('STRIPE_KEY') !!}');
                        jQuery(function ($) {
                            $(document).on('submit', '#payment-form', function (event) {
                                var $form = $(this);
                                //$form.find('#submitBtn').prop('disabled', true);
                                return false;
                            });
                        });
                    })
   </script>
<script>
jQuery(document).ready(function($){
$(".megamenu").mouseenter(function(){
			
			$(".mobile-sub > li:nth-child(2) > a").addClass('active');
			
			});
		$(".megamenu").mouseleave(function(){
			
			$(".mobile-sub > li:nth-child(2) > a").removeClass('active');
			});
			
			$(".wsmenu-submenu").mouseenter(function(){
			$(this).parent( ).find("a").addClass('active');
			});
			$(".wsmenu-submenu").mouseleave(function(){
			$(this).parent( ).find("a").removeClass('active');
			});
			
			
	
	  jQuery(".category_lis a").click(function(){ jQuery('#search_concept').html(jQuery(this).html());});
	});
     $("#navToggle").click(function(){
		$("body.ng-scope").animate(5000).toggleClass('slide_heade');
		
		
		});
		
 /*---------------------------------------------------
        Mobile Main Menu
    ----------------------------------------------------- */
    $('#menu .navbar-header > span').on("click", function() {
        $(this).toggleClass("active");
        $("#menu .navbar-collapse").slideToggle('medium');
        return false;
    });

    //mobile sub menu plus/mines button
    $('#menu .nav > li > div > .column > div, .submenu, #menu .nav > li .dropdown-menu').before('<span class="submore"></span>');

    //mobile sub menu click function
    $('span.submore').on("click", function() {
        $(this).next().slideToggle('fast');
        $(this).toggleClass('plus');
        return false;
    });
    //mobile top link click
    $('.drop-icon').on("click", function() {
        $('#header .htop').find('.left-top').slideToggle('fast');
        return false;
    });

</script>

</body>
</html>
