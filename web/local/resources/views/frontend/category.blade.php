<div class="row panel_spac">
<div class="col-lg-12 col-md-12">
<h3 class="subtitle"><%category_content.cat.category_name%></h3>
<div class="panel-group" id="accordion1">
<div class="panel panel-default">
  <div class="panel-heading-cat">
    <h4 class="panel-title">
      <!--<a href="javascript:void(0)">Shop By Category</a>-->
      <!--<a id="aco1" data-toggle="collapse" data-parent="#accordion" href="javascript:void(0)" class="pull-right arrow_dow">-->
      
      <!--<a data-toggle="collapse" data-parent="#accordion1" href="javascript:void(0);#collapse1" aria-expanded="true" aria-controls="collapse1">-->
      <a data-toggle="collapse" data-parent="#accordion" href="javascript:void(0);#collapse1" class="arrow_dow">
      	<!--Shop By Category <i class="fa fa-angle-up pull-right " aria-hidden="true"></i>-->
        <span>Shop By Category</span> <i class="fa fa-angle-up pull-right"></i>
      </a>
    </h4>
  </div>
  <div id="collapse1" class="panel-collapse collapse in">
    <div class="panel-body">
        <div class="row product_to_sop text-center">
            <div class="col-sm-3 col-xs-12 category-block" ng-repeat="category_1 in category_content.subcats">
                     <img src="{{URL::asset('uploads')}}/<%category_1.image%>" alt="<%category_1.category_name%>" width="310" height="210" class="img-responsive" />
                    <div class="caption">
                        <h3><%category_1.category_name%></h3>
                        <p><a href="<%category_1.slug%>" class="btn button-primary btn-primary">Shop Now</a></p>
                    </div>
            </div>  
        </div>
       
    
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading-cat">
    <h4 class="panel-title">
      <!--<a  href="javascript:void(0)">Shop By Brand</a>
       <a id="aco2" data-toggle="collapse" data-parent="#accordion" href="javascript:void(0)" class="pull-right arrow_dow acc2">-->
       <a data-toggle="collapse" data-parent="#accordion" href="javascript:void(0);#collapse2" class="arrow_dow">
       <span>Shop By Brand</span> <i class="fa fa-angle-up pull-right"></i>
      <!-- <i class="fa fa-angle-up up_angl"></i>-->
      </a>
    </h4>
  </div>
  <div id="collapse2" class="panel-collapse collapse in">
    <div class="panel-body">
       <div class="row brands">
            <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12"  >
               <ul class="brand_list" >
                 <li ng-repeat="brnd in category_content.brands"><a href="<%brnd.slug%>"><img src="{{URL::asset('uploads')}}/<%brnd.image%>" title="<%brnd.brand_name%>" alt="<%brnd.brand_name%>" class="img-responsive" /></a></li>
               </ul>
            </div>
       </div>
        <!--<div class="row brands">
            <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
               <ul class="brand_list">
                 <li ng-repeat="category_1 in shop_by_brand2"><a href=""><img src="<%category_1.brnd_img2%>" title="Brand" alt="Brand" class="img-responsive" /></a></li>
               </ul>
            </div>
       </div> -->                              
    </div>
  </div>
</div>

</div>
</div>

</div> 
<!-- Categories Product Slider Start-->
<div class="category-module" ng-repeat="sub_cat in category_content.subcats" id="latest_category<%sub_cat.id%>" >
<h3 class="subtitle"><%sub_cat.category_name%></h3>
<div class="category-module-content">
<ul id="sub-cat" class="tabs">
<li ng-repeat="sbcat in sub_cat.subcat"  ng-class="{'active': isSetTab(sub_cat.id,sbcat.id)}"><a href="javascript:void(0);" ng-click="setTab(sub_cat.id,sbcat.id)" ng-bind="sbcat.category_name"></a></li>

</ul>
<div ng-repeat="sbcat in sub_cat.subcat" id="tab-cat-<%sub_cat.id%>-<%sbcat.id%>" class="tab_content" ng-show="isSetTab(sub_cat.id,sbcat.id)" >

 <data-owl-carousel  id="carousel_-<%sub_cat.id%>-<%sbcat.id%>" class="owl-carousel about-carousel">
      <div class="latest_category_tabs" owl-carousel-item="" ng-repeat="(key,slid_data) in sbcat.products">
    <div class="product-thumb slid_layou">
     <div class="button-group hidden-xs hidden-sm">
         <div class="add-to-links">

            <div class="row ico_data">
               <div class="col-lg-6 col-md-6 col-sm-6">
                  <button type="button" data-toggle="tooltip" title="" ng-click="cat1_fav(key)" ng-init="cat1_favorite[key]='Favorite'" class="btn btn-primary btn-block"  data-original-title="Add to Wish List">
                  <i class="fa fa-heart"></i> <span><%cat1_favorite[key]%></span></button>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6">
                  <button type="button" data-toggle="tooltip" title="" ng-click="add_to_compare(slid_data)" ng-init="cat1_compare[key]='Compare'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                  <img src="{{URL::asset('frontend')}}/images/compare.png"><span><%cat1_compare[key]%></span></button>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12 vie_prodmod view_pro"> 
                                        
										 <button ui-sref="Modal.productmodel({id:slid_data.id})"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                       </div>
            </div>
         </div>
      </div>
        <div class="image">
            <a href="javascript:void(0)"><img bn-lazy-src="{{URL::asset('uploads')}}/<%slid_data.all_img[0].image%>" alt="" title="<%slid_data.pro_name%>" class="img-responsive" /></a>                                </div>
        <div class="caption">
           <h4><a href="<%slid_data.slug%>"><%slid_data.pro_name%></a></h4>
            <div class="rating">
                <span class="fa fa-stack" ng-repeat="i in  getNumber(slid_data.avg_review)"><i class="fa fa-star fa-stack-2x"></i></span>
                <span class="fa fa-stack" ng-repeat="i in  getNumber(5-slid_data.avg_review)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
                <span class="rev_c">(<%slid_data.count_review%>)</span> 
	   </div>                                    
                    <p class="price">                  
                        <span class="price-old" ng-if="slid_data.prod_price !=''">Rs.<%slid_data.price%></span> 
				        <span class="price-new"  ng-if="slid_data.prod_price !=''">Rs.<%slid_data.prod_price%></span>
				        <span class="price-new"  ng-if="slid_data.prod_price ==''">Rs.<%slid_data.price%></span>
                        <span class="saving" ng-if="slid_data.percent!=''"><%slid_data.percent%> % Off</span>     
			       </p>
        </div>
        
    </div>
    
</div>
</data-owl-carousel>
</div>

</div>
<div class="panel panel-default" ng-repeat="sbcat in sub_cat.subcat"   ng-show="isSetTab(sub_cat.id,sbcat.id)">
<div class="panel-body slid_footer">
    <div class="row slid_foo">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">       
            <span class="pull-left"><b>What’s Popular Now:</b> </span>
            <ul class="list-inline pull-left">
                <li ng-repeat="(key,slid_data) in sbcat.popular"><a href="<%slid_data.slug%>"><%slid_data.pro_name%></a></li>               
            </ul>           
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
         <h5> <a class="view_1" href="<%sbcat.slug%>">View All <%sbcat.category_name%></a> </h5>
        </div>
    </div> 
</div>
</div>

</div>
<!-- Categories Product Slider End-->


<script>
jQuery(document).ready(function($){
	
	
	
	
	
	 function toggleChevron(e){$(e.target).prev('.panel-heading-cat').find("i.fa").toggleClass('fa-angle-up fa-angle-down');}
			  
			  $('#accordion1').on('hidden.bs.collapse',toggleChevron);$('#accordion1').on('shown.bs.collapse',toggleChevron);
			  
			  
  
    $('#carousel').owlCarousel({
        items: 6,
        autoPlay: 3000,
        lazyLoad: true,
        navigation: true,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        pagination: true
    });

			
		
	});
</script>