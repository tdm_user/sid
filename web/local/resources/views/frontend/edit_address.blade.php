<!-- edit address-->

<div class="Modal-box">

   <div id="login-modal">

        

            <div class="modal-content model-width">

                <div class="modal-header">

                   <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>-->

                  <a href="javascript:void(0)"  title="close" class="close" ><img src="{{URL::asset('frontend')}}/images/close.png" alt="close" /></a> 

                </div>

                <div class="modal-body">
                       <div class="alert alert-success" ng-if="success">
		            <p >
		            <% success%>
		            </p>
		        </div>
		        <div class="alert alert-danger"  ng-if="error">
		            <ul>
		                <li ng-repeat ="er in error"><% er %></li>
		         
		            </ul>
		        </div>

                        <div class="loginmodal-container">

                                <h4>Edit Address</h4><br>
									
                              <form>
									<div class="row">
										<div class="form-group">

											<div class="col-md-6 col-sm-6 col-xs-6 columns feild_style">
												<label>First Name</label><span class="required_mark">*</span>
												<input type="text"  class="form-control" id="inputEmail" ng-model="store_data[0].ship_fname">
											</div>

											<div class="col-md-6 col-sm-6 col-xs-6 columns feild_style">
												<label>Last Name</label><span class="required_mark">*</span>
												<input type="text"  class="form-control" id="inputEmail" ng-model="store_data[0].ship_lname">
											</div>
											
										</div>

									</div>

									<div class="row">
										<div class="form-group">

											<div class="col-md-6 col-sm-6 col-xs-6 columns feild_style">
												<label>Country</label><span class="required_mark">*</span>
												<select class="form-control" ng-model="store_data[0].ship_country" convert-to-number>
												  <option value="0">Select Your Country</option>
												  <option  ng-repeat="deta in checkou_country|filter:{pid:0}" value="<%deta.id%>"><%deta.name%></option>
												  
												</select>
											</div>

											<div class="col-md-6 col-sm-6 col-xs-6 columns feild_style">
												<label>State/Region</label><span class="required_mark">*</span>
												<select class="form-control" style="font-weight:normal;"  ng-model="store_data[0].ship_state" convert-to-number>
												  <option  value="0" >Select your region</option>
												  <option  ng-repeat="deta in checkou_country|filter:{pid:store_data[0].country}"     value="<%deta.id%>"  ng-if="store_data[0].country != 0"><%deta.name%></option>
												</select>
											</div>
											
										</div>

									</div>

									<div class="row">
										<div class="form-group">

											<div class="col-md-6 col-sm-6 col-xs-6 columns feild_style">
												<label>City</label><span class="required_mark">*</span>
												<select class="form-control" style="font-weight:normal;"  ng-model="store_data[0].ship_city" convert-to-number>
												 
												 <option  value="0" >Select your city</option>
												  <option  ng-repeat="deta in checkou_country|filter:{pid:store_data[0].state}"  value="<%deta.id%>" ng-if="store_data[0].state!=0"><%deta.name%></option>
												</select>
											</div>

											<div class="col-md-6 col-sm-6 col-xs-6 columns feild_style">
												<label>Street Name</label><span class="required_mark">*</span>
												<input ng-model="store_data[0].ship_address"  type="text"  class="form-control" id="inputEmail">
											</div>
											
										</div>

									</div>

                                <div class="row">
										<div class="form-group">

											<div class="col-md-12 col-sm-12 col-xs-12 columns feild_style">
												<label>Nearest Landmark</label><span class="required_mark">*</span>
												<input ng-model="store_data[0].ship_landmark" type="text" class="form-control" id="landmark">
											</div>
											
										</div>

								</div>

								<div class="row">
										<div class="form-group" style="font-weight:700;">

											<div class="col-md-6 col-sm-6 col-xs-6 columns feild_style">
												<label>Mobile Number</label><span class="required_mark">*</span>
												<div class="input-group">
													<span class="input-group-addon input-group number_prefix">+977</span>
													<input ng-model="store_data[0].ship_mobile" id="login-username" type="text" class="form-control" name="username" value="" placeholder="">            
												</div>
											</div>

											<div class="col-md-6 col-sm-6 col-xs-6 columns feild_style">
												<label>Telephone</label>
												<div class="input-group">
													<span class="input-group-addon input-group number_prefix">+977</span>
													<input ng-model="store_data[0].ship_phone"  type="text" class="form-control" id="inputEmail">
												</div>
											</div>
											
										</div>

								</div>

                                

                                <button type="submit" class="btn btn-primary rg_btn btn-lg btn-sm btn-block save_address" ng-click="store_address(store_data[0]);">Save Address</button>

                               

                            </form>


                        </div>

                </div>

            </div>

        </div>

 



