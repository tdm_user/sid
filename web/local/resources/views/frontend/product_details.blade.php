<div class="row" ng-if="product_compare.length>0 && remove_compare!='1'">
	<div class="compare_box col-md-7 col-md-offset-3">
		<div class="comp-box " ng-repeat="comp_pro in product_compare">
		    	<img src="{{URL::asset('uploads')}}/<%comp_pro.all_img[0].image%>" width="43"> 
		    	<a href="comp_pro.slug"><%comp_pro.pro_name%></a>  
		    	<span class="com_cut" ng-click="remove_comp(comp_pro.id);" style="cursor:pointer">x</span>
		</div>
		<div class="comp-box " ng-repeat="comp_pro in getNumber(4-product_compare.length)">
		    	
		</div>
		<div class=" com-box_but " >
		<button class="btn btn-primary">Compare</button>
		</div>	
		<span class="com_cut_slas" ng-click="rem_comp_box();" style="cursor:pointer">x</span>	
	</div>
</div>
<div class="row main_de">
          
        <div id="content_page" class="col-sm-12 col-xs-12 col-lg-12">
            <div class="row product-info quick">
              <div class="col-lg-5 col-sm-5 col-xs-12 image_zoo">
                <div class="row product_zoom" ng-init="setApproot('');zoom01=false;zoom01a=false;zoom03=false;zoom04=false;">
                    <div class="col-lg-10 pull-right">
                    <div class="image">

               <img 
                 src="{{URL::asset('uploads')}}/<%product_data.images[0].image%>"
                 data-zoom-image="{{URL::asset('uploads')}}/<%product_data.images[0].image%>"
                 ez-plus
                 ezp-model="zoomModelGallery01"
                 ezp-options="zoomOptionsGallery01"
                />
                 </div>
                    <div class="center-block text-center">
                    
                    <span class="zoom-gallery"><i class="fa fa-search"></i> Mouse over to zoom in</span></div>
                    
                    </div>
                    <div class="col-lg-2 pull-right">
                       
                   <div id="gallery_01">
                            <a class="img-group-01"
                               ng-repeat="image2 in product_data.images"
                               ng-click="setActiveImageInGallery('zoomModelGallery01',{{ URL::asset('uploads') }}/image2.image);"
            
                               data-image="{{ URL::asset('uploads') }}/<%image2.image%>"
                               data-zoom-image="{{ URL::asset('uploads') }}/<%image2.image%>"
            
                               href="javascript:void(0)">
                                <img ng-src="{{ URL::asset('uploads') }}/<%image2.image%>" width="46" height="46"/>
                            </a>														
							<!--<button id="video-btn" ui-sref="Modal.video"><img src="{{ URL::asset('frontend/images/mobile/mo_th4.png') }}" /></button>-->
                        </div>
                  		
                    </div>
                  </div>
                  <div class="row pro_descr"> 
                      <div class="col-lg-12">
                        <h5>Description:</h5>
                        <p ng-bind-html="product_data.prod_data.pro_des"></p>
                      </div>
                  </div>
              </div>
              <div class="col-lg-7 col-md-7 col-sm-7 cont_are">
              <div class="set_pa">
              <h3 class="title" itemprop="name"><%product_data.prod_data.pro_name%></h3>
                <div class="rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                  <meta itemprop="ratingValue" content="0" />
                 <p>
                    <span class="fa fa-stack" ng-repeat="i in  getNumber(product_data.avg_review)"><i class="fa fa-star fa-stack-2x"></i></span> 
									 
                    <span class="fa fa-stack" ng-repeat="i in  getNumber(5-product_data.avg_review)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
                   <span class="rev_c">(<%product_data.review.length%>)</span>                                   
                  
                  <a  href="javascript:void(0)" onClick="$('a[href=\'#tab-review\']').trigger('click'); return false;" ng-click="scrollTo('review_tab')"><span itemprop="reviewCount" >reviews <i class="fa fa-angle-down" aria-hidden="true"></i></span>
                  
                  </a> | <a onClick="$('a[href=\'#tab-review\']').trigger('click'); return false;" href="#"><b><%product_data.total_sold%> already sold</b></a></p>
                </div>
                <hr>
                
                <ul class="list-unstyled price_pro">
                  <li>Price:</li>
                  <li class="price">                
			<span class="price-old" ng-if="product_data.prod_data.prod_price !=''">Rs.<%product_data.prod_data.price%></span> 
			<span class="price-new"  ng-if="product_data.prod_data.prod_price !=''">Rs.<%product_data.prod_data.prod_price%></span>
			<span class="price-new"  ng-if="product_data.prod_data.prod_price ==''">Rs.<%product_data.prod_data.price%></span>
                        <span class="label label-danger" ng-if="product_data.prod_data.percent!=''"><%product_data.prod_data.percent%> % Off</span>   
                   </li>
                </ul>
                
                
                <div class="price-box detail_ca">
                  <div class="row" ng-repeat="option_data in product_data.options">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                      <%option_data.option_name%>:
                      </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <ul class="list-unstyled">
                                <li ng-repeat="opt_val in option_data.option_values"><a href="javascript:void(0)" class="btn btn-default"><%opt_val.option_name%></a></li>
                                
                            </ul>
                          </div>
                   </div>
                
                  
                  <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                      Quantity:
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                     <div class="qty">
                        <input type="text" name="quantity" ng-model="quant" ng-init="quant=1" size="2" id="input-quantity" class="form-control" />
                     
                            <a class="qtyBtn plus" href="javascript:void(0);" ng-click="quant=quant+1">+</a>
                            <a class="qtyBtn mines" href="javascript:void(0);" ng-show="quant > 1" ng-click="quant=quant-1">-</a>
                      
                        <div class="clear"></div>
                      </div>
                  </div>
                  
                  </div>
                  <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">Shiping:</div>
                       <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 ship_are">
                       <label>Rs 150 to
                       <button id="ship_button" ui-sref="Modal.shiping">
                       
                       <span>Kathmandu Inside Ring Road via SID Standard Delivery</span><i class="fa fa-angle-down" aria-hidden="true"></i></button> </label>
                       <p>Estimated Delivery Time: 1-3 days (ships out within 3 business days)</p>
                       </div>
                      
                </div>
                </div>
               
                <div class="row but_sec">
                 
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <button type="button" id="button-cart" class="btn btn-primary btn-lg btn-block " ng-click="add_to_cart(product_data.prod_data,quant);">Buy Now</button>
                  <ul class="list-unstyled favor">
                    <li><a href="#"> <i class="fa fa-heart"></i> <span>Add to Favourite</span></a> </li>
                    <li >
					  <a href="javascript:void(0);"  ng-click="add_to_compare(product_data.prod_data)" >
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Compare</span></a>
                     
					  <!--<a href="#"><img src="{{ URL::asset('frontend/images/compare.png') }}"><span>Compare</span></a>-->
					 </li>
                  </ul>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                      <div class="coup">
                        <h4>COUPON</h4>
                        <p>Get<span> 5% discount</span> per Rs. 1500</p>
                      </div>
                  </div>
                  
                </div>
                <div class="row pro_sec">
                 
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="product_de text-center">
                        <h4>Seller Information</h4>
                        <img src="{{ URL::asset('uploads') }}/<%product_data.seller_details.image%>" alt="product" />
                        <b><%product_data.seller_details.fname%> <%product_data.seller_details.lname%></b>
                        <span class="pro_titl">Selling for 2 years</span>
                        <p class="rating">
                   <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
                   <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
                   <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
                   <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
                   <span class="fa fa-stack"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
                   <span class="rev_c">(10)</span></p> 
                   <p class="veri_class"> <a href="#" data-toggle="tooltip" title="Stored, Packed and Delivered by SID"><img src="{{ URL::asset('frontend')}}/images/checkd.png" alt="check" /> Verified by SID [?]</a></p>
                        
                    </div>
                  </div>
                   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 set_us">
                        <div class="user_sec1">
                            
                                <ul class="list-unstyled">
                                   <li>
                                    <a href="#" ><img src="{{ URL::asset('frontend/images/store.png') }}" />Visit Store </a>
                                    </li>
                                    <li>
                                    <a href="#" ><img src="{{ URL::asset('frontend/images/user_ plus.png') }}" />  Follow Seller</a>
                                    </li>
                                    <li>
                                    <a href="#" ><img src="{{ URL::asset('frontend/images/chart.png') }}" />Chat with Seller</a>
                                    </li>
                                </ul>
                          
                        </div>
                        <div class="list_user">
                         <h5>This Seller has <span>30 followers</span></h5>
                         <ul class="list-unstyled list_data">
                               <li><img src="{{ URL::asset('frontend/images/user_ico/user1.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user2.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user3.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user4.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user5.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user6.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user7.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user8.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user5.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/userN.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/userH.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user9.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user10.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user11.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user12.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user13.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user14.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user15.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user16.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user17.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user18.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user19.jpg') }}" alt="user" /></li> 
                          <li><img src="{{ URL::asset('frontend/images/user_ico/user3.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user20.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user21.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user22.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user23.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user8.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user5.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user22.jpg') }}" alt="user" /></li>
                             
                         </ul>
                        </div>
                   </div>
               </div>
              
               </div>
              </div>
            </div>
        </div>
    <br>
    
  </div>

<div class="row">
<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 slid_pa">
<div class = "panel panel-default">
       <div class = "panel-heading">
          <h3 class = "panel-title">Top Selling Products From This Seller</h3>
       </div>
       
       <div class = "panel-body left_con1">
               <data-owl-carousel id="carousel_25" class="owl-carousel pro about-carousel" data-options="{autoPlay: 5000, stopOnHover: true,  
        slideSpeed : 300, paginationSpeed : 400,pagination:true, singleItem : true,navigation :false}" >
              <div owl-carousel-item="" ng-repeat="slid_data in product_data.popular">
              
                    <div class="product-thumb clearfix">
                    
                        <div class="image deta_img">
                            <a href="<%slid_data.slug%>"><img bn-lazy-src="{{URL::asset('uploads')}}/<%slid_data.all_img[0].image%>" alt="<%slid_data.pro_name%>" title="<%slid_data.pro_name%>" class="img-responsive" border="0" /></a>                        </div>
                        <div class="caption">
                            <h4><a href="<%slid_data.slug%>"><%slid_data.pro_name%></a></h4>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="rating">
									  <span class="fa fa-stack" ng-repeat="i in  getNumber(slid_data.avg_review)"><i class="fa fa-star fa-stack-2x"></i></span> 
                                       <span class="fa fa-stack" ng-repeat="i in  getNumber(5-slid_data.avg_review)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
								   </div>
                                </div>                            
                                <div class="col-xs-12 col-md-12">
                                    <p class="price">  
									        <span class="price-old" ng-if="slid_data.prod_price !=''">Rs.<%slid_data.price%></span> 
											<span class="price-new"  ng-if="slid_data.prod_price !=''">Rs.<%slid_data.prod_price%></span>
											<span class="price-new"  ng-if="slid_data.prod_price ==''">Rs.<%slid_data.price%></span>
									</p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
              </div>           
       
      </data-owl-carousel>
      </div>
</div>

<div class="row">
 <div class="col-lg-12">
    <div class = "panel panel-default">
       <div class = "panel-heading">
          <h3 class = "panel-title">Promotional</h3>
       </div>
       
       <div class = "panel-body left_con">
          <span>UseCoupon: SIDPTshirt</span>
          <p> Additional 10% Discount on buying 3 Print Tshirts</p>
       </div>
        <div class = "panel-body left_con">
          <span>Get Free Gifts on Hisense</span>
          <p>  Televisions Euro Cup Giveaways. Grab The offers Now!</p>
       </div>
        <div class = "panel-body left_con">
          <span>Free Gold Coin + Memory Card </span>
          <p>Get Offer with purchase of every AQUA STAR II 16GB Intex Mobile. Hurry!</p>
       </div>
    </div>
 </div> 
</div>
</div>
<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">

<div class="row">
     <div  class="col-sm-12 col-xs-12 col-lg-12">
    <ul class="nav nav-tabs tab_con">
      <li class="active"><a data-toggle="tab" href="#tab-description">Feature</a></li>
      <li><a data-toggle="tab" href="#tab-specification">Product Details</a></li>
      <li id="review_tab"><a data-toggle="tab" href="#tab-review">Reviews</a></li>
       <li><a data-toggle="tab" href="#tab-guarantee">Seller Guarantees</a></li>
    </ul>
    <div class="tab-content detail_content">
      <div itemprop="description" id="tab-description" class="tab-pane in active fade">       
          <div class="row text-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div ng-bind-html="product_data.prod_data.pro_feature_des"></div>
                
             </div>
           </div>        
      </div>
      <div id="tab-specification" class="tab-pane fade">
       
        <table class="table table-bordered" ng-repeat="atr_gr in product_data.attr_grp">
          <thead>
            <tr>
              <td colspan="2"><strong><%atr_gr.option_name%></strong></td>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="atropt in atr_gr.options">
              <td><%atropt.option_name%></td>
              <td><p ng-repeat="opt_val in atropt.option_values"><%opt_val.option_name%></p></td>
            </tr>
          </tbody>
          </table>
       
     
      </div>
      <div id="tab-review" class="tab-pane fade">
         
        <form class="form-horizontal">
          <div id="review">
            <div>
              <table class="table table-striped table-bordered" ng-repeat="review_data in product_data.review">
                <tbody>
                  <tr>
                    <td style="width: 50%;"><strong><span><%review_data.name%></span></strong></td>
                    <td class="text-right"><span><%review_data.created_at%></span></td>
                  </tr>
                  <tr>
                    <td colspan="2"><p><%review_data.review%></p>
                      <div class="rating">
                       <span class="fa fa-stack" ng-repeat="i in  getNumber(review_data.rating)"><i class="fa fa-star fa-stack-2x"></i></span> 
									 
                      <span class="fa fa-stack" ng-repeat="i in  getNumber(5-review_data.rating)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
                       </div></td>
                  </tr>
                </tbody>
              </table>
            
            </div>
            <div class="text-right"></div>
          </div>
          <h2>Write a review</h2>
           <div class="alert alert-success" ng-if="review_success">
              <p><%review_success%></p>
          </div>
           <div class="alert alert-danger"  ng-if="review_errors">
           	<ul>
           	  <li ng-repeat="rvwer in review_errors"><%rvwer%></li>
           	</ul>
           </div>
          <div class="form-group required">
            <div class="col-sm-12">
              <label for="input-name" class="control-label">Your Name</label>
              <input type="text" class="form-control" id="input-name" ng-model="add_review_data.name" name="name">
              
            </div>
          </div>
          <div class="form-group required">
            <div class="col-sm-12">
              <label for="input-review" class="control-label">Your Review</label>
              <textarea class="form-control" id="input-review" ng-model="add_review_data.reviews" rows="5" name="text"></textarea>
              <div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
            </div>
          </div>
          <div class="form-group required">
            <div class="col-sm-12">
              <label class="control-label">Rating</label>
              &nbsp;&nbsp;&nbsp; Bad&nbsp;
              <input type="radio" ng-model="add_review_data.rating" ng-value="1" name="rating">
              &nbsp;
              <input type="radio" ng-model="add_review_data.rating" ng-value="2" name="rating">
              &nbsp;
              <input type="radio" ng-model="add_review_data.rating" ng-value="3" name="rating">
              &nbsp;
              <input type="radio" ng-model="add_review_data.rating" ng-value="4" name="rating">
              &nbsp;
              <input type="radio" ng-model="add_review_data.rating" ng-value="5" name="rating">
              &nbsp;Good</div>
          </div>
          <div class="buttons">
            <div class="pull-right">
              <button class="btn btn-primary" id="button-review" type="button" ng-click="add_review(add_review_data,product_data.prod_data.id)">Continue</button>
            </div>
          </div>
        </form>
      </div>
	  <div itemprop="description" id="tab-guarantee" class="tab-pane in active fade">       
          <div class="row text-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div ng-bind-html="product_data.prod_data.warranty"></div>
                
             </div>
           </div>        
      </div>
    </div>
    </div>
</div>
</div>

</div>

<h3 class="subtitle" ng-if="product_data.rel_pros.length>0">Customers who viewed this product also viewed</h3>

 <div class="category-module" id="latest_category">

<data-owl-carousel id="carousel_6" class="owl-carousel about-carousel" data-options="{itemsCustom : [[320, 2],[768, 3],[1200, 6],[1600, 6]]}">
  <div class="latest_category_tabs" owl-carousel-item="" ng-repeat="slid_data in product_data.rel_pros"  ng-init="slid_data.compare_pro='Compare'">
     <div class="product-thumb slid_layou">
    <div class="button-group hidden-xs hidden-sm">
     <div class="add-to-links">
        <div class="row ico_data">
           <div class="col-lg-6 col-md-6 col-sm-6">
              <button type="button" data-toggle="tooltip" title="" onclick="" class="btn btn-primary btn-block"  data-original-title="Add to Wish List">
              <i class="fa fa-heart"></i> <span>Favorite</span></button>
           </div>
           <div class="col-lg-6 col-md-6 col-sm-6">
             <button type="button" data-toggle="tooltip" title=""  ng-click="add_to_compare(slid_data)" ng-if="slid_data.compare_pro=='Compare'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Compare</span></button>
                                           <button type="button" data-toggle="tooltip" title=""  ng-if="slid_data.compare_pro=='Added'" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Added</span></button>
           </div>
        </div>
        <div class="row">
        
        <div class="col-md-12 vie_prodmod view_pro"> 
                                          <!-- <a href="#product-modal" data-toggle="modal" class="view_pro"><img src="images/eye.png" alt="view" /></a> -->
										 <button ui-sref="Modal.productmodel({id:slid_data.id})"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                    </div>
        </div>
     </div>
  </div>
    <div class="image">
        <a href="<%slid_data.slug%>"><img bn-lazy-src="{{URL::asset('uploads')}}/<%slid_data.all_img[0].image%>" alt="" title="<%slid_data.pro_name%>" alt="<%slid_data.pro_name%>" class="img-responsive" /></a>                                </div>
    <div class="caption">
        <h4><a href="<%slid_data.slug%>"><%slid_data.pro_name%></a></h4>
        <div class="rating">  <span class="fa fa-stack" ng-repeat="i in  getNumber(slid_data.avg_review)"><i class="fa fa-star fa-stack-2x"></i></span>
                <span class="fa fa-stack" ng-repeat="i in  getNumber(5-slid_data.avg_review)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
                <span class="rev_c">(<%slid_data.count_review%>)</span>                                    </div>                                    
        <p class="price">             
                 <span class="price-old" ng-if="slid_data.prod_price !=''">Rs.<%slid_data.price%></span> 
				 <span class="price-new"  ng-if="slid_data.prod_price !=''">Rs.<%slid_data.prod_price%></span>
				 <span class="price-new"  ng-if="slid_data.prod_price ==''">Rs.<%slid_data.price%></span>
                 <span class="saving" ng-if="slid_data.percent!=''"><%slid_data.percent%> % Off</span>    
	    </p>
    </div>
    
</div>
  </div>
</data-owl-carousel>	

</div>

<script>

	
jQuery(document).ready(function(){
		 $(".nav-tabs.tab_con a").click(function(e){
        e.preventDefault();
        $(this).tab('show');
	});    $('[data-toggle="tooltip"]').tooltip();   
	
	
	
	
	});

</script>



            
          