<div class="container-fluid main_div" >
	
	<div class="col-sm-12 col-md-12 col-xs-12 logo_div">
		<div class="col-md-8 col-sm-8 col-xs-12 img_div">
			<img src="{{URL::asset('frontend')}}/images/n_img/company_name.png" class="img-responsive">
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 search_div" >
				
				<input type="search"/>
				<div class="input-group-btn">
					<button class="btn btn-default search_icon" type="button"><span class="fa fa-search"></span></button>
				</div>
			
		</div>
	</div>
	
	<div class="col-lg-12 col-md-12 col-xs-12 nav_d ">
		
			<ul class="nav navbar-nav" >
				  <li><a href="javascript:void(0)">Store Home</a></li>
				  <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Product<span class="caret"></span></a>
						<ul class="dropdown-menu a_custom">
						  <li><a href="javascript:void(0)">Page 1-1</a></li>
						  <li><a href="javascript:void(0)">Page 1-2</a></li>
						  <li><a href="javascript:void(0)">Page 1-3</a></li>
						</ul>
				  </li>
				  <li><a href="javascript:void(0)">Offer Zone</a></li>
				  <li><a href="javascript:void(0)">Top Selling</a></li>
				  <li><a href="javascript:void(0)">Reviews</a></li>
				  <li><a href="javascript:void(0)">Followers</a></li>
				  <li><a href="javascript:void(0)">Following</a></li>
				  <li><a href="javascript:void(0)">Invite Friends</a></li>
				  <li><a href="javascript:void(0)">Activity log</a></li>
				  <li><a href="javascript:void(0)">Contact Details</a></li>
			</ul>
	</div>
	
	<div class="col-lg-12 col-md-12 col-xs-12 filter_d">
		<div class="filter_d_content">	
			<div class="filter_d_company">Store:<b>Chaudhari Group</b></div>
			<div class="filter_d_time">Selling for:<span> 2 years(s)</span></div>
			<div class="filter_d_feedback">95% <span >Positive Feedback</span></div>
			<div class="filter_d_chat">
				<a href="javascript:void(0)" ><img src="{{URL::asset('frontend')}}/images/n_img/chat.png" />Chat with Seller</a>
            </div> 
			<div class="filter_d_follow">
				<a href="javascript:void(0)" ><img src="{{URL::asset('frontend')}}/images/n_img/user.png" />  Follow Seller</a>
            </div>
		</div>
	</div>
</div>
<div class="col-lg-12 col-md-12 col-xs-12 contact_main_d" >
		<img src="{{URL::asset('frontend')}}/images/n_img/best_deal.png" class="best_deal">
</div>

<div class="col-lg-12 col-md-12 col-xs-12 offer_slider">
	
	<div class="col-md-4 col-sm-4 col-xs-12 offer_slider_content">
		<span class="store_offer_detail">Get Free Gifts on Hisense Televisions</span><br>
			Euro Cup Giveaways. Grab the Offers Now!
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 offer_slider_content">
		<span class="store_offer_detail">Televisions Upto 17% Discount</span><br>
		Offer Valid Till Euro Cup on all Branded Televisions. Hurry! 
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 offer_slider_content" >
		<span class="store_offer_detail">Free Gold Coin + Memory Card</span><br>
		Get Offer with purchase of every AQUA STAR Intex Mobile. Hurry!
	</div>
</div>

<div class="col-lg-12 col-md-12 col-xs-12 offer_coupon">
	
	<div class="col-md-3 col-sm-3 col-xs-12 coupon_content">
		<div class="col-sm-12 col-md-12 col-xs-12" >
			<span >10% Off</span><br>
			<span>Buying on Mobile Phones</span><br>
			<span ><i>Expires on 2016/07/20</i></span>
		</div>
		<div class="col-sm-12 col-md-12 col-xs-12 offer_coupon_code">
			Coupon Code: <b style="color:black">PHIPhone10</b>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 coupon_content">
		<div class="col-sm-12 col-md-12 col-xs-12">
			<span >10% Off</span><br>
			<span >Buying on Mobile Phones</span><br>
			<span ><i>Expires on 2016/07/20</i></span> 
		</div>
		<div class="col-sm-12 col-md-12 col-xs-12 offer_coupon_code">
			Coupon Code: <b style="color:black">PHIPhone10</b>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 coupon_content">
		<div class="col-sm-12 col-md-12 col-xs-12">
			<span >10% Off</span><br>
			<span >Buying on Mobile Phones</span><br>
			<span ><i>Expires on 2016/07/20</i></span>
		</div>
		<div class="col-sm-12 col-md-12 col-xs-12 offer_coupon_code">
			Coupon Code: <b style="color:black">PHIPhone10</b>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 coupon_content">
		<div class="col-sm-12 col-md-12 col-xs-12">
			<span>10% Off</span><br>
			<span>Buying on Mobile Phones</span><br>
			<span><i>Expires on 2016/07/20</i></span>
		</div>
		<div class="col-sm-12 col-md-12 col-xs-12 offer_coupon_code">
			Coupon Code: <b style="color:black">PHIPhone10</b>
		</div>
	</div>
</div>
          		<!-- New Product Start-->
               
<h3 class="subtitle">New Products</h3>
 <div class="category-module" id="latest_category">

<data-owl-carousel id="carousel_6" class="owl-carousel about-carousel" data-options="{itemsCustom : [[320, 2],[768, 3],[1200, 6],[1600, 6]]}">
  <div class="latest_category_tabs" owl-carousel-item="" ng-repeat="slid_data in e_product_slide_a_phon">
     <div class="product-thumb slid_layou">
    <div class="button-group hidden-xs hidden-sm">
     <div class="add-to-links">
        <div class="row ico_data">
           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <button type="button" data-toggle="tooltip" title="" onclick="" class="btn btn-primary btn-block"  data-original-title="Add to Wish List">
              <i class="fa fa-heart"></i> <span>Favorite</span></button>
           </div>
           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <button type="button" data-toggle="tooltip" title="" onclick="" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
              <img src="{{URL::asset('frontend')}}/images/compare.png"><span>Compare</span></button>
           </div>
        </div>
        <div class="row">
        
        <div class="col-md-12 col-sm-12 col-xs-12 vie_prodmod view_pro"> 
                                          <!-- <a href="#product-modal" data-toggle="modal" class="view_pro"><img src="{{URL::asset('frontend')}}/images/eye.png" alt="view" /></a> -->
										 <button ui-sref="Modal.productmodel"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                    </div>
        </div>
     </div>
  </div>
    <div class="image">
        <a href="javascript:void(0)"><img bn-lazy-src="<%slid_data.thum_img%>" alt="" title="Aspire Ultrabook Laptop" class="img-responsive" /></a>                                </div>
    <div class="caption">
        <h4><a href="javascript:void(0)"><%slid_data.title%></a></h4>
        <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> <span class="rev_c">(165)</span>                                   </div>                                    
        <p class="price"> 
            
            <span class="price-old"><%slid_data.d_price%></span>
            <span class="price-new"><%slid_data.price%></span>  
            <span class="saving" ng-if="slid_data.off_p"><%slid_data.off_p%></span>                                    </p>
    </div>
    
</div>
  </div>
</data-owl-carousel>	

</div>

<h3 class="subtitle" >Popular Products</h3>
 <div class="category-module" id="latest_category">

<data-owl-carousel id="carousel_6" class="owl-carousel about-carousel" data-options="{itemsCustom : [[320, 2],[768, 3],[1200, 6],[1600, 6]]}">
  <div class="latest_category_tabs" owl-carousel-item="" ng-repeat="slid_data in e_product_slide_a_phon">
     <div class="product-thumb slid_layou">
    <div class="button-group hidden-xs hidden-sm">
     <div class="add-to-links">
        <div class="row ico_data">
           <div class="col-lg-6 col-md-6 col-sm-6">
              <button type="button" data-toggle="tooltip" title="" onclick="" class="btn btn-primary btn-block"  data-original-title="Add to Wish List">
              <i class="fa fa-heart"></i> <span>Favorite</span></button>
           </div>
           <div class="col-lg-6 col-md-6 col-sm-6">
              <button type="button" data-toggle="tooltip" title="" onclick="" class="btn btn-primary btn-block"  data-original-title="Compare this Product">
              <img src="{{URL::asset('frontend')}}/images/compare.png"><span>Compare</span></button>
           </div>
        </div>
        <div class="row">
        
        <div class="col-md-12 vie_prodmod view_pro"> 
                                          <!-- <a href="#product-modal" data-toggle="modal" class="view_pro"><img src="images/eye.png" alt="view" /></a> -->
										 <button ui-sref="Modal.productmodel"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                    </div>
        </div>
     </div>
  </div>
    <div class="image">
        <a href="javascript:void(0)"><img bn-lazy-src="<%slid_data.thum_img%>" alt="" title="Aspire Ultrabook Laptop" class="img-responsive" /></a>                                </div>
    <div class="caption">
        <h4><a href="javascript:void(0)"><%slid_data.title%></a></h4>
        <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
            <span class="fa fa-stack"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> <span class="rev_c">(165)</span>                                   </div>                                    
        <p class="price"> 
            
            <span class="price-old"><%slid_data.d_price%></span>
            <span class="price-new"><%slid_data.price%></span>  
            <span class="saving" ng-if="slid_data.off_p"><%slid_data.off_p%></span>                                    </p>
    </div>
    
</div>
  </div>
</data-owl-carousel>	

</div>

