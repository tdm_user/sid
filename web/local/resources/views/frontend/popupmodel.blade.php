  <div class="Modal-box modl_prod">
  <div id="product-modal">

    <div class="modal-dialog new_moddial">
        <div class="modal-content">
            <!-- <div class="modal-header"> -->
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
				
                <!-- <h4 class="modal-title">Samsung 28 Inch HD Slim LED TV (UA-28J4100)</h4>
            </div> -->
       
             <div class="modal-header">
                   <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>-->
                       <div class="alert alert-success" ng-if="minicart_success">
		            <p >
		            <% minicart_success%>
		            </p>
		        </div>
		          <div class="alert alert-danger"  ng-if="minicart_errors">
		           <p><%minicart_errors%></p>
		        </div>  
                  <a href="javascript:void(0)"  title="close" class="close" ><img src="{{ URL::asset('frontend/images/close.png') }}" alt="close" /></a> 
                </div>
            <div class="modal-body">
                       <div class="row  main_de">
                      <div id="content_page" class="col-sm-12 col-xs-12 col-lg-12" scrollbar>
            <div class="row product-info quick">
              <div class="col-lg-5 col-sm-5 col-xs-12 image_zoo">
                <div class="row product_zoom"  ng-init="setApproot('');zoom01=false;zoom01a=false;zoom03=false;zoom04=false;">
                    <div class="col-lg-10 pull-right">
                    <div class="image">                    
	               <img 
	                 src="{{ URL::asset('uploads') }}/<%model_item.images[0].image%>"
	                 data-zoom-image="{{ URL::asset('uploads') }}/<%model_item.images[0].image%>"
	
	                 ez-plus
	                 ezp-model="zoomModelGallery01"
	                 ezp-options="zoomOptionsGallery01"
	                />
	             </div>
                    <div class="center-block text-center">
                    
                    <span class="zoom-gallery"><i class="fa fa-search"></i> Mouse over to zoom in</span></div>
                    
                    </div>
                    <div class="col-lg-2 pull-left">
                        
                    <div id="gallery_01">
                            <a class="img-group-01" width="46"
                               ng-repeat="image2 in model_item.images"
                               ng-click="setActiveImageInGallery('zoomModelGallery01',image2);"
            
                               data-image="{{ URL::asset('uploads')}}/<%image2.image%>"
                               data-zoom-image="{{ URL::asset('uploads')}}/<%image2.image%>"
            
                               href="javascript:void(0)">
                                <img width="46" height="46" ng-src="{{ URL::asset('uploads')}}/<%image2.image%>"/>
                            </a>
                        </div>
                  		
                    </div>
                  </div>
                  <div class="row pro_descr"> 
                      <div class="col-lg-12">
                        <h5>Description:</h5>
                        <div ng-bind-html="model_item.prod_data.pro_des"></div>
                        
                      </div>
                  </div>
              </div>
              <div class="col-lg-7 col-md-7 col-sm-7 cont_are">
              <div class="set_pa">
              <h3 class="title" itemprop="name"><%model_item.prod_data.pro_name%></h3>
                <div class="rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                  <meta itemprop="ratingValue" content="0" />
                 <p>
                   <span class="fa fa-stack" ng-repeat="i in  getNumber(model_item.avg_review)"><i class="fa fa-star fa-stack-2x"></i></span> 
									 
                    <span class="fa fa-stack" ng-repeat="i in  getNumber(5-model_item.avg_review)"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
                  
                   <span class="rev_c">(<%model_item.review.length%>)</span>                                   
                  
                  <a onClick="$('a[href=\'#tab-review\']').trigger('click'); return false;" href="#"><span itemprop="reviewCount">reviews <i class="fa fa-angle-down" aria-hidden="true"></i></span>
                  
                  </a> | <a onClick="$('a[href=\'#tab-review\']').trigger('click'); return false;" href="#"><b><%model_item.total_sold%> already sold</b></a></p>
                </div>
                <hr>
                
                <ul class="list-unstyled price_pro">
                  <li>Price:</li>
                  <li class="price">
                  <span class="price-old" ng-if="model_item.prod_data.prod_price !=''">Rs.<%model_item.prod_data.price%></span>
                  <span itemprop="price" ng-if="model_item.prod_data.prod_price !=''">Rs.<%model_item.prod_data.prod_price%></span>
                   <span itemprop="price" ng-if="model_item.prod_data.prod_price ==''">Rs.<%model_item.prod_data.price%></span>
                  <span class="label label-danger" ng-if="model_item.prod_data.percent!=''"><%model_item.prod_data.percent%>% Off</span>
                   </li>
                </ul>
                
                
                <div class="price-box detail_ca">
                  <div class="row" ng-repeat="option_data in model_item.options">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                      <%option_data.option_name%>:
                      </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <ul class="list-unstyled">
                                <li ng-repeat="opt_val in option_data.option_values"><a href="#" class="btn btn-default"><%opt_val.option_name%></a></li>                                
                            </ul>
                          </div>
                   </div>
                
                  <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                      Quantity:
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                     <div class="qty">
                        
                        <input type="text" name="quantity" ng-model="quant" ng-init="quant=1" size="2" id="input-quantity" class="form-control" />
                     
                            <a class="qtyBtn plus" href="javascript:void(0);" ng-click="quant=quant+1">+</a>
                            <a class="qtyBtn mines" href="javascript:void(0);" ng-show="quant > 1" ng-click="quant=quant-1">-</a>
                       
                        <div class="clear"></div>
                      </div>
                  </div>
                  
                  </div>
                  <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">Shiping:</div>
                       <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 ship_are">
                       <label>Rs 150 to <a href="javascript:void(0)"><span>Kathmandu Inside Ring Road via SID Standard Delivery</span><i class="fa fa-angle-down" aria-hidden="true"></i></a> </label>
                       <p>Estimated Delivery Time: 1-3 days (ships out within 3 business days)</p>
                       </div>
                      
                </div>
                </div>
               
                <div class="row but_sec">
                 
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                  <button type="button" id="button-cart" class="btn btn-primary btn-lg btn-block " ng-click="add_to_cart(model_item.prod_data,quant);">Buy Now</button>
                  <ul class="list-unstyled favor">
                    <li><a href="#"> <i class="fa fa-heart"></i> <span>Add to Favourite</span></a> </li>
                    <li> 
                        <a href="javascript:void(0);"  ng-click="add_to_compare(model_item.prod_data)" >
                                          <img src="{{URL::asset('frontend')}}/images/compare.png" ><span>Compare</span></a>
                    </li>
                  </ul>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                      <div class="coup">
                        <h4>COUPON</h4>
                        <p>Get<span> 5% discount</span> per Rs. 1500</p>
                      </div>
                  </div>
                  
                </div>
                <div class="row pro_sec">
                 
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="product_de text-center">
                        <h4>Seller Information</h4>
                        <img src="{{ URL::asset('uploads') }}/<%model_item.seller_details.image%>" alt="product" />
                        <b><%model_item.seller_details.fname%> <%model_item.seller_details.lname%></b>
                        <span class="pro_titl">Selling for 2 years</span>
                        <p class="rating">
                   <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
                   <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
                   <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
                   <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span> 
                   <span class="fa fa-stack"><i class="fa fa-star dark-stack fa-stack-2x"></i></span> 
                   <span class="rev_c">(10)</span></p> 
                   <p class="veri_class"> <a href="#"><img src="{{ URL::asset('frontend/images/checkd.png') }}" alt="check" /> Verified by SID [?]</a></p>
                        
                    </div>
                  </div>
                   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 set_us">
                        <div class="user_sec1">
                            
                                <ul class="list-unstyled">
                                    <li>
                                    <a href="#" ><img src="{{ URL::asset('frontend/images/store.png') }}" />Visit Store </a>
                                    </li>
                                    <li>
                                    <a href="#" ><img src="{{ URL::asset('frontend/images/user_ plus.png') }}" />  Follow Seller</a>
                                    </li>
                                    <li>
                                    <a href="#" ><img src="{{ URL::asset('frontend/images/chart.png') }}" />Chat with Seller</a>
                                    </li>
                                </ul>
                          
                        </div>
                        <div class="list_user">
                         <h5>This Seller has <span>30 followers</span></h5>
                         <ul class="list-unstyled list_data">
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user1.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user2.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user3.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user4.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user5.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user6.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user7.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user8.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user5.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/userN.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/userH.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user9.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user10.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user11.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user12.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user13.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user14.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user15.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user16.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user17.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user18.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user19.jpg') }}" alt="user" /></li> 
                          <li><img src="{{ URL::asset('frontend/images/user_ico/user3.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user20.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user21.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user22.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user23.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user8.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user5.jpg') }}" alt="user" /></li>
                         <li><img src="{{ URL::asset('frontend/images/user_ico/user22.jpg') }}" alt="user" /></li>
                             
                             
                         </ul>
                        </div>
                   </div>
               </div>
              
               </div>
              </div>
            </div>
        </div> 
    
                       </div>
            </div>
        </div>
    </div>
</div>

</div>