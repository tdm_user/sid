<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 subtitle top_heading">
			Payment Method
		</div>
	</div>
	<div class="row">
	    <form action = "<?php echo url('front/checkout/post_order'); ?>" method="post" id="payment-form">
		 {{ csrf_field() }}
		   
		   <div class="payment-errors" >
		            
		   </div>
		 
		<div class="col-md-9 col-sm-9 col-xs-12 payment_method">
			<div class="col-md-12 col-sm-12 col-xs-12 payment_sub_heading">
				Select a payment method to pay <span class="payment_price">Rs.<%checkout_data.cart_item.total%></span>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 payment_input">
				
						<div class="col-md-1 col-sm-1 col-xs-1 radio_input">
							<input type="radio" name="payment_method" ng-model="checkpay.payment_method" value="Credit Card">
						</div>
						<div class="col-md-11 col-sm-11 col-xs-11 radio_content">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<div class="col-md-6 col-sm-6 col-xs-12 credit_card">
										Credit Card
									</div>
									<div class="col-md-6 col-xs-12 col-sm-6 credit_image" align="right">
										<img src="{{URL::asset('frontend/')}}/images/n_img/visa.png"><img src="{{URL::asset('frontend/')}}/images/n_img/master_card.png">
									</div>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12 cardholder_name">
										Cardholder's Name<span class="card_required">*</span>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
										<input type="text" class="form-control"  >
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12 card_number">
										Card Number<span class="card_required" >*</span>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
										<input type="text" class="form-control"  data-stripe="number" ng-model="stripe.stripeNum">
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="col-md-4 col-xs-12 col-sm-4 expiry_detail">
										<label class="expiry_date">Expiry Date</label>
										<input type="text" class="form-control expiry_mnth"  ng-model="stripe.stripeMM" data-stripe="exp_month"  placeholder="   MM   ">
										<input type="text" class="form-control year1" ng-model="stripe.stripeYY" data-stripe="exp_year"  placeholder="YYYY"  >
									
									</div>
									<div class="col-md-8 col-xs-12 col-sm-8 security_detail">
										
										<label class="security_code">Security Code</label><span class="card_required" >*</span>
										<input type="text" class="form-control" ng-model="stripe.stripeCVC" data-stripe="cvc">
										<img src="{{URL::asset('frontend/')}}/images/n_img/visa2.png" class="security_img">

									</div>
								</div>
								<div class="col-md-11 col-sm-12 col-xs-12">
										<div class="checkbox form-group checkbox_future_use">
											<input type="checkbox" value="">&nbsp;&nbsp;Save for future use
										</div>
								</div>
						</div>
				
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 payment_sub_heading">
				
					<input type="radio"  name="payment_method" ng-model="checkpay.payment_method" value="Paypal">Paypal
				
					<img src="{{URL::asset('frontend/')}}/images/n_img/paypal.png">
				
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 payment_sub_heading">
				
					<input type="radio" name="payment_method" ng-model="checkpay.payment_method" value="E-banking">E-banking
				
					<img src="{{URL::asset('frontend/')}}/images/n_img/net_banking.png">
				
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 payment_sub_heading">
				
					<input type="radio" name="payment_method"  ng-model="checkpay.payment_method" value="My Wallet">My Wallet
				
					<img src="{{URL::asset('frontend/')}}/images/n_img/wallet.png">
				
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 payment_sub_heading">
				
					<input type="radio" name="payment_method"  ng-model="checkpay.payment_method" value="COD">Cash on Delivery
				
					<img src="{{URL::asset('frontend/')}}/images/n_img/cod.png">
				
			</div>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12 order_shipping">
			<div class="col-md-12 col-sm-12 col-xs-12 order_summary">
				<div class="col-md-12 col-sm-12 col-xs-12 order_summary_heading">
					Order Summary
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12 col-sm-12 col-xs-12 os_total_item">
						Total Items:<% checkout_data.cart_item.total_num %><br>
						Shipping Charge:Rs.300
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 os_total_price">
						Total: <span>Rs.<%checkout_data.cart_item.total%></span>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 os_place_order">
					        <input type="hidden" name="shipping_method" ng-model="checkpay.shipping_method" ng-init="checkpay.shipping_method='SID Standard Delivery'" value="SID Standard Delivery">
						<button type="submit" class="btn btn-primary" id="submitBtn" ng-click="payments();">Place Order</button>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 shipping_address">
				<div class="col-md-12 col-sm-12 col-xs-12 order_summary_heading">
					Shipping Address
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12" style="background-color:white;">
					<div class="col-md-12 col-sm-12 col-xs-12 as_cont_detail">
						<b><%checkout_data.store_address.ship_fname%> <%checkout_data.store_address.ship_lname%></b><br>
						<%checkout_data.store_address.ship_address%>, Near <%checkout_data.store_address.ship_landmark%><br>
						<%checkout_data.store_address.city%>, <%checkout_data.store_address.state%> <%checkout_data.store_address.state%><br>
						<%checkout_data.store_address.ship_mobile%><br>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 sa_shipping_service">
						<b>Shipping Service</b>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 sa_delivery">
						● SID Standard Delivery
					</div>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>
