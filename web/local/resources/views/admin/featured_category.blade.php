    <!-- Main content -->
    <section class="content">
     
	 <div class="alert alert-success" ng-if="success_flash">
            <p >
            <% success_flash %>
            </p>
        </div>
        <div class="alert alert-danger"  ng-if="errors">
            <ul>
                <li ng-repeat ="er in errors"><% er %></li>
         
            </ul>
        </div>
      
          <!-- /.box -->
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-list"></i> Featured Category</h3>    
			   <div class="pull-right"> <a data-toggle="modal" data-target="#featured_category" class="btn btn-primary"><i class="fa fa-plus"></i> Add Featured Category</a></div> 
			    <!--Add Modal -->
		    <div class="modal fade" id="featured_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Featured Category</h4>                            
			      </div>
			      <div class="modal-body">
				   <div class="alert alert-success" ng-if="success_flash_pop">
						<p >
						<% success_flash_pop %>
						</p>
					</div>
				   <div class="alert alert-danger"  ng-if="errors_pop">
						<ul>
							<li ng-repeat ="er in errors_pop"><% er %></li>
					 
						</ul>
					</div>
				     <div class="form-group">
					       <label>Category</label>
					       <select class="form-control" ng-init="add_featured.category_id='0'" ng-change="add_featured.subcategory_id=[]" ng-model="add_featured.category_id" convert-to-number>
						      <option value="0">Please Select</option>
						      <option ng-repeat="cat in category|filter:{parent_id:0}:true" value="<%cat.id%>" >
							  <%cat.category_name%>
							  </option>
						   </select>
					 </div>
					 <div class="form-group"  ng-if="add_featured.category_id!=0">
					       <label>Sub-category</label>
					   		 <div ng-repeat="cat in category|filter:{parent_id:add_featured.category_id}:true"  >
						      <input type ="checkbox" ng-model="add_featured.subcategory_id[cat.id]" value="<%cat.id%>"  />
							  <%cat.category_name%>
							</div>  						  
					 </div>
					 <div class="form-group">
					       <label>Position</label>
					       <select class="form-control" ng-init="add_featured.position=0"  ng-model="add_featured.position"  convert-to-number>				  						      <option value="0">Top</option>
							   <option value="1">Bottom</option>
						   </select>
					 </div>
					 <div class="form-group">
					       <label>Status</label>
					       <select class="form-control" ng-init="add_featured.status=1" ng-model="add_featured.status"  convert-to-number>						  
						      <option value="1">Active</option>
							   <option value="0">Inactive</option>
						   </select>
					 </div>
					  <div class="form-group">
					       <label>Sort</label>
					      <input class="form-control" ng-model="add_featured.sort"   />
					 </div>
			      </div>
			      <div class="modal-footer">
				   <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>  
					<button type="button" ng-click="submit_featured(add_featured)" class="btn btn-primary pull-right" >Submit</button>                        
			      </div>
			    </div>
			  </div>
		    </div>
			  <!--Quick Edit Model-->
		    <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" ng-click="init();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit</h4>                            
			      </div>
			     <div class="modal-body" >
                                    <div class="alert alert-success" ng-if="success_flash_pop">
                                        <p>
                                        <% success_flash_pop %>
                                        </p>
                                    </div>
                                    <div class="alert alert-danger"  ng-if="errors_pop">
                                        <ul>
                                            <li ng-repeat ="er in errors_pop"><% er %></li>         
                                        </ul>
                                    </div>
				    
				   
					<div class="form-group"  ng-if="edit_field=='category'">
					       <label>Category</label>
					       <select class="form-control" ng-change="edit_sub();edit_values.subcategory_id=[];edit_values.subcat_id=[];" ng-model="edit_values.category_id" convert-to-number>
						      
						      <option ng-repeat="cat in category|filter:{parent_id:0}:true" value="<%cat.id%>" >
							  <%cat.category_name%>
							  </option>
						   </select>
						
					 </div>  
                       <div class="form-group"  ng-if="edit_field=='subcategory' || edit_subcat==1">
					       <label>Sub-category</label>
					   		 <div ng-repeat="cat in category|filter:{parent_id:edit_values.category_id}:true"  >							
						      <input type ="checkbox" ng-model="edit_values.subcat_id[cat.id]"  />
							  <%cat.category_name%>
							</div> 						  
					 </div>
					  <div class="form-group" ng-if="edit_field=='position'">
					       <label>Position</label>
					       <select class="form-control"  ng-model="edit_values.position"  convert-to-number>						  
						      <option value="0">Top</option>
							   <option value="1">Bottom</option>
						   </select>
					 </div>
					 <div class="form-group" ng-if="edit_field=='status'">
					       <label>Status</label>
					       <select class="form-control" ng-model="edit_values.status"  convert-to-number>	
						      <option value="1">Active</option>
							   <option value="0">Inactive</option>
						   </select>
					 </div>
					  <div class="form-group" ng-if="edit_field=='sort'">
					       <label>Sort</label>
					      <input class="form-control" ng-model="edit_values.sort"   />
					 </div>
				  
			      </div>
			      <div class="modal-footer">
				  <button type="button" class="btn btn-default pull-left" ng-click="init();" data-dismiss="modal">Close</button>			      
				  <button class="btn btn-primary" ng-click="Quickupdate(edit_values)" >Update</button>                         
			      </div>
			    </div>
			  </div>
		  </div>
			           
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
			       <table id="example1" class="table table-bordered table-striped">
				      <thead>
						<tr>						 
						  <th >#</th>
						  <th>Category Name
						  </th> 
						  <th>Sub Category Name
						  </th> 
						   <th>Sort
						  </th> 
						   <th>Position
						  </th> 
						  <th>Status
						  </th> 
						  <th>Action
						  </th> 
						  </tr>
					 </thead>
					 <tbody>
						<tr ng-repeat="feacat in featured_category">						 
						  <td ><%feacat.id%></td>
						  <td data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('category',feacat)"><span ng-repeat="cat in category|filter:{id:feacat.category_id}:true" ><%cat.category_name%></span>
						  </td> 
						  <td data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('subcategory',feacat)" >
							  <span ng-repeat="(ky,feasub) in feacat.subcats">
							          <span ng-if="ky!=0">,</span>
									  <span ng-repeat="cat in category|filter:{id:feasub}:true" >
									    <%cat.category_name%>
									  </span>
							   </span>
						  </td> 
						   <td data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('sort',feacat)"><%feacat.sort%>
						  </td> 
						   <td data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('position',feacat)"><span ng-if="feacat.position==1">Bottom</span><span ng-if="feacat.position==0">Top</span>
						  </td>
						  <td data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('status',feacat)"><span class="label label-success" ng-if="feacat.status==1">Active</span><span class="label label-danger" ng-if="feacat.status==0">Inactive</span>
						  </td> 
						  <td><i class="fa fa-trash" data-target="#del_modal<%feacat.id%>" data-toggle="modal" style="cursor:pointer" title="Delete"></i>
						 
                 
                  <!-- Modal -->
               <div class="modal fade" id="del_modal<%feacat.id%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete</h4>
                          </div>
                          <div class="modal-body">
                            Are you sure you want to delete this Featured Category ? 
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>                           
                            
                               <button ng-click="deleteCategory(feacat.id)" class="btn btn-primary" data-dismiss="modal" >Delete</button>
                           
                          </div>
                        </div>
                      </div>
                    </div>
		 
						  </td> 
						  </tr>
					 </tbody>
					  <tfoot>
						<tr>						 
						  <th >#</th>
						  <th>Category Name
						  </th> 
						  <th>Sub Category Name
						  </th> 
						   <th>Sort
						  </th> 
						   <th>Position
						  </th> 
						  <th>Status
						  </th> 
						   <th>Action
						  </th> 
						  </tr>
					 </tfoot>
				   </table>
            </div>
            <!-- /.box-body -->
          </div>
         
          <!-- /.box -->
           
        <!-- Button trigger modal -->




          <!-- Form Element sizes -->
         

  

    </section>
   
  <!-- /.content-wrapper -->
