    <!-- Main content -->
    <section class="content">
     
	 <div class="alert alert-success" ng-if="success_flash">
            <p >
            <% success_flash %>
            </p>
        </div>
        <div class="alert alert-danger"  ng-if="errors">
            <ul>
                <li ng-repeat ="er in errors"><% er %></li>
         
            </ul>
        </div>
      
          <!-- /.box -->
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-list"></i> Navigation List</h3>
             
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
			   <div class="col-xs-12">
			         <div class="row">
					    <div class="col-xs-3">
							<div class="form-group">
								<select class="form-control" ng-model="position">
									<option value="Header">Header</option>
									<option value="Footer">Footer</option>
								</select>
							</div>
						</div>
					 </div>
			   </div>
              
			   <div class="col-xs-3">		  
			     <div class="row">
				    <div class="col-xs-12">
					<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">Categories</h3>
								<div class="box-tools pull-right">
								  <button title="Collapse" data-toggle="tooltip" data-widget="collapse" class="btn btn-box-tool" type="button">
									<i class="fa fa-minus"></i></button>
								</div>
							</div>
						  <div class="box-body">			    
						  <div class="form-group">
							<div class="frm-cat">
						   <ul class="ul-cat">
							  <li ng-repeat="cat in categorys">
							  <input type="checkbox" name="category_id[]"  ng-model="cat.check"   />
							  <span><%cat.name%></span>
							  </li>
						  </ul>
						  </div>						
						 </div>
						  </div>
					        <div class="box-footer">
					        <button type="button" class="btn btn-primary" ng-click="add_to_menu('categorys',categorys,position);">Add To Menu</button>
					        </div>
		            </div>
				   </div>
					<div class="col-xs-12">
					<div class="box">
					  <div class="box-header with-border">
					<h3 class="box-title">Brands</h3>
					<div class="box-tools pull-right">
					  <button title="Collapse" data-toggle="tooltip" data-widget="collapse" class="btn btn-box-tool" type="button">
						<i class="fa fa-plus"></i></button>
					</div>
		         </div>
		        	  <div class="box-body collapse">			    
			      <div class="form-group">
				    <div class="frm-cat">
			       <ul class="ul-cat">
					  <li ng-repeat="brand in brands">
					  <input type="checkbox" name="brand_id[]" value="3" ng-model="brand.check"  >
					  <span ><%brand.name%></span>
					  </li>
				  </ul>
			      </div>
				 </div>
			 </div>
			          <div class="box-footer collapse">
					        <button type="button" class="btn btn-primary" ng-click="add_to_menu('brands',brands,position);">Add To Menu</button>
					   </div>
		           </div>
					</div>
					<div class="col-xs-12">
					<div class="box">
					  <div class="box-header with-border">
					<h3 class="box-title">Store</h3>
					<div class="box-tools pull-right">
					  <button title="Collapse" data-toggle="tooltip" data-widget="collapse" class="btn btn-box-tool" type="button">
						<i class="fa fa-plus"></i></button>
					</div>
		         </div>
		              <div class="box-body collapse">			    
			      <div class="form-group">
				    <div class="frm-cat">
			       <ul class="ul-cat">
					  <li ng-repeat="str in store">
					  <input type="checkbox" name="store_id[]" ng-model="str.check"  >
					  <span ><%str.name%></span>
					  </li>
				  </ul>
			      </div>
				 </div>
			 </div>
			          <div class="box-footer collapse">
					        <button type="button" class="btn btn-primary" ng-click="add_to_menu('store',store,position);">Add To Menu</button>
					   </div>
		            </div>
					</div>
					<div class="col-xs-12">
					<div class="box">
					<div class="box-header with-border">
					<h3 class="box-title">Product</h3>
					<div class="box-tools pull-right">
					  <button title="Collapse" data-toggle="tooltip" data-widget="collapse" class="btn btn-box-tool" type="button">
						<i class="fa fa-plus"></i></button>
					</div>
		         </div>
		            <div class="box-body collapse">			    
			      <div class="form-group">
				    <div class="frm-cat">
			       <ul class="ul-cat">
					  <li ng-repeat="pr in product">
					  <input type="checkbox" name="product_id[]"  ng-model="pr.check"  >
					  <span><%pr.name%></span>
					  </li>
				  </ul>
			      </div>
				 </div>
			 </div>
			        <div class="box-footer collapse">
					        <button type="button" class="btn btn-primary"  ng-click="add_to_menu('product',product,position);">Add To Menu</button>
					   </div>
		            </div>
					</div>
					<div class="col-xs-12">
					<div class="box">
					<div class="box-header with-border">
					<h3 class="box-title">Custom Links</h3>
					<div class="box-tools pull-right">
					  <button title="Collapse" data-toggle="tooltip" data-widget="collapse" class="btn btn-box-tool" type="button">
						<i class="fa fa-plus"></i></button>
					</div>
		         </div>
		            <div class="box-body collapse">			    
			      <div class="form-group">
				    <div class="frm-cat">
			         <label>URL</label>
					 <input type="text" ng-model="custom_link.url" class="form-control" />
					 <label>Link Text/Image Url</label>
					 <input type="text" ng-model="custom_link.text" class="form-control" />
			      </div>
				 </div>
			 </div>
			        <div class="box-footer collapse">
					        <button type="button" class="btn btn-primary"  ng-click="add_to_menu('custom_link',custom_link,position);">Add To Menu</button>
					   </div>
		            </div>
					</div>
				 </div>
			   </div>
			
				<div class="col-xs-9">
				 <div class="row">
				    <div class="col-xs-12">
					   <div class="box">
						   <div class="box-header with-border">
								<h3 class="box-title"><%position%> Menu</h3>
								<div class="box-tools pull-right">
								  <button title="Collapse"  ng-click="update_menu(navigation);" class="btn btn-primary" type="button">
									Update Menu</button>
								</div>
							</div>
						   <div class="box-body">			    
						       <div class="col-xs-12">
								   <div class="box" ng-repeat="(key2,nav) in navigation|filter:{position:position}">
										<div class="box-header with-border">
											<h3 class="box-title" ng-if="nav.menu_name!=''" ng-bind="nav.menu_name"></h3>
											<h3 class="box-title" ng-if="nav.nav_menu!=''" ng-bind="nav.nav_menu"></h3>
											<img src="<%nav.menu_image%>" ng-if="nav.menu_image!=''"/>
											<div class="box-tools pull-right">
											  <button title="Collapse" data-toggle="tooltip" data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-plus"></i></button>
											</div>
										</div>
									  <div class="box-body collapse">	
									    <div class="form-group">
										  <div ng-if="nav.url">
											  <label>URL</label>
											 <input  type="text" class="form-control" ng-model="nav.url" /><br/>
										 </div>	
										 <div ng-if="nav.menu_name!=''">
											  <label>Link Text/Image Url</label>
											 <input  type="text" class="form-control" ng-model="nav.menu_name" /><br/>
										 </div>	
										  <div ng-if="nav.menu_image!=''">
											  <label>Link Text/Image Url</label>
											 <input  type="text" class="form-control" ng-model="nav.menu_image" /><br/>
										 </div>	    
									      <label>Sort</label>
										 <input  type="text" class="form-control" ng-model="nav.sort" /> <br/>										 
										   <label>Position</label>
										  <select class="form-control" ng-model="nav.position">
										   	<option value="Header">Header</option>
									        <option value="Footer">Footer</option>
										  </select><br/>
										   <label>Status</label>
										  <select class="form-control" ng-model="nav.status"  convert-to-number>
										   	<option value="0">Inactive</option>
									        <option value="1">Active</option>
										  </select><br/>
										  <div ng-if="nav.mega_menu=='0'">
										  <label>Parent Menu</label>
										  <select class="form-control" ng-model="nav.parent_id" convert-to-number>
											   <option value="0">Please Select</option>
												<option ng-repeat="nav1 in navigation" value="<% nav1.id%>" 
												ng-if="nav1.menu_image =='' ">
												<span ng-if="nav1.menu_name!=''"><%nav1.menu_name%></span>
												<span ng-if="nav1.nav_menu!=''"><%nav1.nav_menu%></span>
												</option>									       
										   </select>
										  <br/>
										  </div>
										  <div ng-if="nav.parent_id!=0">
										   <label>Under Mega Menu Title</label>
										  <select class="form-control" ng-model="nav.mega_menu_titles"  convert-to-number>
										   <option value="0">Please Select</option>
										   	<option ng-repeat="val in mega_menu_title|filter:{navigation_id:nav.parent_id}" value="<%val.id%>"><%val.title%></option>									       
										  </select><br/>
										  </div>
										  <div ng-if="nav.parent_id == 0">
											  <label>Set As Mega Menu</label>
												<input  type="checkbox" ng-click="nav.mega_menu_title=[]" ng-checked="nav.mega_menu=='1'" value='1' ng-model="nav.mega_menu" />		
																			  
											  <div ng-if="nav.mega_menu == '1' || nav.mega_menu == 1" >
												  <div>
													<label>Mega Menu Title</label>
													<input  type="text" class="form-control" ng-model="nav.add_mega_title"    /> <br/>
													<button class="btn btn-primary" type="button" ng-click="add_nav(nav.add_mega_title,nav);">
													Add Mega Menu Title
													</button>
													<span style="width: 100%;" dir="ltr" class="select2 select2-container select2-container--default select2-container--below select2-container--focus">
				<span class="selection">
					  <span tabindex="-1" aria-expanded="false" aria-haspopup="true" role="combobox" class="select2-selection select2-selection--multiple">
					  <ul class="select2-selection__rendered">
					   <li ng-repeat="valmega in nav.mega_menu_title" class="select2-selection__choice ng-binding ng-scope"><%valmega%> 
					   <span style="cursor:pointer" ng-click="remove_nav($index,nav)">x</span></li>
					  </ul>
					  </span>
				</span>
			  </span>
												  </div>
											  </div>
										  </div>
										 </div>
										 <div class="box" ng-repeat="(key1,child_nav) in nav.child_nav|filter:{position:position}">
										<div class="box-header with-border">
											<h3 class="box-title" ng-if="child_nav.menu_name!=''" ng-bind="child_nav.menu_name"></h3>
											<h3 class="box-title" ng-if="child_nav.nav_menu!=''" ng-bind="child_nav.nav_menu"></h3>
											<img src="<%child_nav.menu_image%>" ng-if="child_nav.menu_image!=''"/>
											<div class="box-tools pull-right">
											  <button title="Collapse" data-toggle="tooltip" data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-plus"></i></button>
											</div>
										</div>
									  <div class="box-body collapse">	
									    <div class="form-group">
										  <div ng-if="child_nav.url">
											  <label>URL</label>
											 <input  type="text" class="form-control" ng-model="child_nav.url" /><br/>
										 </div>	
										 <div ng-if="child_nav.menu_name!=''">
											  <label>Link Text/Image Url</label>
											 <input  type="text" class="form-control" ng-model="child_nav.menu_name" /><br/>
										 </div>	
										  <div ng-if="child_nav.menu_image!=''">
											  <label>Link Text/Image Url</label>
											 <input  type="text" class="form-control" ng-model="child_nav.menu_image" /><br/>
										 </div>	    
									      <label>Sort</label>
										 <input  type="text" class="form-control" ng-model="child_nav.sort" /> <br/>								 
										   <label>Status</label>
										  <select class="form-control" ng-model="child_nav.status"  convert-to-number>
										   	<option value="0">Inactive</option>
									        <option value="1">Active</option>
										  </select><br/>
										   <div ng-if="nav.mega_menu_title.length > 0">
											  <label>Under Mega Menu Title</label>
										 <select class="form-control" ng-model="child_nav.mega_menu_id"  convert-to-number>
										     <option value='0'>Please Select</option>
										     <option ng-repeat="mg_title in nav.mega_title_table" value="<%mg_title.id%>"><%mg_title.title%></option>
										  </select><br/>
										 </div>	 										  
										  
										 </div>
									  </div>
										<div class="box-footer collapse">
									      <button class="btn btn-danger" title ="Delete" style="cursor:pointer" data-toggle="modal" data-target="#del_modal<% child_nav.id %>">Delete Menu</button>
                 
                  <!-- Modal -->
               <div class="modal fade" id="del_modal<% child_nav.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete</h4>
                          </div>
                          <div class="modal-body">
                            Are you sure you want to delete this Menu ? 
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>                           
                              
                               <button ng-click="deleteNav(child_nav.id)" class="btn btn-primary" data-dismiss="modal" >Delete</button>
                           
                          </div>
                        </div>
                      </div>
                    </div>
										</div>
								   </div>
									  </div>
										<div class="box-footer collapse">
									      <button class="btn btn-danger" title ="Delete" style="cursor:pointer" data-toggle="modal" data-target="#del_modal<% nav.id %>">Delete Menu</button>
                 
                  <!-- Modal -->
               <div class="modal fade" id="del_modal<% nav.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete</h4>
                          </div>
                          <div class="modal-body">
                            Are you sure you want to delete this Menu ? 
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>                           
                               
                               <button ng-click="deleteNav(nav.id)" class="btn btn-primary" data-dismiss="modal" >Delete</button>
                           
                          </div>
                        </div>
                      </div>
                    </div>
										</div>
								   </div>
								</div>
						    </div>
					        <div class="box-footer">
					      
					        </div>
		               </div>
					</div>
				 </div>
				</div> 
            </div>
            <!-- /.box-body -->
          </div>
         
          <!-- /.box -->
           
        <!-- Button trigger modal -->




          <!-- Form Element sizes -->
         

  

    </section>
   
  <!-- /.content-wrapper -->
