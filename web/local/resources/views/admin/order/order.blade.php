<section class="content">

    <div class="alert alert-success" ng-if="success_flash">
        <p >
            <% success_flash %>
        </p>
    </div>
    <div class="alert alert-danger"  ng-if="errors">
        <ul>
            <li ng-repeat ="er in errors"><% er %></li>         
        </ul>
    </div> 
    <div class="box" ng-if="page == 'index'">
        <div class="box-header">
            <div class="form-group col-md-2 ">		  
                <div id="container" ></div>
                <!--<button class="btn btn-default" data-toggle="modal" data-target="#screen_opt_modal">Screen Options</button>-->
            </div>
            <div class="col-md-10">
                <div class="col-sm-12">
                        <div class="pull-right">300/500</div><br/>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success progress-barstriped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%"></div>
                    </div>
                        <div class="pull-right">300/500</div><br/>
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning progress-barstriped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%"></div>
                    </div>
                        <div class="pull-right">300/500</div><br/>
                    <div class="progress">
                        <div class="progress-bar progress-bar-danger progress-barstriped" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="col-sm-12 margin">
                <ul class="nav  nav-tabs inli-tab responsive">
                    <li ng-class="{
                                active: isSet(1)
                            }">
                        <a href ng-click="setTab(1)">
                            All Orders</a>
                    </li>
                    <li ng-class="{
                                active: isSet(2)
                            }">
                        <a href ng-click="setTab(2)">
                            Completed</a>
                    </li>
                    <li ng-class="{
                                active: isSet(3)
                            }">
                        <a href ng-click="setTab(3)">
                            Processing</a>
                    </li>
                    <li ng-class="{
                                active: isSet(4)
                            }">
                        <a href ng-click="setTab(4)">
                            Cancelled</a>
                    </li>
                </ul>  
            </div>
            <div ng-show="isSet(1)" class="">
                <div class="form-group col-sm-4 pull-right">    
                    <div class="input-group pull-left">
                        <input type="text" class="form-control" placeholder="Search" ng-model="search" />
                        <span class="input-group-addon " id="basic-addon1"><i class="fa fa-search"></i></span>
                        <button class="btn btn-primary pull-right" ng-click="updateOrder()">Update</button>
                    </div>
                </div>
                <table id="allOrder" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><input type="checkbox" ng-click="checkAll()" ng-model="selectedAll"/></th>
                            <th>Date</th>
                            <th>Order ID</th>
                            <th>Tracking</th>
                            <th>Payment Status</th>
                            <th>Country</th>
                            <th>Order Info</th>
                            <th>Customer Note</th>
                            <th>Amount</th>
                            <th>Delivery</th>
                            <th>Assigned to</th>
                            <th>Affiliate</th>
                            <th>Seller</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="o in orders">
                            <td><input type="checkbox" ng-model="order_id[o.id]" /></td>
                            <td ><% o . created_at %></td>
                            <td ><a ng-click="orderDetails($index)"><% o . invoice_no %></a></td>
                            <td ><% o . tracking %></td>
                            <td >
                                <% o . payment_method %>
                                <select ng-model="paymentStatusOption[o.id]" name="paymentStatusOption[<% o . id %>]" data-id="<% o . id %>">
                                    <option></option>
                                    <option ng-repeat="(i, v) in paymentStatus" value="<% i %>"><% v %></option>
                                </select>
                            </td>
                            <td></td>
                            <td><div class="cols-m-12" ng-repeat="info in o.proInfo">
                                    <div><% info . name %></div>
                                    <div><strong>Quantity:</strong><% info . quantity %></div>
                                </div>
                            </td>
                            <td><% o . customer_note %></td>
                            <td><% o . total %></td>
                            <td></td>
                            <td>
                                <select class="" ng-model="couriersOption[o.id]" name="couriersOption[<% o . id %>]" data-id="<% o . id %>">
                                    <option></option>
                                    <option ng-repeat=" item in couriers" value="<% item . id %>"><% item . fname + ' ' + item . lname %></option>
                                </select>
                            </td>
                            <td></td>
                            <td> <span class="col-sm-12" ng-repeat="info in o.proInfo"><% getSeller(info . seller_id) %></span></td>
                            <td>
                                <select class=""  name="orderStatusOption[<% o . id %>]" data-id="<% o . id %>">
                                    <option></option>
                                    <option ng-repeat="(i, v) in orderStatus" ng-selected="i == o.status" value="<% i %>"><% v %></option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12" ng-if="page == 'orderDetail'">
        <div class="panel panel-default">
            <div class="panel-heading">
                Order Information:<% orderInfo . invoice_no %>
            </div>
            <div class="panel-body">
                <div class="col-md-4">
                    <h3>General Details</h3>
                    <div class="col-sm-12">Order Date <% orderInfo . created_at %></div>
                    <div class="col-sm-12">Order ID <% orderInfo . invoice_no %></div>
                    <div class="col-sm-12">Customer <% orderInfo . invoice_no %></div>
                    <div class="col-sm-12">Payment Mode<% orderInfo . payment_method %>
                        <select ng-model="paymentStatusOption[orderInfo.id]" name="paymentStatusOption[<% orderInfo . id %>]" data-id="<% orderInfo . id %>">
                            <option></option>
                            <option ng-repeat="(i, v) in paymentStatus" value="<% i %>"><% v %></option>
                        </select>
                    </div>
                    <div class="col-sm-12">Payment Status<% orderInfo . invoice_no %></div>
                </div>
                <div class="col-md-4">
                    <h3>Tracking Information</h3>
                    <div >
                        <select class="">
                            <option></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <h3>Order Status</h3>
                    <div >
                        <select class=""  name="orderStatusOption[<% orderInfo . id %>]" data-id="<% orderInfo . id %>">
                            <option></option>
                            <option ng-repeat="(i, v) in orderStatus" ng-selected="i == orderInfo.status" value="<% i %>"><% v %></option>
                        </select>
                    </div>
                    <button class="btn btn-primary">Update Order</button>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Order Info
            </div>
            <div class="panel-body">
                <div class="col-sm-4">
                    <h3>Shipping Address <a >Edit</a></h3>
                    <div class="">Phone:</div>
                    <div class="">Email Address:</div>
                </div>
                <div class="col-sm-4">
                    <h3>Delivery Information</h3>

                </div>
                <div class="col-sm-4">
                    <h3>Customer Note</h3>
                    <div class="well"><% orderInfo . customer_note %></div>
                </div>
                <div class="col-sm-12">
                    <table class="table table-responsive table-bordered">
                        <thead>
                            <tr>
                                <th><input type="checkbox"/></th>
                                <th>Items</th>
                                <th>Seller</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Amount</th>
                                <th>Affiliate Fees</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="info in orderInfo.proInfo">
                                <td><input type="checkbox"/></td>
                                <td><% info . name %></td>
                                <td><span class="col-sm-12" ><% getSeller(info . seller_id) %></span></td>
                                <td><% info . price %></td>
                                <td><% info . quantity %></td>
                                <td><% (info . price * info . quantity) %></td>
                                <td>Affiliate Fees</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>All Total</h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-responsive">
                    <tr>
                        <td class="col-sm-6"></td>
                        <td class="col-sm-6">
                            <table class="table table-responsive tablebordered">
                                <thead>
                                    <tr>
                                        <th>Subtotal</th>
                                        <th>RS.</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>Shipping Cost</th>
                                        <th>Rs.</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>Total</th>
                                        <th>Rs.<% orderInfo . total %></th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <button class="btn btn-primary pull-right margin">Update Order</button>
        </div>
    </div>
</section>