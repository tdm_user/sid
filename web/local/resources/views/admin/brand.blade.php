  <!-- Main content -->
    <section class="content">
       <div class="alert alert-success" ng-if="success_flash">
            <p >
            <% success_flash %>
            </p>
        </div>
        <div class="alert alert-danger"  ng-if="errors">
            <ul>
                <li ng-repeat ="er in errors"><% er %></li>
         
            </ul>
        </div> 
          <!-- /.box -->
            
          <div class="box" ng-if="page=='index'">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-list"></i> Brand List</h3>
              <div class="pull-right"> <a href="javascript:void(0);" ng-click="add()" class="btn btn-primary"><i class="fa fa-plus"></i> Add Brand</a></div>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
               <div class="row">
			   <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" ng-click="init();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit</h4>                            
			      </div>
			     <div class="modal-body">
                                    <div class="alert alert-success" ng-if="success_flash_pop">
                                        <p>
                                        <% success_flash_pop %>
                                        </p>
                                    </div>
                                    <div class="alert alert-danger"  ng-if="errors_pop">
                                        <ul>
                                            <li ng-repeat ="er in errors_pop"><% er %></li>         
                                        </ul>
                                    </div>
				    <div class="row" ng-if="edit_field=='brand_name'">
					        <div class="form-group" >
                                        <label for="exampleInputEmail1">Brand Name</label>
                                        <input type="text" class="form-control" ng-model="edit_values.brand_name">
                                        <div class="help-block"></div>
                            </div>  
				    </div>
					<div class="row" ng-if="edit_field=='slug'">
					        <div class="form-group" >
                                        <label for="exampleInputEmail1">Slug</label>
                                          <input type="text" class="form-control" ng-model="edit_values.slug">
                                        <div class="help-block"></div>
                            </div>  
				    </div>
					 <div class="row" ng-if="edit_field=='status'">
					        <div class="form-group" >
                                        <label for="exampleInputEmail1">Status</label>
                                      <input type="radio"  id="" name="status" value="Active" ng-model="edit_values.status" ng-checked="edit_values.status">Active
									   <input type="radio" id="" name="status" value="Inactive" ng-model="edit_values.status" ng-checked="edit_values.status" >Inactive 
                                        <div class="help-block"></div>
                            </div>  
				    </div>
				    <div class="row" ng-if="edit_field=='meta_title'">
					        <div class="form-group" >
                                        <label for="exampleInputEmail1">Meta Title</label>
                                        <input type="text" class="form-control" ng-model="edit_values.meta_title">
                                        <div class="help-block"></div>
                            </div>  
				    </div>
				   
					<div class="row" ng-if="edit_field=='meta_keyword'">
					        <div class="form-group" >
                                        <label for="exampleInputEmail1">Meta Keyword</label>
                                        <input type="text" class="form-control" ng-model="edit_values.meta_keyword">
                                        <div class="help-block"></div>
                            </div>  
				    </div>
					<div class="row" ng-if="edit_field=='meta_description'">
					        <div class="form-group" >
                                        <label for="exampleInputEmail1">Meta Description</label>
                                        <input type="text" class="form-control" ng-model="edit_values.meta_description">
                                        <div class="help-block"></div>
                            </div>  
				    </div>
					<div class="row" ng-if="edit_field=='image'">
						<div class="form-group">
					  <label for="exampleInputEmail1">Image</label>
					 <div class="form-group col-xs-12 show-bn">
					<img src="{{URL::asset('uploads/brand')}}/<% files %>" width="200" height="80" ng-show="files" ng-mouseover="display_cross=1" ng-mouseleave="display_cross=0" >  
					  <br/>
					  <span class="btn btn-primary btn-file" ng-hide="files">
			   Upload <input type="file" onchange="angular.element(this).scope().uploadedFile(this)" >
			  </span>
			<em>Upload a brand image for your store. Image size is(200x80) and not  more than 1 mb.</em>
			<a ng-show="display_cross==1" ng-mouseover="display_cross=1" ng-mouseleave="display_cross=0" class="bnr-del " title="Delete" ng-click="delbrandfiles(files);display_cross=0" href="javascript:void(0);">
						<img src="{{URL::asset('admin/img/del.png')}}">
					</a>
					  
			  <div class="help-block"></div>
					</div>
			  <div class="help-block"></div>
					</div>
				    </div>
			      </div>
			      <div class="modal-footer">
				  <button type="button" class="btn btn-default pull-left" ng-click="init();" data-dismiss="modal">Close</button>			      
				  <button class="btn btn-primary" ng-click="editDes(edit_values)" >Update</button>                         
			      </div>
			    </div>
			  </div>
		  </div>
			    <div class="form-group col-md-2 ">		  
					<button class="btn btn-default" data-toggle="modal" data-target="#screen_opt_modal">Screen Options</button>
				</div>
				
				  <!-- Modal -->
		    <div class="modal fade" id="screen_opt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Screen Options</h4>                            
			      </div>
			      <div class="modal-body">
				  <div class="row" ng-init='screen_opt={"sr_no":true,"slug":false,"brand_name":true,"status":true,"meta_title":false,"meta_description":false,"meta_keywords":false,"image":false}'>  
				  
				    <div class="col-md-4">
				       <div class="form-group">
					    <input type="checkbox" ng-model="screen_opt.brand_name" > Brand Name
				       </div>
				    </div>
					<div class="col-md-4">
				       <div class="form-group">
					    <input type="checkbox" ng-model="screen_opt.slug" > Slug
				       </div>
				    </div>
				     <div class="col-md-4">
				       <div class="form-group">
					    <input type="checkbox"  ng-model="screen_opt.status"> Status
				       </div>
				     </div>
				     
					<div class="col-md-4">
				       <div class="form-group">
					    <input type="checkbox" ng-model="screen_opt.meta_title"> Meta Title
				       </div>
				    </div>
				     <div class="col-md-4">
				       <div class="form-group">
					    <input type="checkbox" ng-model="screen_opt.meta_description"> Meta Description
				       </div>
				    </div>
				     <div class="col-md-4">
				       <div class="form-group">
					    <input type="checkbox"  ng-model="screen_opt.meta_keywords"> Meta Keyword
				       </div>
				     </div>
				     <div class="col-md-4">
				       <div class="form-group">
					    <input type="checkbox"  ng-model="screen_opt.image"> Image Thumbnail
				       </div>
				     </div>
					 
				  </div>
			      </div>
			      <div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>                         
			      </div>
			    </div>
			  </div>
		    </div>
			
                <div class="form-group col-md-3 pull-right">
		  
		  <input type="text" placeholder="Search" class="form-control ng-valid ng-dirty ng-valid-parse ng-touched" ng-model="search">
		</div>
              </div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th  ng-click="sort('id')" style="cursor:pointer">#
                  <span class="glyphicon sort-icon"  ng-show="sortKey=='id'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
                  <th ng-if="screen_opt.brand_name"  ng-click="sort('brand_name')" style="cursor:pointer">Brand Name
                      <span class="glyphicon sort-icon"  ng-show="sortKey=='brand_name'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
				  <th ng-if="screen_opt.slug"  ng-click="sort('slug')" style="cursor:pointer">Slug
                      <span class="glyphicon sort-icon"  ng-show="sortKey=='slug'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
                  <th ng-if="screen_opt.status"  ng-click="sort('status')" style="cursor:pointer"> Status
                   <span class="glyphicon sort-icon"  ng-show="sortKey=='status'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
				     <th ng-if="screen_opt.meta_title"  ng-click="sort('meta_title')" style="cursor:pointer">Meta Title
                   <span class="glyphicon sort-icon"  ng-show="sortKey=='meta_title'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
				     <th ng-if="screen_opt.meta_description"  ng-click="sort('meta_description')" style="cursor:pointer">Meta Description
                   <span class="glyphicon sort-icon"  ng-show="sortKey=='meta_description'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
				     <th ng-if="screen_opt.meta_keywords"  ng-click="sort('meta_keywords')" style="cursor:pointer">Meta Keyword
                   <span class="glyphicon sort-icon"  ng-show="sortKey=='meta_keywords'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
				  
				   <th ng-if="screen_opt.image"  ng-click="sort('image')" style="cursor:pointer"> Image
                   <span class="glyphicon sort-icon"  ng-show="sortKey=='image'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
                  <th> </th>                 
                </tr>
                </thead>
                <tbody>
                
                <tr dir-paginate="val in brands|orderBy:sortKey:reverse|itemsPerPage:10|filter:search">
                  <td><% val.id %></td>
                  <td ng-if="screen_opt.brand_name"  data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('brand_name',val)"><% val.brand_name %></td>
				    <td ng-if="screen_opt.slug"  data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('slug',val)"><% val.slug%></td>
                  <td ng-if="screen_opt.status" data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('status',val)" ><% val.status %></td>
				  <td ng-if="screen_opt.meta_title"  data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('meta_title',val)"><% val.meta_title %></td>
				  <td ng-if="screen_opt.meta_description"  data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('meta_description',val)"><% val.meta_description %></td>
				  <td ng-if="screen_opt.meta_keywords"  data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('meta_keyword',val)"><% val.meta_keyword %></td>
				  <td ng-if="screen_opt.image"  data-toggle="modal" data-target="#edit_modal" style="cursor:pointer"  ng-click="edit_modal('image',val)"> <img src="{{URL::asset('uploads')}}/<% val.image %>" width="200" height="80" ng-show="val.image !=''"  > </td>
                  <td><i ng-click="editbrand(val)" class="fa fa-edit" title="Edit" style="cursor:pointer" ></i> <i title="Delete" class="fa fa-trash" style="cursor:pointer" data-toggle="modal" data-target="#del_modal<% val.id %>"></i>                 
                  <!-- Modal -->
                    <div class="modal fade" id="del_modal<% val.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete</h4>
                          </div>
                          <div class="modal-body">
                            Are you sure you want to delete this brand ? 
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            
                                  {{ csrf_field() }}
                               <input type="hidden" name="del_id" value="<% val.id %>" />
                               <button data-dismiss="modal" ng-click="deleteBrand($index)" class="btn btn-primary" >Delete</button>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                  
                </tr>
                
                </tbody>
                <tfoot>
                 <tr>
                  <th>#</th>
                  <th ng-if="screen_opt.brand_name"  ng-click="sort('brand_name')" style="cursor:pointer">Brand Name
                      <span class="glyphicon sort-icon"  ng-show="sortKey=='brand_name'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
				   <th ng-if="screen_opt.slug"  ng-click="sort('slug')" style="cursor:pointer">Slug
                      <span class="glyphicon sort-icon"  ng-show="sortKey=='slug'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
                  <th ng-if="screen_opt.status"  ng-click="sort('status')" style="cursor:pointer"> Status
                   <span class="glyphicon sort-icon"  ng-show="sortKey=='status'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
				     <th ng-if="screen_opt.meta_title"  ng-click="sort('meta_title')" style="cursor:pointer">Meta Title
                   <span class="glyphicon sort-icon"  ng-show="sortKey=='meta_title'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
				     <th ng-if="screen_opt.meta_description"  ng-click="sort('meta_description')" style="cursor:pointer">Meta Description
                   <span class="glyphicon sort-icon"  ng-show="sortKey=='meta_description'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
				     <th ng-if="screen_opt.meta_keywords"  ng-click="sort('meta_keywords')" style="cursor:pointer">Meta Keyword
                   <span class="glyphicon sort-icon"  ng-show="sortKey=='meta_keywords'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
				   <th ng-if="screen_opt.image"  ng-click="sort('image')" style="cursor:pointer"> Image
                   <span class="glyphicon sort-icon"  ng-show="sortKey=='image'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                  </th>
                  <th> </th>                 
                </tr>
                </tfoot>
              </table>
                <dir-pagination-controls
					max-size="10"
					direction-links="true"
					boundary-links="true" >
		</dir-pagination-controls>
            </div>
            <!-- /.box-body -->
          </div>
        
          <!-- /.box -->
        <!-- Button trigger modal -->

          <!-- Form Element sizes -->
         <div class="box box-primary" ng-if="page=='add'">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-plus"></i> Create Brand</h3>
                <div class="pull-right"> <a href="javascript:void(0);" ng-click="init()" class="btn btn-default">Back</a></div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
              <div class="box-body">
			 
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" id="" name="name" ng-model="brand.brand_name" placeholder="Name" >
		        <div class="help-block"></div>
                </div>
				<div class="form-group" ng-init="brand.edit_slug=0">
                  <label for="exampleInputEmail1">Slug</label>
				  <br/>
				  <span class="float_slug" style="">{{configs_value('Website')}}<span ng-show="brand.edit_slug==0" >brand/<%last_id%></span></span>
                  <i class="fa fa-edit" ng-show="brand.edit_slug==0" ng-click="brand.edit_slug=1" class="float_slug2" style=""></i> 
				  <i class="fa fa-remove" ng-show="brand.edit_slug==1" ng-click="brand.edit_slug=0" class="float_slug1" style=" "></i> 
				  <input type="text" ng-show="brand.edit_slug==1" style="width:50%" class="form-control" id="" name="slug" ng-model="brand.slug"  placeholder="Slug" >
                </div>
				<div style="clear:both"></div>
                 <div class="form-group">
                  <label for="exampleInputEmail1">Meta Title</label>
                  <input type="text" class="form-control" id="" name="meta_title" placeholder="Meta Title" ng-model="brand.meta_title">
		  <div class="help-block"></div>
                </div> 
                 <div class="form-group">
                  <label for="exampleInputEmail1">Meta Description</label>
                  <input type="text" class="form-control" id="" name="meta_description" placeholder="Meta Description" ng-model="brand.meta_description">
		  <div class="help-block"></div>
                </div> 
                <div class="form-group">
                  <label for="exampleInputEmail1">Meta Keyword</label>
                  <input type="text" class="form-control" id="" name="meta_keyword" placeholder="Meta Keyword" ng-model="brand.meta_keyword">
		  <div class="help-block"></div>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <div text-angular ng-model="brand.description" name="description" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>  
		  <div class="help-block"></div>
                </div> 
                <div class="form-group">
                  <label for="exampleInputEmail1">Image</label>

                <div class="form-group col-xs-12 show-bn">
                <img src="{{URL::asset('uploads/brand')}}/<% files %>" width="200" height="80" ng-show="files" ng-mouseover="display_cross=1" ng-mouseleave="display_cross=0" >  
                  <br/>
                  <span class="btn btn-primary btn-file" ng-hide="files">
		   Upload <input type="file" onchange="angular.element(this).scope().uploadedFile(this)" >
		  </span>
		<em>Upload a brand image for your store. Image size is(200x80) and not  more than 1 mb.</em>
		<a ng-show="display_cross==1" ng-mouseover="display_cross=1" ng-mouseleave="display_cross=0" class="bnr-del " title="Delete" ng-click="delbrandfiles(files);display_cross=0" href="javascript:void(0);">
                    <img src="{{URL::asset('admin/img/del.png')}}">
                </a>
				  
		  <div class="help-block"></div>
                </div>
                </div> 
                 
                  <div class="form-group">
                  <label for="exampleInputEmail1">Status </label>
                  <input type="radio"  id="" name="status" ng-model="brand.status" ng-init="brand.status='Active'" value="Active">Active <input type="radio" id="" name="status" value="Inactive" ng-model="brand.status" >Inactive 
		  <div class="help-block"></div>
                </div> 
             </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button ng-click="store(brand)" class="btn btn-primary">Submit</button>
              </div>
           
          </div>

   <!--- Edit Brand------>
         <div class="box box-primary" ng-if="page=='edit'">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-edit"></i> Edit Brand</h3>
                 <div class="pull-right"> <a href="javascript:void(0);" ng-click="init()" class="btn btn-default">Back</a></div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
                 {{ csrf_field() }}
                  <input type="hidden" class="form-control" id="" name="brand_id" ng-model="brand.id" placeholder="Name" value="<% brands.id %>">
              <div class="box-body">
			 
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" id="" name="brand_name" ng-model="brand.brand_name" placeholder="Name" >
		  <div class="help-block"></div>
                </div> 
				<div class="form-group" >
                  <label for="exampleInputEmail1">Slug</label>
				  <br/>
				  <span class="float_slug">{{configs_value('Website')}}<span ng-show="slug.slug_name && edit_slug1=='0'" ><% slug.slug_name %></span>
				  <span ng-show="!slug.slug_name && edit_slug1=='0'" ><% brand.id %></span>
				  </span>
				  <%brand1%>
                  <i class="fa fa-edit" ng-show="edit_slug1=='0'" ng-click="edit_slug1=1" class="float_slug2"></i> 
				  <i class="fa fa-remove" ng-show="edit_slug1==1" ng-click="edit_slug1=0" class=" float_slug1"></i> 
				  <input type="text" ng-show="edit_slug1==1" style="width:50%" class="form-control" id="" name="slug" ng-model="brand.slug"  placeholder="Slug" >
                </div>
				<div style="clear:both"></div>
                   <div class="form-group">
                  <label for="exampleInputEmail1">Meta Title</label>
                  <input type="text" class="form-control" id="" name="meta_title" placeholder="Meta Title" ng-model="brand.meta_title">
		  <div class="help-block"></div>
                </div> 
                 <div class="form-group">
                  <label for="exampleInputEmail1">Meta Description</label>
                  <input type="text" class="form-control" id="" name="meta_description" placeholder="Meta Description" ng-model="brand.meta_description">
		  <div class="help-block"></div>
                </div> 
                <div class="form-group">
                  <label for="exampleInputEmail1">Meta Keyword</label>
                  <input type="text" class="form-control" id="" name="meta_keyword" placeholder="Meta Keyword" ng-model="brand.meta_keyword">
		  <div class="help-block"></div>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <div text-angular ng-model="brand.description" name="description" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>  
		  <div class="help-block"></div>
                </div> 
                <div class="form-group">
                  <label for="exampleInputEmail1">Image</label>
                 <div class="form-group col-xs-12 show-bn">
                <img src="{{URL::asset('uploads/brand')}}/<% files %>" width="200" height="80" ng-show="files" ng-mouseover="display_cross=1" ng-mouseleave="display_cross=0" >  
                  <br/>
                  <span class="btn btn-primary btn-file" ng-hide="files">
		   Upload <input type="file" onchange="angular.element(this).scope().uploadedFile(this)" >
		  </span>
		<em>Upload a brand image for your store. Image size is(200x80) and not more than 1 mb.</em>
		<a ng-show="display_cross==1" ng-mouseover="display_cross=1" ng-mouseleave="display_cross=0" class="bnr-del " title="Delete" ng-click="delbrandfiles(files);display_cross=0" href="javascript:void(0);">
                    <img src="{{URL::asset('admin/img/del.png')}}">
                </a>
				  
		  <div class="help-block"></div>
                </div>
		  <div class="help-block"></div>
                </div> 
                <div class="form-group">
                  <label for="exampleInputEmail1">Status </label>
                  <input type="radio"  id="" name="status" value="Active" ng-model="brand.status" ng-checked="brands.status">Active <input type="radio" id="" name="status" value="Inactive" ng-model="brand.status" ng-checked="brands.status" >Inactive 
		  <div class="help-block"></div>
                </div> 
             </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button ng-click="update(brand)" class="btn btn-primary">Submit</button>
              </div>
          
          </div>

    </section>
   
  <!-- /.content-wrapper -->
  