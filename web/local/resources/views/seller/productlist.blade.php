<section class="content">
    <div class="alert alert-success" ng-if="success_flash">
        <p >
            <% success_flash %>
        </p>
    </div>
    <div class="alert alert-danger"  ng-if="errors">
        <ul>
            <li ng-repeat ="er in errors"><% er %></li>
        </ul>
    </div>
    <div class="col-sm-12 vk-product-search" >
        <div class="row">
            <div class="col-sm-12" >
                <div class="col-sm-12">
                    <h3 class="col-sm-12 text-center">What do you want to list?</h3>
                    <form ng-submit="searchItem()">
                        <div class="input-group">
                            <input type="text" ng-model="searchTag" class="form-control" placeholder="Search by Product Title" /><span class=" input-group-addon btn-primary" ng-click="searchItem()"><i class="fa fa-search"></i></span>
                        </div>
                        <div class="col-sm-12" ng-if="search == ''">
                            <span>Or start selected by <a href="javascript:;" ng-click="getCategory()" >category tree</a></span>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-sm-12" ng-if="search == ''">
                        <ul class="col-sm-12 prenext" ng-if="all_category != ''">
                            <li class="list-groupitem next" style="width:2%;float:left;" ng-click="next()">
                                <div class="listgroup">&nbsp;</div>
                            </li>
                            <li style="width:96%;float:left;">
                                <ul class="list-inline vk-categorylist border" ng-if="all_category != ''" style="right:<% categoryShow %>px;">
                                    <li class="pullleft colsm-3" >
                                        <div class="input-group">
                                            <span class=" input-group-addon btn-primary" ><i class="fa fa-search"></i></span><input type="text" class="vk-catsearch form-control " placeholder="" />
                                        </div>
                                        <ul class="listgroup" data-lavel="1">
                                            <li class="list-groupitem vk-child-cat" ng-click="getProducts(category.id)"  data-id="<% category . id %>" ng-repeat="category in all_category"><% category . category_name %></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="list-groupitem prev" style="width:1%;" ng-click="prev()">
                                <div class="listgroup">&nbsp;</div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" >
                        <ul class="list-inline col-sm-12 text-center">
                            <li class="col-sm-3 well boxmarginpadding" ng-click="getProducts(tag.id)" data-id="<% tag . id %>" ng-repeat="tag in tags"><% tag . category_name %></li>
                        </ul>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12 well" ng-repeat="p in product">
                            <div class="col-sm-3">
                                <img width="100" height="80" ng-if="p.images.length" src="{{ url('/')}}/uploads/<% p . images[$index] . image %>" />
                            </div>
                            <div class="col-sm-7">
                                <div class="col-sm-12">
                                    <h4 ><% p . pro_name %></h4>
                                </div>
                                <div class="col-sm-12" data-prodetail="">

                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary" ng-click="sellThisProduct(p.id)" >Sell This Product</button>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <div class="col-sm-12" ng-if="resell != ''">
        <div class="row">
            <div class="">Offer Details</div>
            <div class="box col-sm-12">
                <div class="col-sm-4">
                    <img class="img-thumbnail img-responsive" ng-if="resell.images.length" src="{{ url('/')}}/uploads/<% resell . images[$index] . image %>" />
                    <% resell . images[$index] . image %>
                </div>
                <div class="col-sm-8">
                    <div class="col-sm-12">Product</div>
                    <div class="" brandoption><% resell . pro_name %></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box col-sm-12">
                <div class="col-sm-6" style="border-right: #ecf0f5 solid;">
                    <div class="col-sm-12">Offer Details</div>    
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label for="">Condition</label>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label>
                                    <input type="radio" ng-model="resell.pro_cond"  name="pro_cond" value="1" checked="" class="" placeholder=""/>New
                                </label>
                                <label>
                                    <input type="radio" ng-model="resell.pro_cond" name="pro_cond" value="2" class="" placeholder=""/>Old
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="product-quantity">Quantity</label>
                        <input type="text" id="product-quantity" ng-model="resell.no_stock" name="no_stock" class="form-control" placeholder=""/>
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" id="price" ng-model="resell.price" name="price" class="form-control" placeholder=""/>
                    </div>
                    <div class="form-group">
                        <label for="product-sku">SKU</label>
                        <input type="text" id="product-sku" ng-model="resell.sku" name="sku" class="form-control" placeholder=""/>
                    </div>
                    <div class="form-group">
                        <label for="offer-note">Offer Note</label>
                        <input type="text" id="offer-note"  name="note" class="form-control" placeholder=""/>
                    </div>
                    <div class="form-group">
                        <label for="handling-time">Handling Time</label>
                        <select  id="handling-time"  name="handlingtime" class="form-control" >
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="handling-time">Courier Handover</label>
                        <label for="handling-time">Item Pick-Up(Collection)</label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="seller-helper-block no-arrow" style="top: 61.9844px;">
                        <div class="seller-helper-title ng-binding">
                            <div class="ng-binding">Seller Helper</div>
                        </div>
                        <div class="seller-helper-description">
                            <div  class="">
                                <p class="ng-binding">The Seller Helper will help guide you with filling in accurate information about your product and your offer during the listing process.</p><p class="ng-binding">Please read the message carefully.</p><p class="ng-binding">Seller Helper will appear vertically alongside the fields as you land on any field.</p><p class="ng-binding">For non-field entries, click on (?) to open the Seller Helper for that specific entry.</p>
                            </div>
                            <p class="ng-binding"></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="pull-right">
                        <button type="button" ng-click="discard()" class="btn btn-lg ">Discard</button>
                        <!--<button type="button" ng-click="setTab(2)" ng-if="tab == 1" class="btn btn-lg btn-primary">Next</button>-->
                        <button type="button" ng-click="store(resell)"  class="btn btn-lg btn-primary">Publish</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>