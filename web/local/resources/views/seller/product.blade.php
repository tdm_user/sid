<section class="content">
    <div class="alert alert-success" ng-if="success_flash">
        <p >
            <% success_flash %>
        </p>
    </div>
    <div class="alert alert-danger"  ng-if="errors">
        <ul>
            <li ng-repeat ="er in errors"><% er %></li>
        </ul>
    </div>
    <div class="col-sm-12" ng-if="tab == ''">
        <div class="panel panel-info">
            <div class="panel-body">
                <h4>Product View</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-8">
                    <div id="linechart" ></div>
                </div>
                <div class="col-md-4">
                    <hc-pie-chart  data="pieData">Placeholder for pie chart</hc-pie-chart>
                </div>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-9">
                        <button class="btn btn-default" ><i class="fa fa-download"></i></button>
                        <button class="btn btn-default" >Resubmit</button>
                        <button class="btn btn-default" >Reactivate</button>
                        <button class="btn btn-default" ng-click="bulkAction('delete', product_id);">Delete</button>
                        <button class="btn btn-default" >Pause</button>
                        <button class="btn btn-default" >Promote</button>
                    </div>
                    <div class="form-group col-md-3 pull-right">		  
                        <input class="btn btn-primary pull-right" type="button" ng-click="add(1)" value="Add Product" />
                    </div>
                </div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><input type="checkbox" ng-model="selectedAll" ng-click="checkAll()" /></th>
                            <th><span class=''>Image</span> </th>
                            <th  ng-click="sort('pro_name')" style="cursor:pointer">Title
                                <span class="glyphicon sort-icon"  ng-show="sortKey == 'pro_name'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>          
                            <th   ng-click="sort('status')" style="cursor:pointer">Status
                                <span class="glyphicon sort-icon"  ng-show="sortKey == 'status'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort('sku')" style="cursor:pointer">SKU
                                <span class="glyphicon sort-icon"  ng-show="sortKey == 'sku'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th  ng-click="sort('price')" style="cursor:pointer">Price
                                <span class="glyphicon sort-icon"  ng-show="sortKey == 'price'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th  ng-click="sort('no_stock')" style="cursor:pointer">Quantity 
                                <span class="glyphicon sort-icon"  ng-show="sortKey == 'no_stock'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>                  
                            <th>
                            </th>
                        </tr>
                    </thead>
                    <tbody>                
                        <tr dir-paginate="val in products|itemsPerPage:10">
                            <td><input type="checkbox" ng-model="product_id[val.id]" ng-change="optionToggled(val.id)" value="<% val . id %>"/></td>
                            <td ><img width="80" height="50" class="img-thumbnail img-responsive" ng-if="val.image.length" src="{{ url('/')}}/uploads/<% val . image[0] . image %>"/></td>
                            <td ><% val . pro_name %></td>
                            <td ><% val . status %></td>
                            <td><% val . sku %></td>
                            <td ><% val . price %></td>
                            <td ><% val . no_stock %></td>
                            <td ><a class="btn btn-default" style="cursor:pointer"  ng-click="editproduct(val)">Edit</a></td>
                        </tr>
                    </tbody>
                </table>
                <dir-pagination-controls max-size="10" direction-links="true" boundary-links="true" ></dir-pagination-controls>
            </div>
        </div>
    </div>
    <div class="box box-primary " ng-if="page == 'index' && tab != ''">
        <div class="box-body">  
            <div class="row" ng-if="page == 'index' && tab == 2">
                <div class="col-md-12 ">
                    <div class="col-sm-6" style="border-right: #ecf0f5 solid;">
                        <div class="form-group">
                            <h4 for="product-Warranty">Product Warranty</h4>
                        </div>
                        <div class="form-group">
                            <label for="product-Warranty">Warranty Period</label>
                            <input type="text" id="product-Warranty" ng-model="product.warranty" name="pro_warranty" class="form-control" placeholder=""/>
                        </div>
                        <div class="form-group">
                            <h4 for="product-Warranty">Offer Detail</h4>
                        </div>
                        <div class="form-group">
                            <label for="product-handle">Handling Time</label>
                            <select type="text" id="product-handle" ng-model="product.pro_handle" name="pro_handle" class="form-control" >
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="product-pro_courier">Courier Handover</label>
                            <div class="input-group">
                                <input type="radio" id="product-pro_courier" ng-model="product.pro_courier" name="pro_courier" class="" />Item pickup
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div>
            <div class="row" ng-if="page == 'index' && tab == 1">
                <div class="col-md-12 ">
                    <div class="col-sm-6" style="border-right: #ecf0f5 solid;">
                        <div class="form-group">
                            <label for="product-title">Product Title</label>
                            <input type="hidden"  ng-model="product.id" name="id" ng-if="product.id != ''" class="form-control" placeholder=""/>
                            <input type="text" id="product-title" ng-model="product.pro_name" name="pro_name" class="form-control" placeholder=""/>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="">Product Condition</label>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <label>
                                        <input type="radio" ng-model="product.pro_cond"  name="pro_cond" value="1" checked="" class="" placeholder=""/>New
                                    </label>
                                    <label>
                                        <input type="radio" ng-model="product.pro_cond" name="pro_cond" value="2" class="" placeholder=""/>Old
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="">This Product Expire on</label>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <label><input type="radio" ng-model="product.pro_exp_on" name="pro_exp_on" value="1" checked="" class="" placeholder=""/>One Week Starting Today</label>
                                    <label><input type="radio" ng-model="product.pro_exp_on" name="pro_exp_on" value="2" class="" placeholder=""/>Two Week Starting Today</label>
                                    <label><input type="radio" ng-model="product.pro_exp_on" name="pro_exp_on" value="3" class="" placeholder=""/>One Month Week Starting Today</label>
                                    <label><input type="radio" ng-model="product.pro_exp_on" name="pro_exp_on" value="4" class="" placeholder=""/>Custom Date Range</label>
                                </div>
                                <div class="input-group col-sm-12">
                                    <ng-datepicker  class="hasDatepicker vk-date" view-format="D/MM/YYYY"  ng-model="product.date_from" first-week-day-sunday="true" placeholder="From date"></ng-datepicker>

                                    <ng-datepicker  class="hasDatepicker vk-date" view-format="D/MM/YYYY"  ng-model="product.date_to" first-week-day-sunday="true" placeholder="To date"></ng-datepicker>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="">Product Specification</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <h4 class="col-sm-5">  Product Category</h4> <h4 class="vk-cat-show col-sm-3" ng-bind="c_cat"></h4><a class="col-sm-4" style="padding: 8px;" ng-click="changeCat()" href="javascript:;" ng-if="c_cat != ''">[Change Category]</a>
                                <div class="frm-cat vk-cat col-sm-12">
                                    <script type="text/ng-template" id="categoryTree1">
                                        <input type="checkbox" ng-click="attribute_gr_add(product.pro_category_id);" ng-model="product.pro_category_id[category.id]" value="<% category . id %>" name="pro_category_id[]" data-cat="<% category . category_name %>" ><% category . category_name %>
                                        <ul ng-if="category.all_category">
                                        <li class="cat-tree" ng-repeat="category in category.all_category | filter:test" ng-include="'categoryTree1'">           
                                        </li>
                                        </ul>
                                    </script>
                                    <ul class="ul-cat ">
                                        <li class="cat-tree" ng-repeat="category in all_category| filter:test" ng-include="'categoryTree1'"></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                        <div class="form-group" ng-repeat=" attr in Selectpro">
                            <label for=""><% attr . option_name %></label>
                            <select ng-model="pro_option[$index].options" ng-init="pro_option[$index].id = attr.id" ng-if="attr.type == 'select'" name="options[<% attr . id %>]" class="form-control">
                                <option></option>
                                <option value="<% sop . id %>" ng-repeat="sop in attr.options"><% sop . option_name %></option>
                            </select>
                            <div class="input-form-group" ng-if="attr.type == 'radio'">
                                <label ng-repeat="sop in attr.options"><input ng-model="pro_option[$index]" name="options[<% attr . id %>]" value="<% sop . id %>" type="radio" /><% sop . option_name %> </label> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Brand</label>
                            <select name="brand" ng-model="product.brand_id" class="form-control">
                                <option></option>
                                <option ng-repeat="v in brand" value="<% v . id %>"><% v . brand_name %></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <h3>Choose Option</h3>
                        </div>
                        <div class="form-group " ng-repeat=" attr in SelectproOption">
                            <label for=""><% attr . option_name %></label>
                            <select chosen data-placeholder="Choose <% attr . option_name %> Option" ng-if="attr.allow == 1" name="options[s][<% attr . id %>]" class="form-control chosen-select" multiple="" ng-model="SelectVariations[$index]" ng-change="createProVariation(SelectVariations)" >
                                <option></option>
                                <option value="<% sop . id %>" ng-repeat="sop in attr.options" ><% sop . option_name %></option>
                            </select>
                            <!--div class="input-form-group" ng-if="attr.type == 'radio' && attr.allow == 1" >
                                <label  ng-repeat="sop in attr.options"><input ng-model="checkboxVariation[attr.id]" ng-change="createProVariation(checkboxVariation)" name="options[c][< attr . id >]" value="< sop . id %>" type="checkbox" />< sop . option_name %> </label> 
                            </div-->
                        </div>

                        <div class="form-group">
                            <h3>Product Description</h3>
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <div text-angular ng-model="product.pro_des" name="pro_des" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
                        </div>
                        <div class="form-group">
                            <label for="">Feature Description</label>
                            <div text-angular ng-model="product.pro_feature_des" name="pro_feature_des" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label>Product Image</label>
                        <div class="col-sm-12 vk-border">
                            <label >Image Guidlines:</label>
                            <ul>
                                <li>Put Your Product in front  of a plain backdrop.</li>
                                <li>Be sure that the entire product is visible in the picture.</li>
                                <li>Product image size should be square and 800x800 pixels.</li>
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-img-chk col-xs-12 show-bn">
                                <div class="imgs_ro" ng-repeat="val in pr_imgs">               
                                    <img src="{{URL::asset('uploads')}}/<% val . img %>"  alt="" ng-mouseleave="tr_dis[$index] = 0" ng-mouseover="tr_dis[$index] = 1"/>
                                    <a class="ms_ov" href="javascript:void(0);" ng-if="$index != values.length - 1"  ng-mouseover="tr_dis[$index] = 1" ng-show="tr_dis[$index] == 1" >
                                        <i class="fa fa-check" title="Set default" ng-show="val.def == 0"  ng-click="setdefault($index)"></i>
                                        <i class="fa fa-check grr_ic"  title="Unset default"  ng-show="val.def == 1"  ng-click="unsetdefault($index)"></i>
                                        <i class="fa fa-trash" title="Delete"  ng-click="removeimgs(val.img, $index)" ></i>
                                    </a>

                                </div>

                                <span ng-hide="product.image" class="btn btn-primary btn-file">
                                    Upload Product Image<input style="left: 0px;" type="file" onchange="angular.element(this).scope().uploadedFile(this)" />
                                </span>

                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <h3>Meta Title & Description</h3>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Meta Title</label>
                            <input type="text" class="form-control" id="" name="meta_title" maxlength="60" placeholder="Meta Title" ng-model="product.meta_title">
                            <div class="help-block"></div>
                            <span>Characters left: <% 60 - product . meta_title . length %></span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Meta Description</label>
                            <input type="text" class="form-control" id="" name="meta_description" maxlength="150" placeholder="Meta Description" ng-model="product.meta_description">
                            <div class="help-block"></div>
                            <span>Characters left: <% 150 - product . meta_description . length %></span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Meta Keywords</label>
                            <input type="text" class="form-control" id="" name="meta_keywords" placeholder="Meta Keywords" ng-model="product.meta_keywords">
                            <div class="help-block"></div>
                            <span>Use 1 to max 10 keywords divided by comma Used</span>:<span ><% product . meta_keywords ? product . meta_keywords . split(',') . length : 0 %> keywords</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Product Option</div>
                        <div class="panel-body table-responsive">
                            <table class="table-striped table table-bordered table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Variables</th>
                                        <th>Price</th>
                                        <th>Discounted Price</th>
                                        <th>Quantity</th>
                                        <th>SKU</th>
                                        <th>Weight(Kg)</th>
                                        <th>Dimensions(cm)</th>
                                        <th>Purchase Note</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="variant in variations">
                                        <td><span type="button" ng-click="delVariant($index)" class="btn btn-danger"><i class="fa fa-trash"></i></span></td>
                                        <td><span ng-repeat="o in variant"><% o . option_name %>&nbsp;<input type="hidden" ng-model="pro_variant[$parent.$index].id[$index]" ng-init="pro_variant[$parent.$index].id[$index] = o.id" value="<% o . id %>" /></span></td>
                                        <td><input ng-model="pro_variant[$index].vari_price" /></td>
                                        <td><input ng-model="pro_variant[$index].vari_sale_price" /></td>
                                        <td><input ng-model="pro_variant[$index].vari_stock" /></td>
                                        <td><input ng-model="pro_variant[$index].vari_sku" /></td>
                                        <td><input ng-model="pro_variant[$index].weight" /></td>
                                        <td >
                                            <input placeholder="Length" ng-model="pro_variant[$index].dimension_len" />
                                            <input placeholder="Width" ng-model="pro_variant[$index].dimension_width" />
                                            <input placeholder="Height" ng-model="pro_variant[$index].dimension_height" />
                                        </td>
                                        <td><input ng-model="pro_variant[$index].note" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="pull-right">
                    <button type="button" ng-click="discard()" class="btn btn-lg ">Discard</button>
                    <button type="button" ng-click="setTab(2)" ng-if="tab == 1" class="btn btn-lg btn-primary">Next</button>
                    <button type="button" ng-click="store(product)" ng-if="tab == 2" class="btn btn-lg btn-primary">Publish</button>
                </div>
            </div>
        </div>
    </div>
</section>