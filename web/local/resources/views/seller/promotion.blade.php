<section class="content">
    <div class="alert alert-success" ng-if="success_flash">
        <p >
            <% success_flash %>
        </p>
    </div>
    <div class="alert alert-danger"  ng-if="errors">
        <ul>
            <li ng-repeat ="er in errors"><% er %></li>
        </ul>
    </div>
    <div class="col-sm-12">
        <div class="bread">
            <a href="javascript:void(0)" ng-class="{active:wizard == 1}" ng-click="step_wizard(1)"><button type="button" class="btn btn-default btn-circle">1</button><span> Plan and Package</span></a>
            <a href="javascript:void(0)" ng-class="{active:wizard == 2}" ng-if='create_promo.id'   ng-click="step_wizard(2)">
                <button type="button" class="btn btn-default btn-circle">2</button><span> Choose Products</span>
            </a>
            <a href="javascript:void(0)" ng-class="{active:wizard == 2}" ng-if='!create_promo.id'   >
                <button type="button" class="btn btn-default btn-circle">2</button><span> Choose Products</span>
            </a>
            <a href="javascript:void(0)" ng-class="{active:wizard == 3}" ng-if='create_promo.add_discrip' ng-click="step_wizard(3);preview(create_promo);">
                <button type="button" class="btn btn-default btn-circle">3</button><span> Review and Payment</span>
            </a>
            <a href="javascript:void(0)" ng-class="{active:wizard == 3}" ng-if='!create_promo.add_discrip'>
                <button type="button" class="btn btn-default btn-circle">3</button><span> Review</span>
            </a>
        </div>
    </div>
    <div class="box box-primary crt_prom step1"  ng-if="wizard == 1">

        <!-- /.box-header -->
        <div class="form-group">
            <label for="exampleInputEmail1" class="col-md-4 col-sm-4">Select or Create Campaign </label>
            <div class="col-md-8 col-sm-8">
                <select class="form-control "  ng-change="selcrt_camp(create_promo.campain)" ng-model="create_promo.campain">
                    <option value="">Please select</option>
                    <option value="create_new">Create Campaign</option>
                    <option value="update_campn">Update Campaign</option> 
                    <option value="copy_campn">Copy Campaign</option>
                </select> 

                <select ng-show="updcampshow == true" class="form-control camp"  ng-change="update_camp(create_promo.upd_camp, create_promo.actn)" ng-model="create_promo.upd_camp">
                    <option value="">Please select</option>
                    <option ng-repeat="campd in campdata" value="<% campd . id %>"><% campd . compaign_name %></option>
                </select>
                <select ng-show="copycampshow == true" class="form-control camp"  ng-change="update_camp(create_promo.cpy_camp, create_promo.actn)" ng-model="create_promo.cpy_camp">
                    <option value="">Please select</option>
                    <option ng-repeat="cpy_campd in copy_campdata" value="<% cpy_campd . id %>"><% cpy_campd . compaign_name %></option>
                </select>
                <div class="camp" ng-show="campinputshow == true">
                    <input  class="form-control" type="text" ng-model="create_promo.newcamp" placeholder="Please enter campaign name"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1" class="col-md-4 col-sm-4">Ad Type </label>
            <div class="col-md-8 col-sm-8">
                <select ng-if="disablesel == false" class="form-control" ng-model="create_promo.ad_type" ng-change="change_Ad(create_promo.ad_type)">
                    <option value="">Select Ad Type</option>
                    <option value="text_ad">Ad Text</option>
                    <option value="banner_ad">Ad Banner</option>
                </select>  

                <input ng-if="disablesel == true" class="form-control" type="text" ng-model="create_promo.ad_type" disabled/>

            </div>     
        </div>
        <div class="form-group" ng-if="create_promo.ad_type == 'banner_ad' || create_promo.ad_type == 'Banner Ad'">
            <label for="exampleInputEmail1" class="col-md-4 col-sm-4">Select Ad Placement </label>
            <div class="col-md-8 col-sm-8">
                <select ng-disabled="disablesel == true"  class="form-control" ng-model="create_promo.ad_placement" ng-change="change_placement(create_promo.ad_placement)">
                    <option value="">Select Ad Placement</option>
                    <option value="home_banner">Home Page- Top/Side/Bottom Banner</option>
                    <option value="cat_banner">Category Page- Top/Side/Bottom Banner</option>
                    <option value="pro_banner">Product Page- Top/Side/Bottom Banner</option>                      
                </select> 

            </div>     
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1" class="col-md-4 col-sm-4">Select views </label>
            <div class="col-md-8 col-sm-8">                    
                <select class="form-control" ng-model="create_promo.select_view">
                    <option value=""> Select view</option>
                    <option value="unlimited" > Unlimited</option>
                    <option  ng-repeat="select_view in promot_rec.create_package" value="<% select_view . id %>" ><% select_view . nview %> views - Rs <% select_view . price %></option>
                </select>    
            </div>    
        </div>
        <div class="form-group" ng-if="create_promo.select_view != 'unlimited'">

            <label for="exampleInputEmail1" class="col-md-4 col-sm-4">Schedule </label>
            <div class="col-md-8 col-sm-8" >  
                <div class="form-check" ng-repeat="shedules in promot_rec.schedule_status">
                    <label class="form-check-label" >
                        <input class="form-check-input" name="shedul" ng-value="shedules.id"   ng-model="create_promo.schedule" type="radio" id="inlineCheckbox1"  ng-disabled="!create_promo.newcamp" > <% shedules . field_value %>
                    </label>
                </div>

                <div class="form-group" ng-show="create_promo.schedule == 9">
                    <div class="col-sm-4">
                        <label  class="col-md-12 col-sm-12" ng-init="create_promo.start_date = CurrentDate">Starting Date </label>
                        <ng-datepicker  class="hasDatepicker" view-format="Do MMMM YYYY" ng-model="create_promo.start_date" first-week-day-sunday="true" placeholder="Pick a date">
                        </ng-datepicker>
                    </div>
                    <div class="col-sm-4">
                        <label  class="col-md-12 col-sm-12" ng-init="create_promo.end_date = CurrentDate">End Date </label>
                        <ng-datepicker  class="hasDatepicker" view-format="Do MMMM YYYY" ng-model="create_promo.end_date" first-week-day-sunday="true"  placeholder="Pick a date">
                        </ng-datepicker>
                    </div>    
                </div>

            </div>    
        </div>

        <div class="form-group butn">
            <!--    <button type="button" class="btn btn-default" ng-click="discard()">Discard</button>-->
            <button type="button" class="btn btn-primary" ng-click="saveAdtext(create_promo)">Save Draft</button>
            <button type="button" class="btn btn-success" ng-disabled="!create_promo.id" ng-click="step_wizard(2)">Next</button>

        </div>    

    </div>
    <!----------------------Step 2------------------->    

    <div class="box box-primary crt_prom step2" ng-if="wizard == 2">
        <!-- /.box-header -->

        <div class="form-group">
            <label for="exampleInputEmail1" class="col-md-4 col-sm-4">Select or Create Promotion </label>
            <div class="col-md-8 col-sm-8">
                <select class="form-control "  ng-change="selcrt_prom(create_promo.promot)" ng-model="create_promo.promot">
                    <option value="">Please select</option>
                    <option value="create_new">Create New</option> 
                    <option value="update_promn">Update Promotion</option>
                    <option value="copy_promo">Copy Promotion</option>
                </select> 

                <select ng-show="updpromshow == true" class="form-control camp"  ng-change="select_promo_data(create_promo.upd_promot, create_promo.promact, create_promo.ad_type)" ng-model="create_promo.upd_promot">
                    <option value="">Please select</option>
                    <option ng-repeat="promotnd in promotndata" value="<% promotnd . id %>"><% promotnd . promotion_name %></option>
                </select> 
                <select ng-show="copypromshow == true" class="form-control camp"  ng-change="select_promo_data(create_promo.cpy_promot, create_promo.promact, create_promo.ad_type)" ng-model="create_promo.cpy_promot">
                    <option value="">Please select</option>
                    <option ng-repeat="promotnd in cpy_promotn_data" value="<% promotnd . id %>"><% promotnd . promotion_name %></option>
                </select>
                <div class="camp" ng-show="prom_input_show == true">
                    <input  class="form-control" type="text" ng-model="create_promo.newpromot" placeholder="Please enter promotion name"/>
                </div>
            </div>
        </div>
        <!---<% product_name %>--->
        <div ng-if="create_promo.ad_type == 'text_ad' || create_promo.ad_type == 'Text Ad'">
            <div class="form-group">
                <label for="exampleInputEmail1" class="col-md-12 col-sm-12">Product to Promote </label>
                <label for="exampleInputEmail1" class="col-md-4 col-sm-4">Select your Product 
                    <!-- <a href="#" data-toggle="tooltip" data-placement="right" title="SID">?</a> -->
                </label>
                <div class="col-md-8 col-sm-8">

                    <select class="form-control" ng-model="create_promo.product">
                        <option  value=""> Select Product </option>
                        <option ng-repeat=" product in promot_rec.product_name" ng-selected="product.id == create_promo.product" ng-value="product.id"> <% product . pro_name %> </option>
                    </select>   
                </div>     
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1" class="col-md-4 col-sm-4">Destination category </label>
                <div class="col-md-8 col-sm-8">  
                    <label  class="col-md-12 col-sm-12">Text Ad also displays in home page by default </label> 
                </div>    
            </div>
            <!-- <% category_name %>-->
            <div class="form-group">
                <label for="exampleInputEmail1" class="col-md-4 col-sm-4">Select Category
                    <a href="#" data-toggle="tooltip" data-placement="right" title="SID">?</a> 
                </label>
                <div class="col-md-8 col-sm-8"> 
                    <div class="col-sm-12">

                        <select multiple class="form-control" ng-model="create_promo.category">

                            <option ng-repeat=" category in  promot_rec.category_name" value="<% category . id %>"><% category . category_name %></option>
                        </select>                             


                    </div>
                </div>    
            </div>
            <div class="form-group">
                <label  class="col-md-4 col-sm-4">Add Content 
                    <a href="#" data-toggle="tooltip" data-placement="right" title="SID">?</a>
                </label>
                <div class="col-md-8 col-sm-8"> 
                    <div class="col-sm-12">                        
                        <label  class="col-md-12 col-sm-12">Add Content </label>
                        <input class="form-control" type="text" ng-model="create_promo.add_content"  ng-trim="false" maxlength="100" />
                        <label  class="col-md-12 col-sm-12"> <% 100 - create_promo . add_content . length %> character remaining </label>
                    </div>  
                    <div class="col-sm-12">                        
                        <label  class="col-md-12 col-sm-12">Description </label>
                        <input class="form-control" type="text" ng-model="create_promo.add_discrip"  ng-trim="false" maxlength="100"/> 
                        <label  class="col-md-12 col-sm-12"> <% 100 - create_promo . add_discrip . length %> character remaining </label>
                    </div> 
                </div>    
            </div>
        </div>
        <div  ng-if="create_promo.ad_type == 'banner_ad' || create_promo.ad_type == 'Banner Ad'">			  
            <div class="form-group">
                <label for="exampleInputEmail1" class="col-md-12 col-sm-12"></label>
                <label for="exampleInputEmail1" class="col-md-12 col-sm-12"> Your Store/Category/Product to Promote </label>
                <label for="exampleInputEmail1" class="col-md-4 col-sm-4">Select Store/Category/Product 
                    <!-- <a href="#" data-toggle="tooltip" data-placement="right" title="SID">?</a>--> 
                </label>
                <div class="col-md-8 col-sm-8" >                      
                    <select class="form-control" ng-model="create_promo.catprosto" ng-change="change_catpro(create_promo.catprosto)">
                        <option  value="">Please Select</option>
                        <option value="category">Category</option>
                        <option value="product">Product </option>
                        <option value="store">Store</option>
                    </select>   
                    <select class="form-control" ng-model="create_promo.val_cps" >
                        <option  value="">Please Select</option>
                        <option ng-repeat="val in val_cps" value="<% val . id %> "><% val . name %></option>
                    </select>   
                </div>     
            </div>
            <div class="form-group" >

                <label for="exampleInputEmail1" class="col-md-4 col-sm-4" ng-if="create_promo.ad_placement == 'home_banner'"> Upload Banners For Home Page</label>   
                <label for="exampleInputEmail1" class="col-md-4 col-sm-4" ng-if="create_promo.ad_placement == 'cat_banner'"> Upload Banners For Category Page</label>   
                <label for="exampleInputEmail1" class="col-md-4 col-sm-4" ng-if="create_promo.ad_placement == 'pro_banner'"> Upload Banners For Product Page</label>                 
                <div class="col-md-8 col-sm-8"> 
                    <div>
                        <input type="checkbox"  ng-model="create_promo.ad_place.top_banner" ng-click="create_promo.banner_img.top_banner = false" ng-true-value="true" ng-false-value="false"/> Top Banner	
                        <br />

                        <div class="form-group col-xs-12 show-bn" ng-show="create_promo.ad_place.top_banner == true">

                            <div >	
                                <img ng-mouseleave="display_cross = 0" ng-mouseover="display_cross = 1" ng-show="create_promo.banner_img.top_banner" src="{{URL::asset('uploads/')}}/<% create_promo . banner_img . top_banner %>" height="300" width="465"> 
                                <div class="pro_ban" ng-if="img_prog_top != 0 && img_prog_top != 100">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: <% img_prog_top %>%" aria-valuenow=" <% img_prog_top %>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div> 
                                </div>
                                <span ng-hide="store_data.banner" class="btn btn-primary btn-file">
                                    Upload <input type="file" ng-model="store_data.store_banner.top_banner" onchange="angular.element(this).scope().uploadedBannerFile(this, 'top_banner')">
                                </span>
                                <em>Upload a banner for your store.Banner Should be in proper size (<% create_promo . ban_size . top[0] %>) and  (<% create_promo . ban_size . top[1] %> MB)  </em>
                                <a href="javascript:void(0);" ng-click="delBanner(create_promo.banner_img.top_banner, 'top_banner');display_cross = 0" title="Delete" class="bnr-del" ng-mouseleave="display_cross = 0" ng-mouseover="display_cross = 1" ng-show="display_cross == 1"><img src="{{URL::asset('admin/img/del.png')}}"></a>

                                <div class="help-block"></div>
                            </div>
                        </div>

                    </div>  
                    <div>
                        <input type="checkbox"  ng-model="create_promo.ad_place.side_banner"  ng-click="create_promo.banner_img.side_banner = false" ng-true-value="true" ng-false-value="false"/> Side Banner
                        <div class="form-group col-xs-12 show-bn"  ng-show="create_promo.ad_place.side_banner == true">

                            <div >	
                                <img ng-mouseleave="display_cross1 = 0" ng-mouseover="display_cross1 = 1" ng-show="create_promo.banner_img.side_banner" src="{{URL::asset('uploads/')}}/<% create_promo . banner_img . side_banner %>" height="300" width="465">  
                                <div class="pro_ban" ng-if="img_prog_side != 0 && img_prog_side != 100">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: <% img_prog_side %>%" aria-valuenow=" <% img_prog_side %>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div> 
                                </div>
                                <span ng-hide="store_data.banner" class="btn btn-primary btn-file">
                                    Upload <input type="file" ng-model="store_data.store_banner.side_banner" onchange="angular.element(this).scope().uploadedBannerFile(this, 'side_banner')">
                                </span>
                                <em>Upload a banner for your store.Banner Should be in proper size (<% create_promo . ban_size . side[0] %>) and  (<% create_promo . ban_size . side[1] %> MB)</em>
                                <a href="javascript:void(0);" ng-click="delBanner(create_promo.banner_img.home_page_right, 'side_banner');display_cross = 0" title="Delete" class="bnr-del" ng-mouseleave="display_cross1 = 0" ng-mouseover="display_cross1 = 1" ng-show="display_cross1 == 1"><img src="{{URL::asset('admin/img/del.png')}}"></a>

                                <div class="help-block"></div>
                            </div>
                        </div>			         

                    </div>  
                    <div>
                        <input type="checkbox"  ng-model="create_promo.ad_place.bottom_banner"  ng-click="create_promo.banner_img.bottom_banner = false"  ng-true-value="true" ng-false-value="false"/> Bottom Banner 
                        <div class="form-group col-xs-12 show-bn"  ng-show="create_promo.ad_place.bottom_banner == true">

                            <div >	
                                <img ng-mouseleave="display_cross2 = 0" ng-mouseover="display_cross2 = 1" ng-show="create_promo.banner_img.bottom_banner" src="{{URL::asset('uploads/')}}/<% create_promo . banner_img . bottom_banner %>" height="300" width="465">
                                <div class="pro_ban" ng-if="img_prog_bottom != 0 && img_prog_bottom != 100">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: <% img_prog_bottom %>%" aria-valuenow=" <% img_prog_bottom %>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div> 
                                </div>

                                <span ng-hide="store_data.banner" class="btn btn-primary btn-file">
                                    Upload <input type="file" ng-model="store_data.store_banner.bottom_banner" onchange="angular.element(this).scope().uploadedBannerFile(this, 'bottom_banner')">
                                </span>
                                <em>Upload a banner for your store.Banner Should be in proper size (<% create_promo . ban_size . bottom[0] %>) and  (<% create_promo . ban_size . bottom[1] %> MB)</em>
                                <a href="javascript:void(0);" ng-click="delBanner(create_promo.banner_img.bottom_banner, 'bottom_banner');display_cross = 0" title="Delete" class="bnr-del" ng-mouseleave="display_cross2 = 0" ng-mouseover="display_cross2 = 1" ng-show="display_cross2 == 1"><img src="{{URL::asset('admin/img/del.png')}}"></a>

                                <div class="help-block"></div>
                            </div>
                        </div>		         


                    </div> 

                </div>     
            </div>

        </div>
        <div class="form-group butn">
            <button type="button" class="btn btn-default" ng-click="step_wizard(1)">Back</button> 
            <button type="button" class="btn btn-primary" ng-click="save_promotion(create_promo)" >Save Draft</button>                  
            <button type="button" class="btn btn-success" ng-disabled="btnenble == false" ng-click="step_wizard(3);preview(create_promo.id)">Next</button>
        </div>  
    </div>
    <!----------------------------------------Step 3----------------------------------->
    <div class="box box-primary crt_prom step3" ng-if="wizard == 3">
        <!-- /.box-header -->
        <div class="col-sm-6">
            <h2>Review</h2>
            <div ng-if="create_promo.ad_type == 'text_ad' || create_promo.ad_type == 'Text Ad'">			  
                <div class="form-group">
                    <label for="exampleInputEmail1" class="col-md-12 col-sm-12"> Product to Promote </label>
                    <div class="col-md-12 col-sm-12">                
                        <div class="camp" >
                            <input  class="form-control" type="text" disabled value="<% previw_data . product['pro_name'] %>"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1" class="col-md-12 col-sm-12"> Destination Category </label>

                    <div class="col-md-12 col-sm-12">                      
                        <ul>
                            <li ng-repeat="cat in previw_data.category"><% cat . category_name %></li>
                        </ul>  
                    </div>     
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1" class="col-md-12 col-sm-12">Content </label>
                    <div class="col-md-12 col-sm-12">  
                        <% previw_data . prviw_data . adcontent_title %>
                    </div>    
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1" class="col-md-12 col-sm-12">Description               
                    </label>
                    <div class="col-md-12 col-sm-12"> 
                        <% previw_data . prviw_data . adcontent_discrip %>
                    </div>    
                </div>
            </div>
            <div ng-if="create_promo.ad_type == 'banner_ad' || create_promo.ad_type == 'Banner Ad'">			  
                <div class="form-group">
                    <label for="exampleInputEmail1" class="col-md-12 col-sm-12" ng-if="create_promo.ad_placement == 'home_banner'">Home Page- Top/Side/Bottom Banner</label>
                    <label for="exampleInputEmail1" class="col-md-12 col-sm-12" ng-if="create_promo.ad_placement == 'cat_banner'">Category Page- Top/Side/Bottom Banner</label>
                    <label for="exampleInputEmail1" class="col-md-12 col-sm-12" ng-if="create_promo.ad_placement == 'pro_banner'">Product Page- Top/Side/Bottom Banner</label>
                    <div class="col-md-12 col-sm-12">                
                        <div class="camp" >

                            <label  style="text-transform:uppercase"> <% previw_data . prviw_data . type %> : </label>
                            <label > <% previw_data . catpro . name %></label>
                        </div>
                    </div>
                    <label for="exampleInputEmail1" class="col-md-12 col-sm-12"> Banners </label>
                    <div class="col-md-12 col-sm-12" ng-if=" previw_data.prviw_data.top != ''">                
                        <div class="camp" >
                            <label class="type_pro"> Top Banner: </label>
                            <img src="{{URL::asset('uploads/')}}/<% previw_data . prviw_data . top %>"  width="700"/>

                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12" ng-if=" previw_data.prviw_data.side != ''"  >                
                        <div class="camp" >
                            <label class="type_pro"> Side Banner: </label>
                            <img src="{{URL::asset('uploads/')}}/<% previw_data . prviw_data . side %>"  width="700"/>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12" ng-if=" previw_data.prviw_data.bottom != ''">                
                        <div class="camp" >
                            <label class="type_pro"> Bottom Banner: </label>
                            <img src="{{URL::asset('uploads/')}}/<% previw_data . prviw_data . bottom %>"width="700" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group butn">
                <button type="button" class="btn btn-default" ng-click="step_wizard(2)">Back</button> 
            </div>  
        </div>
        <div class="col-sm-6">
            <h2>Payment</h2>
            <form id="payment-form" method="post" ng-submit="payments()">
            <div class="payment-errors"></div>
                <div id="accordion" class="well col-sm-12">
                    <h3><input type="radio" name="payment_type" ng-model="promotionPay.payment_type" value="stripe"/>Credit Card</h3>
                    <div class="vk-accordion">
                        <div class="form-group">
                            <label>Cardholder's Name</label>
                            <input type="text" class="form-control" ng-model="promotionPay.holder_name"  />
                        </div>
                        <div class="form-group">
                            <label>Card Number</label>
                            <input type="text" class="form-control"  maxlength="16" data-stripe="number" ng-model="promotionPay.stripeNum"/>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 vk-mp-0">
                                <label>Expiry Date</label>
                                <div class="vk-mp-0">
                                    <div class="col-sm-6 vk-mp-0">
                                        <input placeholder="mm" type="text" ng-model="promotionPay.stripeMM" data-stripe="exp_month" class="form-control" maxlength="2" />
                                    </div>
                                    <div class="col-sm-6 vk-mp-0">
                                        <input placeholder="yy" type="text" ng-model="promotionPay.stripeYY" data-stripe="exp_year" class="form-control" maxlength="2" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Security Code</label>
                                <div class="vk-mp-0">
                                    <div class="col-sm-6">
                                        <input placeholder="CVV" size="4" ng-model="promotionPay.stripeCVC" data-stripe="cvc" type="text" class="form-control" maxlength="4" />
                                    </div>
                                    <div class="col-sm-6">
                                        <i class="fa fa-credit-card-alt "></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <label><input type="checkbox" ng-model="promotionPay.stripeSave" /> Save for future use</label>
                    </div>
                    <h3><input type="radio" name="payment_type" value="paypal"/>PayPal</h3>
                    <div class="vk-accordion hidden">
                        <div class="form-group">
                            <label>Cardholder's Name</label>
                            <input type="text" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label>Card Number</label>
                            <input type="text" class="form-control"  maxlength="16"/>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 vk-mp-0" >
                                <label>Expiry Date</label>
                                <div class="vk-mp-0">
                                    <div class="col-sm-6 vk-mp-0">
                                        <input placeholder="mm" type="text" class="form-control" maxlength="2" />
                                    </div>
                                    <div class="col-sm-6 vk-mp-0">
                                        <input placeholder="yyyy" type="text" class="form-control" maxlength="4" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Security Code</label>
                                <div class="vk-mp-0">
                                    <div class="col-sm-6">
                                        <input placeholder="CVV" type="text" class="form-control" maxlength="3" />
                                    </div>
                                    <div class="col-sm-6">
                                        <i class="fa fa-credit-card-alt "></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="form-control" id="submitBtn" value="Pay Now" />
                </div>
            </form>
        </div>
    </div>  
</section>