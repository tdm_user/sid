<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class FeaturedCategory extends Model
{
	protected $table = 'featured_category';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id','subcategory_id','position','sort','status'
    ];

   
}
