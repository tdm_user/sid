<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order_detail';
    const ORDER_STATUS_PENDING=1;
    const ORDER_STATUS_ONHOLD=2;
    const ORDER_STATUS_COMPLETED=3;
    const ORDER_STATUS_CANCELED=4;
    const ORDER_STATUS_RETURNED=5;
    
    const PAYMENT_STATUS_PENDING=1;
    const PAYMENT_STATUS_COMPLETED=2;
    const PAYMENT_STATUS_CANCELED=3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_note','payment_method','delivery_method','shipping_address','status','payment_status','payment_address','tracking','affilate',
        'total','invoice_no','user_id',
    ];
   
    static function orderStatusOption(){
        return array(self::ORDER_STATUS_PENDING=>'Pending',  self::ORDER_STATUS_ONHOLD=>'Hold',  self::ORDER_STATUS_COMPLETED=>'Completed',
        self::ORDER_STATUS_CANCELED=>'Canceled',  self::ORDER_STATUS_RETURNED=>'Returned');
    }
    static function paymentStatusOption(){
        return array(self::PAYMENT_STATUS_PENDING=>'Pending', self::PAYMENT_STATUS_COMPLETED=>'Completed',
        self::PAYMENT_STATUS_CANCELED=>'Canceled');
    }
}
?>
