<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ProductView extends Model
{
	protected $table = 'product_views';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'product_id','user_id','no_view'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   
}
