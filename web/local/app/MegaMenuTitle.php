<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class MegaMenuTitle extends Model
{
	protected $table = 'mega_menu_title';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'navigation_id','title','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   
}
