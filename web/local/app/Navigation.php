<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Navigation extends Model
{
	protected $table = 'navigation';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'menu_name', 'menu_image','url','sort','position','table_id','table_name','status','parent_id','mega_menu','mega_menu_id'
    ];

   
}
