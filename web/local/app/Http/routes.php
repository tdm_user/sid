<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
// Authentication routes...
//Route::get('auth/login', 'Auth\AuthController@getLogin');
//Route::post('auth/login', 'Auth\AuthController@postLogin');
//Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
//Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');

//Route::resource('api/admins','HomeController');

//Admin Panel
Route::get('admins/', 'Admin\HomeController@index');
Route::get('admins', 'Admin\HomeController@index');
Route::get('admins/login', 'Admin\HomeController@index');
Route::post('admins/log_user', 'Admin\HomeController@log_user');
Route::get('admins/log_out', 'Admin\HomeController@log_out');
Route::get('admins/dashboard', 'Admin\HomeController@dashboard');
Route::get('admins/home', 'Admin\HomeController@home');
Route::post('admins/imageupload', 'Admin\HomeController@imageupload');
Route::post('admins/Allimageupload', 'Admin\HomeController@Allimageupload');
Route::post('admins/imagemutipleupload', 'Admin\HomeController@imagemutipleupload');
Route::get('admins/not_access', 'Admin\HomeController@not_access');

Route::get('admins/category', 'Admin\CategoryController@index');
Route::get('admins/category/all', 'Admin\CategoryController@all');
Route::post('admins/category/store', 'Admin\CategoryController@store');
Route::post('admins/category/delete', 'Admin\CategoryController@delete');
Route::get('admins/category/edit/{id}', 'Admin\CategoryController@edit');
Route::post('admins/category/update', 'Admin\CategoryController@update');
Route::get('admins/category/getcataegorywithSub', 'Admin\CategoryController@getcataegorywithSub');
Route::post('admins/category/changeStatus', 'Admin\CategoryController@changeStatus');

Route::get('admins/brand', 'Admin\BrandController@index');
Route::get('admins/brand/all', 'Admin\BrandController@all');
Route::post('admins/brand/store', 'Admin\BrandController@store');
Route::post('admins/brand/delete', 'Admin\BrandController@delete');
Route::get('admins/brand/edit/{id}', 'Admin\BrandController@edit');
Route::post('admins/brand/update', 'Admin\BrandController@update');

Route::get('admins/faq', 'Admin\FaqController@index');
Route::get('admins/faq/all', 'Admin\FaqController@all');
Route::post('admins/faq/store', 'Admin\FaqController@store');
Route::post('admins/faq/delete', 'Admin\FaqController@delete');
Route::get('admins/faq/edit/{id}', 'Admin\FaqController@edit');
Route::post('admins/faq/update', 'Admin\FaqController@update');

Route::get('admins/newsletter', 'Admin\NewsletterController@index');
Route::get('admins/newsletter/all', 'Admin\NewsletterController@all');
Route::get('admins/newsletter/export', 'Admin\NewsletterController@export');
Route::post('admins/newsletter/store', 'Admin\NewsletterController@store');
Route::post('admins/newsletter/delete', 'Admin\NewsletterController@delete');
Route::post('admins/newsletter/update', 'Admin\NewsletterController@update');
Route::post('admins/newsletter/update_subscribe', 'Admin\NewsletterController@update_subscribe');

Route::get('admins/template', 'Admin\TemplateController@index');
Route::get('admins/template/all', 'Admin\TemplateController@all');
Route::post('admins/template/store', 'Admin\TemplateController@store');
Route::post('admins/template/delete', 'Admin\TemplateController@delete');
Route::get('admins/template/edit/{id}', 'Admin\TemplateController@edit');
Route::post('admins/template/update', 'Admin\TemplateController@update');
Route::post('admins/template/sent', 'Admin\TemplateController@sent');

Route::get('admins/config', 'Admin\ConfigController@index');
Route::get('admins/config/all', 'Admin\ConfigController@all');
Route::get('admins/config/edit', 'Admin\ConfigController@edit');
Route::post('admins/config/update', 'Admin\ConfigController@update');

Route::get('admins/enquiry', 'Admin\EnquiryController@index');
Route::get('admins/enquiry/all', 'Admin\EnquiryController@all');
Route::get('admins/enquiry/edit/{id}', 'Admin\EnquiryController@edit');
Route::post('admins/enquiry/update', 'Admin\EnquiryController@update');
Route::post('admins/enquiry/delete', 'Admin\EnquiryController@delete');

Route::get('admins/user', 'Admin\UserController@index');
Route::get('admins/user/all', 'Admin\UserController@all');
Route::post('admins/user/all', 'Admin\UserController@all');
Route::get('admins/user/add', 'Admin\UserController@add');
Route::post('admins/user/store', 'Admin\UserController@store');
Route::get('admins/user/edit/{id}', 'Admin\UserController@edit');
Route::post('admins/user/update', 'Admin\UserController@update');
Route::post('admins/user/delete', 'Admin\UserController@delete');
Route::post('admins/user/checkUser', 'Admin\UserController@checkUser');
Route::post('admins/user/changeStatus', 'Admin\UserController@changeStatus');
Route::post('admins/user/getProfileImage', 'Admin\UserController@getProfileImage');
Route::post('admins/user/changeRole', 'Admin\UserController@changeRole');
Route::post('admins/user/deleteAll', 'Admin\UserController@deleteAll');
Route::post('admins/user/checkLink', 'Admin\UserController@checkLink');

Route::get('admins/static-content', 'Admin\StaticContentController@index');
Route::get('admins/static-content/all', 'Admin\StaticContentController@all');
Route::get('admins/static-content/edit/{id}', 'Admin\StaticContentController@edit');
Route::post('admins/static-content/update', 'Admin\StaticContentController@update');

Route::get('admins/banner', 'Admin\BannerController@index');
Route::get('admins/banner/all', 'Admin\BannerController@all');
Route::post('admins/bannerImageUpload', 'Admin\BannerController@bannerImageUpload');
Route::get('admins/banner/add', 'Admin\BannerController@add');
Route::post('admins/banner/store', 'Admin\BannerController@store');
Route::get('admins/banner/edit/{id}', 'Admin\BannerController@edit');
Route::post('admins/banner/update', 'Admin\BannerController@update');
Route::post('admins/banner/delete', 'Admin\BannerController@delete');

Route::get('admins/seller', 'Admin\SellerController@index');
Route::get('admins/seller/all', 'Admin\SellerController@all');
Route::get('admins/seller/add', 'Admin\SellerController@add');
Route::post('admins/seller/store', 'Admin\SellerController@store');
Route::get('admins/seller/edit/{id}', 'Admin\SellerController@edit');
Route::post('admins/seller/update', 'Admin\SellerController@update');
Route::post('admins/seller/delete', 'Admin\SellerController@delete');

Route::get('admins/country', 'Admin\CountryController@index');
Route::get('admins/country/all', 'Admin\CountryController@all');
Route::post('admins/country/store', 'Admin\CountryController@store');
Route::get('admins/country/edit/{id}', 'Admin\CountryController@edit');
Route::post('admins/country/update', 'Admin\CountryController@update');
Route::post('admins/country/delete', 'Admin\CountryController@delete');
Route::post('admins/country/getState', 'Admin\CountryController@getState');
Route::post('admins/country/getCity', 'Admin\CountryController@getCity');

Route::get('admins/option', 'Admin\OptionController@index');
Route::get('admins/option/all', 'Admin\OptionController@all');
Route::get('admins/option/add', 'Admin\OptionController@add');
Route::post('admins/option/attribues', 'Admin\OptionController@attribues');
Route::post('admins/option/store', 'Admin\OptionController@store');
Route::post('admins/option/delete', 'Admin\OptionController@delete');
Route::get('admins/option/edit/{id}', 'Admin\OptionController@edit');
Route::post('admins/option/update', 'Admin\OptionController@update');

Route::get('admins/product', 'Admin\ProductController@index');
Route::get('admins/product/all', 'Admin\ProductController@all');
Route::post('admins/product/store', 'Admin\ProductController@store');
Route::post('admins/product/delete', 'Admin\ProductController@delete');
Route::get('admins/product/edit/{id}', 'Admin\ProductController@edit');
Route::post('admins/product/update', 'Admin\ProductController@update');
Route::post('admins/product/getoptionvalue', 'Admin\ProductController@getoptionvalue');
Route::post('admins/product/image_delete', 'Admin\ProductController@image_delete');
Route::get('admins/product/export', 'Admin\ProductController@export');
Route::post('admins/product/getvariation', 'Admin\ProductController@getvariation');
Route::post('admins/product/getMainCat', 'Admin\ProductController@getMainCat');

Route::post('admins/product/get_attr_gr', 'Admin\ProductController@get_attr_gr');

Route::post('admins/product/deleteAll', 'Admin\ProductController@deleteAll');
Route::post('admins/product/updateDes', 'Admin\ProductController@updateDes');

Route::get('admins/plan', 'Admin\PlanController@index');
Route::get('admins/plan/all', 'Admin\PlanController@all');
Route::post('admins/plan/store', 'Admin\PlanController@store');
Route::get('admins/plan/edit/{id}', 'Admin\PlanController@edit');
Route::post('admins/plan/update', 'Admin\PlanController@update');

Route::get('admins/permission', 'Admin\PermissionController@index');
Route::get('admins/permission/all', 'Admin\PermissionController@all');
Route::post('admins/permission/store', 'Admin\PermissionController@store');

Route::get('admins/coupon', 'Admin\CouponController@index');
Route::get('admins/coupon/all', 'Admin\CouponController@all');
Route::post('admins/coupon/store', 'Admin\CouponController@store');
Route::get('admins/coupon/edit/{id}', 'Admin\CouponController@edit');
Route::post('admins/coupon/update', 'Admin\CouponController@update');
Route::post('admins/coupon/delete', 'Admin\CouponController@deletecoupon');
Route::post('admins/coupon/changeStatus', 'Admin\CouponController@changeStatus');
Route::post('admins/coupon/getProduct', 'Admin\CouponController@getProduct');
Route::post('admins/coupon/getCategory', 'Admin\CouponController@getCategory');
Route::post('admins/coupon/getUser', 'Admin\CouponController@getUser');

//Seller Panel
Route::get('seller/login', 'Seller\HomeController@index');
Route::post('seller/log_user', 'Seller\HomeController@log_user');
Route::get('seller/profile/all', 'Seller\HomeController@all');
Route::get('seller/profile', 'Seller\HomeController@profile');
Route::post('seller/profile/update', 'Seller\HomeController@update');
Route::get('seller/log_out', 'Seller\HomeController@log_out');
Route::get('seller/dashboard', 'Seller\HomeController@dashboard');
Route::get('seller/home', 'Seller\HomeController@home');
Route::post('seller/imageupload', 'Seller\HomeController@imageupload');
Route::post('seller/Allimageupload', 'Seller\HomeController@Allimageupload');
Route::get('seller/not_access', 'Seller\HomeController@not_access');

Route::get('seller/setting', 'Seller\SettingController@index');
Route::get('seller/setting/all', 'Seller\SettingController@all');
Route::post('seller/country/getState', 'Admin\CountryController@getState');
Route::post('seller/country/getCity', 'Admin\CountryController@getCity');
Route::post('seller/setting/update', 'Seller\SettingController@update');
Route::get('seller/product-list', 'Seller\ProductController@category_list');
Route::get('seller/getCategory', 'Seller\ProductController@getCategory');
Route::post('seller/getSubCategory', 'Seller\ProductController@getSubCategory');
Route::post('seller/product_list','Seller\ProductController@getProduct');
Route::post('seller/product/all','Seller\ProductController@all');
Route::post('seller/product/getOptionValue','Seller\ProductController@getoptionvalue');
Route::post('seller/product/image_delete','Seller\ProductController@image_delete');
Route::get('seller/product/getAllCategory','Seller\ProductController@getAllCategory');


Route::get('admins/special-offer', 'Admin\SpecialOfferController@index');
Route::get('admins/special-offer/all', 'Admin\SpecialOfferController@all');
Route::post('admins/special-offer/getUser', 'Admin\SpecialOfferController@getUser');
Route::post('admins/special-offer/getRole', 'Admin\SpecialOfferController@getRole');
Route::post('admins/special-offer/store', 'Admin\SpecialOfferController@store');
Route::get('admins/special-offer/edit/{id}', 'Admin\SpecialOfferController@edit');
Route::post('admins/special-offer/update', 'Admin\SpecialOfferController@update');
Route::post('admins/special-offer/changeStatus', 'Admin\SpecialOfferController@changeStatus');
Route::post('admins/special-offer/delete', 'Admin\SpecialOfferController@delete');

Route::get('admins/bulk-discount', 'Admin\BulkDiscountController@index');
Route::get('admins/bulk-discount/all', 'Admin\BulkDiscountController@all');
Route::post('admins/bulk-discount/store', 'Admin\BulkDiscountController@store');
Route::get('admins/bulk-discount/edit/{id}', 'Admin\BulkDiscountController@edit');
Route::post('admins/bulk-discount/update', 'Admin\BulkDiscountController@update');
Route::post('admins/bulk-discount/changeStatus', 'Admin\BulkDiscountController@changeStatus');
Route::post('admins/bulk-discount/delete', 'Admin\BulkDiscountController@delete');

/*********************promotion************************/
Route::get('admins/create_promotion', 'Admin\PromotionCreateController@index');
Route::get('admins/create_promotion/get_campaign', 'Admin\PromotionCreateController@getcampaign');

Route::get('admins/create_promotion/get_copy_campaign', 'Admin\PromotionCreateController@get_copy_campaign');
Route::get('admins/create_promotion/get_promotn/{id}', 'Admin\PromotionCreateController@get_promotn');

Route::get('admins/create_promotion/get_copy_promotn/{type}', 'Admin\PromotionCreateController@get_copy_promotn');
Route::get('admins/create_promotion/get_promo_rec/{id}/{type}', 'Admin\PromotionCreateController@get_promo_rec');
Route::get('admins/create_promotion/get_camp_previw/{id}/{type}', 'Admin\PromotionCreateController@get_camp_preview');
Route::get('admins/create_promotion/change_catpro/{catpro}', 'Admin\PromotionCreateController@change_catpro');
Route::get('admins/create_promotion/get_promotion/{adtype}', 'Admin\PromotionCreateController@getpromotion');
Route::get('admins/create_promotion/placement/{placement}', 'Admin\PromotionCreateController@placement');
Route::get('admins/create_promotion/get_upd_campdata/{id}', 'Admin\PromotionCreateController@get_updcamp_rec');
Route::post('admins/create_promotion/save_promotion_adtext', 'Admin\PromotionCreateController@save_promotion_adtext');
Route::post('admins/create_promotion/insert_promotion_adtext', 'Admin\PromotionCreateController@insert_promotion_adtext');
Route::post('admins/create_promotion/update_adbanr', 'Admin\PromotionCreateController@update_adbanr');

Route::get('admins/promotion_setting', 'Admin\PromotionSettingController@index');
Route::get('admins/promotion/get_setting/{setting}', 'Admin\PromotionSettingController@get_setting');
Route::post('admins/promotion/update_adtext', 'Admin\PromotionSettingController@update_adtext');
Route::post('admins/promotion/update_adbanr', 'Admin\PromotionSettingController@update_adbanr');

/*******************************Navigation*********************************************/
Route::get('admins/navigation', 'Admin\NavigationController@index');
Route::get('admins/navigation/all', 'Admin\NavigationController@all');
Route::post('admins/navigation/add_navigation', 'Admin\NavigationController@add_navigation');
Route::post('admins/navigation/update_navigation', 'Admin\NavigationController@update_navigation');
Route::post('admins/navigation/delete_nav', 'Admin\NavigationController@delete_nav');
/*******************************Featured Category*********************************************/
Route::get('admins/featured_category', 'Admin\FeaturedCategoryController@index');
Route::get('admins/featured_category/all', 'Admin\FeaturedCategoryController@all');
Route::post('admins/featured_category/submit_featured', 'Admin\FeaturedCategoryController@submit_featured');
Route::post('admins/featured_category/delete_fea', 'Admin\FeaturedCategoryController@delete_fea');
Route::post('admins/featured_category/update', 'Admin\FeaturedCategoryController@update');

/*******************front end*********************/
Route::get('/','frontend\HomeController@index');
Route::get('home','frontend\HomeController@home');
Route::get('front/get_layout','frontend\HomeController@get_layout');
Route::post('front/subscribe_newsletter','frontend\HomeController@subscribe_newsletter');
Route::post('front/cart/add_to_cart','frontend\CartController@add_to_cart');
Route::get('front/cart/remove_item_minicart/{id}','frontend\CartController@remove_item_minicart');
Route::get('front/cart','frontend\CartController@index');
Route::get('front/user/login','frontend\UserController@login');
Route::get('front/user/logout','frontend\UserController@logout');
Route::post('front/user/user_login','frontend\UserController@user_login');
Route::get('glogin',array('as'=>'glogin','uses'=>'frontend\UserController@googleLogin')) ;
Route::get('google-user',array('as'=>'user.glist','uses'=>'frontend\UserController@listGoogleUser')) ;
Route::get('facebook', 'frontend\UserController@redirectToProvider');
Route::get('facebook/callback', 'frontend\UserController@handleProviderCallback');
Route::get('front/cart/items','frontend\CartController@cart_items');
Route::get('front/cart/empty_cart','frontend\CartController@empty_cart');
Route::post('front/cart/items_update','frontend\CartController@items_update');
Route::get('front/get_home','frontend\HomeController@get_home');
Route::get('front/modal','frontend\PopupmodelController@index');
Route::get('front/prod_model/{id}','frontend\PopupmodelController@prod_model');
Route::get('front/pro_modl_data/{id}','frontend\PopupmodelController@pro_mod_data');
// category page
Route::get('front/category/{id}','frontend\CategoryController@get_cat');
Route::get('front/category/get_cat_content/{id}','frontend\CategoryController@get_cat_content');
//product page
Route::get('front/product_details_page/{id}','frontend\ProductController@get_pro');
Route::get('front/product_deta/{id}','frontend\ProductController@get_prod_data');
Route::post('front/add_review','frontend\ProductController@add_review');
Route::post('front/product/compare','frontend\ProductController@add_compare');
Route::post('front/product/compare_delete','frontend\ProductController@compare_delete');
//checkout page
Route::get('front/checkout','frontend\CheckoutController@index');
Route::get('front/checkout/shipping_data','frontend\CheckoutController@fetch_cart_data');
Route::get('front/checkout/addaddress','frontend\CheckoutController@addaddress');
Route::get('front/checkout/country','frontend\CheckoutController@country');
Route::post('front/checkout/add_address','frontend\CheckoutController@add_address');
Route::get('front/checkout/editaddress/{id}','frontend\CheckoutController@editaddress');
Route::get('front/checkout/edit_address/{id}','frontend\CheckoutController@edit_address');
Route::post('front/checkout/update_address','frontend\CheckoutController@update_address');
Route::post('front/checkout/continue_checkout','frontend\CheckoutController@continue_checkout');
Route::get('front/checkout/checkout_payment','frontend\CheckoutController@checkout_payment');
Route::get('front/checkout/checkout_payment_detail','frontend\CheckoutController@checkout_payment_detail');
Route::post('front/checkout/post_order','frontend\CheckoutController@post_order');
Route::get('front/checkout/order_confirm/{id}','frontend\CheckoutController@order_confirm');
Route::get('front/checkout/confirm_order_content/{id}','frontend\CheckoutController@confirm_order_content');

Route::post('front/checkout/apply_coupon','frontend\CheckoutController@apply_coupon');

Route::get('front/checkout/paypal_confirm/{id}','frontend\CheckoutController@paypal_confirm');
Route::get('front/checkout/paypal_reject/{id}','frontend\CheckoutController@paypal_reject');

Route::get('front/store/{id}','frontend\StoreController@store');
Route::get('front/store_content/{id}','frontend\StoreController@store_content');

/* seller Product Route Start*/
Route::get('seller/product', 'Seller\ProductsController@index');
Route::get('seller/product/all', 'Seller\ProductsController@all');
Route::post('seller/product/options', 'Seller\ProductsController@getOption');
Route::get('seller/product/getCategory', 'Seller\ProductsController@getCategory');
Route::post('seller/product/store', 'Seller\ProductsController@productSave');
Route::post('seller/imagemutipleupload', 'Seller\ProductsController@imagemutipleupload');
/* seller Product Route End*/
/* seller ProductList Route Start*/
Route::get('seller/product', 'Seller\ProductsController@index');
Route::get('seller/product/all', 'Seller\ProductsController@all');
Route::post('seller/product/options', 'Seller\ProductsController@getOption');
Route::get('seller/product/getCategory', 'Seller\ProductsController@getCategory');
Route::post('seller/product/store', 'Seller\ProductsController@productSave');
Route::get('seller/product/edit/{id}', 'Seller\ProductsController@edit');
Route::post('seller/imagemutipleupload', 'Seller\ProductsController@imagemutipleupload');
Route::post('seller/product/image_delete','Seller\ProductsController@image_delete');
Route::post('seller/product/delete','Seller\ProductsController@delete');
/* seller ProductList Route End*/
/**************Product Details*****************/
Route::get('seller/productlist', 'Seller\ProductlistController@index');
Route::get('seller/productlist/getCategory', 'Seller\ProductlistController@getCategory');
Route::get('seller/productlist/searchTag', 'Seller\ProductlistController@searchTag');
Route::get('seller/productlist/getProduct', 'Seller\ProductlistController@getProduct');
/**************Product Details*****************/
/**************Order *****************/
Route::get('admins/order', 'Admin\OrderController@index');
Route::get('admins/order/all', 'Admin\OrderController@all');
Route::post('admins/order/update', 'Admin\OrderController@update');
/**************Order *****************/

/**************Promotion*****************/
Route::get('seller/promotion', 'Seller\PromotionController@index');
Route::get('seller/create_promotion', 'Admin\PromotionCreateController@index');
Route::get('seller/create_promotion/get_campaign', 'Admin\PromotionCreateController@getcampaign');

Route::get('seller/create_promotion/get_copy_campaign', 'Admin\PromotionCreateController@get_copy_campaign');
Route::get('seller/create_promotion/get_promotn/{id}', 'Admin\PromotionCreateController@get_promotn');

Route::get('seller/create_promotion/get_copy_promotn/{type}', 'Admin\PromotionCreateController@get_copy_promotn');
Route::get('seller/create_promotion/get_promo_rec/{id}/{type}', 'Admin\PromotionCreateController@get_promo_rec');
Route::get('seller/create_promotion/get_camp_previw/{id}/{type}', 'Admin\PromotionCreateController@get_camp_preview');
Route::get('seller/create_promotion/change_catpro/{catpro}', 'Admin\PromotionCreateController@change_catpro');
Route::get('seller/create_promotion/get_promotion/{adtype}', 'Admin\PromotionCreateController@getpromotion');
Route::get('seller/create_promotion/placement/{placement}', 'Admin\PromotionCreateController@placement');
Route::get('seller/create_promotion/get_upd_campdata/{id}', 'Admin\PromotionCreateController@get_updcamp_rec');
Route::post('seller/create_promotion/save_promotion_adtext', 'Admin\PromotionCreateController@save_promotion_adtext');
Route::post('seller/create_promotion/insert_promotion_adtext', 'Admin\PromotionCreateController@insert_promotion_adtext');
Route::post('seller/create_promotion/update_adbanr', 'Admin\PromotionCreateController@update_adbanr');
Route::post('seller/promotion/payment', 'Seller\PromotionController@postPayWithStripe');
/**************Promotion*****************/

Route::get('seller/special-offer', 'Admin\SpecialOfferController@index');
Route::get('seller/special-offer/all', 'Admin\SpecialOfferController@all');
Route::post('seller/special-offer/getUser', 'Admin\SpecialOfferController@getUser');
Route::post('seller/special-offer/getRole', 'Admin\SpecialOfferController@getRole');
Route::post('seller/special-offer/store', 'Admin\SpecialOfferController@store');
Route::get('seller/special-offer/edit/{id}', 'Admin\SpecialOfferController@edit');
Route::post('seller/special-offer/update', 'Admin\SpecialOfferController@update');
Route::post('seller/special-offer/changeStatus', 'Admin\SpecialOfferController@changeStatus');
Route::post('seller/special-offer/delete', 'Admin\SpecialOfferController@delete');

Route::get('seller/bulk-discount', 'Admin\BulkDiscountController@index');
Route::get('seller/bulk-discount/all', 'Admin\BulkDiscountController@all');
Route::post('seller/bulk-discount/store', 'Admin\BulkDiscountController@store');
Route::get('seller/bulk-discount/edit/{id}', 'Admin\BulkDiscountController@edit');
Route::post('seller/bulk-discount/update', 'Admin\BulkDiscountController@update');
Route::post('seller/bulk-discount/changeStatus', 'Admin\BulkDiscountController@changeStatus');
Route::post('seller/bulk-discount/delete', 'Admin\BulkDiscountController@delete');

Route::get('seller/coupon', 'Admin\CouponController@index');
Route::get('seller/coupon/all', 'Admin\CouponController@all');
Route::post('seller/coupon/store', 'Admin\CouponController@store');
Route::get('seller/coupon/edit/{id}', 'Admin\CouponController@edit');
Route::post('seller/coupon/update', 'Admin\CouponController@update');
Route::post('seller/coupon/delete', 'Admin\CouponController@deletecoupon');
Route::post('seller/coupon/changeStatus', 'Admin\CouponController@changeStatus');
Route::post('seller/coupon/getProduct', 'Admin\CouponController@getProduct');
Route::post('seller/coupon/getCategory', 'Admin\CouponController@getCategory');
Route::post('seller/coupon/getUser', 'Admin\CouponController@getUser');

