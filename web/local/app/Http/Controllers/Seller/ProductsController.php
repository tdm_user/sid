<?php

namespace App\Http\Controllers\Seller;

use App\User;
use App\Store;
use DB;
use Crypt;
use Auth;
use Hash;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Http\Controllers\cart\Cart;
use Session;
use Request;
use Image;
use File;
use App\Product;
use App\ProductImage;
use App\ProductVariation;
use App\ProductAttribute;

class ProductsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('seller/product')->with('title', 'Product');
    }

    public function add() {
        return view('seller/productadd')->with('title', 'Product');
    }

    public function getCategory() {
        $result = array();
        $result['brands'] = DB::table('brands')->where('status', '=', 'Active')->where('is_delete', '=', '0')->get();
        $result['all_category'] = self::getcataegorywithSub();
        return $result;
    }

    public function getcataegorywithSub($pid = 0) {
        $categories = array();
        $result = DB::table('categorys')->where('is_delete', '=', '0')->where('parent_id', '=', $pid)->get();

        foreach ((array) $result as $key => $mainCategory) {
            $product = DB::table('product')->whereRaw('FIND_IN_SET("' . $mainCategory->id . '",pro_category_id)')->get();
            $category = array();
            $condition = 'condition';
            $category['id'] = $mainCategory->id;
            $category['name'] = $mainCategory->category_name;
            $category['parent_id'] = $mainCategory->parent_id;
            $mainCategory->pro_count = count($product);
            $mainCategory->$condition = true;
            $mainCategory->all_category = self::getcataegorywithSub($category['id']);
            $categories[$mainCategory->id] = $category;
        }

        return $result;
    }

    public function productSave() {

        $product = Request::all();
        $list = array();

        $columns = array('pro_name' => '', 'pro_des' => '', 'pro_category_id' => '', 'brand_id' => '', 'meta_title' => '', 'date_from' => '', 'date_to' => '',
            'meta_description' => '', 'pro_feature_des' => '', 'meta_keywords' => '', 'images' => '', 'pro_cond' => '', 'pro_exp_on' => '', 'warranty');
        $product = array_merge($columns, $product);
        $validator = Validator::make(Request::all(), [
                    'pro_name' => 'required',
                    'pro_exp_on' => 'required',
                    'pro_des' => 'required',
                    'pro_feature_des' => 'required',
                    'pro_category_id' => 'required',
                    'brand_id' => 'required',
                    'variationsOption' => 'required',
                    'attributeOption' => 'required',
                    'meta_title' => 'required',
                    'meta_description' => 'required',
                    'meta_keywords' => 'required',
                    'warranty' => 'required'
        ]);
        $friendly_names = array(
            'pro_name' => 'Product Title',
            'pro_exp_on' => 'Product Expire on',
            'pro_des' => 'Product Description',
            'pro_short_des' => 'Product Short Description',
            'pro_feature_des' => 'Product Feature Description',
            'pro_category_id' => 'Product Category',
            'brand_id' => 'Brand',
            'price' => 'Price',
            'sale_price' => 'Sale Price',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'attributeOption' => 'Option',
            'variationsOption' => 'Product Option',
            'no_stock' => 'No. of Stock',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'warranty' => 'Warranty',
            'meta_keywords' => 'Meta Keywords'
        );
        $validator->setAttributeNames($friendly_names);
        if ($validator->fails()) {
            $list[] = 'error';
            $list[] = $validator->errors()->all();
            ;
            return $list;
        }
        $catids = $product['pro_category_id'];
        $newproids = '';
        if ($catids && is_array($catids)) {
            $newcatarr = array();
            foreach ($catids as $ck => $cv) {
                if ($cv == '1') {
                    $newcatarr[] = $ck;
                }
            }
            $newproids = implode(",", $newcatarr);
        }
        $prod = array();
        $proData = array('pro_name' => $product['pro_name'],
            'pro_des' => $product['pro_des'],
            'pro_feature_des' => $product['pro_feature_des'],
            'pro_category_id' => $newproids ? $newproids : '',
            'brand_id' => $product['brand_id'],
            'meta_title' => $product['meta_title'],
            'meta_description' => $product['meta_description'],
            'meta_keywords' => $product['meta_keywords'],
            'pro_exp_on' => $product['pro_exp_on'],
            'pro_cond' => (isset($product['pro_cond']) ? $product['pro_cond'] : ''),
            'date_from' => (isset($product['date_from']) ? $product['date_from'] : ''),
            'date_to' => (isset($product['date_to']) ? $product['date_to'] : ''),
            'warranty' => $product['warranty'],
            'seller_id' => Auth::user()->id,
        );
        if (isset($product['id']) && $product['id']) {
            DB::table('product')->where('id', '=', $product['id'])->update($proData);
            $insertedId = $product['id'];
        } else {
            $prod = Product::create($proData);
            $insertedId = $prod->id;
        }
        if ($insertedId) {
            $images = $product['images'];
            if (!empty($images)) {
                DB::table('product_images')->where('product_id', '=', $insertedId)->delete();
                foreach ($images as $img) {
                    ProductImage::create(['image' => $img['img'],
                        'product_id' => $insertedId,
                        'def' => $img['def']]);
                }
            }
        }

        if (isset($product['variationsOption']) && $product['variationsOption']) {
            $proVariant = array('id', 'vari_sku', 'vari_price', 'vari_sale_price', 'vari_stock', 'dimension_len', 'dimension_width', 'dimension_height', 'note', 'weight');
            DB::table('product_variation')->where('product_id', '=', $insertedId)->delete();
            foreach ($product['variationsOption'] as $key => $value) {
                $combination_set = (is_array($value['id']) && isset($value['id']) ? implode(',', $value['id']) : '');
                if ((!$combination_set) && isset($value['vari_comb_value_ids'])) {
                    $combination_set = $value['vari_comb_value_ids'];
                }
                ProductVariation::create(['vari_comb_value_ids' => $combination_set,
                    'product_id' => $insertedId,
                    'vari_sku' => (isset($value['vari_sku']) ? $value['vari_sku'] : ''),
                    'vari_price' => (isset($value['vari_price']) ? $value['vari_price'] : ''),
                    'vari_sale_price' => (isset($value['vari_sale_price']) ? $value['vari_sale_price'] : ''),
                    'vari_stock' => (isset($value['vari_stock']) ? $value['vari_stock'] : ''),
                    'dimension_len' => (isset($value['dimension_len']) ? $value['dimension_len'] : ''),
                    'dimension_width' => (isset($value['dimension_width']) ? $value['dimension_width'] : ''),
                    'dimension_height' => (isset($value['dimension_height']) ? $value['dimension_height'] : ''),
                    'note' => (isset($value['note']) ? $value['note'] : ''),
                    'weight' => (isset($value['weight']) ? $value['weight'] : '')
                ]);
            }
        }
        if (isset($product['attributeOption']) && $product['attributeOption'] && (!empty($product['attributeOption']))) {
            DB::table('product_attribute')->where('product_id', '=', $insertedId)->delete();
            foreach ($product['attributeOption'] as $kop => $vop) {
                if (isset($vop['options']) && $vop['options']) {
                    ProductAttribute::create(['option_name_id' => $vop['id'],
                        'option_value_ids' => $vop['options'],
                        'product_id' => $insertedId
                    ]);
                }
            }
        }
        return array('1' => 'All Data Save');
    }

    public function imagemutipleupload() {

        if (Request::input('folder'))
            $folder = '/' . Request::input('folder');
        $image = Input::file('image');

        if (Request::input('width') && Request::input('height')) {

            $image_info = getimagesize(Input::file('image'));
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            $width = Request::input('width');
            $height = Request::input('height');
            if ($image_width < $width && $image_height && File::size($image) > 2000) {
                $list[] = 'error';
                $msgs[] = 'Fix your image dimension (' . Request::input('width') . 'x' . Request::input('height') . ') or size.';
                $list[] = $msgs;
                return $list;
            }
        } else {
            $width = 200;
            $height = 200;
        }

        $destinationPath = 'uploads' . @$folder;

        $extension = Input::file('image')->getClientOriginalExtension();
        if (($extension == 'jpg') || ($extension == 'jpeg') || ($extension == 'png')) {
            $fileName = time() . '.' . $extension;
            $path = ($destinationPath . '/thumb_' . $fileName);
            Image::make(Input::file('image')->getRealPath())->resize(200, 200)->save($path);
            $path = ($destinationPath . '/mid_' . $fileName);
            Image::make(Input::file('image')->getRealPath())->resize(400, 400)->save($path);
            Input::file('image')->move($destinationPath, $fileName);
            return 'product/' . $fileName;
        } else {
            return false;
        }
    }

    public function image_delete() {
        $image = Request::input('image');
        File::delete('uploads/' . str_replace('product/', 'product/thumb_', $image));
        File::delete('uploads/' . str_replace('product/', 'product/mid_', $image));
        File::delete('uploads/' . $image);
    }

    public function all() {
        $products = DB::table('product')->select('*')->where('product.is_delete', '=', '0')->where('product.seller_id', '=', Auth::user()->id)->get();
        foreach ($products as $key => $value) {
            $category_ids = explode(',', $value->pro_category_id);
            $category_name = '';
            $all_cats = DB::table('categorys')->select('*')->whereIn('id', $category_ids)->where('is_delete', '=', '0')->get();
            foreach ($all_cats as $k => $v) {
                $category_name.=$v->category_name;
                if ($k < (count($all_cats) - 1)) {
                    $category_name.=',';
                }
            }
            $products[$key]->category_name = $category_name;
        }

//        $sellers = DB::table('users')->where('status', '=', 'Active')->where('is_delete', '=', 0)->where('role', '=', 5)->get();
//        $categories = DB::table('categorys')->where('status', '=', 'Active')->where('is_delete', '=', 0)->get();
        $brands = DB::table('brands')->where('status', '=', 'Active')->where('is_delete', '=', '0')->get();
        $all_category = self::getcataegorywithSub();
//        $datatyps = DB::table('product_data_type')->get();
        $options = DB::table('pro_option')->where('is_delete', '=', '0')->where('parent_id', '=', '0')->where('status', '=', 'Active')->get();
        $images = array();
        foreach ($products as $kk => $vv) {
            $images[$vv->id] = DB::table('product_images')->where('product_id', '=', $vv->id)->get();
        }
        $productView = DB::table('product_views')->select(DB::raw('DATE(created_at) as created_at,SUM(no_view) as total'))->groupBy(DB::raw('DATE(created_at)'))->get();
        $return['images'] = $images;
        $return['products'] = $products;
//        $return['sellers'] = $sellers;
//        $return['categories'] = $categories;
        $return['brands'] = $brands;
//        $return['datatyps'] = $datatyps;
        $return['productView'] =$productView ;
        $return['options'] = $options;
        $return['all_category'] = $all_category;
        return $return;
    }

    public function getOptions($cat = '') {
        $attr_gr = array();
        $attr_gr = DB::table('pro_option')->where('is_delete', '=', '0')->where('parent_id', '=', '0')
                        ->whereRaw('categorys_id IN(' . $cat . ')')->get();
        foreach ($attr_gr as $key => $val) {
            $attr = DB::table('pro_option')->where('is_delete', '=', '0')->where('parent_id', '=', $val->id)->get();
            $attr_gr[$key]->attribute = $attr;
            if (count($attr_gr[$key]->attribute) > 0) {
                foreach ($attr_gr[$key]->attribute as $key1 => $val1) {
                    $opt = DB::table('pro_option')->where('is_delete', '=', '0')->where('parent_id', '=', $val1->id)->get();
                    $attr_gr[$key]->attribute[$key1]->options = $opt;
                    foreach ($opt as $k => $op) {
                        $opt[$k]->options[] = DB::table('pro_option')->where('is_delete', '=', '0')->where('parent_id', '=', $op->id)->get();
                    }
                }
            }
        }
        return $attr_gr;
    }

    public function getOption() {

        $param = Request::all();
        $attr_gr = array();
        if (isset($param['cat']) && $param['cat']) {
            $cat = array_filter($param['cat']);
            $cat = implode(',', array_keys($cat));
        }
        $options = array();
        if (isset($cat) && $cat) {
            $attr_gr = $this->getOptions($cat);
        }
        $return['attr_option'] = $attr_gr;
        return $return;
    }

    function edit($id) {
        $result = array();
        $result['product'] = DB::table('product')->where('id', '=', $id)->first();
        $cat = '';
        if (isset($result['product']->pro_category_id) && $result['product']->pro_category_id) {
            $cat = $result['product']->pro_category_id;
            $result['product']->pro_category_id = explode(',', $result['product']->pro_category_id);
            $result['product']->pro_category_id = (array_flip($result['product']->pro_category_id));
            foreach ($result['product']->pro_category_id as $k => $v) {
                @$result['product']->pro_category_id[$k] = TRUE;
            }
        } else {
            $result['product']->pro_category_id = array();
        }
        $result['product_img'] = DB::table('product_images')->select('image as img', 'def as def')->where('product_id', '=', $id)->get();
        $result['product_attr'] = DB::table('product_attribute')->where('product_id', '=', $id)->get();
        $proAttr = array();
        foreach ($result['product_attr'] as $k => $v) {
            $proAttr[] = array('id' => $v->option_name_id, 'options' => $v->option_value_ids);
        }
        $result['product_attr'] = $proAttr;
        $result['brands'] = DB::table('brands')->where('status', '=', 'Active')->where('is_delete', '=', '0')->get();
        $variation = DB::table('product_variation')->where('product_id', '=', $id)->get();
        $result['selectOption'] = $options = array();
        $ids = array();
        foreach ($variation as $k => $v) {
            if ($v->vari_comb_value_ids) {
                $options[] = DB::table('pro_option')->where('is_delete', '=', '0')->whereRaw('id IN(' . $v->vari_comb_value_ids . ')')->where('status', '=', 'Active')->get();
            }
        }

        foreach ($options as $key => $val) {
            foreach ($val as $k => $v) {
                $ids[$v->parent_id][] = $v->id;
            }
        };
        $attr_gr = array();
        if (isset($cat) && $cat) {
            $attr_gr = $this->getOptions($cat);
        }
        $result['pro_attr'] = $attr_gr;
        $result['choosen'] = $ids;
        $result['pro_variant'] = $variation;
        $result['all_category'] = self::getcataegorywithSub();
        return $result;
    }

    public function delete() {
        $param = Request::all();
        if (isset($param['id']) && $param['id']) {
            $ids = implode(',', $param['id']);
        }
        $list=array();
        $proData = array('is_delete' => '1');
        DB::table('product')->whereRaw('id IN(' . $ids . ')')->update($proData);
        $list[] = 'success';
        $list[] = 'Record is deleted successfully.';
        return $list;
    }

}
