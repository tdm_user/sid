<?php

namespace App\Http\Controllers\Seller;

use App\User;
use App\Store;
use DB;
use Crypt;
use Auth;
use Hash;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Http\Controllers\cart\Cart;
use Session;
use Request;
use Image;
use File;

class WalletController extends Controller {

  public function __construct() {
      $this->middleware('auth');
  }

    function index() {
        return view('seller/wallet')->with('title', 'Wallet');
    }

}