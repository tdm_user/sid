<?php

namespace App\Http\Controllers\Seller;

use App\User;
use App\Store;
use DB;
use Crypt;
use Auth;
use Hash;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Http\Controllers\cart\Cart;
use Session;
use Request;
use Image;
use File;
use App\Product;
use App\ProductImage;
use App\Promotion;
use App\PromotionAdd;
use App\PromotionAdtext;
use App\PramotionPromoBanner;
use Paypal;

class PromotionController extends Controller {

private $_apiContext;

    public function __construct() {
        $this->middleware('auth');
    $this->_apiContext = PayPal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));
$this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        )); 
    }

    function index() {
        return view('seller/promotion')->with('title', 'Promotion');
    }

    public function postPayWithStripe() {
        $param=Request::all();
        return $this->chargeCustomer(15, 12350, 'Product Promotion', $param['stripeToken']);
    }

    public function chargeCustomer($product_id, $product_price, $product_name, $token) {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if (!$this->isStripeCustomer()) {
            $customer = $this->createStripeCustomer($token);
        } else {
            $customer = \Stripe\Customer::retrieve(Auth::user()->stripe_id);
        }

        return $this->createStripeCharge($product_id, $product_price, $product_name, $customer);
    }

    /**
     * Create a Stripe charge.
     */
    public function createStripeCharge($product_id, $product_price, $product_name, $customer) {
        try {
            $charge = \Stripe\Charge::create(array(
                        "amount" => $product_price,
                        "currency" => "INR",
                        "customer" => $customer->id,
                        "description" => $product_name
            ));
        } catch (\Stripe\Error\Card $e) {
            return redirect()->route('index')->with('error', 'Your credit card was been declined. Please try again or contact us.');
        }
        return $this->postStoreOrder($product_name);
    }

    /**
     * Create a new Stripe customer for a given user.
     */
    public function createStripeCustomer($token) {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $customer = \Stripe\Customer::create(array("description" => Auth::user()->email,"source" => $token));

        Auth::user()->stripe_id = $customer->id;
        Auth::user()->save();
        return $customer;
    }

    /**
     * Check if the Stripe customer exists.
     *
     * @return boolean
     */
    public function isStripeCustomer() {
        return Auth::user() && \App\User::where('id', Auth::user()->id)->whereNotNull('stripe_id')->first();
    }

    /**
     * Store a order.
     *
     * @param string $product_name
     * @return redirect()
     */
    public function postStoreOrder($product_name) {
        
        $adtext = PromotionAdtext::find(Request::input('CampID'));
        $adtext->payment_status=1;
        $adtext->save();
        return array('msg', 'Thanks for your purchase!');
    }

}
