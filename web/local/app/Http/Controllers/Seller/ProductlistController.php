<?php

namespace App\Http\Controllers\Seller;

use App\User;
use App\Store;
use DB;
use Crypt;
use Auth;
use Hash;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Http\Controllers\cart\Cart;
use Session;
use Request;
use Image;
use File;
use App\Product;
use App\ProductImage;

class ProductlistController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('seller/productlist')->with('title', 'Product');
    }

    public function getCategory() {
        $result = array();
        $cid = Request::input('cat_id');
        $pid = (isset($cid) ? $cid : 0);
        $result['all_category'] = DB::table('categorys')->where('is_delete', '=', '0')->where('parent_id', '=', $pid)->get();
        return $result;
    }

    public function searchTag() {
        $tag = Request::input('tag');
        $products = DB::table('product')
                ->where('pro_name', 'LIKE', '%' . $tag . '%')
                ->orWhere('meta_keywords', 'LIKE', '%' . $tag . '%')
                ->get();
        $allIds = array();
        foreach ($products as $product) {
            $allIds[] = $product->pro_category_id;
        }
        $sortedIds = $sortedId = array();
        foreach ($allIds as $cid) {
            if ($cid) {
                $sortedIds[] = explode(',', $cid);
            }
        }
        foreach ($sortedIds as $key => $value) {
            $sortedId = array_merge($sortedId, $value);
        }
        if (!empty($sortedId)) {
            $sortedId = array_unique(array_filter($sortedId));
        }

        $result['tags'] = DB::table('categorys')
                ->where('is_delete', '=', '0')
                ->whereRaw('id ' . ($sortedId ? 'IN(' . implode(',', $sortedId) . ')' : ' NOT IN(0)'))
                ->get();
        return $result;
    }

    public function getProduct() {

//        DB::enableQueryLog();
        $tagID = Request::input('tag');
        $products = DB::table('product')->select('*')->whereRaw('FIND_IN_SET(' . $tagID . ',pro_category_id)')->get();
        foreach ($products as $key => $p) {
                @$p->images = DB::table('product_images')->where('product_id', '=', $p->id)->get();
//                @$p->attribute = DB::table('product_attribute')->where('product_id', '=', $p->id)->get();
        }
//        dd(DB::getQueryLog());
        return $products;
    }

}
