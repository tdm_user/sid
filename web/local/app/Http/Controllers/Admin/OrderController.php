<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Option;
use DB;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use Request;
use App\Order;

class OrderController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('admin/order/order')->with('title', 'Order')->with('subtitle', 'List');
    }

    function all() {
        $result = array();
        $seller = array();
        $order = DB::table('order_detail')->select(DB::raw('*,DATE_FORMAT(DATE(created_at),"%b %d %Y") as created_at'))->get();
        foreach ($order as $key => $value) {
            $value->proInfo = DB::table('order_product')->select(DB::raw('name,quantity,price,total,seller_id'))->where('order_id', '=', $value->id)->get();
            foreach ($value->proInfo as $key => $value) {
                $seller[] = $value->seller_id;
            }
        }
        $seller = (!empty($seller) ? array_unique(array_filter($seller)) : array());
        $allSeller = array();
        if (!empty($seller)) {
            $sID = implode(',', $seller);
            $seller = DB::table('users')->select(DB::raw('id,fname,lname,display_name'))->whereRaw('id IN(' . $sID . ')')->get();
            foreach ($seller as $value) {
                $allSeller[$value->id] = $value;
            }
        }
        $courier = DB::table('users')->select(DB::raw('id,fname,lname,display_name'))->where('role', '=', '4')->get();
        $allCourier = array();
        foreach ($courier as $value) {
            $allCourier[$value->id] = $value;
        }
        $result['courier'] = $allCourier;
        $result['seller'] = $allSeller;
        $result['order'] = $order;
        $result['orderStatus'] = Order::orderStatusOption();
        $result['paymentStatus'] = Order::paymentStatusOption();
        return $result;
    }

    function update() {
       $param = Request::all();
       foreach ($param as $value) {
            $orderData = array(
                'status' => $value['orderStatus'],
//                'orderStatus' => $value['paymentStatus'],
//                'orderStatus' => $value['couriersOption'],
            );
           DB::table('order_detail')->where('id', '=', $value['order_id'])->update($orderData);
        }
        return array('1' => 'All Data Save');
    }

}
