<?php namespace App\Http\Controllers\Admin;
use App\User; 
use App\FeaturedCategory; 
use App\Category;
use DB;

use Illuminate\Support\Facades\Input;
use Auth;
use Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
class FeaturedCategoryController extends Controller
{      
            public function __construct()
        {
            $this->middleware('auth');
            

        }
	public function index(){ 
            
             return view('admin/featured_category')->with('title','Featured Category')->with('subtitle','List');
		
	}
	
       public function all(){ 
	        $featured_category = DB::table('featured_category')->select('*')->get();
           
			foreach($featured_category as $key => $val)
			{
			  $sub_cat=array();
			  $sub_cats=explode(';',$val->subcategory_id);
			  	foreach($sub_cats as $ky => $vy){
				  $sub_cat[]=(int)$vy;
				  $sub_cat_id[(int)$vy]=true;
				}
			  $featured_category[$key]->category_id = (int)$val->category_id;
			  $featured_category[$key]->subcats=$sub_cat;
			  $featured_category[$key]->subcat_id=$sub_cat_id;
			}
			$list['featured_category'] = $featured_category ;
			$category = DB::table('categorys')->select('*')->where('is_delete','0')->where('status','Active')->get();
			foreach($category as $key => $val)
			{
			 			 
			  $category[$key]->parent_id=(int)$val->parent_id;
			  $category[$key]->id=(int)$val->id;
			}
                        $list['category'] = $category ;
			return  $list ;
		
	}
	 
    public function delete_fea(){
		
	   $chk_id=Request::input('del_id');
       DB::table('featured_category')->where('id', '=',$chk_id)->delete();	
       $list[]='success';
       $list[]='Record is deleted successfully.';	 
	   return $list;	 
	    
	}
	 public function update(){		
	
	   $validator = Validator::make(Request::all(), [
            'edit_featured.category_id' => 'required',		        
            'edit_featured.sort'=>'required|numeric',
           
        ]);
          $friendly_names = array(
			  'edit_featured.category_id' => 'Category',
			  'edit_featured.sort'=>'Sort',
		    );
	    $validator->setAttributeNames($friendly_names); 
        if ($validator->fails()) {
                  $list[]='error';
                  $msg=$validator->errors()->all();
			      $list[]=$msg;
			      return $list;
        }	
		 $input = Request::input('edit_featured');
		 $subcat='';
			
         if(isset($input['subcat_id']))
		 {
		  foreach($input['subcat_id'] as $key=>$value)
		  {
		   if($value==1)
		   {
		      if($subcat !='')
			  {
			     $subcat .= ';';
			  }
			  	$subcat .= $key;
			  
		   }
		  }
		 }	  
		 
		 $featurecat = FeaturedCategory::find($input['id']);
		 $featurecat->category_id=$input['category_id'];
		 $featurecat->subcategory_id=$subcat ;
		 $featurecat->sort=$input['sort'];
		 $featurecat->status=$input['status'];
		 $featurecat->position=$input['position'];
		 $featurecat->save();
		 $list[]='success';
         $list[]='Record is Updated successfully.';	 
	     return $list;
	    
	}   
      public function submit_featured(){		
	
	   $validator = Validator::make(Request::all(), [
            'add_featured.category_id' => 'required',		        
            'add_featured.sort'=>'required|numeric',
           
        ]);
          $friendly_names = array(
			  'add_featured.category_id' => 'Category',
			  'add_featured.sort'=>'Sort',
		    );
	    $validator->setAttributeNames($friendly_names); 
        if ($validator->fails()) {
                  $list[]='error';
                  $msg=$validator->errors()->all();
			      $list[]=$msg;
			      return $list;
        }	
		 $input = Request::input('add_featured');
		 $subcat='';
         if(isset($input['subcategory_id']))
		 {
		  foreach($input['subcategory_id'] as $key=>$value)
		  {
		   if($value==1)
		   {
		      if($subcat !='')
			  {
			     $subcat .= ';';
			  }
			  	$subcat .= $key;
			  
		   }
		  }
		 }
		          FeaturedCategory::create(['category_id'=>$input['category_id'],'subcategory_id'=>$subcat,'sort'=>$input['sort'],'status'=>$input['status'],'position'=>$input['position']]);
         $list[]='success';
         $list[]='Record is Added successfully.';	 
	     return $list;
	    
	}   
	 
       
 }
 
?>