<?php namespace App\Http\Controllers\Admin;
use App\User; 
use App\Brand; 
use App\Slug; 
use DB;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use Request;
class BrandController extends Controller
{      
            public function __construct()
        {
            $this->middleware('auth');
            

        }
	public function index(){ 
             $brands = DB::table('brands')->where('is_delete', '=','0')->get();  
             return view('admin/brand')->with('brands',$brands)->with('title','Brands')->with('subtitle','List');
		
	}
      
	public function all(){ 
	         $list=array();
             $brand = DB::table('brands')->where('is_delete','=','0')->get(); // print_r($brand);
			 foreach( $brand as $key=>$val){ 
			  $slug = DB::table('slug')->where('table_id','=',$val->id)->where('table_name','=','brands')->first();
			  if(isset($slug->slug_name)){ 
			  $brand[$key]->slug= $slug->slug_name;
			  }else{
			  $brand[$key]->slug='';
			  }
			 }
			 $brand1 = DB::table('brands')->orderBy('id', 'desc')->first();
			 $list['brand']= $brand;
			 $list['last_id']= $brand1->id+1;
			// print_r($list);
             return  $list;
			 
	}
        public function store(){
	
  
	   $validator = Validator::make(Request::all(), [
            'brand_name' => 'required|soft_unique_single:brands,brand_name',
			'slug' => 'unique:slug,slug_name',
            'meta_title'=>'required',
            'meta_description'=>'required',
            'meta_keyword'=>'required',
	        'image'=>'required',
            'description'=>'required',            
            
        ]);
         
        if ($validator->fails()) {
				$list[]='error';
				$msg=$validator->errors()->all();
				$list[]=$msg;
				return $list;
        }
		 
         $destinationPath = 'uploads'; // upload path
         $brand=Brand::create(['image' =>'brand/'.Request::input('image'),'meta_title' =>Request::input('meta_title'),'meta_description' =>Request::input('meta_description'),'meta_keyword' =>Request::input('meta_keyword'),'brand_name' =>Request::input('brand_name'),'description' =>Request::input('description'),'is_delete'=>'0','status' =>Request::input('status'),'user_id'=>Auth::user()->id]); 
		 if(Request::input('slug')!='') {
		     $slug=Slug::create(['slug_name' =>Request::input('slug'),'table_name' =>'brands','table_id' =>$brand->id]); 
		 }
         $list[]='success';
            $list[]='Record is added successfully.';	 
	    return $list;
	   
	}
        public function delete(Request $request){
	
	       $chk_id=Request::input('del_id');
           $brand = Brand::find($chk_id);
           $brand->is_delete = '1';
           $brand->save(); 	
		   $slug = DB::table('slug')->where('table_id','=',Request::input('del_id'))->where('table_name','=','brands')->first(); 
		   if(isset($slug->id)){
			   $slug1 = Slug::find($slug->id);
			   $slug1->delete(); 
		   }		 
           $list[]='success';
           $list[]='Record is deleted successfully.';	 
			return $list;
	    
	}
        
	 public function edit($id){
	
		$brands= DB::table('brands')->where('id', '=',$id)->first();	
		$slug= DB::table('slug')->where('table_id', '=',$brands->id)->where('table_name', '=','brands')->first();	
		$return['brands']=$brands;
		$return['slug']=$slug;			
		return $return; 
	     
	}
         public function update(){
	
	  $validator = Validator::make(Request::all(), [
            'brand_name' => 'required',	    
            'description'=>'required',
			'slug' => 'unique:slug,slug_name',        
            'meta_title'=>'required',
            'meta_description'=>'required',
            'meta_keyword'=>'required',
        ]);
         
        if ($validator->fails()) {
			$list[]='error';
			$msg=$validator->errors()->all();
			$list[]=$msg;
			return $list;
        }
	 if(Request::input('image')!=''){	 
         //$destinationPath = 'uploads'; // upload path
         //$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
         $fileName = Request::input('image'); // renameing image
         //Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
         }
         $brand = Brand::find(Request::input('id'));
         $brand->brand_name = Request::input('brand_name');
         if((isset($fileName)) && ($fileName!='')){
	      $brand->image = 'brand/'.$fileName;
         }
         $brand->meta_title =Request::input('meta_title');
         $brand->meta_description =Request::input('meta_description');
         $brand->meta_keyword =Request::input('meta_keyword');
	     $brand->description =Request::input('description');	 
         $brand->status=Request::input('status');
         $brand->save(); 
		  
		 $slug= DB::table('slug')->where('table_id', '=',$brand->id)->where('table_name', '=','brands')->first();
		 if(Request::input('slug') !=''){	
			 if(isset($slug->id)){
				 $slug1 = Slug::find($slug->id);
				 $slug1->slug_name=Request::input('slug');
				 $slug1->save();
			 }else{
			     $slug1=Slug::create(['slug_name' =>Request::input('slug'),'table_name' =>'brands','table_id' =>$brand->id]); 
			 }
		 }
		  
        $list[]='success';
		$msgs='Record updated successfully.';
		$list[]=$msgs;
		return $list;
	     
	}
       
 }
 
?>