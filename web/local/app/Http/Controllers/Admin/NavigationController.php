<?php namespace App\Http\Controllers\Admin;
use App\User; 
use App\Navigation; 
use App\MegaMenuTitle; 
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use Auth;
use Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
class NavigationController extends Controller
{      
            public function __construct()
        {
            $this->middleware('auth');
            

        }
	public function index(){ 
            
             return view('admin/navigation')->with('title','Navigation')->with('subtitle','List');
		
	}
	public function add_navigation()
	{
	     $position = Request::input('position');
		 $ids = Request::input('ids');
		 $type = Request::input('type');		
		 if( $type =='custom_link'){
		   if(isset($ids['url']) && isset($ids['text']))	
		   {	
		     if (filter_var($ids['text'], FILTER_VALIDATE_URL) === FALSE) {
				 Navigation::create(['status'=>1,'parent_id'=>0,'position'=>$position,'menu_name'=>$ids['text'],'url'=>$ids['url']]);
			 }else{
			     Navigation::create(['status'=>1,'parent_id'=>0,'position'=>$position,'menu_image'=>$ids['text'],'url'=>$ids['url']]);
			 }    
		    
			}
		 }else{
		  foreach($ids as $ky => $vy)
		  {		
		  if(isset($vy['check']))
		  {
		   Navigation::create(['status'=>1,'parent_id'=>0,'position'=>$position,'table_name'=>$type,'table_id'=>$vy['id']]);
		  }
		 }
		}
	   $list[]='success';
       $list[]='Record is added successfully.';	 
	   return $list;	 
	   
	}
       public function all(){ 
	        $cat = DB::table('categorys')->select('*')->where('is_delete','0')->where('status','Active')->get();
            foreach($cat  as $val){
			 $list['categorys'][]=array('id'=>$val->id,'name'=>$val->category_name);			
			} 
			$pro = DB::table('product')->select('*')->where('is_delete','0')->where('status','Active')->get();
            foreach($pro  as $val){
			 $list['product'][]=array('id'=>$val->id,'name'=>$val->pro_name);			
			} 
			$brands = DB::table('brands')->select('*')->where('is_delete','0')->where('status','Active')->get();
            foreach($brands  as $val){
			 $list['brands'][]=array('id'=>$val->id,'name'=>$val->brand_name);			
			} 
			 $store = DB::table('store')->select('*')->where('store_status','Active')->get();
            foreach($store  as $val){
			 $list['store'][]=array('id'=>$val->id,'name'=>$val->store_name);			
			}			
			$mega_menu_title = DB::table('mega_menu_title')->select('*')->where('status','1')->get();
            foreach($mega_menu_title  as $val){
			 $list['mega_menu_title'][]=array('navigation_id'=>$val->navigation_id,'title'=>$val->title,'id'=>$val->id);			
			} 
			$navigation=DB::table('navigation')->where('parent_id','0')->orderBy('sort','asc')->get();  
			foreach($navigation as $key=>$value)
			{
			  $mega_menu_title = array();
			  if($value->mega_menu=='1')
			  {
			    $mega_title_table = DB::table('mega_menu_title')->select('*')->where('status','1')->where('navigation_id',$value->id)->get();
				foreach($mega_title_table as $kys => $vys){
				  $mega_menu_title[] = $vys->title;
				}
				$navigation[$key]->mega_title_table =$mega_title_table;
				$navigation[$key]->mega_menu_title = $mega_menu_title;
			  }
			  if($value->table_name != ''){		
			   $menu_name = DB::table($value->table_name)->select('*')->where('id',$value->table_id)->first();			 
			   if($value->table_name=='categorys')
			   {
			   $navigation[$key]->nav_menu=$menu_name->category_name;
			   }
			    if($value->table_name=='product')
			   {
			   $navigation[$key]->nav_menu=$menu_name->pro_name;
			   }
			    if($value->table_name=='brands')
			   {
			   $navigation[$key]->nav_menu=$menu_name->brand_name;
			   }
			    if($value->table_name=='store')
			   {
			   $navigation[$key]->nav_menu=$menu_name->store_name;
			   }
			  }
			  $nav=DB::table('navigation')->where('parent_id',$value->id)->orderBy('sort','asc')->get(); 
			  foreach($nav as $ky=>$vy){
			  		    
			     if($vy->table_name != ''){		
				   $menu_name = DB::table($vy->table_name)->select('*')->where('id',$vy->table_id)->first();			 
				   if($vy->table_name=='categorys')
				   {
				   $nav[$ky]->nav_menu=$menu_name->category_name;
				   }
					if($vy->table_name=='product')
				   {
				   $nav[$ky]->nav_menu=$menu_name->pro_name;
				   }
					if($vy->table_name=='brands')
				   {
				   $nav[$ky]->nav_menu=$menu_name->brand_name;
				   }
					if($vy->table_name=='store')
				   {
				   $nav[$ky]->nav_menu=$menu_name->store_name;
				   }
				  }
				 $navigation[$key]->child_nav = $nav;
			  } 
			}
			 $list['navigation']=$navigation;
             return  $list ;
		
	}
	 
    public function delete_nav(){
		
	   $chk_id=Request::input('del_id');
       DB::table('navigation')->where('id', '=',$chk_id)->delete();	
       $list[]='success';
       $list[]='Record is deleted successfully.';	 
	   return $list;	 
	    
	}
        
	 public function update_navigation(){
	
	 /* $validator = Validator::make(Request::all(), [
            'reply' => 'required',	    
                  
            
        ]);
         
        if ($validator->fails()) {
                $list[]='error';
                $msg=$validator->errors()->all();
				$list[]=$msg;
				return $list;
        }*/
        
	     $navigation=Request::input('navigation');
		 foreach($navigation as $key=>$value)
		 {
		       
		         $nav = Navigation::find($value['id']);
				 $nav->parent_id='';
				 $nav->mega_menu_id='';
				 if($value['parent_id'] !='' &&  $value['parent_id']!=0)
				 {
				   $nav->parent_id=$value['parent_id'];
				   $navig= DB::table('navigation')->select('*')->where('id',$value['parent_id'])->first();
				   if($navig->mega_menu == '1'){
				      $nav->mega_menu_id=  $value['mega_menu_id'];
				   }
				 }
				 $nav->menu_name='';
				 $nav->menu_image='';
				 if ((filter_var($value['menu_name'], FILTER_VALIDATE_URL) === FALSE) && ($value['menu_name']!='') ) {
				    $nav->menu_name=$value['menu_name'];
				 }
				 else 
				 {
				    if($value['menu_name']!=''){ $nav->menu_image=$value['menu_name'];}
				 }
				  if ((filter_var($value['menu_image'], FILTER_VALIDATE_URL) === FALSE) && ($value['menu_image']!='') ) {
				    $nav->menu_name=$value['menu_image'];
				 }
				 else
				 {
				     if($value['menu_image']!=''){ $nav->menu_image=$value['menu_image'];}
					 
				 }				
				 $nav->position=$value['position'];
				 $nav->sort=$value['sort'];
				 $nav->url=$value['url'];
				 $nav->status=$value['status'];
				 $nav->mega_menu=$value['mega_menu'];
				 $nav->save();
				
				 DB::table('mega_menu_title')->where('navigation_id', $value['id'])->update(['status' => 0]);
				 if($value['mega_menu'] == 1 && $value['parent_id']==0)
				 {
				   if(isset($value['mega_menu_title'])){
				    foreach($value['mega_menu_title'] as $ky=>$vy){
					 $mega_menu_title= DB::table('mega_menu_title')->select('*')->where('navigation_id', $value['id'])->where('title', $vy)->where('status',0)->first();
					  if(isset($mega_menu_title->title) && ($mega_menu_title->title!=''))
					  {
					     $megatitle= MegaMenuTitle::find($mega_menu_title->id);
						 $megatitle->status='1';
						 $megatitle->save();
					  }
					  else{
					      MegaMenuTitle::create(['status'=>1,'navigation_id'=>$value['id'],'title'=>$vy]);
					  }
					}
				 }
			  }
			
			  if(isset($value['child_nav']) )
			  {			 
			  foreach($value['child_nav'] as $kys=>$vys)
			  {
			     $nav = Navigation::find($vys['id']);
				 $nav->parent_id='';
				 $nav->mega_menu_id='';
				 if($vys['parent_id'] !='' &&  $vys['parent_id']!=0)
				 {
				   $nav->parent_id=$vys['parent_id'];
				   $navig= DB::table('navigation')->select('*')->where('id',$vys['parent_id'])->first();
				   if($navig->mega_menu == '1'){
				      $nav->mega_menu_id=  $vys['mega_menu_id'];
				   }
				 }
				 $nav->menu_name='';
				 $nav->menu_image='';
				 if ((filter_var($vys['menu_name'], FILTER_VALIDATE_URL) === FALSE) && ($vys['menu_name']!='') ) {
				    $nav->menu_name=$vys['menu_name'];
				 }
				 else 
				 {
				    if($vys['menu_name']!=''){ $nav->menu_image=$vys['menu_name'];}
				 }
				  if ((filter_var($vys['menu_image'], FILTER_VALIDATE_URL) === FALSE) && ($vys['menu_image']!='') ) {
				    $nav->menu_name=$vys['menu_image'];
				 }
				 else
				 {
				     if($vys['menu_image']!=''){ $nav->menu_image=$vys['menu_image'];}
					 
				 }				
				 $nav->position=$value['position'];
				 $nav->sort=$vys['sort'];
				 $nav->url=$vys['url'];
				 $nav->status=$vys['status'];
				 $nav->mega_menu=$vys['mega_menu'];
				 $nav->save();
			
			 }
			} 
		 }
	   $list[]='success';
       $list[]='Record is updated successfully.';	 
	   return $list;	 
	}
       
 }
 
?>