<?php
namespace App\Http\Controllers\frontend;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\cart\Cart;
use Carbon\Carbon;
use DB;
use Category;
use App\User;
use Session;
use Request;
use Response;
use Image;
use Validator;
use App\Newsletter; 
class CartController extends BaseController
{
    // cart page
    public function index(){
    return view('frontend/cart');
    }
    //get cart items
    public function cart_items()
    {
                $cart = new Cart();
                $cart_item['cart_data']= $cart->getItems();
	        $cart_item['total']=$cart->getTotal();
	        $cart_item['total_num']=$cart->totalQuantity();
	        return $cart_item;
    }
     //update cart items
    public function items_update()
    {
            $crt_itm = Request::input('cart_item');
            $cart = new Cart();
            $cart->updateQty($crt_itm['id'], $crt_itm['quantity']);
            $cart_item['cart_data']= $cart->getItems();
	    $cart_item['total']=$cart->getTotal();
	    $cart_item['total_num']=$cart->totalQuantity();
	    return $cart_item;
               
    }
      //empty Cart
    public function empty_cart()
    {
            $crt_itm = Request::input('cart_item');
            $cart = new Cart();
            $cart->clear();
            $cart_item['cart_data']= $cart->getItems();
	    $cart_item['total']=$cart->getTotal();
	    $cart_item['total_num']=$cart->totalQuantity();
	    return $cart_item;
               
    }
   // add to cart function
    public function add_to_cart(){
       	
        $product=Request::input('product');        
        $qty=Request::input('qty');
       $product_query =  DB::table('product')
          ->where('is_delete','=',0)
          ->where('stock_status','=','In Stock')
          ->where('status','=','Active')
          ->where('no_stock','>',$qty)
          ->where('id','=',$product['id'])
          ->first();
        if( isset($product_query->id) ){
             $price='';
             $now = Carbon::now();
             $newfr = explode('-',$product_query->date_from);
             $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
             $newse = explode('-',$product_query->date_to);
             $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
             if($now->between($first, $sec) &&  $product_query->sale_price!='0'){            
               $price = $product_query->sale_price;
             }else{
              $price = $product_query->price;
             }
               $image_query =  DB::table('product_images')->where('product_id','=',$product['id'])->orderBy('def', 'desc')->first();
               if($image_query->image !='' )
               {
               $image=$image_query->image ;
               }else{
               $image='no_image.png' ;
               }
	        $cart = new Cart();
	        $cart->add([
	       'id'       =>$product_query->id,
	       'name'     => $product_query->pro_name,
	       'quantity' => $qty,
	       'price'    => $price,
	       'image'=> $image
	       ]);
	        $minicart['cart_data']= $cart->getItems();
	        $minicart['total']=$cart->getTotal();
	        $minicart['total_num']=$cart->totalQuantity();
	        $return[] = 'success';
	        $return[] ='Item is successfully added in cart.';
	        $return[] = $minicart;
	        return $return;
	  }else{
	     $return[]='error';
	     $return[] ='Sorry ! Some error occurs.';
	     return $return;
	  }
	   
	}
	
      // remove item from cart
	  public function remove_item_minicart($itemid){
		   $cart = new Cart();
		   $cart->remove($itemid);
		   $minicart['cart_data']= $cart->getItems();
		   $minicart['total']=$cart->getTotal();
		   $minicart['total_num']=$cart->totalQuantity();
		   return $minicart;
	  }
	
	
}
