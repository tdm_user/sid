<?php
namespace App\Http\Controllers\frontend;
use Illuminate\Routing\Controller as BaseController;
use Carbon\Carbon;
use DB;
use Category;
use App\User;
use Session;
use Request;
use Response;
use Image;
use Validator;
use App\Newsletter; 
use App\Http\Controllers\cart\Cart;
use Auth;
class HomeController extends BaseController
{
    public function index(){	
	return view('frontend/layout')->with('title','Home');
	}
	public function home(){
		return view('frontend/home')->with('title','Home');
	}
	
	// get category subcategory
	public function getcataegorywithSub($pid=0)
 {
  $categories = array();
  $result = DB::table('categorys')->where('is_delete', '=','0')->where('parent_id','=',$pid) ->where('status','=','Active')->get();
  
  foreach((array)$result as $key=>$mainCategory)
  {
   $category = array(); 
            $slug = DB::table('slug')->where('table_id', '=',$mainCategory->id)->where('table_name', '=','categorys')->first();
   if(isset($slug->slug_name) && $slug->slug_name!='')
   {
   $mainCategory->slug=$slug->slug_name;
   }else{
   $mainCategory->slug='';
   }      
   $category['id'] = $mainCategory->id;   
   $category['name'] = $mainCategory->category_name;
   $category['parent_id'] = $mainCategory->parent_id;
   $mainCategory->all_category = self::getcataegorywithSub($category['id']);
   $categories[$mainCategory->id] = $category;   
  }  
  return $result;
  }
  // layout structure
  public function get_layout(){
          // mini cart
           $cart = new Cart();
           $minicart['cart_data']= $cart->getItems();
	   $minicart['total']=$cart->getTotal();
	   $minicart['total_num']=$cart->totalQuantity();	   
	   $data['minicart']= $minicart;
	   // navigation
	  $data['nav_menu']['all_menu']=self::getmemuwithsub();
	  $data['cat_menu']['menu'] = self::getcataegorywithSub();
	  $data['city_news']=array();
	  $country= DB::table('country')->where('status', '=','Active')->where('is_delete', '=','0')->where('pid', '=','0')->get();
	   foreach( $country as $key=>$value)
	   {
	     $state = DB::table('country')->where('status', '=','Active')->where('is_delete', '=','0')->where('pid', '=',$value->id)->get();
	     foreach($state as $ky=>$vy){
	      $city= DB::table('country')->where('status', '=','Active')->where('is_delete', '=','0')->where('pid', '=',$vy->id)->get();
	       foreach($city as $kys=>$vys){
	        $data['city_news'][]=$vys->name.'('.$value->name.'-'.$vy->name.')';
	      }
	     }
	   }
	   // Auth Login
	      if(Auth::check()){ 
	      		$data['auth_user']=Auth::user();
	      }else{
	      	        $data['auth_user']=false;
	      }
	  return $data;
  }
  // get menu function
  public function getmemuwithsub($pid=0){
	  $menus_listing=array();
	
	 $menu_list=DB::table('navigation')->where('parent_id','0')->where('status','1')->orderBy('sort','asc')->get();  
			foreach($menu_list as $key=>$value)
			{
			  $mega_menu_title = array();
			  $menu_list[$key]->slug ='';
			  if($value->mega_menu=='1')
			  {
			        $mega_title_table = DB::table('mega_menu_title')->select('*')->where('status','1')->where('navigation_id',$value->id)->get();
				foreach($mega_title_table as $kys => $vys){
				  $mega_menu_title[] = $vys->title;
				}
				$menu_list[$key]->mega_title_table =$mega_title_table;
				$menu_list[$key]->mega_menu_title = $mega_menu_title;
			  }
			  if($value->table_name != ''){	
			   $slug= DB::table('slug')->where('table_id','=',$value->table_id)->where('table_name','=',$value->table_name)->first(); 
			   if(isset($slug->slug_name) && $slug->slug_name!='')
			   {
				$menu_list[$key]->slug=$slug->slug_name;
			   } else{
				$menu_list[$key]->slug = $value->table_name.'/'.$value->table_id;
			   }      	
			   $menu_name = DB::table($value->table_name)->select('*')->where('id',$value->table_id)->first();			 
			   if($value->table_name=='categorys')
			   {
			     if($menu_name->is_delete=='0' && $menu_name->status=='Active' ){
			     	$menu_list[$key]->menu_name=$menu_name->category_name;
			     }else{
			     	$menu_list[$key]->menu_name = '';
			     }
			   }
			    if($value->table_name=='product')
			   {
			     if($menu_name->is_delete=='0' && $menu_name->status=='Active' ){
			    	 $menu_list[$key]->menu_name=$menu_name->pro_name;
			     }else{
			     	$menu_list[$key]->menu_name = '';
			     }
			   
			   }
			    if($value->table_name=='brands')
			   {
			      if($menu_name->is_delete=='0' && $menu_name->status=='Active' ){
			    	 $menu_list[$key]->menu_name=$menu_name->brand_name;
			     }else{
			     	$menu_list[$key]->menu_name = '';
			     }
			  
			   }
			    if($value->table_name=='store')
			   {
			     
			     $menu_list[$key]->menu_name=$menu_name->store_name;
			   }
			  }
			
			  $nav=DB::table('navigation')->where('parent_id',$value->id)->where('status','1')->orderBy('sort','asc')->get(); 
			  foreach($nav as $ky=>$vy){
			     $nav[$ky]->slug ='';	    
			     if($vy->table_name != ''){	
			     	   $menu_name = DB::table($vy->table_name)->select('*')->where('id',$vy->table_id)->first();
				   $slug= DB::table('slug')->where('table_id','=',$value->table_id)->where('table_name','=',$value->table_name)->first(); 
				   if(isset($slug->slug_name) && $slug->slug_name!='')
				   {
					$nav[$ky]->slug=$slug->slug_name;
				   } else{
					$nav[$ky]->slug=$vy->table_name.'/'.$vy->table_id;
				   } 			 
			           if($vy->table_name=='categorys')
				   {
				   
				    if($menu_name ->is_delete=='0' && $menu_name ->status=='Active' ){
				     	$nav[$ky]->menu_name=$menu_name->category_name;
				     }else{
				     	$nav[$ky]->menu_name = '';
				     }
				   }
				   if($vy->table_name=='product')
				   {
				 
				     if($menu_name->is_delete=='0' && $menu_name->status=='Active' ){
				       $nav[$ky]->menu_name=$menu_name->pro_name;
				     }else{
				     	$nav[$ky]->menu_name = '';
				     }
				   }
				   if($vy->table_name=='brands')
				   {
				     if($menu_name->is_delete=='0' && $menu_name->status=='Active' ){
				     	$nav[$ky]->menu_name=$menu_name->brand_name;
				     }else{
				     	$nav[$ky]->menu_name = '';
				     }
				  
				   }
				   if($vy->table_name=='store')
				   {
				   $nav[$ky]->menu_name=$menu_name->store_name;
				   }
				  }
				 $menu_list[$key]->child_nav = $nav;
			  } 
			}	   
	   return $menu_list;				 
  }  
  // Newsletter Subscriber
  public function subscribe_newsletter(){
  
        $validator = Validator::make(Request::all(), [
            'name' => 'required',
            'email' => 'required|unique:newsletters',	    
            'mobile' => 'required',
            'city'   =>  'required',
            'gender'=> 'required'  ,
            'occupation' => 'required'    
            
        ]);
         
        if ($validator->fails()) {
                $list[]='error';
                $msg=$validator->errors()->all();
				$list[]=$msg;
				return $list;
        }
        $news= Newsletter::create(['name' =>Request::input('name'),'email' =>Request::input('email'),'mob_no' =>Request::input('mobile'),'occupation' =>Request::input('occupation'),'city' =>Request::input('city'),'gender' =>Request::input('gender'),'subscribe' =>'1']);  
          $list[]='success';
          $list[]='You have successfully subscribed';	 
	  return $list;
  }
  // get home
  public function get_home(){
      
	  $order_pros = DB::table('order_product')->select('*', DB::raw('COUNT(*) as products_count'))->groupBy('product_id')->orderBy('products_count','desc')->get();
	  $data['popular'] = array();
	  foreach($order_pros as $ky=>$vy)
	  {
	         $pro = DB::table('product')                  
                     ->where('product.status','=','Active')->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where('product.id','=',$vy->product_id)->first();
			 if($pro->id!=''){
				 if($pro->sale_price>0){
				 $now = Carbon::now();
				 $newfr = explode('-',$pro->date_from);
				 $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
				 $newse = explode('-',$pro->date_to);
				 $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
				 if($now->between($first, $sec)){            
				   $prod_price=$pro->sale_price;
				   $percent=( ( $prod_price - $pro->price) / $pro->price) * 100;
				   $percent=number_format($percent, 2);
				   $percent=abs($percent);
				   $pro->prod_price= $prod_price;
				   $pro->percent=$percent;
				 
				 }else{
				   $prod_price='';
				   $pro->prod_price= "";
				   $pro->percent="";
				 }
				}        
				 $all_img = DB::table('product_images')                  
						 ->where('product_id','=',$pro->id)->orderBy('product_images.def', 'desc')
						 ->get();
				 $pro->all_img =$all_img;             
				 $review = DB::table('review')                  
						 ->where('product_id','=',$pro->id)->where('status','=','Active')
						 ->avg('rating');
				 $pro->avg_review= $review;
				 $slug= DB::table('slug')->where('table_id','=',$pro->id)->where('table_name','=','product')->first(); 
			 if(isset($slug->slug_name) && $slug->slug_name!='')
			  {
					$pro->slug='#/'.$slug->slug_name;
			  } else{
					$pro->slug='#/product/'.$pro->id;
			  } 
			  $data['popular'][]=$pro;
		  }
	  }
	 
      $result = DB::table('product')                  
                     ->where('product.status','=','Active')
                     ->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')               
                    ->orderBy('product.created_at', 'desc')->get();
        
			 
       foreach($result as $key=>$value){
            if($value->sale_price>0){
             $now = Carbon::now();
             $newfr = explode('-',$value->date_from);
             $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
             $newse = explode('-',$value->date_to);
             $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
             if($now->between($first, $sec)){            
               $prod_price=$value->sale_price;
               $percent=( ( $prod_price - $value->price) / $value->price) * 100;
               $percent=number_format($percent, 2);
               $percent=abs($percent);
               $result[$key]->prod_price= $prod_price;
               $result[$key]->percent=$percent;
             
             }else{
               $prod_price='';
               $result[$key]->prod_price= "";
               $result[$key]->percent="";
             }
            }        
             $all_img = DB::table('product_images')                  
                     ->where('product_id','=',$value->id)->orderBy('product_images.def', 'desc')
                     ->get();
             $result[$key]->all_img =$all_img;             
             $review = DB::table('review')                  
                     ->where('product_id','=',$value->id)->where('status','=','Active')
                     ->avg('rating');
             $result[$key]->avg_review= $review;
             $slug= DB::table('slug')->where('table_id','=',$value->id)->where('table_name','=','product')->first(); 
	     if(isset($slug->slug_name) && $slug->slug_name!='')
	      {
		$result[$key]->slug='#/'.$slug->slug_name;
	      } else{
		$result[$key]->slug='#/product/'.$value->id;
	      } 
          
       } 
       $data['new_pro']=$result;
	   $tabsarray=array();
       $fetur_categ= DB::table('featured_category')                  
                     ->where('featured_category.status','=',1)                     
                     ->where('featured_category.position','=',0)               
                    ->orderBy('featured_category.sort', 'asc')
                    ->get();
          $categ=array();  		            
         foreach($fetur_categ as $key=>$value){                         
    	   $fetur_categ[$key]->category=DB::table('categorys')                  
                     ->where('status','=','Active')
                     ->where('is_delete','=',0)
                     ->where('id','=',$value->category_id)
                     ->first(); 
            $subcat_data= explode(",",$value->subcategory_id)   ;      
            foreach($subcat_data as $key1=>$value1){  
			                 
                    $subcates =DB::table('categorys')->where('status','=','Active')->where('is_delete','=',0)->where('id','=',$value1)->first(); 
					if($key1==0){
						$tabsarray[$value->id]=$subcates->id;
					}
                                         $resultpro = DB::table('product')                  
                     ->where('product.status','=','Active')
                     ->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where(function ($query)  use ( $subcates) {$query->where('product.pro_category_id', 'like', '%,'. $subcates->id.',%')
                     ->orWhere('product.pro_category_id', 'like', ''.$subcates->id.',%')->orWhere('product.pro_category_id', 'like', '%,'. $subcates->id.'')->orWhere('product.pro_category_id', 'like', ''. $subcates->id.'');})->orderBy('product.created_at', 'desc')->get();          
		       foreach($resultpro as $key2=>$value2){
		            if($value2->sale_price>0){
		             $now = Carbon::now();
		             $newfr = explode('-',$value2->date_from);
		             $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
		             $newse = explode('-',$value2->date_to);
		             $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
		             if($now->between($first, $sec)){            
		               $prod_price=$value2->sale_price;
		               $percent=( ( $prod_price - $value2->price) / $value2->price) * 100;
		               $percent=number_format($percent, 2);
		               $percent=abs($percent);
		               $resultpro[$key2]->prod_price= $prod_price;
		              $resultpro[$key2]->percent=$percent;
		             
		             }else{
		               $prod_price='';
		               $resultpro[$key2]->prod_price= "";
		               $resultpro[$key2]->percent="";
		             }
		            }        
		             $all_img = DB::table('product_images')                  
		                     ->where('product_id','=',$value2->id)->orderBy('product_images.def', 'desc')
		                     ->get();
		             $resultpro[$key2]->all_img =$all_img;  
		             $review = DB::table('review')                  
		                     ->where('product_id','=',$value2->id)->where('status','=','Active')
		                     ->avg('rating');
		            $count_review =  DB::table('review')                  
		                     ->where('product_id','=',$value2->id)->where('status','=','Active')
		                     ->count();
		             $resultpro[$key2]->avg_review= $review; 
		             $resultpro[$key2]->count_review=  $count_review; 
		            		             
		             $slug= DB::table('slug')->where('table_id','=',$value2->id)->where('table_name','=','product')->first(); 
			     if(isset($slug->slug_name) && $slug->slug_name!='')
			      {
					$resultpro[$key2]->slug='#/'.$slug->slug_name;
			      } else{
					$resultpro[$key2]->slug='#/product/'.$value2->id;
			      } 
		          
		       }   
			     $subcates->products=$resultpro;
			     $fetur_categ[$key]->subcats[$key1]=$subcates;
		  
            }
           }                        
               $data['feature_cat_upr']= $fetur_categ;      
			    $fetur_categ1= DB::table('featured_category')                  
                     ->where('featured_category.status','=',1)                     
                     ->where('featured_category.position','=',1)               
                    ->orderBy('featured_category.sort', 'asc')
                    ->get();
          $categ=array();            
         foreach($fetur_categ1 as $key=>$value){                         
    	   $fetur_categ1[$key]->category=DB::table('categorys')                  
                     ->where('status','=','Active')
                     ->where('is_delete','=',0)
                     ->where('id','=',$value->category_id)
                     ->first(); 
            $subcat_data= explode(",",$value->subcategory_id)   ;      
            foreach($subcat_data as $key1=>$value1){  
			                 
                    $subcates =DB::table('categorys')->where('status','=','Active')->where('is_delete','=',0)->where('id','=',$value1)->first();                   if($key1==0){
						$tabsarray[$value->id]=$subcates->id;
					}
					 $resultpro = DB::table('product')                  
                     ->where('product.status','=','Active')
                     ->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where(function ($query)  use ( $subcates) {$query->where('product.pro_category_id', 'like', '%,'. $subcates->id.',%')
                     ->orWhere('product.pro_category_id', 'like', ''.$subcates->id.',%')->orWhere('product.pro_category_id', 'like', '%,'. $subcates->id.'')->orWhere('product.pro_category_id', 'like', ''. $subcates->id.'');})->orderBy('product.created_at', 'desc')->get();          
		       foreach($resultpro as $key2=>$value2){
		            if($value2->sale_price>0){
		             $now = Carbon::now();
		             $newfr = explode('-',$value2->date_from);
		             $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
		             $newse = explode('-',$value2->date_to);
		             $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
		             if($now->between($first, $sec)){            
		               $prod_price=$value2->sale_price;
		               $percent=( ( $prod_price - $value2->price) / $value2->price) * 100;
		               $percent=number_format($percent, 2);
		               $percent=abs($percent);
		               $resultpro[$key2]->prod_price= $prod_price;
		              $resultpro[$key2]->percent=$percent;
		             
		             }else{
		               $prod_price='';
		               $resultpro[$key2]->prod_price= "";
		               $resultpro[$key2]->percent="";
		             }
		            }        
		             $all_img = DB::table('product_images')                  
		                     ->where('product_id','=',$value2->id)->orderBy('product_images.def', 'desc')
		                     ->get();
		             $resultpro[$key2]->all_img =$all_img;  
		             $review = DB::table('review')                  
		                     ->where('product_id','=',$value2->id)->where('status','=','Active')
		                     ->avg('rating');
		             $count_review =  DB::table('review')                  
		                     ->where('product_id','=',$value2->id)->where('status','=','Active')
		                     ->count();
		             $resultpro[$key2]->avg_review= $review; 
		             $resultpro[$key2]->count_review=  $count_review; 
		             $slug= DB::table('slug')->where('table_id','=',$value2->id)->where('table_name','=','product')->first(); 
			     if(isset($slug->slug_name) && $slug->slug_name!='')
			      {
					$resultpro[$key2]->slug='#/'.$slug->slug_name;
			      } else{
					$resultpro[$key2]->slug='#/product/'.$value2->id;
			      } 
		          
		       }   
			     $subcates->products=$resultpro;
			     $fetur_categ1[$key]->subcats[$key1]=$subcates;
		  
            }
           }                        
               $data['feature_cat_lwr']= $fetur_categ1;  
			   $data['tabs']= $tabsarray;          
                                                   		    
	return $data;	   
   }
     
   
}

