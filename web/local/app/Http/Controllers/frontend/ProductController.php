<?php
namespace App\Http\Controllers\frontend;
use Illuminate\Routing\Controller as BaseController;
use Carbon\Carbon;
use DB;
use App\Category;
use App\Product;
use App\ProductAttribute;
use App\ProductImage;
use App\ProductTags;
use App\ProductVariation;
use App\Option;
use App\Review;
use App\ProductView;
use App\User;
use Session;
use Request;
use Response;
use Image;
use Validator;
use App\Newsletter; 
use App\Http\Controllers\cart\Cart;
use Auth;
class ProductController extends BaseController
{
   //delete compare
   public function compare_delete(){
       $id =Request::input('product');
       $comparepros = Session::get('compare_prod');
       $key = array_search($id , array_column( $comparepros, 'id'));
       unset($comparepros [$key]);
       Session::put('compare_prod', $comparepros); 
       $list[]="success";
       $list[]="You have successfully deleted product in compare list.";
       $list[]=Session::get('compare_prod');
       return $list;
       
   }
   // add compare
    public function add_compare(){
            
              $product =Request::input('product');
              if( Session::has('compare_prod') ){
                  $comparepros = Session::get('compare_prod');
                    
                  $key = array_search($product['id'] , array_column( $comparepros, 'id'));                 
                  if( is_numeric($key)){ 
                   
                  } else
                  {  
                    $comparepros[] = $product;
                  }
                             
               }else{     
	            $comparepros[]=$product;
	       }
	       Session::put('compare_prod', $comparepros); 
	       $list[]="success";
	       $list[]="You have successfully added product in compare list.";
	       $list[]=Session::get('compare_prod');
	       return $list;
            
	}
   // get product template      
   public function get_pro($id){
    
        return view('frontend/product_details')->with('title','Product');
	}
	// get product data
    public function get_prod_data($id){
       if (Auth::check()) {
	    $user_id= Auth::id();
		}else{
			$user_id =0;
		}
       ProductView::create(['product_id' => $id,'no_view' => '1','user_id' =>$user_id]);  
	  
       $data['prod_data']= DB::table('product')
        		->where('is_delete','=',0)
        		->where('stock_status','=','In Stock')
        		->where('status','=','Active')
        		->where('no_stock','>',0)
        		->where('id','=',$id)
        		->first(); 
         $sel_id=$data['prod_data']->seller_id;		
         $data['seller_details']=DB::table('users')->where('id','=',$sel_id)->first(); 		
        if($data['prod_data']->sale_price>0){
             $now = Carbon::now();
             $newfr = explode('-',$data['prod_data']->date_from);
             $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
             $newse = explode('-',$data['prod_data']->date_to);
             $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
             if($now->between($first, $sec)){            
               $prod_price=$data['prod_data']->sale_price;
               $percent=( ( $prod_price - $data['prod_data']->price) / $data['prod_data']->price) * 100;
               $percent=number_format($percent, 2);
               $percent=abs($percent);
               $data['prod_data']->prod_price= $prod_price;
               $data['prod_data']->percent=$percent;
             
             }else{
               $prod_price='';
               $data['prod_data']->prod_price= "";
               $data['prod_data']->percent="";
             }
            }
        		      
        $pid=$data['prod_data']->id;
        $data['images']= DB::table('product_images')->where('product_id','=',$id)->orderBy('def','desc')->get(); 
        $data['review']= DB::table('review')->where('product_id','=',$id)->where('status','=','Active')->get(); 
        $data['avg_review'] = DB::table('review')                  
		                     ->where('product_id','=',$id)->where('status','=','Active')
		                     ->avg('rating');
        $data['prod_attr'] =DB::table('product_attribute')->where('product_id','=',$pid)->get(); 
        if( $data['prod_data']->pro_datatype_id=='2'){        
        $data['options']=array();   	
        foreach($data['prod_attr'] as $key=>$value){  
		if($value->variation_status != '0'){             
                $option_name_id=$value->option_name_id; 
                $opt_val_all =DB::table('pro_option')->where('id','=',$option_name_id)->where('is_delete','=','0')->first(); 
                if($opt_val_all !=''){
                $data['options'][$key]=$opt_val_all ;               
        	    $option_arr = array();
				if($value->option_value_ids) { 
				$option_arr = explode(',',$value->option_value_ids);		   
				   foreach($option_arr as $ky=>$opt_val){		     
					   $pro_opt_val= DB::table('pro_option')->where('id','=',$opt_val)->where('is_delete','=','0')->first();  
					   if($pro_opt_val !=''){ 
						  $data['options'][$key]->option_values[$ky]=$pro_opt_val;
					  		 }
				  		 }
					}
				}
	   		}
	   	  }
        } 
		$data['attr_grp']=array(); 
		foreach($data['prod_attr'] as $key=>$value){ 
		        $options=array();
		        $option_name_id=$value->option_name_id; 
				
                $opt_val_all =DB::table('pro_option')->where('id','=',$option_name_id)->where('is_delete','=','0')->first(); 
				$opt_val_all_grp =DB::table('pro_option')->where('id','=',$opt_val_all->parent_id)->where('is_delete','=','0')->first();
				if (!array_key_exists($opt_val_all_grp->id,$data['attr_grp']))
				{
				 $data['attr_grp'][$opt_val_all_grp->id] = $opt_val_all_grp;
				}			
				
                if($opt_val_all !=''){
                $options[$key]=$opt_val_all ;               
        	    $option_arr = array();
				if($value->option_value_ids) { 
				$option_arr = explode(',',$value->option_value_ids);		   
				   foreach($option_arr as $ky=>$opt_val){		     
					   $pro_opt_val= DB::table('pro_option')->where('id','=',$opt_val)->where('is_delete','=','0')->first();  
					   if($pro_opt_val !=''){ 
						 $options[$key]->option_values[$ky]=$pro_opt_val;
					  		 }
				  		 }
					}
		       } 
			    $data['attr_grp'][$opt_val_all_grp->id]->options=$options;
		}  
		$rela_pro=array();
		 if($data['prod_data']->cross_sell!='')
				  {
				     $exp_pro = explode(',',$data['prod_data']->cross_sell);
					 foreach($exp_pro as $pro_id)
					 {
					   $rela_pro1=DB::table('product')->where('product.status','=','Active')->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where('product.id','=',$pro_id)->first();
					  if($rela_pro1->sale_price>0){
		             $now = Carbon::now();
		             $newfr = explode('-',$rela_pro1->date_from);
		             $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
		             $newse = explode('-',$rela_pro1->date_to);
		             $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
		             if($now->between($first, $sec)){            
		               $prod_price=$rela_pro1->sale_price;
		               $percent=( ( $prod_price - $rela_pro1->price) / $rela_pro1->price) * 100;
		               $percent=number_format($percent, 2);
		               $percent=abs($percent);
		               $rela_pro1->prod_price= $prod_price;
		               $rela_pro1->percent=$percent;
		             
		             }else{
		               $prod_price='';
		               $rela_pro1->prod_price= "";
		               $rela_pro1->percent="";
		             }
		            }        
		             $all_img = DB::table('product_images')                  
		                     ->where('product_id','=',$pro_id)->orderBy('product_images.def', 'desc')
		                     ->get();
		             $rela_pro1->all_img =$all_img;   
		             $review = DB::table('review')                  
		                     ->where('product_id','=',$pro_id)->where('status','=','Active')
		                     ->avg('rating');
		             $count_review =  DB::table('review')                  
		                     ->where('product_id','=',$pro_id)->where('status','=','Active')
		                     ->count();
		             $rela_pro1->avg_review= $review; 
		             $rela_pro1->count_review=  $count_review; 
		             $slug= DB::table('slug')->where('table_id','=',$pro_id)->where('table_name','=','product')->first(); 
						 if(isset($slug->slug_name) && $slug->slug_name!='')
						  {
							 $rela_pro1->slug='#/'.$slug->slug_name;
						  } else{
							 $rela_pro1->slug='#/product/'.$pro_id;
						  }
						$rela_pro[]= $rela_pro1;
					 }
				  } 
		 $data['rel_pros']=$rela_pro;
		 $order_pros = DB::table('order_product')->select('*', DB::raw('COUNT(*) as products_count'))->groupBy('product_id')->orderBy('products_count','desc')->where('seller_id','=',$sel_id)->limit(4)->get();
	    $data['popular'] = array();
		$order_pros = DB::table('order_product')->where('order_product.product_id','=',$id)->get();
		$data['total_sold']=0;
		foreach($order_pros as $ky_ord=>$vy_ord)
		{
		  $data['total_sold']=$data['total_sold']+$vy_ord->quantity;
		}
	  foreach($order_pros as $ky=>$vy)
	  {
	         $pro = DB::table('product')                  
                     ->where('product.status','=','Active')->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where('product.id','=',$vy->product_id)->first();
			 if($pro->id!=''){
				 if($pro->sale_price>0){
				 $now = Carbon::now();
				 $newfr = explode('-',$pro->date_from);
				 $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
				 $newse = explode('-',$pro->date_to);
				 $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
				 if($now->between($first, $sec)){            
				   $prod_price=$pro->sale_price;
				   $percent=( ( $prod_price - $pro->price) / $pro->price) * 100;
				   $percent=number_format($percent, 2);
				   $percent=abs($percent);
				   $pro->prod_price= $prod_price;
				   $pro->percent=$percent;
				 
				 }else{
				   $prod_price='';
				   $pro->prod_price= "";
				   $pro->percent="";
				 }
				}        
				 $all_img = DB::table('product_images')                  
						 ->where('product_id','=',$pro->id)->orderBy('product_images.def', 'desc')
						 ->get();
				 $pro->all_img =$all_img;             
				 $review = DB::table('review')                  
						 ->where('product_id','=',$pro->id)->where('status','=','Active')
						 ->avg('rating');
				 $pro->avg_review= $review;
				 $slug= DB::table('slug')->where('table_id','=',$pro->id)->where('table_name','=','product')->first(); 
			 if(isset($slug->slug_name) && $slug->slug_name!='')
			  {
					$pro->slug='#/'.$slug->slug_name;
			  } else{
					$pro->slug='#/product/'.$pro->id;
			  } 
			  $data['popular'][]=$pro;
		  }
	  }
	    return $data;
				
    }
    
    public function add_review(){
		      $validator = Validator::make(Request::all(), [
		            'revw_data.name' => 'required',
		            'revw_data.reviews' => 'required',           
		             'revw_data.rating' => 'required'
		        ]);
		    $friendly_names = array(
		      'revw_data.name' => 'User Name',
		      'revw_data.reviews' => 'Review Text',
		      'revw_data.rating' => 'Rating',
		
		      );
		 $validator->setAttributeNames($friendly_names);
		        if ($validator->fails()) {
		          $list[]='error';
		         $msg=$validator->errors()->all();
		         $list[]=$msg;
		         return $list;
		        }
		  
		  Review::create(['product_id' =>Request::input('prod_id'),'name' => Request::input('revw_data.name'),'review' =>Request::input('revw_data.reviews'),'rating'=>Request::input('revw_data.rating'),'status' =>'Inactive']);  
                  $list[]='success';
		  $msg="Thank you for giving your review.";
		  $list[]=$msg;
		  return $list;     		        
    }
    	
}

