<?php
namespace App\Http\Controllers\frontend;
use Illuminate\Routing\Controller as BaseController;
use Carbon\Carbon;
use DB;
use Category;
use Validator;
use Auth;
use App\User;
use Session;
use Request as ReqInp;
use Response;
use Image;
use Illuminate\Http\Request;
use Socialite;
use App\Http\Controllers\cart\Cart;
class UserController extends BaseController
{
   
        public function login(){	
      	
		return view('frontend/popuplogin');
	}
	
	
	public function user_login (){
		$validator = Validator::make(ReqInp::all(), [          
		   'login_data.email' => 'required|email',
		   'login_data.password' => 'required|min:6',
		   
				]);
		  
		   if ($validator->fails()) {
			       $list[]='error';
			       $msg=$validator->errors()->all();
				   $list[]=$msg;
				   return $list;
				 
			 
		  }else{   
                      if (Auth::attempt(['email' => ReqInp::input('login_data.email'), 'password' => ReqInp::input('login_data.password'),'status'=>'Active','role'=>3,'is_delete'=>'0'])) {
                        // Authentication passed...
                       if (Session::has('redirect_url'))
                       {
                                        $value1 = Session::get('redirect_url');
					$list[]='success';
					$msgs[]=$value1;
					$list[]=$msgs;
					$list[]=Auth::user();
					return $list;
                           
                        }
                        else{
					$list[]='success';
					$msgs[]='#/';
					$list[]=$msgs;
					return $list;
                          
                         }
                    }else
                    {
						 $list[]='error';
						 $msgs[]='Your E-mail or Password is not correct';
						 $list[]=$msgs;
						return $list;
						  
                    
                    }
		  }
	}
	        public function googleLogin(Request $request)  {
            $google_redirect_url = route('glogin');
            $gClient = new \Google_Client();
            $gClient->setApplicationName(config('services.google.app_name'));
            $gClient->setClientId(config('services.google.client_id'));
            $gClient->setClientSecret(config('services.google.client_secret'));
            $gClient->setRedirectUri($google_redirect_url);
            $gClient->setDeveloperKey(config('services.google.api_key'));
            $gClient->setScopes(array(
                'https://www.googleapis.com/auth/plus.me',
                'https://www.googleapis.com/auth/userinfo.email',
                'https://www.googleapis.com/auth/userinfo.profile',
            ));
            $google_oauthV2 = new \Google_Service_Oauth2($gClient);
            if ($request->get('code')){
                $gClient->authenticate($request->get('code'));
                $request->session()->put('token', $gClient->getAccessToken());
            }
            if ($request->session()->get('token'))
            {
                $gClient->setAccessToken($request->session()->get('token'));
            }
            if ($gClient->getAccessToken())
            {
                //For logged in user, get details from google using access token
                $guser = $google_oauthV2->userinfo->get();  
                   
                    $request->session()->put('name', $guser['name']);
                    if ($user =User::where('email',$guser['email'])->first())
                    {
                        Auth::login($user);
                    }else{
                      $user = User::create(['image'=> $guser->picture,'fname' => $guser->givenName,'lname' => $guser->familyName,'username' => $guser->givenName ,'email' =>$guser->email,'password' =>'','gender' => 'male','website' => '','mobile' =>'','home_number' =>'','address'=>'','nationality' =>'','country' =>'','state' =>'','city' =>'','bio' =>'','status' =>'Active','role'=>'3']); 
                      Auth::login($user);
                    }               
             return redirect('/');          
            } else
            {
                //For Guest user, get google login url
                $authUrl = $gClient->createAuthUrl();
                return redirect('/');        
            }
        }
        
     public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }
 
    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
        } catch (Exception $e) {
            return redirect('facebook');
        }
 
        $authUser = $this->findOrCreateUser($user);
 
        Auth::login($authUser, true);
 
        return redirect()->route('home');
    }
 
    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     */
    private function findOrCreateUser($facebookUser)
    {
        $authUser = User::where('facebook_id', $facebookUser->id)->first();
 
        if ($authUser){
            return $authUser;
        }
 
        return User::create([
            'name' => $facebookUser->name,
            'email' => $facebookUser->email,
            'facebook_id' => $facebookUser->id,
            'avatar' => $facebookUser->avatar
        ]);
    }
    
    public function logout(){
           Auth::logout();
           // mini cart
           $cart = new Cart();
           $cart->clear();
           $minicart['cart_data']= $cart->getItems();
	   $minicart['total']=$cart->getTotal();
	   $minicart['total_num']=$cart->totalQuantity();	   
	   $data['minicart']= $minicart;
	   return $data;
    }
		
}	