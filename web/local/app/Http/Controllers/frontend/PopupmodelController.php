<?php
namespace App\Http\Controllers\frontend;
use Illuminate\Routing\Controller as BaseController;
use Carbon\Carbon;
use DB;
use Category;
use App\User;
use Session;
use Request;
use Response;
use Image;
class PopupmodelController extends BaseController
{
    public function index(){	
	return view('frontend/modal');
	}
    public function prod_model($id){	               
	return view('frontend/popupmodel');
	}
	
   public function pro_mod_data($id){
	    $data['prod_data']= DB::table('product')
        		->where('is_delete','=',0)
        		->where('stock_status','=','In Stock')
        		->where('status','=','Active')
        		->where('no_stock','>',0)
        		->where('id','=',$id)
        		->first(); 
         $sel_id=$data['prod_data']->seller_id;		
         $data['seller_details']=DB::table('users')->where('id','=',$sel_id)->first(); 		
        if($data['prod_data']->sale_price>0){
             $now = Carbon::now();
             $newfr = explode('-',$data['prod_data']->date_from);
             $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
             $newse = explode('-',$data['prod_data']->date_to);
             $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
             if($now->between($first, $sec)){            
               $prod_price=$data['prod_data']->sale_price;
               $percent=( ( $prod_price - $data['prod_data']->price) / $data['prod_data']->price) * 100;
               $percent=number_format($percent, 2);
               $percent=abs($percent);
               $data['prod_data']->prod_price= $prod_price;
               $data['prod_data']->percent=$percent;
             
             }else{
               $prod_price='';
               $data['prod_data']->prod_price= "";
               $data['prod_data']->percent="";
             }
            }
        		      
        $pid=$data['prod_data']->id;
        $data['images']= DB::table('product_images')->where('product_id','=',$id)->orderBy('def','desc')->get(); 
        $data['review']= DB::table('review')->where('product_id','=',$id)->where('status','=','Active')->get(); 
        $data['avg_review'] = DB::table('review')                  
		                     ->where('product_id','=',$id)->where('status','=','Active')
		                     ->avg('rating');
        $order_pros = DB::table('order_product')->where('order_product.product_id','=',$id)->get();
	$data['total_sold']=0;
	    foreach($order_pros as $ky_ord=>$vy_ord)
	{
		  $data['total_sold']=$data['total_sold']+$vy_ord->quantity;
	}
		
        if( $data['prod_data']->pro_datatype_id=='2'){
        $data['prod_attr'] =DB::table('product_attribute')->where('product_id','=',$pid)->get(); 
        $data['options']=array();   	
        foreach($data['prod_attr'] as $key=>$value){               
                $option_name_id=$value->option_name_id; 
                $opt_val_all =DB::table('pro_option')->where('id','=',$option_name_id)->where('is_delete','=','0')->first(); 
                if($opt_val_all !=''){
                $data['options'][$key]=$opt_val_all ;               
        	$option_arr = array();
		if($value->option_value_ids) { 
		$option_arr = explode(',',$value->option_value_ids);		   
		   foreach($option_arr as $ky=>$opt_val){		     
		       $pro_opt_val= DB::table('pro_option')->where('id','=',$opt_val)->where('is_delete','=','0')->first();  
		       if($pro_opt_val !=''){ 
		          $data['options'][$key]->option_values[$ky]=$pro_opt_val;
		       }
		   }
		}
		}
	   }
        }        
	return $data;
	}		
}	