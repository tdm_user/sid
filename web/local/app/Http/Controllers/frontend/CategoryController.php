<?php
namespace App\Http\Controllers\frontend;
use Illuminate\Routing\Controller as BaseController;
use Carbon\Carbon;
use DB;
use Category;
use App\User;
use Session;
use Request;
use Response;
use Image;
use Validator;
use App\Newsletter; 
use App\Http\Controllers\cart\Cart;
use Auth;
class CategoryController extends BaseController
{
       
	public function get_cat($id){
		$cate=DB::table('categorys')->where('is_delete','=',0)->where('status','=','Active')->where('id','=',$id)->first();
		if($cate->parent_id == '0')
		{
		 return view('frontend/category');
		}
		else{
		 return view('frontend/sub_category');
		}
	}
	
	public function get_cat_content($id){
		$data['cat']=DB::table('categorys')->where('is_delete','=',0)->where('status','=','Active')->where('id','=',$id)->first();
		$data['subcats']=DB::table('categorys')->where('is_delete','=',0)->where('status','=','Active')->where('parent_id','=',$id)->get();
		$data['products']= DB::table('product')                  
                     ->where('product.status','=','Active')
                     ->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where(function ($query)  use ($id) {$query->where('product.pro_category_id', 'like', '%,'.$id.',%')->orWhere('product.pro_category_id', 'like', ''.$id.',%')->orWhere('product.pro_category_id', 'like', '%,'.$id.'')->orWhere('product.pro_category_id', 'like', ''.$id.'');})->get(); 
		   foreach($data['products'] as $key=>$value){
		            if($value->sale_price>0){
		             $now = Carbon::now();
		             $newfr = explode('-',$value->date_from);
		             $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
		             $newse = explode('-',$value->date_to);
		             $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
		             if($now->between($first, $sec)){            
		               $prod_price=$value->sale_price;
		               $percent=( ( $prod_price - $value->price) / $value->price) * 100;
		               $percent=number_format($percent, 2);
		               $percent=abs($percent);
		               $data['products'][$key]->prod_price= $prod_price;
		               $data['products'][$key]->percent=$percent;
		             
		             }else{
		               $prod_price='';
		               $data['products'][$key]->prod_price= "";
		               $data['products'][$key]->percent="";
		             }
		            }        
		             $all_img = DB::table('product_images')                  
		                     ->where('product_id','=',$value->id)->orderBy('product_images.def', 'desc')
		                     ->get();
		             $data['products'][$key]->all_img =$all_img;   
		            
		             $review = DB::table('review')                  
		                     ->where('product_id','=',$value->id)->where('status','=','Active')
		                     ->avg('rating');
		             $count_review =  DB::table('review')                  
		                     ->where('product_id','=',$value->id)->where('status','=','Active')
		                     ->count();
			     $count_popularity =  DB::table('product_views')->where('product_id','=',$value->id)->count();
		             $data['products'][$key]->count_popularity= $count_popularity; 
		             $data['products'][$key]->avg_review= $review; 
		             $data['products'][$key]->count_review=  $count_review; 
		            		             
		             $slug= DB::table('slug')->where('table_id','=',$value->id)->where('table_name','=','product')->first(); 
			     if(isset($slug->slug_name) && $slug->slug_name!='')
			      {
					 $data['products'][$key]->slug='#/'.$slug->slug_name;
			      } else{
					 $data['products'][$key]->slug='#/product/'.$value->id;
			      } 
				  $pro_attr=DB::table('product_attribute')->where('product_attribute.product_id','=',$value->id)->get(); 
				  $opt_arr=array(); 
				  foreach($pro_attr as $atr_ky => $atr_vy)
				  {
				    $opt_val = explode(',',$atr_vy->option_value_ids);
				    $opt_arr = array_merge($opt_arr,$opt_val);
				  }
				  $data['products'][$key]->options=$opt_arr ;
		                 $rela_pro=array();
		          if($value->up_sell!='')
				  {
				     $exp_pro = explode(',',$value->up_sell);
					 foreach($exp_pro as $pro_id)
					 {
					   $rela_pro1=DB::table('product')->where('product.status','=','Active')->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where('product.id','=',$pro_id)->first();
					  if($rela_pro1->sale_price>0){
		             $now = Carbon::now();
		             $newfr = explode('-',$rela_pro1->date_from);
		             $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
		             $newse = explode('-',$rela_pro1->date_to);
		             $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
		             if($now->between($first, $sec)){            
		               $prod_price=$rela_pro1->sale_price;
		               $percent=( ( $prod_price - $rela_pro1->price) / $rela_pro1->price) * 100;
		               $percent=number_format($percent, 2);
		               $percent=abs($percent);
		               $rela_pro1->prod_price= $prod_price;
		               $rela_pro1->percent=$percent;
		             
		             }else{
		               $prod_price='';
		               $rela_pro1->prod_price= "";
		               $rela_pro1->percent="";
		             }
		            }        
		             $all_img = DB::table('product_images')                  
		                     ->where('product_id','=',$pro_id)->orderBy('product_images.def', 'desc')
		                     ->get();
		             $rela_pro1->all_img =$all_img;   
					 $slug= DB::table('slug')->where('table_id','=',$pro_id)->where('table_name','=','product')->first(); 
						 if(isset($slug->slug_name) && $slug->slug_name!='')
						  {
							 $rela_pro1->slug='#/'.$slug->slug_name;
						  } else{
							 $rela_pro1->slug='#/product/'.$pro_id;
						  }
						$rela_pro[]= $rela_pro1;
					 }
				  }
				  $data['products'][$key]->rela_pro=$rela_pro;
		       }
		$data['brands']=DB::table('brands')->where('is_delete','=',0)->where('status','=','Active')->get();
		foreach($data['brands'] as $ky_brnd=>$vy_brnd)
		{
		  $slug= DB::table('slug')->where('table_id','=',$vy_brnd->id)->where('table_name','=','brands')->first(); 
		   if(isset($slug->slug_name) && $slug->slug_name!='')
		    {
			 $data['brands'][$ky_brnd]->slug='#/'.$slug->slug_name;
		    } else{
		         $data['brands'][$ky_brnd]->slug='#/brands/'.$vy_brnd->id;
		    } 
		}
		$tabsarray=array();
		foreach($data['subcats'] as $ky=>$vy)
		{  
	           $subcates=array();	
	           $slug= DB::table('slug')->where('table_id','=',$vy->id)->where('table_name','=','categorys')->first(); 
		   if(isset($slug->slug_name) && $slug->slug_name!='')
		    {
			 $data['subcats'][$ky]->slug='#/'.$slug->slug_name;
		    } else{
		         $data['subcats'][$ky]->slug='#/categorys/'.$vy->id;
		    } 
		   $subcates=DB::table('categorys')->where('is_delete','=',0)->where('status','=','Active')->where('parent_id','=',$vy->id)->get();
		   foreach( $subcates as $sub_ky=>$sub_vy){ 
		   if($sub_ky==0){
			$tabsarray[$vy->id]=$sub_vy->id;
		    }
		    $slug= DB::table('slug')->where('table_id','=',$sub_vy->id)->where('table_name','=','categorys')->first(); 
		   if(isset($slug->slug_name) && $slug->slug_name!='')
		    {
			 $subcates[$sub_ky]->slug='#/'.$slug->slug_name;
		    } else{
		         $subcates[$sub_ky]->slug='#/categorys/'.$sub_vy->id;
		    } 
		     $result = DB::table('product')                  
                     ->where('product.status','=','Active')
                     ->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where(function ($query)  use ($sub_vy) {$query->where('product.pro_category_id', 'like', '%,'.$sub_vy->id.',%')
                     ->orWhere('product.pro_category_id', 'like', ''.$sub_vy->id.',%')->orWhere('product.pro_category_id', 'like', '%,'.$sub_vy->id.'')->orWhere('product.pro_category_id', 'like', ''.$sub_vy->id.'');})->orderBy('product.created_at', 'desc')->get();          
		       foreach($result as $key=>$value){
		            if($value->sale_price>0){
		             $now = Carbon::now();
		             $newfr = explode('-',$value->date_from);
		             $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
		             $newse = explode('-',$value->date_to);
		             $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
		             if($now->between($first, $sec)){            
		               $prod_price=$value->sale_price;
		               $percent=( ( $prod_price - $value->price) / $value->price) * 100;
		               $percent=number_format($percent, 2);
		               $percent=abs($percent);
		               $result[$key]->prod_price= $prod_price;
		             $result[$key]->percent=$percent;
		             
		             }else{
		               $prod_price='';
		               $result[$key]->prod_price= "";
		               $result[$key]->percent="";
		             }
		            }        
		             $all_img = DB::table('product_images')                  
		                     ->where('product_id','=',$value->id)->orderBy('product_images.def', 'desc')
		                     ->get();
		             $result[$key]->all_img =$all_img;   
		            
		             $review = DB::table('review')                  
		                     ->where('product_id','=',$value->id)->where('status','=','Active')
		                     ->avg('rating');
		             $count_review =  DB::table('review')                  
		                     ->where('product_id','=',$value->id)->where('status','=','Active')
		                     ->count();
		             $result[$key]->avg_review= $review; 
		             $result[$key]->count_review=  $count_review; 
		            		             
		             $slug= DB::table('slug')->where('table_id','=',$value->id)->where('table_name','=','product')->first(); 
			     if(isset($slug->slug_name) && $slug->slug_name!='')
			      {
					 $result[$key]->slug='#/'.$slug->slug_name;
			      } else{
					 $result[$key]->slug='#/product/'.$value->id;
			      } 
		          
		       }   
			    $subcates[$sub_ky]->products=$result;
				$order_pros = DB::table('order_product')->select('*', DB::raw('COUNT(*) as products_count'))->groupBy('order_product.product_id')->join('product', 'product.id', '=', 'order_product.product_id')->where(function ($query)  use ($sub_vy) {$query->where('product.pro_category_id', 'like', '%,'.$sub_vy->id.',%')
                     ->orWhere('product.pro_category_id', 'like', ''.$sub_vy->id.',%')->orWhere('product.pro_category_id', 'like', '%,'.$sub_vy->id.'')->orWhere('product.pro_category_id', 'like', ''.$sub_vy->id.'');})->orderBy('products_count','desc')->limit(4)->get();
	      $popular = array();
	  foreach($order_pros as $ky1=>$vy1)
	  {
	         $pro = DB::table('product')                  
                     ->where('product.status','=','Active')->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where('product.id','=',$vy1->product_id)->first();
			 if($pro->id!=''){
				 if($pro->sale_price>0){
				 $now = Carbon::now();
				 $newfr = explode('-',$pro->date_from);
				 $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
				 $newse = explode('-',$pro->date_to);
				 $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
				 if($now->between($first, $sec)){            
				   $prod_price=$pro->sale_price;
				   $percent=( ( $prod_price - $pro->price) / $pro->price) * 100;
				   $percent=number_format($percent, 2);
				   $percent=abs($percent);
				   $pro->prod_price= $prod_price;
				   $pro->percent=$percent;
				 
				 }else{
				   $prod_price='';
				   $pro->prod_price= "";
				   $pro->percent="";
				 }
				}        
				 $all_img = DB::table('product_images')                  
						 ->where('product_id','=',$pro->id)->orderBy('product_images.def', 'desc')
						 ->get();
				 $pro->all_img =$all_img;             
				 $review = DB::table('review')                  
						 ->where('product_id','=',$pro->id)->where('status','=','Active')
						 ->avg('rating');
				 $pro->avg_review= $review;
				 $slug= DB::table('slug')->where('table_id','=',$pro->id)->where('table_name','=','product')->first(); 
			 if(isset($slug->slug_name) && $slug->slug_name!='')
			  {
					$pro->slug='#/'.$slug->slug_name;
			  } else{
					$pro->slug='#/product/'.$pro->id;
			  } 
			 $subcates[$sub_ky]->popular[]=$pro;
		  }
	  }
		  }  
		
		   		    $data['subcats'][$ky]->subcat =  $subcates;
					
			
           
		}
		$data['tabs']=$tabsarray;
		$pro_opt_grp= DB::table('pro_option')                  
                     ->where('status','=','Active')
                     ->where('is_delete','=',0)->where(function ($query)  use ($id) {$query->where('pro_option.categorys_id', 'like', '%,'.$id.',%')->orWhere('pro_option.categorys_id', 'like', ''.$id.',%')->orWhere('pro_option.categorys_id', 'like', '%,'.$id.'')->orWhere('pro_option.categorys_id', 'like', ''.$id.'');})->get(); //print_r($pro_opt_grp);
                $pro_opt=array();
		foreach($pro_opt_grp as $key=>$value)
		{
		   $pro_opt1= DB::table('pro_option')->where('status','=','Active')->where('is_delete','=',0)->where('parent_id','=',$value->id)->get(); 
		   foreach($pro_opt1 as $ky1=>$vy1){
		      $pro_opt[]=$vy1;
		   }
		   foreach($pro_opt as $ky=>$vy)
		   {
		    
		  
		     $pro_opt[$ky]->options= DB::table('pro_option')->where('status','=','Active')->where('is_delete','=',0)->where('parent_id','=',$vy->id)->get(); 
		    
		   }
		} 
		$data['pro_options'] = $pro_opt;
		return $data;
		
	}
	
}
