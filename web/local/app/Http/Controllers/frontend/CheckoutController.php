<?php
namespace App\Http\Controllers\frontend;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\cart\Cart;
use Carbon\Carbon;
use DB;
use Category;
use App\User;
use App\Shipping;
use Session;
use Request;
use Response;
use Image;
use Validator;
use App\Newsletter; 
use Auth;
use App\Order;
use App\OrderProduct;
use App\Product;
Use Paypal;
use Redirect;
class CheckoutController extends BaseController
{
   
     private $_apiContext;

    public function __construct()
    {
        $this->_apiContext = Paypal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));

    }   
    // shipping page
    public function index(){
    return view('frontend/checkout');
    }
     // apply coupon
    public function apply_coupon(){
        $currentDate = date('Y-m-d');
        $coupon= Request::input('coupon');
        $coupon_table = DB::table('coupons')->where('is_delete','=','0')->where('coupon_status','=','Active')->where('expire_date','>',$currentDate)->where('coupon_name','like',$coupon)->first();
        print_r($coupon_table);
    }
    // cart items
    public function fetch_cart_data(){
	     $currentDate = date('Y-m-d');
	     $coupons = DB::table('coupons')->where('is_delete','=','0')->where('coupon_status','=','Active')->where('expire_date','>',$currentDate)->get();
		 $cart_item['coupons'] = $coupons;
		 
         if (Auth::check()) {
		    $user_id= Auth::id();
		    $address=DB::table('shipp_address')->where('ship_status','=','Active')->where('user_id','=',$user_id)->get();
		    foreach($address as $ky=>$vy)
		    {
			    $conty=DB::table('country')->where('id','=',$vy->ship_country)->first();
			    $state=DB::table('country')->where('id','=',$vy->ship_state)->first();
			    $city=DB::table('country')->where('id','=',$vy->ship_city)->first();
			    $vy->country=$conty->name;
			    $vy->state=$state->name;
			    $vy->city=$city->name;
		    }
		    $cart_item['logged_in']="yes";
		    $cart_item['addresses']=$address;
		}
		else{
		    $cart_item['logged_in']="no";
		    $cart_item['addresses']='';
		}
                $cart = new Cart();
                $cart_item['cart_data']= $cart->getItems();
                foreach($cart_item['cart_data'] as $ky=>$vy)
                {
                 $pro = DB::table('product')                  
                     ->where('product.status','=','Active')->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where('product.id','=',$vy->id)->first();
			       if($pro->id!=''){
					 if($pro->sale_price>0){
					 $now = Carbon::now();
					 $newfr = explode('-',$pro->date_from);
					 $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
					 $newse = explode('-',$pro->date_to);
					 $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
					 if($now->between($first, $sec)){            
					   $prod_price=$pro->sale_price;
					   $percent=( ( $prod_price - $pro->price) / $pro->price) * 100;
					   $percent=number_format($percent, 2);
					   $percent=abs($percent);
					   $vy->prod_price= $prod_price;
					   $vy->percent=$percent;
					 
					 }else{
					   $prod_price='';
					   $vy->prod_price= "";
					   $vy->percent="";
					 }
					 $vy->pri=$pro->price;
					}   
                }
			}
	        $cart_item['total']=$cart->getTotal();
	        $cart_item['total_num']=$cart->totalQuantity();
	        if (Auth::check()) {
		    $user_id= Auth::id();
		}else{
		    $user_id =0;
		}
	        return $cart_item;
    }
    
     // addaddress
     public function addaddress(){
          return view('frontend/add_address');
    }
    // editaddress
     public function editaddress($id){
          return view('frontend/edit_address');
    }
    //order confirm
    public function order_confirm($id)
    {
           return view('frontend/order_confirm');
    }
     //order confirm content
    public function confirm_order_content($id)
    {
         $conf = DB::table('order_detail')->where('id','=',$id)->first();
         $list[] =$conf;
         return $list;
    }
     // edit_address
     public function edit_address($id){ 
       	  $edit_address = DB::table('shipp_address')->where('id','=',$id)->first();  
       	  $return[] = $edit_address;	
          return  $return;  
    }
     // continue checkout
     public function continue_checkout(){ 
       	       $store_data=Request::input('store_data');
       	       if(isset($store_data['address'])){
	       	       Session::put('store_address', $store_data['address']);        	    
		       $list[]="success";
		       $list[]="You successfully set shipping address.";
	       }else{
	                $list[]="error";
	                $list[]="Please select shipping address.";
	       }	     
	       return $list;
    }
    // payment page
    public function checkout_payment(){
        return view('frontend/checkout_payment');
    }
    
    public function chargeCustomer($product_id, $product_price, $product_name, $token) {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if (!$this->isStripeCustomer()) {
            $customer = $this->createStripeCustomer($token);
        } else {
            $customer = \Stripe\Customer::retrieve(Auth::user()->stripe_id);
        }

        return $this->createStripeCharge($product_id, $product_price, $product_name, $customer);
    }

    /**
     * Create a Stripe charge.
     */
    public function createStripeCharge($product_id, $product_price, $product_name, $customer) {
        try {
            $charge = \Stripe\Charge::create(array(
                        "amount" => $product_price,
                        "currency" => "INR",
                        "customer" => $customer->id,
                        "description" => $product_name
            ));
        } catch (\Stripe\Error\Card $e) {
            return redirect()->route('index')->with('error', 'Your credit card was been declined. Please try again or contact us.');
        }
        return $this->postStoreOrder($product_id);
    }

    /**
     * Create a new Stripe customer for a given user.
     */
    public function createStripeCustomer($token) {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $customer = \Stripe\Customer::create(array("description" => Auth::user()->email,"source" => $token));

        Auth::user()->stripe_id = $customer->id;
        Auth::user()->save();
        return $customer;
    }
    // Store Data Post
     public function postStoreOrder($order_id) {
        
        $order= Order::find($order_id);
        $order->payment_status=2;
        $order->save();
        $list[]='#/checkout/order_confirm/'.$order->id;
        $cart = new Cart();
	$cart_item['cart_data']= $cart->getItems();
	$cart_item['total']=$cart->getTotal();
	$cart_item['total_num']=$cart->totalQuantity();
	$list[]=$cart_item;
	return  $list;  
        //return array('msg', 'Thanks for your purchase!');
    }

    /**
     * Check if the Stripe customer exists.
     *
     * @return boolean
     */
    public function isStripeCustomer() {
        return Auth::user() && \App\User::where('id', Auth::user()->id)->whereNotNull('stripe_id')->first();
    }
    //fetch payment page  
    public function checkout_payment_detail(){
      
       $store_add = Session::get('store_address');
       $store_address = DB::table('shipp_address')->where('id','=',$store_add)->first();
       $conty=DB::table('country')->where('id','=',$store_address->ship_country)->first();
       $state=DB::table('country')->where('id','=',$store_address->ship_state)->first();
       $city=DB::table('country')->where('id','=',$store_address->ship_city)->first();
       $store_address->country=$conty->name;
       $store_address->state=$state->name;
       $store_address->city=$city->name;
       $list['store_address']=$store_address;
       $cart = new Cart();
       $cart_item['cart_data']= $cart->getItems();
       $cart_item['total']=$cart->getTotal();
       $cart_item['total_num']=$cart->totalQuantity();
       $list['cart_item']=$cart_item;
       return $list;
    }
    //post order

    public function post_order()
    {
       $req = Request::all();
      
       $cart = new Cart();
       $cart_data = $cart->getItems();
       $total = $cart->getTotal();
       $total_num = $cart->totalQuantity();
       if (Auth::check()) {
		    $user_id= Auth::id();
	}else{
	   $user_id=0;
	}
	$order_detail = DB::table('order_detail')->orderBy('id', 'desc')->first();
	$ord_id = $order_detail->id + 1;
	$inv_no = 'INV-'.$ord_id;	
        $order = Order::create(['user_id' =>$user_id,'invoice_no' => $inv_no,'payment_method' =>$req['payment_method'], 'delivery_method' => $req['shipping_method'],'customer_note' =>'','status'=> '1','total'=>$total ,'payment_status'=> '1','affilate'=>'','shipping_address'=>Session::get('store_address'),'payment_address'=>Session::get('store_address')]);  
        foreach($cart_data as $ky=>$vy)
                {
                 $pro = DB::table('product')                  
                     ->where('product.status','=','Active')->where('product.is_delete','=',0)->where('no_stock','>',0)
                     ->where('product.stock_status','=','In Stock')->where('product.id','=',$vy->id)->first();
			       if($pro->id!=''){
					 if($pro->sale_price>0){
					 $now = Carbon::now();
					 $newfr = explode('-',$pro->date_from);
					 $first = Carbon::create($newfr[0],$newfr[1], $newfr[2]);
					 $newse = explode('-',$pro->date_to);
					 $sec= Carbon::create($newse[0],$newse[1], $newse[2]);            
					 if($now->between($first, $sec)){            
					   $prod_price=$pro->sale_price;
					   $percent=( ( $prod_price - $pro->price) / $pro->price) * 100;
					   $percent=number_format($percent, 2);
					   $percent=abs($percent);
					   $vy->prod_price= $prod_price;
					   $vy->percent=$percent;
					 
					 }else{
					   $prod_price='';
					   $vy->prod_price= "";
					   $vy->percent="";
					 }
					  $vy->pri=$pro->price;
					} 
					if($pro->no_stock > $vy->quantity && $pro->stock_status == 'In Stock') {
					$order_product = OrderProduct::create(['order_id' => $order->id,'product_id' => $vy->id,'name' =>$vy->name, 'model' => $vy->name,'quantity' =>$vy->quantity,'seller_id'=>$pro->id,'price'=>$pro->price,'total'=>$pro->price*$vy->quantity,'tax'=>0]);  
					 $produc =  Product::find($pro->id);
					 $produc->no_stock = $pro->no_stock - $vy->quantity;
					 $produc->save();
					}
                }
                 
	}
	$cart->clear(); 
	if($req['payment_method']=="Credit Card")
	{
	  $param=Request::all();
          return $this->chargeCustomer($order->id,  $total , 'Order',$req['stripeToken']);
	
	}
	else if($req['payment_method'] =="Paypal")
	{
	   
			    $payer = Paypal::Payer();
			    $payer->setPaymentMethod('paypal');
			
			    $amount = Paypal:: Amount();
			    $amount->setCurrency('USD');
			    $amount->setTotal($total); // This is the simple way,
			    // you can alternatively describe everything in the order separately;
			    // Reference the PayPal PHP REST SDK for details.
			
			    $transaction = Paypal::Transaction();
			    $transaction->setAmount($amount);
			    $transaction->setDescription('What are you selling?');
			
			    $redirectUrls = Paypal:: RedirectUrls();
			    $redirectUrls->setReturnUrl(action('frontend\CheckoutController@paypal_confirm', ['id' => $order->id]));
			    $redirectUrls->setCancelUrl(action('frontend\CheckoutController@paypal_reject', ['id' => $order->id]));
			
			    $payment = Paypal::Payment();
			    $payment->setIntent('sale');
			    $payment->setPayer($payer);
			    $payment->setRedirectUrls($redirectUrls);
			    $payment->setTransactions(array($transaction));
			
			    $response = $payment->create($this->_apiContext);
			    $redirectUrl = $response->links[1]->href;
			    
			    $list[]=$redirectUrl ;
			    $cart_item['cart_data']= $cart->getItems();
			    $cart_item['total']=$cart->getTotal();
			    $cart_item['total_num']=$cart->totalQuantity();
			    $list[]=$cart_item;
			    return $list;
	}
	else{
		$list[]='#/checkout/order_confirm/'.$order->id;
		$cart_item['cart_data']= $cart->getItems();
		$cart_item['total']=$cart->getTotal();
		$cart_item['total_num']=$cart->totalQuantity();
		$list[]=$cart_item;
		return  $list;  
	}
	  
       
    }
    // confirm paypal
    public function paypal_confirm($order_id)
    {
        
        $id = Request::input('paymentId');
	$token = Request::input('token');
	$payer_id = Request::input('PayerID');
	$payment = Paypal::getById($id, $this->_apiContext);
	$paymentExecution = Paypal::PaymentExecution();
	$paymentExecution->setPayerId($payer_id);
	$executePayment = $payment->execute($paymentExecution, $this->_apiContext);
        $order= Order::find($order_id);
        $order->payment_status=2;
        $order->save();       
	return redirect('/#/checkout/order_confirm/'.$order->id);      
       
    }
    public function paypal_reject($id)
    {
        $order= Order::find($order_id);
        $order->payment_status=3;
        $order->save();       
	return redirect('/#/checkout/order_confirm/'.$order->id);     
    }
     // update_address
     public function update_address(){ 
       	    $validator = Validator::make(Request::all(), [          
		   'store_data.ship_fname' => 'required',
		   'store_data.ship_lname' => 'required',
		   'store_data.ship_country' => 'required|min:0',
		   'store_data.ship_state' => 'required|min:0',
		   'store_data.ship_city' => 'required|min:0',
		   'store_data.ship_address' => 'required',
		   'store_data.ship_phone' => 'required',
		   'store_data.ship_mobile' => 'required',
		   'store_data.ship_landmark' => 'required',
	 ]);
	    $friendly_names = array(
			'store_data.ship_fname' =>   'First Name',
			'store_data.ship_lname' =>   'Last Name',
			'store_data.ship_country' => 'Country',
			'store_data.ship_state' =>   'State',
			'store_data.ship_city' =>    'City',
			'store_data.ship_address' =>  'Address',
			'store_data.ship_mobile' =>   'Mobile',			
			'store_data.ship_phone' =>'Telephone',
			'store_data.ship_landmark' =>'Landmark',
		
		    );
	   $validator->setAttributeNames($friendly_names);
	   if ($validator->fails()) {
			       $list[]='error';
			       $msg=$validator->errors()->all();
			       $list[]=$msg;
			       return $list;
				 
			 
	  }else{  
	        if (Auth::check()) {
		    $user_id= Auth::id();
		}else{
		    $user_id =0;
		}
		$address=Request::input('store_data');
		$ship_add = Shipping::find($address['id']);
		$ship_add->ship_fname=$address['ship_fname'];
		$ship_add->ship_lname=$address['ship_lname'];
		$ship_add->ship_mobile=$address['ship_mobile'];
		$ship_add->ship_phone=$address['ship_phone'];
		$ship_add->ship_address=$address['ship_address'];
		$ship_add->ship_country=$address['ship_country'];
		$ship_add->ship_state=$address['ship_state'];
		$ship_add->ship_city=$address['ship_city'];
		$ship_add->ship_landmark=$address['ship_landmark'];
	        $ship_add->save(); 
	        
	        $list[]='success';
	        $list[]='Address is updated successfully.';	 
		return $list;
	     
        } 
    }
    // getcountry
    public function country(){
          $country=DB::table('country')->where('status','=','Active')->where('is_delete','=','0')->get();
          return  $country;
    }
    // add address
    public function add_address()
    {
   
      
       $validator = Validator::make(Request::all(), [          
		   'store_data.fname' => 'required',
		   'store_data.lname' => 'required',
		   'store_data.country' => 'required|min:0',
		   'store_data.state' => 'required|min:0',
		   'store_data.city' => 'required|min:0',
		   'store_data.address' => 'required',
		   'store_data.telephone' => 'required',
		   'store_data.mobile' => 'required',
		   'store_data.landmark' => 'required',
	 ]);
	    $friendly_names = array(
			'store_data.fname' =>   'First Name',
			'store_data.lname' =>   'Last Name',
			'store_data.country' => 'Country',
			'store_data.state' =>   'State',
			'store_data.city' =>    'City',
			'store_data.address' =>  'Address',
			'store_data.mobile' =>   'Mobile',			
			'store_data.telephone' =>'Telephone',
			'store_data.landmark' =>'Landmark',
		
		    );
	   $validator->setAttributeNames($friendly_names);
	   if ($validator->fails()) {
			       $list[]='error';
			       $msg=$validator->errors()->all();
			       $list[]=$msg;
			       return $list;
				 
			 
	  }else{  
	        if (Auth::check()) {
		    $user_id= Auth::id();
		}else{
		    $user_id =0;
		}
		$address=Request::input('store_data');
	        $address= Shipping::create(['ship_fname' =>$address['fname'],'ship_lname' =>$address['lname'],'ship_mobile' => $address['mobile'],'ship_phone' => $address['telephone'],'ship_address' => $address['address'],'ship_country' => $address['country'],'ship_state' => $address['state'],'ship_city' => $address['city'],'ship_landmark' => $address['landmark'],'ship_status' => 'Active','user_id' =>$user_id]); 
	        $list[]='success';
	        $list[]='Address is added successfully.';	 
		return $list;
	     
        }
         
    }
}

