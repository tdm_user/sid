var app = angular.module('sellers', ['ngRoute', 'textAngular', 'localytics.directives', 'angularUtils.directives.dirPagination', 'jkuri.datepicker'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});
app.filter('capitalize', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.directive('hcPieChart', function () {
    return {
        restrict: 'E',
        template: '<div></div>',
        replace: true,
        scope: {
            title: '@',
            data: '='
        },
        link: function (scope, element) {
            Highcharts.chart(element[0], {
                chart: {
                    type: 'pie',
                    width: 200, height: 180
                },
                title: {
                    text: scope.title
                },
                legend: {
                    align: 'left',
                    layout: 'horizontal',
                    verticalAlign: 'top',
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        data: scope.data
                    }]
            });
        }
    };
});
app.directive("passwordStrength", function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(attrs.passwordStrength, function (value) {

                if (angular.isDefined(value)) {
                    var numbers = /^[0-9]+$/;
                    var chars = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
                    if (chars.test(value))
                    {
                        scope.formSubmit = true;
                        scope.strength = 'strong';
                        scope.strengthClass = 'progress-sm';
                        scope.barClass = 'progress-bar progress-bar-success';
                    } else if (value.length > 6) {
                        scope.strength = 'medium';
                        scope.strengthClass = 'progress-xs';
                        scope.barClass = 'progress-bar progress-bar-warning';
                        scope.formSubmit = false;
                    }
                    //else if (value.length > 3) {
                    //    scope.strength = 'medium';
                    //	scope.strengthClass= 'progress-xs';
                    //	scope.barClass='progress-bar-warning';
                    //} 
                    else {
                        scope.strength = 'weak';
                        scope.strengthClass = 'progress-xxs';
                        scope.barClass = 'progress-bar progress-bar-danger';
                        //scope.formSubmit=false;
                    }
                }
            });
        }
    };
});
app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider.
                when('/category', {
                    templateUrl: 'category', controller: 'CategoryController'
                }).
                when('/setting', {
                    templateUrl: 'setting', controller: 'SettingController'
                }).
                when('/template', {
                    templateUrl: 'template', controller: 'TemplateController'
                }).
                when('/enquiry', {
                    templateUrl: 'enquiry', controller: 'EnquiryController'
                }).
                when('/config', {
                    templateUrl: 'config', controller: 'ConfigController'
                }).
                when('/faq', {
                    templateUrl: 'faq', controller: 'FaqController'
                }).
                when('/dashboard', {
                    templateUrl: 'dashboard', controller: 'DashboardController'
                }).
                when('/user', {
                    templateUrl: 'user', controller: 'UserController'
                }).
                when('/static-content', {
                    templateUrl: 'static-content', controller: 'StaticContentController'
                }).
                when('/seller', {
                    templateUrl: 'seller', controller: 'SellerController'

                }).
                when('/brand', {
                    templateUrl: 'brand', controller: 'BrandsController'
                }).
                when('/banner', {
                    templateUrl: 'banner', controller: 'BannerController'
                }).
                when('/country', {
                    templateUrl: 'country', controller: 'CountryController'
                }).
                when('/option', {
                    templateUrl: 'option', controller: 'OptionController'
                }).
                when('/product', {
                    templateUrl: 'product', controller: 'ProductController'
                }).
                when('/productlist', {
                    templateUrl: 'productlist', controller: 'ProductListController'
                }).
                when('/profile', {
                    templateUrl: 'profile', controller: 'ProfileController'
                }).
                when('/promotion', {
                    templateUrl: 'promotion', controller: 'PromotionController'
                }).when('/special-offer', {
                    templateUrl: 'special-offer', controller: 'SpecialOfferController'
                }). when('/bulk-discount', {
                    templateUrl: 'bulk-discount', controller: 'BulkDiscountController'
                }).
                when('/coupon', {
                    templateUrl: 'coupon', controller: 'CouponController'
                }).
                otherwise({
                    redirectTo: 'dashboard', controller: 'DashboardController'
                });
//               $locationProvider.html5Mode({enabled: true,requireBase: false});
    }]);
app.controller('HomeController', function ($scope, $http) {

//alert($location.path());
    $scope.errors = false;
    $scope.loading = true;
    $scope.includes_function = false;
    $scope.init = function () {


//		$scope.loading = true;
//		$http.get('/log_user').
//		success(function(data, status, headers, config) {
//			$scope.todos = data;
//				$scope.loading = false;
// 
//		});
    }

    $scope.sign_in = function () {


        $http.post('log_user', {
            email: $scope.email,
            password: $scope.password,
        }).success(function (data, status, headers, config) {
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                location.href = data[1];
            }
            $scope.loading = false;
        })
    };
    //$scope.category = function() {
    //$scope.loading = true;alert('hello');
    //$scope.includes_function='/admins/category';
    /* 	$http.get('/admins/category').
     success(function(data, status, headers, config) {
     $scope.categorys = data;
     $scope.loading = false;
     
     }); */
    //}

//	$scope.addTodo = function() {
//				$scope.loading = true;
// 
//		$http.post('/api/todos', {
//			title: $scope.todo.title,
//			done: $scope.todo.done
//		}).success(function(data, status, headers, config) {
//			$scope.todos.push(data);
//			$scope.todo = '';
//				$scope.loading = false;
// 
//		});
//	};
// 
//	$scope.updateTodo = function(todo) {
//		$scope.loading = true;
// 
//		$http.put('/api/todos/' + todo.id, {
//			title: todo.title,
//			done: todo.done
//		}).success(function(data, status, headers, config) {
//			todo = data;
//				$scope.loading = false;
// 
//		});;
//	};
// 
//	$scope.deleteTodo = function(index) {
//		$scope.loading = true;
// 
//		var todo = $scope.todos[index];
// 
//		$http.delete('/api/todos/' + todo.id)
//			.success(function() {
//				$scope.todos.splice(index, 1);
//					$scope.loading = false;
// 
//			});;
//	};


    $scope.init();
});
app.controller('DashboardController', function ($scope, $http) {
    $scope.$parent.ptitle = '';
});
// Category Management
app.controller('CategoryController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.categories = false;
    $scope.page = 'index';
    $scope.category = {};
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.files = '';
        $scope.category = {};
        $scope.errors = false;
        $scope.loading = true;
        $http.get('category/all').
                success(function (data, status, headers, config) {
                    $scope.categories = data;
                    $scope.loading = false;
                });
    }
    $scope.add = function () {
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $http.get('category/all').
                success(function (data, status, headers, config) {
                    $scope.all_cat = data;
                    $scope.loading = false;
                });
    }
    $scope.editcategory = function (category) {

        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('category/edit/' + category.id, {
        }).success(function (data, status, headers, config) {
            $scope.category = data['category'];
            var im = $scope.category.image;
            var im1 = im.split("category/");
            $scope.files = im1[1];
            $scope.all_cat = data['all_cat'];
            $scope.loading = false;
        });
        ;
    };
    $scope.uploadedFile = function (element) {
        $scope.$apply(function ($scope) {

            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'category');
            fd.append("width", '310');
            fd.append("height", '210');
            $http.post('imageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                if (data[0] == 'error') {
                    $scope.errors = data[1];
                } else
                {
                    $scope.errors = false;
                    $scope.files = data;
                    $scope.loading = false;
                }
            });
        });
    }
    $scope.delcatefiles = function (file) {
        $scope.files = '';
    }

    $scope.update = function (category) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('category/update', {
            category_name: category.category_name,
            description: category.description,
            id: category.id,
            status: category.status,
            parent_id: category.parent_id,
            image: $scope.files,
            meta_title: category.meta_title,
            meta_description: category.meta_description,
            meta_keyword: category.meta_keyword

        }).success(function (data, status, headers, config) {
            $scope.files = '';
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.store = function (category) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('category/store', {
            category_name: category.category_name,
            description: category.description,
            image: $scope.files,
            id: category.id,
            status: category.status,
            parent_id: category.parent_id,
            meta_title: category.meta_title,
            meta_description: category.meta_description,
            meta_keyword: category.meta_keyword

        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                $scope.files = '';
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.deleteCategory = function (index) {
        $scope.loading = true;
        var category = $scope.categories[index];
        $http.post('category/delete', {
            del_id: category.id
        }).success(function (data, status, headers, config) {
            $scope.categories.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.init();
});
// Enquiry Management
app.controller('EnquiryController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = false;
    $scope.loading = true;
    $scope.enquirys = false;
    $scope.enquiry = false;
    $scope.page = 'index';
    $scope.faq = false;
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.get('enquiry/all').
                success(function (data, status, headers, config) {
                    $scope.enquirys = data;
                    $scope.loading = false;
                });
    }
    $scope.replys = function (enquiry) {
        $scope.page = 'reply';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.enquiry = enquiry;
        $http.get('enquiry/edit/' + enquiry.id, {
        }).success(function (data, status, headers, config) {
            $scope.reply = data['reply'];
            $scope.loading = false;
        });
        ;
    }



    $scope.send = function (enquiry) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('enquiry/update', {
            reply: enquiry.reply,
            name: enquiry.name,
            subject: enquiry.subject,
            email: enquiry.email,
            reply_to: enquiry.id
        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.deleteenquiry = function (index) {
        $scope.loading = true;
        var enquiry = $scope.enquirys[index];
        $http.post('enquiry/delete', {
            del_id: enquiry.id
        }).success(function (data, status, headers, config) {
            $scope.enquirys.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.init();
});
// Faq Management
app.controller('SettingController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.faqs = false;
    $scope.$parent.ptitle = 'Store Setting';
    $scope.page = 'index';
    $scope.faq = false;
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.get('setting/all').
                success(function (data, status, headers, config) {
                    $scope.store_data = data['store_data'];
                    $scope.country = data['country'];
                    console.log($scope.store_data);
                    $scope.loading = false;
                    $scope.getState($scope.store_data.store_country, 'store');
                    $scope.getCity($scope.store_data.store_state, 'store');
                });
    }
    $scope.getState = function (pid, type) {
        //console.log(pid);
        $http.post('country/getState', {
            store_country: pid
        }).
                success(function (data, status, headers, config) {
                    console.log(data);
                    var vari = type + 'state';
                    //console.log(vari)
                    if (type == 'user')
                        $scope.user_state = data;
                    else if (type == 'store')
                    {
                        $scope.store_state = data;
                    }
                });
    }
    $scope.getCity = function (pid, type) {
        //console.log(pid);
        $http.post('country/getCity', {
            store_country: pid
        }).
                success(function (data, status, headers, config) {//console.log(data);
                    if (type == 'user')
                        $scope.user_city = data;
                    else if (type == 'store')
                    {
                        $scope.store_city = data;
                    }

                });
    }
    $scope.add = function () {
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.faq = false;
    }
    $scope.editfaq = function (faq) {

        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('faq/edit/' + faq.id, {
        }).success(function (data, status, headers, config) {
            $scope.faq = data['data'];
            $scope.loading = false;
        });
    };
    $scope.removelogo = function ()
    {
        $scope.store_data.logo = false;
    }
    $scope.delBanner = function ()
    {
        $scope.store_data.banner = false;
    }
    $scope.delPic = function ()
    {
        $scope.store_data.profile_picture = false;
    }
    $scope.update = function (storeData) {
        $scope.errors = false;
        $scope.success_flash = false;
        console.log(storeData);
        $http.post('setting/update', {
            store_id: storeData.id,
            profile_pic: storeData.profile_picture,
            store_name: storeData.store_name,
            store_link: storeData.store_link,
            store_address: storeData.store_address,
            banner: $scope.bannerfiles,
            store_country: storeData.store_country,
            store_state: storeData.store_state,
            store_city: storeData.store_city,
            store_phone: storeData.phone,
            logo: $scope.logo,
        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.uploadlogo = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'store_logo');
            fd.append("width", '150');
            fd.append("height", '150');
            $http.post('imageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                if (data[0] == 'error') {
                    $scope.errors = data[1];
                } else
                {
                    $scope.logo = data;
                    $scope.store_data.logo = $scope.logo;
                    $scope.loading = false;
                }
            });
        });
    }

    $scope.uploadedBannerFile = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'store_banner');
            fd.append("width", '1300');
            fd.append("height", '400');
            $http.post('Allimageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                if (data[0] == 'error') {
                    $scope.errors = data[1];
                } else
                {
                    $scope.bannerfiles = data;
                    $scope.store_data.banner = $scope.bannerfiles;
                    //console.log($scope.user.banner);
                    $scope.loading = false;
                }
            });
        });
    }
    $scope.uploadProfilePic = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'seller');
            fd.append("width", '150');
            fd.append("height", '150');
            $http.post('imageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                if (data[0] == 'error') {
                    $scope.errors = data[1];
                } else
                {
                    $scope.profilePic = data;
                    $scope.store_data.profile_picture = $scope.profilePic;
                    console.log($scope.profilePic);
                    $scope.loading = false;
                }
            });
        });
    }


    $scope.init();
});
// Template Management
app.controller('TemplateController', function ($scope, $http) {
    $scope.errors = false;
    $scope.loading = true;
    $scope.templates = false;
    $scope.template = false;
    $scope.temp = false;
    $scope.page = 'index';
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.get('template/all').
                success(function (data, status, headers, config) {
                    $scope.templates = data;
                    $scope.loading = false;
                });
    }
    $scope.add = function () {
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.template = false;
    }
    $scope.edittemplate = function (template) {

        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('template/edit/' + template.id, {
        }).success(function (data, status, headers, config) {
            $scope.template = data['data'];
            $scope.loading = false;
        });
        ;
    };
    $scope.store = function (template) {
        $scope.errors_modal = false;
        $scope.success_flash_modal = false;
        $http.post('template/store', {
            subject: template.subject,
            message: template.message,
        }).success(function (data, status, headers, config) {
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.send_email = function (user_send, temp_id) {
        $http.post('template/sent', {
            user: user_send,
            template_id: temp_id,
        }).success(function (data, status, headers, config) {
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    }
    $scope.sendtemplate = function (template) {
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'send';
        $scope.temp = template;
    }
    $scope.update = function (template) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('template/update', {
            subject: template.subject,
            message: template.content,
            template_id: template.id
        }).success(function (data, status, headers, config) {
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.deletetemplate = function (index) {
        $scope.loading = true;
        var templates = $scope.templates[index];
        $http.post('template/delete', {
            del_id: templates.id
        }).success(function (data, status, headers, config) {
            $scope.templates.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.init();
});
// Newsletter Management
app.controller('NewsletterController', function ($scope, $http) {
    $scope.errors_modal = false;
    $scope.success_flash_modal = false;
    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.newsletters = false;
    $scope.page = 'index';
    $scope.newsl = {};
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.get('newsletter/all').
                success(function (data, status, headers, config) {
                    $scope.newsletters = data;
                    $scope.loading = false;
                });
    }
    $scope.store = function (newsletter) {
        $scope.errors_modal = false;
        $scope.success_flash_modal = false;
        $http.post('newsletter/store', {
            name: newsletter.name,
            email: newsletter.email,
            mobile_no: newsletter.mob_no,
            occupation: newsletter.occupation,
            city: newsletter.city,
            gender: newsletter.gender,
        }).success(function (data, status, headers, config) {
            if (data[0] == 'error') {
                $scope.errors_modal = data[1];
            } else {

                $scope.errors_modal = false;
                $scope.success_flash_modal = data[1];
                $scope.newsl = {};
                $scope.newsl.gender = 'male';
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.update = function (newsletter) {
        $scope.errors_modal = false;
        $scope.success_flash_modal = false;
        $http.post('newsletter/update', {
            name: newsletter.name,
            email: newsletter.email,
            mobile_no: newsletter.mob_no,
            occupation: newsletter.occupation,
            city: newsletter.city,
            gender: newsletter.gender,
            edit_id: newsletter.id

        }).success(function (data, status, headers, config) {

            $scope.files = '';
            if (data[0] == 'error') {
                $scope.errors_modal = data[1];
            } else {

                $scope.errors_modal = false;
                $scope.success_flash_modal = data[1];
            }
            $scope.loading = false;
        });
    };
    $scope.update_sub = function (newsletter) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('newsletter/update_subscribe', {
            subscribe: newsletter.subscribe,
            edit_id: newsletter.id

        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors_modal = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.delete = function (index) {
        $scope.loading = true;
        var newsletter = $scope.newsletters[index];
        $http.post('newsletter/delete', {
            del_id: newsletter.id
        }).success(function (data, status, headers, config) {
            $scope.newsletters.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.init();
});
//Configuration 
app.controller('ConfigController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.configs = false;
    $scope.page = 'index';
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.get('config/all').
                success(function (data, status, headers, config) {
                    $scope.configs = data;
                    $scope.loading = false;
                });
    }

    $scope.edit = function () {

        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('config/edit').success(function (data, status, headers, config) {
            $scope.configs = data;
            $scope.loading = false;
        });
        ;
    };
    $scope.update = function (config) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('config/update', {
            all_data: config
        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.init();
});
//user management
app.controller('UserController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = '';
    $scope.bannerfiles = '';
    $scope.promotion = '';
    $scope.logo = '';
    $scope.profileImage = '';
    $scope.loading = true;
    $scope.users = false;
    $scope.user = {};
    $scope.userq = {};
    $scope.user_data = false;
    $scope.user_ddata = false;
    $scope.banner = false;
    $scope.page = 'index';
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        //$scope.success_flash=false;
        $scope.loading = true;
        $http.get('user/all').
                success(function (data, status, headers, config) {
                    $scope.users = data['users'];
                    $scope.country = data['country'];
                    $scope.usersRecord = data['total'];
                    $scope.roles = data['roles'];
                    //console.log($scope.usersRecord);
                    $scope.loading = false;
                });
    }
    $scope.users_id = {};
    //id: null

    $scope.checkAll = function () {


        if (!$scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }

        angular.forEach($scope.users, function (item) {
            //alert(item);			

            if ($scope.selectedAll)
            {
                $scope.users_id[item.id] = true;
            } else
            {

                $scope.users_id[item.id] = false;
            }
        });
    };
    /* $scope.optionToggled = function (ids) {
     
     console.log( $scope.users_id[ids] ); 
     //                if($scope.users_id[ids]==true)
     //                {
     //                var use_id=$scope.getObjectKeyIndex($scope.users_id, ids);
     //                $scope.users_id.splice(use_id, 1);
     //               }
     // Returns int(1) (or null if the key doesn't exist)
     
     
     //     var idx = $scope.users_id.indexOf(employeeName);
     // alert(idx);
     //     // is currently selected
     //     if (idx > -1) {
     //       $scope.selection.splice(idx, 1);
     //	   $scope.users_id.push({
     //	   id:employeeName});
     //     }
     // 
     //     // is newly selected
     //     else {
     //       $scope.selection.push(employeeName);
     //	   $scope.users_id.splice(employeeName+1, 1);
     //     }
     //	 console.log($scope.selection);
     //	 console.log($scope.users_id);
     };
     $scope.getObjectKeyIndex =function(obj, keyToFind) {
     var i = 0, key;
     
     for (key in obj) {
     if (key == keyToFind) {
     return i;
     }
     
     i++;
     }
     
     return null;
     } */
    $scope.roleChange = function (roless) {
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.loading = true;
        console.log($scope.users_id);
        $http.post('user/changeRole', {
            id: $scope.users_id,
            role: roless
        }).
                success(function (data, status, headers, config) {

                    $scope.success_flash = data[1];
                    //console.log($scope.usersRecord);
                    $scope.loading = false;
                    $scope.init();
                });
    }
    //bulk delete
    $scope.bulkDelete = function (userData) {
        console.log(userData);
        $scope.page = 'index';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.loading = true;
        $http.post('user/deleteAll', {
            action: userData,
            id: $scope.users_id,
        }).
                success(function (data, status, headers, config) {
                    $scope.success_flash = data[1];
                    console.log($scope.success_flash);
                    $scope.loading = false;
                    $scope.init();
                });
    }
    $scope.userlist = function (userData) {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.loading = true;
        $http.post('user/all', {
            role: userData.role_id
        }).
                success(function (data, status, headers, config) {
                    $scope.users = data['users'];
                    $scope.country = data['country'];
                    $scope.usersRecord = data['total'];
                    //console.log($scope.usersRecord);
                    $scope.loading = false;
                });
    }
    $scope.add = function () {
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $http.get('user/all').
                success(function (data, status, headers, config) {
                    $scope.all_user = data['users'];
                    $scope.country = data['country'];
                    $scope.category = data['category'];
                    //console.log($scope.all_user);
                    $scope.loading = false;
                });
    }
    $scope.inputs = [{
            value: null
        }];
    $scope.addInput = function () {

        $scope.inputs.push({
            value: ''
        });
    }

    $scope.removeInput = function (index) {
        $scope.inputs.splice(index, 1);
    }
    $scope.useradd = function () {
        $scope.user = {};
        $scope.page = 'useradd';
        $scope.errors = false;
        $scope.success_flash = false;
        $http.get('user/all').
                success(function (data, status, headers, config) {
                    $scope.all_user = data['category'];
                    $scope.country = data['country'];
                    $scope.roles = data['roles'];
                    //console.log($scope.all_user);
                    $scope.loading = false;
                });
    }
    $scope.checkUser = function (userData) {
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('user/checkUser', {
            username: userData.username,
            email: userData.email,
            password: userData.password,
            confirm_password: userData.repassword,
            role: userData.role,
            ownpassword: userData.ownpassword
        }).success(function (data, status, headers, config) {//console.log(data['user']);

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                //console.log($scope.user.image);
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.user.username = userData.username;
                $scope.user.email = userData.email;
                $scope.user.role = userData.role;
                $scope.user.password = userData.password;
                $scope.loading = false;
                //console.log($scope.user.username);			
                $scope.add();
            }

        });
    };
    $scope.edituser = function (category) {
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('user/edit/' + category.id, {
        }).success(function (data, status, headers, config) {//console.log(data['user']);
            $scope.user_ddata = data['user'];
            $scope.all_user = data['all_user'];
            $scope.roles = data['roles'];
            $scope.loading = false;
            console.log($scope.user_ddata);
            $scope.getState($scope.user_ddata.store_country);
            $scope.getCity($scope.user_ddata.store_state);
            $scope.getState($scope.user_ddata.ship_country);
            $scope.getCity($scope.user_ddata.ship_state);
        });
    };
    $scope.getState = function (pid, type) {
        //console.log(pid);
        $http.post('country/getState', {
            store_country: pid
        }).
                success(function (data, status, headers, config) {
                    console.log(data);
                    var vari = type + 'state';
                    //console.log(vari)
                    if (type == 'user')
                        $scope.user_state = data;
                    else if (type == 'store')
                    {
                        $scope.store_state = data;
                    }
                    //console.log($scope.vari);
                });
    }
    $scope.getCity = function (pid, type) {
        //console.log(pid);
        $http.post('country/getCity', {
            store_country: pid
        }).
                success(function (data, status, headers, config) {//console.log(data);
                    if (type == 'user')
                        $scope.user_city = data;
                    else if (type == 'store')
                    {
                        $scope.store_city = data;
                    }

                });
    }
    $scope.uploadedFile = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'user');
            fd.append("width", '150');
            fd.append("height", '150');
            $http.post('imageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                $scope.files = data;
                $scope.loading = false;
            });
        });
    }
    $scope.uploadlogo = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'store_logo');
            fd.append("width", '150');
            fd.append("height", '150');
            $http.post('imageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                if (data[0] == 'error') {
                    $scope.errors = data[1];
                } else
                {
                    $scope.logo = data;
                    $scope.user.logo = $scope.logo;
                    $scope.loading = false;
                }
            });
        });
    }

    $scope.uploadedPromotionBannerFile = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'promotion');
            fd.append("width", '150');
            fd.append("height", '150');
            $http.post('imageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                $scope.promotion = data;
                $scope.loading = false;
            });
        });
    }
    $scope.uploadedBannerFile = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'store_banner');
            fd.append("width", '1300');
            fd.append("height", '400');
            $http.post('Allimageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                if (data[0] == 'error') {
                    $scope.errors = data[1];
                } else
                {
                    $scope.bannerfiles = data;
                    $scope.user.banner = $scope.bannerfiles;
                    //console.log($scope.user.banner);
                    $scope.loading = false;
                }
            });
        });
    }
    $scope.getProfileImage = function (user_data)
    {

        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('user/getProfileImage',
                {email: user_data.email,
                    name: user_data.username

                }).success(function (data, status, headers, config)
        {
            //$scope.user.profile = data;
            var dt = data.split("<img src='");
            var dt1 = dt[1].split("'");
            console.log(dt1[0]);
            $scope.profileImage = dt1[0]
            $scope.user.profile = $scope.profileImage;
        });
    }

    $scope.genratePassword = function () {
        var length = 6;
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        var charsa = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        $scope.formSubmit = true;
        console.log($scope.formSubmit);
        return pass;
        //console.log(pass);

    }
    $scope.delBanner = function (bannerData)
    {
        $scope.user.banner = false;
    }
    $scope.removelogo = function ()
    {
        $scope.user.logo = false;
    }
    $scope.update = function (user_data) { //console.log($scope.bannerfiles);
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('user/update', {
            role: user_data.role,
            fname: user_data.fname,
            lname: user_data.lname,
            display_name: user_data.display_name,
            username: user_data.username,
            nickname: user_data.nickname,
            email: user_data.email,
            gender: user_data.gender,
            mobile: user_data.mobile,
            home_number: user_data.home_number,
            website: user_data.website,
            bio: user_data.bio,
            password: user_data.pass,
            confirm_password: user_data.repassword,
            nationality: user_data.nationality,
            country: user_data.country,
            address: user_data.address,
            id: user_data.userid,
            status: user_data.status,
            profile_image: $scope.profileImage,
            store_name: user_data.store_name,
            store_link: user_data.store_link,
            store_address: user_data.store_address,
            ship_name: user_data.ship_name,
            ship_mobile: user_data.ship_mobile,
            ship_address: user_data.ship_address,
            ship_country: user_data.ship_country,
            ship_state: user_data.ship_state,
            ship_city: user_data.ship_city,
            banner: $scope.bannerfiles,
            store_country: user_data.store_country,
            store_state: user_data.store_state,
            store_city: user_data.store_city,
            store_phone: user_data.phone,
            facebook_link: user_data.facebook_link,
            google_plus_link: user_data.google_plus_link,
            twitter_link: user_data.twitter_link,
            linkedin_link: user_data.linkedin_link,
            youtube_link: user_data.youtube_link,
            instagram_link: user_data.instagram_link,
            flickr_link: user_data.flickr_link,
            store_id: user_data.store_id,
            shipp_id: user_data.shipp_id,
            affiliatefees: $scope.inputs,
            logo: $scope.logo,
            selling: userData.selling,
            publishing: userData.publishing,
            commission: userData.commission,
            featured: userData.featured,
            verified: userData.verified,
            promotinoal_link: userData.promotinoal_link,
            promotion: $scope.promotion,
        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                if ($scope.files)
                    $scope.user.image = $scope.files;
                //console.log($scope.user.image);
                $scope.files = '';
                $scope.errors = false;
                $scope.success_flash = data[1];
            }
            $scope.loading = false;
        });
    };
    $scope.store = function (userData) {
        console.log($scope.files);
        $scope.errors = false;
        $scope.success_flash = false;
        console.log($scope.albumNameArray);
        $http.post('user/store', {
            role: userData.role,
            fname: userData.fname,
            lname: userData.lname,
            username: userData.username,
            nickname: userData.nickname,
            display_name: userData.display_name,
            email: userData.email,
            gender: userData.gender,
            mobile: userData.mobile,
            website: userData.website,
            bio: userData.bio,
            password: userData.password,
            confirm_password: userData.repassword,
            nationality: userData.nationality,
            country: userData.country,
            address: userData.address,
            state: userData.state,
            city: userData.city,
            id: userData.id,
            status: userData.status,
            profile_image: $scope.files,
            store_name: userData.store_name,
            store_link: userData.store_link,
            store_address: userData.store_address,
            ship_fname: userData.ship_fname,
            ship_lname: userData.ship_lname,
            ship_mobile: userData.ship_mobile,
            ship_address: userData.ship_address,
            ship_country: userData.ship_country,
            ship_state: userData.ship_state,
            ship_city: userData.ship_city,
            banner: $scope.bannerfiles,
            store_country: userData.store_country,
            store_state: userData.store_state,
            store_city: userData.store_city,
            store_phone: userData.store_phone,
            facebook_link: userData.facebook_link,
            google_plus_link: userData.google_plus_link,
            twitter_link: userData.twitter_link,
            linkedin_link: userData.linkedin_link,
            youtube_link: userData.youtube_link,
            instagram_link: userData.instagram_link,
            flickr_link: userData.flickr_link,
            affiliatefees: $scope.inputs,
            logo: $scope.logo,
            selling: userData.selling,
            publishing: userData.publishing,
            commission: userData.commission,
            featured: userData.featured,
            verified: userData.verified,
            promotinoal_link: userData.promotinoal_link,
            promotion: $scope.promotion,
        }).success(function (data, status, headers, config) {//console.log($scope.files);

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                $scope.files = '';
                $scope.inputs = false;
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.users.push(userData);
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.deleteUser = function (index) {
        $scope.loading = true;
        var user = $scope.users[index];
        $http.post('user/delete', {
            del_id: user.id
        }).success(function (data, status, headers, config) {
            $scope.users.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.changeStatus = function (userData)
    {
        $scope.loading = true;
        $http.post('user/changeStatus', {
            id: userData.id,
            status: userData.status

        }).success(function (data, status, headers, config) {


            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    }

    $scope.init();
});
//Static Content
app.controller('StaticContentController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.contents = false;
    $scope.content = false;
    $scope.user = false;
    $scope.page = 'index';
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.loading = true;
        $http.get('static-content/all').
                success(function (data, status, headers, config) {
                    $scope.contents = data;
                    $scope.loading = false;
                });
    }
    $scope.add = function () {
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $http.get('static-content/all').
                success(function (data, status, headers, config) {
                    $scope.all_user = data;
                    $scope.loading = false;
                });
    }
    $scope.editcontent = function (category) {
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('static-content/edit/' + category.id, {
        }).success(function (data, status, headers, config) {
            $scope.content = data['content'];
            $scope.all_content = data['all_content'];
            $scope.loading = false;
        });
        ;
    };
    $scope.uploadedFile = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'static');
            fd.append("width", '150');
            fd.append("height", '150');
            $http.post('imageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                $scope.files = data;
                $scope.loading = false;
            });
        });
    }
    $scope.update = function (contents) { //console.log(contents);
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('static-content/update', {
            title: contents.title,
            short_description: contents.short_description,
            description: contents.description,
            image: $scope.files, id: contents.id

        }).success(function (data, status, headers, config) {

            //console.log(contents);
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                if ($scope.files)
                    $scope.content.image = $scope.files;
                //console.log($scope.content.image);
                $scope.files = '';
                $scope.errors = false;
                $scope.success_flash = data[1];
            }
            $scope.loading = false;
        });
    };
    $scope.store = function (userData) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('static-content/store', {
            name: userData.name,
            email: userData.email,
            gender: userData.gender,
            address: userData.address,
            id: userData.id,
            status: userData.status,
            image: $scope.files

        }).success(function (data, status, headers, config) {
            $scope.files = '';
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.users.push(userData);
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.deleteUser = function (index) {
        $scope.loading = true;
        var user = $scope.users[index];
        $http.post('user/delete', {
            del_id: user.id
        }).success(function (data, status, headers, config) {
            $scope.users.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.init();
});
//Brands
app.controller('BrandsController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.brands = {};
    $scope.brand = {};
    $scope.page = 'index';
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.files = '';
        $scope.success_flash = false;
        $scope.loading = true;
        $http.get('brand/all').
                success(function (data, status, headers, config) {
                    $scope.brands = data;
                    $scope.loading = false;
                });
    }
    $scope.add = function () {
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
    }
    $scope.delbrandfiles = function (file) {
        $scope.files = '';
    }
    $scope.editbrand = function (brand_data) {
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('brand/edit/' + brand_data.id, {
        }).success(function (data, status, headers, config) {
            $scope.brand = data['brands'];
            var im = $scope.brand.image;
            var im1 = im.split("brand/");
            $scope.files = im1[1];
            $scope.loading = false;
        });
        ;
    };
    $scope.uploadedFile = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'brand');
            fd.append("width", '200');
            fd.append("height", '80');
            $http.post('imageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                if (data[0] == 'error') {
                    $scope.errors = data[1];
                } else
                {
                    $scope.errors = false;
                    $scope.files = data;
                    $scope.loading = false;
                }
            });
        });
    }
    $scope.update = function (brands) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('brand/update', {
            brand_name: brands.brand_name,
            description: brands.description,
            status: brands.status,
            image: $scope.files, id: brands.id,
            meta_title: brands.meta_title,
            meta_description: brands.meta_description,
            meta_keyword: brands.meta_keyword

        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.files = '';
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.store = function (brand) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('brand/store', {
            brand_name: brand.brand_name,
            description: brand.description,
            id: brand.id,
            status: brand.status,
            image: $scope.files,
            meta_title: brand.meta_title,
            meta_description: brand.meta_description,
            meta_keyword: brand.meta_keyword


        }).success(function (data, status, headers, config) {
            $scope.files = '';
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.user = false;
                $scope.success_flash = data[1];
                $scope.brands.push(brand);
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.deleteBrand = function (index) {
        $scope.loading = true;
        var brand = $scope.brands[index];
        $http.post('brand/delete', {
            del_id: brand.id
        }).success(function (data, status, headers, config) {
            $scope.brands.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.init();
});
//Banners
app.controller('BannerController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.banners = false;
    $scope.banner = false;
    $scope.user = false;
    $scope.itemSelected = false;
    $scope.page = 'index';
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.loading = true;
        $http.get('banner/all').
                success(function (data, status, headers, config) {
                    $scope.banners = data['banner'];
                    $scope.loading = false;
                });
    }
    $scope.add = function () {
        $scope.banner = false;
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $http.get('banner/all').
                success(function (data, status, headers, config) {
                    $scope.all_banner = data['banner'];
                    $scope.bannerType = data['banner_type'];
                    $scope.loading = false;
                });
    }
    $scope.editbanner = function (banner_data) {
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('banner/edit/' + banner_data.id, {
        }).success(function (data, status, headers, config) {
            $scope.banner = data['banners'];
            $scope.banner_Type = data['banners']['position_id'];
            console.log($scope.banner_Type);
            $scope.bannerType = data['banner_type'];
            $scope.loading = false;
        });
        ;
    };
    $scope.onCategoryChange = function (aa) {

        $scope.banner_Type = aa;
    };
    $scope.uploadedFile = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'banner');
            fd.append("banner_type", $scope.banner_Type);
            fd.append("width", '150');
            fd.append("height", '150');
            $http.post('bannerImageUpload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                $scope.files = data;
                $scope.loading = false;
            });
        });
    }
    $scope.update = function (userData) {
        console.log(userData);
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('banner/update', {
            title: userData.title,
            banner_type: userData.position_id,
            url: userData.url,
            id: userData.id,
            status: userData.status,
            image: $scope.files

        }).success(function (data, status, headers, config) {
            console.log(data);
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                if ($scope.files)
                    $scope.banner.image = $scope.files;
                $scope.errors = false;
                $scope.files = '';
                $scope.success_flash = data[1];
            }
            $scope.loading = false;
        });
    };
    $scope.store = function (userData) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('banner/store', {
            title: userData.title,
            banner_type: userData.position_id,
            url: userData.url,
            status: userData.status,
            image: $scope.files

        }).success(function (data, status, headers, config) {
            $scope.files = '';
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.banner = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.deleteBanner = function (index) {
        $scope.loading = true;
        var banner = $scope.banners[index];
        $http.post('banner/delete', {
            del_id: banner.id
        }).success(function (data, status, headers, config) {
            $scope.banners.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.init();
});
//Seller management
app.controller('SellerController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.sellers = false;
    $scope.seller = false;
    $scope.user_data = false;
    $scope.page = 'index';
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.loading = true;
        $http.get('seller/all').
                success(function (data, status, headers, config) {
                    $scope.users = data;
                    $scope.loading = false;
                });
    }
    $scope.add = function () {
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $http.get('seller/all').
                success(function (data, status, headers, config) {
                    $scope.all_seller = data;
                    $scope.loading = false;
                });
    }
    $scope.editseller = function (category) {
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('seller/edit/' + category.id, {
        }).success(function (data, status, headers, config) {
            $scope.seller = data['user'];
            $scope.all_seller = data['all_user'];
            $scope.loading = false;
        });
        ;
    };
    $scope.uploadedFile = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            //Take the first selected file
            fd.append("image", element.files[0]);
            fd.append("folder", 'seller');
            fd.append("width", '150');
            fd.append("height", '150');
            $http.post('imageupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                $scope.files = data;
                $scope.loading = false;
            });
        });
    }
    $scope.update = function (userData) {
        console.log($scope.user);
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('seller/update', {
            name: userData.name,
            email: userData.email,
            gender: userData.gender,
            address: userData.address,
            id: userData.id,
            status: userData.status,
            image: $scope.files,
            mobile: userData.mobile,
            company_name: userData.company_name,
            company_pan_no: userData.company_pan_no,
            company_address: userData.company_address,
            company_tin_no: userData.company_tin_no,
            store_link: userData.store_link,
        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                if ($scope.files)
                    $scope.seller.image = $scope.files;
                //console.log($scope.user.image);
                $scope.files = '';
                $scope.errors = false;
                $scope.success_flash = data[1];
            }
            $scope.loading = false;
        });
    };
    $scope.store = function (userData) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('seller/store', {
            name: userData.name,
            email: userData.email,
            gender: userData.gender,
            address: userData.address,
            id: userData.id,
            status: userData.status,
            image: $scope.files,
            mobile: userData.mobile,
            company_name: userData.company_name,
            company_pan_no: userData.company_pan_no,
            company_address: userData.company_address,
            company_tin_no: userData.company_tin_no,
            store_link: userData.store_link,
        }).success(function (data, status, headers, config) {
            console.log($scope.files);
            $scope.files = '';
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.sellers.push(userData);
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.deleteSeller = function (index) {
        $scope.loading = true;
        var seller = $scope.sellers[index];
        $http.post('seller/delete', {
            del_id: seller.id
        }).success(function (data, status, headers, config) {
            $scope.sellers.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.init();
});
//Country
app.controller('CountryController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.contents = false;
    $scope.content = false;
    $scope.user = false;
    $scope.page = 'index';
    $scope.success_flash = false;
    $scope.wordCount = 0;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.loading = true;
        $http.get('country/all').
                success(function (data, status, headers, config) {
                    $scope.contents = data;
                    $scope.loading = false;
                });
    }


    $scope.add = function () {
        $scope.country = false;
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $http.get('country/all').
                success(function (data, status, headers, config) {
                    console.log(data);
                    $scope.all_country = data;
                    $scope.loading = false;
                });
    }
    $scope.editcontent = function (category) {
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('country/edit/' + category.id, {
        }).success(function (data, status, headers, config) {
            $scope.country = data['content'];
            $scope.all_country = data['all_content'];
            $scope.loading = false;
        });
        ;
    };
    $scope.update = function (contents) {
        console.log(contents);
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('country/update', {
            name: contents.name,
            country_id: contents.pid,
            id: contents.id,
        }).success(function (data, status, headers, config) {

            //console.log(contents);
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                //console.log($scope.content.image);
                $scope.files = '';
                $scope.errors = false;
                $scope.success_flash = data[1];
            }
            $scope.loading = false;
        });
    };
    $scope.store = function (userData) {
        console.log(userData);
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('country/store', {
            name: userData.name,
            country_id: userData.id
        }).success(function (data, status, headers, config) {
            $scope.files = '';
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.deleteCity = function (index) {
        $scope.loading = true;
        //var user = $scope.contents[index];

        $http.post('country/delete', {
            del_id: index
        }).success(function (data, status, headers, config) {
            //$scope.contents.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.deleteCountry = function (index) {
        $scope.loading = true;
        var user = $scope.contents[index];
        $http.post('country/delete', {
            del_id: user.id
        }).success(function (data, status, headers, config) {
            $scope.contents.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.init();
});
// Product Option Management
app.controller('OptionController', function ($scope, $http) {
    $scope.values = [{
            option_name: null
        }];
    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.options = false;
    $scope.page = 'index';
    $scope.option = {};
    $scope.values = {};
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.get('option/all').
                success(function (data, status, headers, config) {
                    $scope.options = data;
                    $scope.loading = false;
                });
    }
    $scope.add = function () {
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.option = false;
    }
    $scope.editoption = function (option) {

        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('option/edit/' + option.id, {
        }).success(function (data, status, headers, config) {
            $scope.option = data['option'];
            $scope.values = data['values'];
            if ($scope.values.length == '0')
            {
                $scope.addInput();
            }
            $scope.loading = false;
        });
        ;
    };
    $scope.update = function (option, values) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('option/update', {
            option_name: option.option_name,
            status: option.status,
            id: option.id,
            option_value: values
        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {

                $scope.errors = false;
                $scope.option = {};
                $scope.values = {};
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.store = function (option) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('option/store', {
            option_name: option.option_name,
            status: option.status
        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.deleteoption = function (index) {
        $scope.loading = true;
        var option = $scope.options[index];
        $http.post('option/delete', {
            del_id: option.id
        }).success(function (data, status, headers, config) {
            $scope.options.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.addInput = function () {

        $scope.values.push({
            option_name: null
        });
    }

    $scope.removeInput = function (index) {
        $scope.values.splice(index, 1);
    }

    $scope.init();
});
/*****Products*****/
app.controller('ProductListController', function ($scope, $http) {
    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.$parent.ptitle = 'Product List';
    $scope.page = 'index';
    $scope.product = {};
    $scope.all_category = {};
    $scope.sub_category = {};
    $scope.success_flash = false;
    $scope.tab = 1;
    $scope.search = '';
    $scope.tags = '';
    $scope.resell = '';
    $scope.categoryShow = 0;
    $scope.searchTag = '';
    $scope.init = function () {
        $scope.page = 'index';
        $scope.resell = '';
        $scope.errors = false;
        $scope.loading = true;
        $scope.product = {};
        $scope.setTab = 1;
        $scope.tags = '';
        $scope.all_category = '';
        $scope.search = '';
        $scope.searchTag = '';
    }
    $scope.discard = function () {
        jQuery('.vk-product-search').show();
        $scope.resell = '';
    }
    $scope.store = function (resellproduct) {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('product/store', resellproduct).success(function (data, status, headers, config) {
            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                jQuery('.vk-product-search').show();
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    }
    $scope.sellThisProduct = function (id) {
        angular.forEach($scope.product, function (value, key) {
            if (value.id == id) {
                $scope.resell = value;
                jQuery('.vk-product-search').hide();
            }
        });
    }
    jQuery(document).on('click', '.vk-child-cat', function () {
        var id = jQuery(this).attr('data-id'), lavel = 1;
        $scope.getProducts(id);
        $scope.loading = true;
        lavel = jQuery(this).closest('ul').attr('data-lavel');
        $http.get('productlist/getCategory?cat_id=' + id).success(function (data, status, headers, config) {
            $scope.sub_category = data.all_category;
            var subHtml = '';
            lavel = parseInt(lavel) + 1;
            if (data.all_category.length) {
                for (var i in data.all_category) {
                    subHtml += '<li class="list-groupitem vk-child-cat" data-id="' + data.all_category[i].id + '" >' + data.all_category[i].category_name + '</li>';
                }
                var html = '<li class="pullleft colsm-3">' +
                        '<div class="input-group"><span class=" input-group-addon btn-primary" ><i class="fa fa-search"></i></span><input type="text" class="vk-catsearch form-control" placeholder="" /></div>' +
                        '<ul class="listgroup" data-lavel="' + lavel + '">' + subHtml + '</ul></li>';
                if (jQuery('ul[data-lavel="' + lavel + '"]').length) {
                    jQuery('ul[data-lavel="' + lavel + '"]').html(subHtml);
                } else {
                    jQuery('.vk-categorylist').append(html);
                }
            } else {
                jQuery('ul[data-lavel]').each(function () {
                    var l = jQuery(this).attr('data-lavel');
                    if (l >= lavel) {
                        jQuery(this).closest('li').remove();
                    }
                })
            }
            $scope.loading = false;
        });
    });
    jQuery(document).on('keyup', '.vk-catsearch', function () {
        var li = jQuery(this).closest('li');
        var searchText = jQuery(this).val().toLowerCase();
        jQuery('ul > li', li).each(function () {
            var currentLiText = jQuery(this).text().toLowerCase(), showCurrentLi = currentLiText.indexOf(searchText) !== -1;
            jQuery(this).toggle(showCurrentLi);
        });
    });
    $scope.searchItem = function (th) {

        if ($scope.searchTag) {
            $scope.search = 1;
            $http.get('productlist/searchTag?tag=' + $scope.searchTag).success(function (data, status, headers, config) {
                $scope.tags = data.tags;
            });
        } else {
            $scope.search = '';
            $scope.tags = '';
        }
    }
    $scope.prev = function () {

    }
    $scope.next = function () {

    }
    $scope.getCategory = function () {
        $scope.loading = true;
        $http.get('productlist/getCategory').success(function (data, status, headers, config) {
            $scope.all_category = data.all_category;
            $scope.loading = false;
        });
    }
    $scope.getProducts = function (id) {
        $scope.loading = true;
        $http.get('productlist/getProduct?tag=' + id).success(function (data, status, headers, config) {
            $scope.loading = false;
            $scope.product = data;
        });
    }
    $scope.init();
});
app.controller('ProductController', function ($scope, $http) {
    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.products = false;
    $scope.$parent.ptitle = 'Product Detail';
    $scope.page = 'index';
    $scope.product = {};
    $scope.images = {};
    $scope.product.images = [];
    $scope.pr_imgs = [];
    $scope.success_flash = false;
    $scope.tab = 1;
    $scope.variations = [];
    $scope.checkboxVariation = [];
    $scope.SelectVariations = [];
    $scope.Selectpro = [];
    $scope.SelectproOption = [];
    $scope.category_attr = '';
    $scope.pro_variant = {}
    $scope.pro_option = [];
    $scope.c_cat = '';
    $scope.discard = function (newTab) {

        $scope.tab = '';
        $scope.product = false;
        $scope.pro_variant = {}
        $scope.variations = [];
        $scope.checkboxVariation = [];
        $scope.SelectVariations = [];
        $scope.Selectpro = [];
        $scope.SelectproOption = [];
        $scope.tab = '';
        $scope.pro_option = [];
        $scope.variations = [];
        $scope.pr_imgs = [];
    };
    $scope.setTab = function (newTab) {
        $scope.tab = newTab;
    };
    $scope.isSet = function (tabNum) {
        return $scope.tab === tabNum;
    };
    $scope.showMe = false;
    $scope.myFunc = function () {
        $scope.showMe = !$scope.showMe;
    }
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $scope.product = {};
        $scope.products = '';
        $scope.pro_variant = {}
        $scope.variations = [];
        $scope.checkboxVariation = [];
        $scope.SelectVariations = [];
        $scope.Selectpro = [];
        $scope.SelectproOption = [];
        $scope.product_id = [];
        $scope.tab = '';
        $scope.pro_option = [];
        $scope.category_attr = '';
        $http.get('product/getCategory').success(function (data, status, headers, config) {
            $scope.pro_category = data.all_category;
            $scope.all_category = data.all_category;
            $scope.brand = data['brands'];
            $scope.loading = false;
            jQuery(".chosen-select").chosen();
        });
        $scope.productView = [];
        var c = [], v = [], option = {
            title: {
                text: ''
            },
            chart: {
                type: 'line',
                renderTo: 'linechart',
                defaultSeriesType: 'line',
                height: 180
            },
            xAxis: {
                categories: []
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                    showInLegend: false,
                    data: []
                }]
        };
        var chart;
        $http.get('product/all').success(function (data, status, headers, config) {
            $scope.products = data['products'];
            $scope.group_all_pros = data['products'];
            $scope.productView = data['productView'];

            angular.forEach($scope.products, function (item, key) {
                $scope.products[key].image = data.images[item.id];
            });
            angular.forEach($scope.productView, function (item, i) {
                option.xAxis.categories.push(item.created_at);
                option.series[0].data.push(parseInt(item.total));
            })

            chart = new Highcharts.Chart(option);
            $scope.loading = false;
        });


//        console.log($scope.productView);

//                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
//                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
//                    data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        $scope.pieData = [{
                name: "Chrome",
                y: 24.03,
                sliced: true,
                selected: true
            }, {
                name: "Firefox",
                y: 10.38
            }];

    }
    $scope.variant = function (obj, next, index, main) {
        next = typeof next == 'undefined' ? null : next;
        for (var i in obj) {
            if (next) {
                for (var n in next) {
                    $scope.variations.push([$scope.getAttrObj(obj[i]), $scope.getAttrObj(next[n])]);
                }
            } else {
                $scope.variations.push([$scope.getAttrObj(obj[i])]);
            }
        }
        if (next && (typeof main[parseInt(index) + 2] != 'undefined') && main[parseInt(index) + 2].length) {
            $scope.variant(next, main[parseInt(index) + 1], parseInt(index) + 1, main);
        }
    }
    $scope.getAttrObj = function (id) {
        var r, f = false;
        for (var i in $scope.category_attr) {
            for (var att in $scope.category_attr[i].attribute) {
                if ($scope.category_attr[i].attribute[att].id == id) {
                    r = $scope.category_attr[i].attribute[att];
                    f = true;
                    break;
                }
                for (var sop in $scope.category_attr[i].attribute[att].options) {
                    if ($scope.category_attr[i].attribute[att].options[sop].id == id) {
                        r = $scope.category_attr[i].attribute[att].options[sop];
                        f = true;
                        break;
                    }
                }
                if (f) {
                    break;
                }
            }
            if (f) {
                break;
            }
        }
        return r;
    }
    $scope.delVariant = function (id) {
        $scope.variations.splice(id, 1);
    }
    $scope.createProVariation = function () {
        var totalS = 0, totalC = 0, v = [], c = 1;
        $scope.variations = [];
        for (var i in $scope.SelectVariations) {
            if ($scope.SelectVariations[i].length && (typeof $scope.SelectVariations[parseInt(i) + 1] != 'undefined') && $scope.SelectVariations[parseInt(i) + 1].length) {
                c = 0;
                $scope.variant($scope.SelectVariations[i], $scope.SelectVariations[parseInt(i) + 1], parseInt(i) + 1, $scope.SelectVariations);
            } else {
                c && $scope.SelectVariations[i].length ? $scope.variant($scope.SelectVariations[i]) : '';
            }
        }
    }
    $scope.attribute_gr_add = function (cat_id) {
        $scope.c_cat = '';
        $scope.Selectpro = [];
        $scope.SelectproOption = [];
        angular.forEach(cat_id, function (v, i) {
            if (cat_id[i]) {
                $scope.c_cat = $scope.c_cat + ' ' + $scope.getCatName(i);
            }
        });
        if ($scope.c_cat.length) {
            jQuery('.vk-cat').hide();
        }
        $http.post('product/options', {cat: cat_id}).success(function (data, status, headers, config) {
            $scope.category_attr = data.attr_option;
            $scope.Selectpro = [];
            $scope.SelectproOption = [];
            $scope.variations = [];
            angular.forEach(data.attr_option, function (item, key) {
                for (var attr in item.attribute) {
                    if (item.attribute[attr].allow == 0) {
                        $scope.Selectpro.push(item.attribute[attr]);
                    } else {
                        $scope.SelectproOption.push(item.attribute[attr]);
                    }
                }
            });
        });
    }
    $scope.changeCat = function () {
        jQuery('.vk-cat').show();
    }
    $scope.add = function (newTab) {
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.tab = newTab;
        $scope.product = {};
        $scope.pro_variant = [];
        $scope.checkboxVariation = [];
        $scope.SelectVariations = [];
        $scope.pro_option = [];
        $scope.variations = [];
        $scope.pr_imgs = [];
    }

    $scope.uploadedFile = function (element) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            var fd = new FormData();
            fd.append("image", element.files[0]);
            fd.append("folder", 'product');
            fd.append("width", '800');
            fd.append("height", '800');
            $http.post('imagemutipleupload', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                if (data[0] == 'error') {
                    $scope.errors = data[1];
                } else {
                    $scope.errors = false;
                    $scope.pr_imgs.push({
                        img: data, def: 0
                    });
                    $scope.product.images.push({img: data, def: 0});
                    $scope.loading = false;
                }
                $scope.loading = false;
            });
        });
    }
    $scope.checkAll = function () {
        if (!$scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }
        $scope.product_id = [];
        angular.forEach($scope.products, function (item) {
            if ($scope.selectedAll)
            {
                $scope.product_id[item.id] = true;
            } else
            {
                $scope.product_id[item.id] = false;
            }
        });
    }
    $scope.setdefault = function (index) {
        angular.forEach($scope.pr_imgs, function (item, key) {
            $scope.pr_imgs[key]['def'] = 0;
        });
        $scope.pr_imgs[index]['def'] = 1;
    }
    $scope.unsetdefault = function (index) {

        $scope.pr_imgs[index]['def'] = 0;
    }
    $scope.removeimgs = function (img_nam, index)
    {
        $scope.errors = false;
        $scope.success_flash = false;
        $http.post('product/image_delete', {
            image: img_nam
        }).success(function (data, status, headers, config) {
            $scope.pr_imgs.splice(index, 1);
        });
    }
    $scope.store = function (product) {
        $scope.errors = false;
        $scope.success_flash = false;
        product.variationsOption = $scope.pro_variant;
        product.attributeOption = $scope.pro_option;
        $http.post('product/store', product).success(function (data, status, headers, config) {
            if (data[0] == 'error') {
                $scope.tab = 1;
                $scope.errors = data[1];
            } else {
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });

    };
    $scope.bulkAction = function (action) {

        $scope.page = 'index';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.loading = true;
        var ids = [];
        angular.forEach($scope.product_id, function (v, i) {
            if (v) {
                this.push(i);
            }
        }, ids);
        if (action == 'delete')
        {
            $http.post('product/delete', {
                action: action,
                id: ids
            }).success(function (data, status, headers, config) {
                $scope.success_flash = data[1];
                $scope.loading = false;
                $scope.init();
            });
        }
    }
    $scope.deleteproduct = function (index) {
        $scope.loading = true;
        var product = $scope.products[index];
        $http.post('product/delete', {
            del_id: product.id
        }).success(function (data, status, headers, config) {
            $scope.products.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    };
    $scope.editproduct = function (product) {

        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'index';
        $http.get('product/edit/' + product.id, {
        }).success(function (data, status, headers, config) {
            $scope.setTab(1);
            $scope.product = data['product'];
            $scope.product.images = data.product_img;
            $scope.pr_imgs = data.product_img;
            $scope.brand = data['brands'];
            $scope.pro_category = data.all_category;
            $scope.all_category = data.all_category;
            $scope.pro_option = data.product_attr;
            $scope.category_attr = data.pro_attr;
            $scope.Selectpro = [];
            $scope.SelectproOption = [];
            $scope.variations = [];
            $scope.pro_variant = data.pro_variant;
            angular.forEach(data.pro_attr, function (item, key) {
                for (var attr in item.attribute) {
                    if (item.attribute[attr].allow == 0) {
                        $scope.Selectpro.push(item.attribute[attr]);
                    } else {
                        $scope.SelectproOption.push(item.attribute[attr]);
                    }
                }
            });
            var t = [], c = 1;
            for (var i in data.choosen) {
                t.push(data.choosen[i]);
            }

            $scope.SelectVariations = t;
            $scope.createProVariation();
            $scope.loading = false;
        });
    };
    $scope.getCatName = function (id, all) {

        var name = '', cat, findCat = (typeof all == 'undefined' ? $scope.all_category : all);
        for (cat in findCat) {
            if (id == findCat[cat].id) {
                name = findCat[cat].category_name;
                break;
            }
            name = $scope.getCatName(id, findCat[cat].all_category);
            if (name.length) {
                break;
            }
        }
        if (name.length) {
            return name;
        } else {
            return '';
        }
    }
    $scope.update = function (product) {
        $scope.errors = false;
        $scope.success_flash = false; //console.log(product);
        $http.post('product/update', {
            id: product.id,
            pro_name: product.pro_name,
            pro_des: product.pro_des,
            pro_short_des: product.pro_short_des,
            pro_feature_des: product.pro_feature_des,
            seller_id: product.seller_id,
            pro_category_id: product.pro_category_id,
            brand_id: product.brand_id,
            product_tags: product.product_tags,
            price: product.price,
            no_stock: product.no_stock,
            meta_title: product.meta_title,
            meta_description: product.meta_description,
            meta_keywords: product.meta_keywords,
            status: product.status
        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
                $scope.success_flash = false;
            } else {

                $scope.errors = false;
                $scope.product = {};
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    };
    $scope.init();
});
app.controller('ProfileController', function ($scope, $http) {

    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.page = 'index';
    $scope.success_flash = false;
    $scope.init = function () {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.get('profile/all').
                success(function (data, status, headers, config) {
                    $scope.seller = data['seller'];
                    $scope.loading = false;
                });
    }
    $scope.update = function (sellerData) {
        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.post('profile/update', {
            id: sellerData.id,
            first_name: sellerData.fname,
            last_name: sellerData.lname,
            email: sellerData.email,
            current_password: sellerData.current_password,
            new_password: sellerData.new_password,
            confirm_new_password: sellerData.confirm_new_password
        }).
                success(function (data, status, headers, config) {
                    //$scope.seller = data['seller'];
                    $scope.loading = false;
                    if (data[0] == 'error') {
                        $scope.errors = data[1];
                    } else {

                        $scope.errors = false;
                        $scope.product = {};
                        $scope.success_flash = data[1];
                        $scope.init();
                    }
                    $scope.loading = false;
                });
    }
    $scope.init();
});

app.controller('PromotionController', function ($scope, $http, $interval) {
    $scope.errors = false;
    $scope.files = false;
    $scope.$parent.ptitle = 'Create Promotion';
    $scope.loading = true;
    $scope.enquirys = false;
    $scope.enquiry = false;
    $scope.btnenble = false;
    $scope.page = 'index';
    $scope.faq = false;
    $scope.disablesel = false;
    $scope.wizard = 1;
    $scope.promotionPay ={};
    $scope.create_promo = {};
    $scope.create_promo.banner_img = {};
    $scope.promot_rec = {};
    $scope.CurrentDate = new Date();
    $scope.success_flash = false;
    $scope.img_prog_bottom = false;
    $scope.img_prog_top = false;
    $scope.img_prog_side = false;
    $scope.init = function () {
        $scope.errors = false;

        $scope.loading = true;
        $http.get('create_promotion/get_campaign').
                success(function (data, status, headers, config) {
                    console.log(data);
                    $scope.campdata = data;

                    //console.log($scope.campdata);
                    $scope.loading = false;
                });

        $http.get('create_promotion/get_copy_campaign').success(function (data, status, headers, config) {
            $scope.copy_campdata = data;
            $scope.loading = false;
        });


    };

    $scope.selcrt_camp = function (camp) {
        $scope.campinputshow = false;
        $scope.updcampshow = false;
        $scope.copycampshow = false;
        //  console.log(camp);
        $scope.create_promo.start_date = $scope.CurrentDate;
        $scope.create_promo.end_date = $scope.CurrentDate;
        $scope.create_promo.add_content = '';
        $scope.create_promo.add_discrip = '';
        $scope.create_promo.product = '';
        $scope.create_promo.category = '';
        $scope.create_promo.select_view = '';
        $scope.create_promo.schedule = '';
        $scope.create_promo.ad_type = '';
        if (camp == 'create_new') {
            $scope.create_promo.newcamp = '';
            $scope.campinputshow = true;
            $scope.create_promo.id = '';
            $scope.disablesel = false;
            delete $scope.create_promo["upd_camp"];
        }
        if (camp == 'update_campn') {
            $scope.create_promo.upd_camp = '';
            $scope.create_promo.actn = 'upd';
            $scope.updcampshow = true;
            $scope.disablesel = true;
            //  delete $scope.create_promo["upd_camp"];
        }

        if (camp == 'copy_campn') {
            $scope.create_promo.cpy_camp = '';
            $scope.create_promo.actn = 'cpy';
            $scope.updcampshow = false;
            $scope.disablesel = false;
            $scope.copycampshow = true;
            delete $scope.create_promo["upd_camp"];
        }
        $scope.create_promo.promot = {};
    };

    $scope.selcrt_prom = function (prom) {
        $scope.prom_input_show = false;
        $scope.updpromshow = false;
        $scope.copypromshow = false;
        if (prom == 'create_new') {
            delete $scope.create_promo["upd_promot"];
            $scope.prom_input_show = true;
            //  $scope.create_promo.id='';
            $scope.create_promo.newpromot = '';
            $scope.create_promo.add_content = '';
            $scope.create_promo.add_discrip = '';
            $scope.create_promo.product = '';
            $scope.create_promo.category = '';
            $scope.btnenble = false;
        }
        if (prom == 'update_promn') {
            $scope.updpromshow = true;
            $scope.create_promo.promact = 'upd';
            var cam_id = $scope.create_promo.upd_camp;

            $http.get('create_promotion/get_promotn/' + cam_id).
                    success(function (data, status, headers, config) {
                        $scope.promotndata = data;
                        console.log($scope.promotndata);
                        $scope.loading = false;
                    });


            delete $scope.create_promo["newpromot"];
            $scope.create_promo.upd_promot = '';
            $scope.create_promo.add_content = '';
            $scope.create_promo.add_discrip = '';
            $scope.create_promo.product = '';
            $scope.create_promo.category = '';

            $scope.btnenble = false;
        }
        if (prom == 'copy_promo') {
            delete $scope.create_promo["upd_promot"];
            $scope.copypromshow = true;
            $scope.create_promo.promact = 'cpy';
            $http.get('create_promotion/get_copy_promotn/' + $scope.create_promo.ad_type).
                    success(function (data, status, headers, config) {
                        $scope.cpy_promotn_data = data;
                        console.log($scope.cpy_promotn_data);
                        $scope.loading = false;
                    });

        }
    };
    $scope.delBanner = function (prom, upload_type) {

        if (upload_type == 'top_banner')
        {
            $scope.create_promo.banner_img.top_banner = false;
        }
        if (upload_type == 'side_banner')
        {
            $scope.create_promo.banner_img.side_banner = false;
        }
        if (upload_type == 'bottom_banner')
        {
            $scope.create_promo.banner_img.bottom_banner = false;
        }
    }

    $scope.change_Ad = function (adtype) {
        $scope.errors = false;
        $scope.loading = true;
        //console.log(adtype);
        $http.get('create_promotion/get_promotion/' + adtype).
                success(function (data, status, headers, config) {
                    $scope.promot_rec = data;
                    //console.log($scope.promot_rec);
                    $scope.loading = false;
                });
    };
    $scope.change_catpro = function (catpro) {
        $scope.errors = false;
        $scope.loading = true;
        $http.get('create_promotion/change_catpro/' + catpro).
                success(function (data, status, headers, config) {
                    $scope.val_cps = data;
                    $scope.loading = false;
                });
    };
    $scope.uploadedBannerFile = function (element, upload_type) {
        $scope.$apply(function ($scope) {
            $scope.loading = true;
            if (upload_type == 'top_banner')
            {
                var img_size = $scope.create_promo.ban_size.top[0];
                $scope.img_prog_top = 0;
                $interval(function () {

                    if ($scope.img_prog_top < 80) {
                        $scope.img_prog_top = $scope.img_prog_top + 20;
                    }
                }, 100);
                var img_wt = $scope.create_promo.ban_size.top[1];
            }
            if (upload_type == 'side_banner')
            {
                var img_size = $scope.create_promo.ban_size.side[0];
                $scope.img_prog_side = 0;
                $interval(function () {
                    if ($scope.img_prog_side < 80) {
                        $scope.img_prog_side = $scope.img_prog_side + 20;
                    }
                }, 100);
                var img_wt = $scope.create_promo.ban_size.side[1];
            }
            if (upload_type == 'bottom_banner')
            {
                var img_size = $scope.create_promo.ban_size.bottom[0];
                $scope.img_prog_bottom = 0;
                $interval(function () {
                    if ($scope.img_prog_bottom < 80) {
                        $scope.img_prog_bottom = $scope.img_prog_bottom + 20;
                    }
                }, 100);
                var img_wt = $scope.create_promo.ban_size.bottom[1];
            }

            var fd = new FormData();
            fd.append("image", element.files[0]);
            fd.append("folder", 'promotion_banner');
            fd.append("size", img_size);
            fd.append("weight", img_wt);
            $http.post('create_promotion/update_adbanr', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (data, status, headers, config) {
                if (data[0] == 'error') {
                    $scope.errors = data[1];
                } else
                {
                    if (upload_type == 'top_banner')
                    {
                        $scope.create_promo.banner_img.top_banner = data;
                        $scope.img_prog_top = 100;
                    }
                    if (upload_type == 'side_banner')
                    {
                        $scope.create_promo.banner_img.side_banner = data;
                        $scope.img_prog_side = 100;
                    }
                    if (upload_type == 'bottom_banner')
                    {
                        $scope.create_promo.banner_img.bottom_banner = data;
                        $scope.img_prog_bottom = 100;
                    }


                }
            });

        });
    }
    $scope.change_placement = function (placement) {
        $scope.errors = false;
        $scope.loading = true;
        //console.log(placement);
        $http.get('create_promotion/placement/' + placement).
                success(function (data, status, headers, config) {
                    //	 console.log(data.create_package);
                    $scope.promot_rec.create_package = data.create_package;

                    $scope.loading = false;
                });
    };


    $scope.step_wizard = function (val) {
        $scope.wizard = val;
    };

    $scope.update_camp = function (upd_val_id, act) {
        console.log(upd_val_id);
        console.log(act);
        $scope.disablesel = false;
        $scope.promot_rec = {};
        $scope.create_promo.id = upd_val_id;
        $http.get('create_promotion/get_upd_campdata/' + upd_val_id).
                success(function (data, status, headers, config) {
                    //  console.log(data);
                    $scope.disablesel = true;
                    if ((data.updrec['ad_type']) == 'text_ad') {
                        $scope.create_promo.ad_type = 'Text Ad';
                    } else {
                        $scope.create_promo.ad_type = 'Banner Ad';
                    }
                    if (act == 'cpy') {
                        $scope.create_promo.newcamp = data.updrec['compaign_name'];
                    }

                    if (act == 'upd') {
                        $scope.create_promo.newcamp = '';
                    }
                    $scope.promot_rec.create_package = data.create_pakg;
                    $scope.promot_rec.schedule_status = data.schedule;
                    $scope.create_promo.ad_placement = data.updrec['ad_placement'];
                    $scope.create_promo.ban_size = data.ban_size;
                    $scope.camp_selct_data = data.updrec;

                    $scope.create_promo.select_view = data.updrec['view_price'];
                    $scope.create_promo.schedule = data.updrec['schedule'];
                    $scope.create_promo.start_date = data.updrec['start_date'];
                    $scope.create_promo.end_date = data.updrec['end_date'];
                    $scope.promot_rec.product_name = data.product;
                    $scope.promot_rec.category_name = data.all_cat;
                    $scope.create_promo.promot = {};
//                         $scope.create_promo.schedule=data[0].schedule; 
//                         $scope.create_promo.product=data[0].product_promote; 
//                         $scope.create_promo.category=data[0].destination_cat; 
//                         $scope.create_promo.add_content=data[0].adcontent_title; 
//                          $scope.create_promo.add_discrip=data[0].adcontent_discrip; 
                    $scope.loading = false;
                });
    };


    $scope.preview = function (prev_id) {
        //  console.log(prev_id);
        $http.get('create_promotion/get_camp_previw/' + prev_id + '/' + $scope.create_promo.ad_type).
                success(function (data, status, headers, config) {
                    console.log(data);
                    $scope.previw_data = data;
                    $scope.loading = false;

                });

    };

    $scope.saveAdtext = function (promot_rec) {
        $scope.errors = false;
        $scope.loading = true;
        console.log(promot_rec);
        $http.post('create_promotion/save_promotion_adtext', {
            adtext_data: promot_rec
        }).
                success(function (data, status, headers, config) {
//			$scope.promot_rec = data;
                    $scope.loading = false;
                    if (data[0] == 'error') {
                        //console.log($scope.create_promo);
                        $scope.errors = data[1];
                        $scope.success_flash = false;

                    } else {
                        $scope.success_flash = data[1];
                        $scope.errors = false;
                        $scope.create_promo.id = data[2];
                        $scope.create_promo.upd_camp = data[2];
                        if (data[3])
                        {
                            $scope.create_promo.ban_size = data[3];
                        }
                    }
                });


    };

    $scope.save_promotion = function (text_data) {
        //console.log(text_data);
        $scope.errors = false;
        $scope.btnenble = false;
        $scope.loading = true;
        $http.post('create_promotion/insert_promotion_adtext', {
            adtext_data: text_data
        }).success(function (data, status, headers, config) {
//			$scope.promot_rec = data;
            if (data[0] == 'error') {
                $scope.errors = data[1];
                $scope.success_flash = false;
            } else {
                $scope.success_flash = data[1];
                $scope.errors = false;

                $scope.loading = false;
                $scope.btnenble = true;
                $scope.create_promo.id = data[2];
            }
        });
    };

    $scope.select_promo_data = function (value, act, ad_type) {

        $scope.errors = false;
        $scope.loading = true;
        $scope.create_promo.ad_place = {};
        $http.get('create_promotion/get_promo_rec/' + value + '/' + ad_type).
                success(function (data, status, headers, config) {
                    // console.log(data);
                    if (ad_type == 'banner_ad' || ad_type == 'Banner Ad')
                    {
                        $scope.create_promo.id = data.promo_rec['id'];
                        $scope.create_promo.catprosto = data.promo_rec['type'];
                        /*$scope.create_promo.val_cps='';
                         $scope.create_promo.val_cps1= data.promo_rec['type_id']; */
                        $scope.create_promo.val_cps = data.promo_rec['type_id'];
                        //$scope.create_promo.val_cps ='8'; 
                        $scope.create_promo.banner_img.top_banner = data.promo_rec['top'];
                        if (data.promo_rec['top'] != '') {
                            $scope.create_promo.ad_place.top_banner = true;
                        }
                        $scope.create_promo.banner_img.side_banner = data.promo_rec['side'];
                        if (data.promo_rec['side'] != '') {
                            $scope.create_promo.ad_place.side_banner = true;
                        }
                        $scope.create_promo.banner_img.bottom = data.promo_rec['bottom'];
                        if (data.promo_rec['bottom'] != '') {
                            $scope.create_promo.ad_place.bottom_banner = true;
                        }
                        $scope.val_cps = data.listtype;
                        if (act == 'cpy') {
                            $scope.create_promo.newpromot = data.promo_rec['promotion_name'];
                        }

                        if (act == 'upd') {
                            $scope.create_promo.newpromot = '';
                        }
                        $scope.create_promo.category = [];
                        angular.forEach(data.selected_cat, function (val) {
                            $scope.create_promo.category.push(val.id.toString());
                        });
                    } else {
                        $scope.create_promo.id = data.promo_rec['id'];
                        $scope.create_promo.add_content = data.promo_rec['adcontent_title'];
                        $scope.create_promo.add_discrip = data.promo_rec['adcontent_discrip'];
                        $scope.create_promo.product = data.product['id'];
                        // $scope.create_promo.product='12';
                        if (act == 'cpy') {
                            $scope.create_promo.newpromot = data.promo_rec['promotion_name'];
                        }

                        if (act == 'upd') {
                            $scope.create_promo.newpromot = '';
                        }
                        $scope.create_promo.category = [];
                        angular.forEach(data.selected_cat, function (val) {
                            $scope.create_promo.category.push(val.id.toString());
                        });
                    }
                    console.log($scope.create_promo.category);
                    $scope.loading = false;
                    $scope.btnenble = true;
                });
    };
    $scope.payments = function () {
        console.log('payment');
        var $form = $('#payment-form');
        Stripe.card.createToken($form, function (status, response) {
            var $form = $('#payment-form');
            if (response.error) {
                $form.find('.payment-errors').text(response.error.message);
                $form.find('.payment-errors').addClass('alert alert-danger');
                $form.find('#submitBtn').prop('disabled', false);
            } else {
                var token = response.id;
                $form.find('.payment-errors').text('');
                $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                $scope.promotionPay.stripeToken=token;
                $scope.promotionPay.campain_id=$scope.create_promo.id;
                $scope.promotionPay.CampID=$scope.create_promo.upd_camp;
                $http.post('promotion/payment',$scope.promotionPay).success(function (data) {
                    console.log(data);
                })
                  $form.find('#submitBtn').prop('disabled', false);
            }
        });
    }
    $(document).on('click', "#accordion h3", function () {
        $(this).siblings('div.vk-accordion').addClass('hidden');
        $(this).next('div.vk-accordion').removeClass('hidden');
        $(this).siblings('input[name="payment_type"]').removeAttr('checked');
        $(this).find('input').attr('checked', 'checked');
    });
    $scope.init();
});
    app.controller('SpecialOfferController', function ($scope, $http) {
    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.page = 'index';
    $scope.offer = '';
    $scope.success_flash = false;
    $scope.tab = 1;
    $scope.coupon_datas = false;
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.setTab = function (newTab) {
        $scope.tab = newTab;
    };

    $scope.isSet = function (tabNum) {
        return $scope.tab === tabNum;
    };
    $scope.init = function () {
        $scope.image = '';

        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.get('special-offer/all').
                success(function (data, status, headers, config) {
                    $scope.offers = data['offers'];
                    console.log($scope.offers);
                    $scope.loading = false;
                });
    };

    $scope.add = function () {
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.offer = false;
        $scope.selectedRoles = [];
        $scope.selectedUsers = [];
        $scope.exselectedCats = [];
        $scope.selectedCats = [];
        $scope.exselectedItems = [];
        $scope.selectedItems = [];
        $scope.spselectedCats = [];
        $scope.spselectedItems = [];
        $http.get('coupon/all').
                success(function (data, status, headers, config) {
                    $scope.loading = false;

                });
    };

    $scope.store = function (offer) {
        $scope.errors = false;
        $scope.success_flash = false;
        console.log(offer);
        $scope.method = 'special';
        $http.post('special-offer/store', {
            adjustment_type: offer.adjustment_type,
            adjustment_value: offer.adjustment_value,
            amount_adjust: offer.amount_adjust,
            amount_purchase: offer.amount_purchase,
            apply_to: offer.apply_to,
            category_list: $scope.selectedCats,
            product_list: $scope.selectedItems,
            role_list: $scope.selectedRoles,
            customer_list: $scope.selectedUsers,
            specific_category: $scope.spselectedCats,
            specific_product: $scope.spselectedItems,
            condition_match: offer.condition_match,
            customers: offer.customers,
            end_date: offer.end_date,
            from_date: offer.from_date,
            products_adujst: offer.products_adujst,
            quantity_based_on: offer.quantity_based_on,
            rule_name: offer.role_name,
            method: $scope.method



        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;

        });
    };

    $scope.editoffer = function (offerData) {

        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('special-offer/edit/' + offerData.id, {
        }).success(function (data, status, headers, config) {
            $scope.offer_datas = data['offer'];
            $scope.selectedItems = $scope.offer_datas.product_data;
            $scope.spselectedItems = $scope.offer_datas.spproduct_data;
            $scope.selectedCats = $scope.offer_datas.category_data;
            $scope.spselectedCats = $scope.offer_datas.spcategory_data;
            $scope.selectedRoles = $scope.offer_datas.role_data;

            $scope.selectedUsers = $scope.offer_datas.user_data;
            console.log($scope.offer_datas);
            $scope.loading = false;
        });
    };

    $scope.update = function (offerData)
    {
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.post('special-offer/update', {
            id: offerData.id,
            adjustment_type: offerData.adjustment_type,
            adjustment_value: offerData.adjustment_value,
            amount_adjust: offerData.amount_adjust,
            amount_purchase: offerData.amount_purchase,
            apply_to: offerData.apply_to,
            category_list: $scope.selectedCats,
            product_list: $scope.selectedItems,
            role_list: $scope.selectedRoles,
            customer_list: $scope.selectedUsers,
            specific_category: $scope.spselectedCats,
            specific_product: $scope.spselectedItems,
            condition_match: offerData.condition_match,
            customers: offerData.customers,
            end_date: offerData.end_date,
            from_date: offerData.start_date,
            products_adujst: offerData.products_adujst,
            quantity_based_on: offerData.quantity_based_on,
            rule_name: offerData.offer_name,
            method: offerData.method

        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    }
    $scope.deleteoffer = function (index)
    {
        $scope.loading = true;

        var offer = $scope.offers[index];

        $http.post('special-offer/delete', {
            del_id: offer.id
        }).success(function (data, status, headers, config) {
            $scope.offers.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    }

    $scope.changeStatus = function (userData)
    {

        $scope.loading = true;
        $http.post('special-offer/changeStatus', {
            id: userData.id,
            status: userData.offer_status

        }).success(function (data, status, headers, config) {


            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    }

    $scope.getProduct = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getProduct', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.err = true;
                $scope.msg = data[1];
                $scope.products = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.products = data;
                $scope.err = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedItems = [];
    $scope.selectedItem = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedItems, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedItems.push(item);
            $scope.products = '';
            $scope.offer.product = '';
            console.log($scope.selectedItems);
        }
    }

    $scope.removeItem = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedItems.splice(index, 1);

    }
    //explode product

    $scope.getexProduct = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getProduct', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.exerr = true;
                $scope.msg = data[1];
                $scope.exproducts = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.exproducts = data;
                console.log($scope.exproducts);
                $scope.exerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.exselectedItems = [];
    $scope.exselectedItem = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.exselectedItems, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.exselectedItems.push(item);
            $scope.exproducts = '';
            $scope.offer.exproduct = '';
            console.log($scope.exselectedItems);
        }
    }

    $scope.exremoveItem = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.exselectedItems.splice(index, 1);

    }
    //category
    $scope.getCategory = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getCategory', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.cerr = true;
                $scope.msg = data[1];
                $scope.categories = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.categories = data;
                $scope.cerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedCats = [];
    $scope.selectedCat = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedCats, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedCats.push(item);
            $scope.categories = '';
            $scope.offer.category = '';
            console.log($scope.selectedCats);
        }
    }
    $scope.removeCat = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedCats.splice(index, 1);

    }
    //exclude category
    $scope.getexCategory = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getCategory', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.excerr = true;
                $scope.msg = data[1];
                $scope.excategories = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.excategories = data;
                $scope.excerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.exselectedCats = [];
    $scope.exselectedCat = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.exselectedCats, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.exselectedCats.push(item);
            $scope.excategories = '';
            $scope.offer.excategory = '';
            console.log($scope.exselectedCats);
        }
    }
    $scope.exremoveCat = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.exselectedCats.splice(index, 1);

    }

    $scope.getUser = function (pData)
    {
        $scope.loading = true;
        $http.post('special-offer/getUser', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.uerr = true;
                $scope.msg = data[1];
                $scope.users = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.users = data;
                console.log($scope.users);
                $scope.uerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedUsers = [];
    $scope.selectedUser = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedUsers, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedUsers.push(item);
            $scope.users = '';
            $scope.offer.user = '';
            console.log($scope.selectedUsers);
        }
    }

    $scope.removeUser = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedUsers.splice(index, 1);

    }
    //get Role
    $scope.getRole = function (pData)
    {
        $scope.loading = true;
        $http.post('special-offer/getRole', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.rerr = true;
                $scope.msg = data[1];
                $scope.role = '';
                console.log($scope.msg);
            } else
            {
                //$scope.ab=false;
                //console.log($scope.ab);
                $scope.loading = false
                $scope.role = data;
                console.log($scope.role);
                $scope.rerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedRoles = [];
    $scope.selectedRole = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedRoles, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedRoles.push(item);
            $scope.role = '';
            $scope.offer.role = '';
            //$scope.ab=true;
            //console.log($scope.ab);
        }
    }

    $scope.removeRole = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedRoles.splice(index, 1);

    }

    //Specific product

    $scope.getSpecificProduct = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getProduct', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.sperr = true;
                $scope.msg = data[1];
                $scope.spproducts = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.spproducts = data;
                ///console.log($scope.spproducts);
                $scope.sperr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.spselectedItems = [];
    $scope.spselectedItem = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.spselectedItems, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.spselectedItems.push(item);
            $scope.spproducts = '';
            $scope.offer.spproduct = '';
            console.log($scope.spselectedItems);
        }
    }

    $scope.spremoveItem = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.spselectedItems.splice(index, 1);

    }
    //specific category
    $scope.getspecificCategory = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getCategory', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.spcerr = true;
                $scope.msg = data[1];
                $scope.spcategories = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.spcategories = data;
                $scope.spcerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.spselectedCats = [];
    $scope.spselectedCat = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.spselectedCats, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.spselectedCats.push(item);
            $scope.spcategories = '';
            $scope.offer.spcategory = '';
            console.log($scope.spselectedCats);
        }
    }
    $scope.spremoveCat = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.spselectedCats.splice(index, 1);

    }
    $scope.init();
});
app.controller('BulkDiscountController', function ($scope, $http) {
    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.page = 'index';
    $scope.offer = '';
    $scope.success_flash = false;
    $scope.tab = 1;
    $scope.coupon_datas = false;
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.setTab = function (newTab) {
        $scope.tab = newTab;
    };

    $scope.isSet = function (tabNum) {
        return $scope.tab === tabNum;
    };
    $scope.init = function () {
        $scope.image = '';

        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.get('bulk-discount/all').
                success(function (data, status, headers, config) {
                    $scope.offers = data['offers'];
                    console.log($scope.offers);
                    $scope.loading = false;
                });
    };
    $scope.inputs = [{
            value: null
        }];

    $scope.addInput = function () {

        $scope.inputs.push({
            value: ''
        });
    }

    $scope.removeInput = function (index) {
        $scope.inputs.splice(index, 1);
    }
    $scope.add = function () {
        $scope.inputs = [{
                value: null
            }];
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.offer = false;
        $scope.selectedRoles = [];
        $scope.selectedUsers = [];
        $scope.exselectedCats = [];
        $scope.selectedCats = [];
        $scope.exselectedItems = [];
        $scope.selectedItems = [];
        $scope.spselectedCats = [];
        $scope.spselectedItems = [];
        $http.get('bulk-discount/all').
                success(function (data, status, headers, config) {
                    $scope.loading = false;

                });
    };

    $scope.store = function (offer) {
        $scope.errors = false;
        $scope.success_flash = false;
        console.log(offer);
        $scope.method = 'quantity';
        $http.post('bulk-discount/store', {
            quantity: $scope.inputs,
            apply_to: offer.apply_to,
            category_list: $scope.selectedCats,
            product_list: $scope.selectedItems,
            role_list: $scope.selectedRoles,
            customer_list: $scope.selectedUsers,
            condition_match: offer.condition_match,
            customers: offer.customers,
            end_date: offer.end_date,
            from_date: offer.from_date,
            quantity_based_on: offer.quantity_based_on,
            role_name: offer.role_name,
            method: $scope.method



        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;

        });
    };

    $scope.editoffer = function (offerData) {
        $scope.inputs = [{
                value: null
            }];
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('bulk-discount/edit/' + offerData.id, {
        }).success(function (data, status, headers, config) {
            $scope.offer_datas = data['offer'];
            $scope.selectedItems = $scope.offer_datas.product_data;
            $scope.spselectedItems = $scope.offer_datas.spproduct_data;
            $scope.selectedCats = $scope.offer_datas.category_data;
            $scope.spselectedCats = $scope.offer_datas.spcategory_data;
            $scope.selectedRoles = $scope.offer_datas.role_data;
            $scope.selectedUsers = $scope.offer_datas.user_data;

            if ($scope.offer_datas.quantity)
                $scope.inputs = $scope.offer_datas.quantity;

            $scope.loading = false;
        });
    };

    $scope.update = function (offerData)
    {
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.post('bulk-discount/update', {
            id: offerData.id,
            quantity: $scope.inputs,
            apply_to: offerData.apply_to,
            category_list: $scope.selectedCats,
            product_list: $scope.selectedItems,
            role_list: $scope.selectedRoles,
            customer_list: $scope.selectedUsers,
            condition_match: offerData.condition_match,
            customers: offerData.customers,
            end_date: offerData.end_date,
            from_date: offerData.start_date,
            quantity_based_on: offerData.quantity_based_on,
            role_name: offerData.role_name,
            method: offerData.method

        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    }
    $scope.deleteoffer = function (index)
    {
        $scope.loading = true;

        var offer = $scope.offers[index];

        $http.post('bulk-discount/delete', {
            del_id: offer.id
        }).success(function (data, status, headers, config) {
            $scope.offers.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    }

    $scope.changeStatus = function (userData)
    {

        $scope.loading = true;
        $http.post('bulk-discount/changeStatus', {
            id: userData.id,
            status: userData.offer_status

        }).success(function (data, status, headers, config) {


            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    }

    $scope.getProduct = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getProduct', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.err = true;
                $scope.msg = data[1];
                $scope.products = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.products = data;
                $scope.err = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedItems = [];
    $scope.selectedItem = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedItems, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedItems.push(item);
            $scope.products = '';
            $scope.offer.product = '';
            console.log($scope.selectedItems);
        }
    }

    $scope.removeItem = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedItems.splice(index, 1);

    }
    //explode product

    $scope.getexProduct = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getProduct', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.exerr = true;
                $scope.msg = data[1];
                $scope.exproducts = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.exproducts = data;
                console.log($scope.exproducts);
                $scope.exerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.exselectedItems = [];
    $scope.exselectedItem = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.exselectedItems, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.exselectedItems.push(item);
            $scope.exproducts = '';
            $scope.offer.exproduct = '';
            console.log($scope.exselectedItems);
        }
    }

    $scope.exremoveItem = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.exselectedItems.splice(index, 1);

    }
    //category
    $scope.getCategory = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getCategory', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.cerr = true;
                $scope.msg = data[1];
                $scope.categories = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.categories = data;
                $scope.cerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedCats = [];
    $scope.selectedCat = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedCats, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedCats.push(item);
            $scope.categories = '';
            $scope.offer.category = '';
            console.log($scope.selectedCats);
        }
    }
    $scope.removeCat = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedCats.splice(index, 1);

    }
    //exclude category
    $scope.getexCategory = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getCategory', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.excerr = true;
                $scope.msg = data[1];
                $scope.excategories = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.excategories = data;
                $scope.excerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.exselectedCats = [];
    $scope.exselectedCat = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.exselectedCats, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.exselectedCats.push(item);
            $scope.excategories = '';
            $scope.offer.excategory = '';
            console.log($scope.exselectedCats);
        }
    }
    $scope.exremoveCat = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.exselectedCats.splice(index, 1);

    }

    $scope.getUser = function (pData)
    {
        $scope.loading = true;
        $http.post('special-offer/getUser', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.uerr = true;
                $scope.msg = data[1];
                $scope.users = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.users = data;
                console.log($scope.users);
                $scope.uerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedUsers = [];
    $scope.selectedUser = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedUsers, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedUsers.push(item);
            $scope.users = '';
            $scope.offer.user = '';
            console.log($scope.selectedUsers);
        }
    }

    $scope.removeUser = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedUsers.splice(index, 1);

    }
    //get Role
    $scope.getRole = function (pData)
    {
        $scope.loading = true;
        $http.post('special-offer/getRole', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.rerr = true;
                $scope.msg = data[1];
                $scope.role = '';
                console.log($scope.msg);
            } else
            {
                //$scope.ab=false;
                //console.log($scope.ab);
                $scope.loading = false
                $scope.role = data;
                console.log($scope.role);
                $scope.rerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedRoles = [];
    $scope.selectedRole = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedRoles, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedRoles.push(item);
            $scope.role = '';
            $scope.offer.role = '';
            //$scope.ab=true;
            //console.log($scope.ab);
        }
    }

    $scope.removeRole = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedRoles.splice(index, 1);

    }

    //Specific product

    $scope.getSpecificProduct = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getProduct', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.sperr = true;
                $scope.msg = data[1];
                $scope.spproducts = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.spproducts = data;
                ///console.log($scope.spproducts);
                $scope.sperr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.spselectedItems = [];
    $scope.spselectedItem = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.spselectedItems, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.spselectedItems.push(item);
            $scope.spproducts = '';
            $scope.offer.spproduct = '';
            console.log($scope.spselectedItems);
        }
    }

    $scope.spremoveItem = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.spselectedItems.splice(index, 1);

    }
    //specific category
    $scope.getspecificCategory = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getCategory', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.spcerr = true;
                $scope.msg = data[1];
                $scope.spcategories = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.spcategories = data;
                $scope.spcerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.spselectedCats = [];
    $scope.spselectedCat = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.spselectedCats, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.spselectedCats.push(item);
            $scope.spcategories = '';
            $scope.offer.spcategory = '';
            console.log($scope.spselectedCats);
        }
    }
    $scope.spremoveCat = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.spselectedCats.splice(index, 1);

    }
    $scope.init();
});
app.controller('CouponController', function ($scope, $http) {
    $scope.errors = false;
    $scope.files = '';
    $scope.loading = true;
    $scope.page = 'index';
    $scope.coupon = '';
    $scope.success_flash = false;
    $scope.tab = 1;
    $scope.coupon_datas = false;
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.setTab = function (newTab) {
        $scope.tab = newTab;
    };

    $scope.isSet = function (tabNum) {
        return $scope.tab === tabNum;
    };
    $scope.init = function () {
        $scope.image = '';

        $scope.page = 'index';
        $scope.errors = false;
        $scope.loading = true;
        $http.get('coupon/all').
                success(function (data, status, headers, config) {
                    $scope.coupons = data['coupon'];
                    $scope.category = data['category'];

                    //console.log($scope.plans);
                    $scope.loading = false;
                });
    };

    $scope.add = function () {
        $scope.page = 'add';
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.coupons = false;
        $scope.selectedUsers = [];
        $scope.exselectedCats = [];
        $scope.selectedCats = [];
        $scope.exselectedItems = [];
        $scope.selectedItems = [];
        $http.get('coupon/all').
                success(function (data, status, headers, config) {
                    $scope.loading = false;

                });
    };

    $scope.store = function (coupon) {
        $scope.errors = false;
        $scope.success_flash = false;
        console.log(coupon);
        $http.post('coupon/store', {
            coupon_name: coupon.coupon_name,
            description: coupon.description,
            discount_type: coupon.discount_type,
            discount_value: coupon.discount_value,
            free_shipp: coupon.free_shipp,
            usage_limit_coupon: coupon.usage_limit_coupon,
            usage_limit_user: coupon.usage_limit_user,
            expire_date: coupon.expire_date,
            exclude_sale: coupon.exclude_sale,
            min_spend: coupon.min_amount,
            max_spend: coupon.max_amount,
            individual: coupon.individual,
            products: $scope.selectedItems,
            exclude_products: $scope.exselectedItems,
            category: $scope.selectedCats,
            exclude_category: $scope.exselectedCats,
            user_email: $scope.selectedUsers,
            coupon_status: coupon.status


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;

        });
    };

    $scope.editcoupon = function (couponData) {

        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.get('coupon/edit/' + couponData.id, {
        }).success(function (data, status, headers, config) {
            $scope.coupon_datas = data['coupon'];
            $scope.selectedItems = $scope.coupon_datas.product_data;
            $scope.exselectedItems = $scope.coupon_datas.exproduct_data;
            $scope.selectedCats = $scope.coupon_datas.category_data;
            $scope.exselectedCats = $scope.coupon_datas.excategory_data;
            $scope.selectedUsers = $scope.coupon_datas.user_email;
            console.log($scope.coupon_datas);
            $scope.loading = false;
        });
    };

    $scope.update = function (couponData)
    {
        $scope.loading = true;
        $scope.errors = false;
        $scope.success_flash = false;
        $scope.page = 'edit';
        $http.post('coupon/update', {
            coupon_id: couponData.id,
            coupon_name: couponData.coupon_name,
            description: couponData.description,
            discount_type: couponData.discount_type,
            discount_value: couponData.discount_value,
            free_shipp: couponData.free_shipp,
            usage_limit_coupon: couponData.usage_limit_coupon,
            usage_limit_user: couponData.usage_limit_user,
            expire_date: couponData.expire_date,
            exclude_sale: couponData.exclude_sale,
            min_spend: couponData.min_spend,
            max_spend: couponData.max_spend,
            individual: couponData.individual,
            products: $scope.selectedItems,
            exclude_products: $scope.exselectedItems,
            category: $scope.selectedCats,
            exclude_category: $scope.exselectedCats,
            user_email: $scope.selectedUsers,
            coupon_status: couponData.coupon_status
        }).success(function (data, status, headers, config) {

            if (data[0] == 'error') {
                $scope.errors = data[1];
            } else {
                $scope.errors = false;
                $scope.success_flash = data[1];
                $scope.init();
            }
            $scope.loading = false;
        });
    }
    $scope.deletecoupon = function (index)
    {
        $scope.loading = true;

        var coupon = $scope.coupons[index];

        $http.post('coupon/delete', {
            del_id: coupon.id
        }).success(function (data, status, headers, config) {
            $scope.coupons.splice(index, 1);
            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    }

    $scope.changeStatus = function (userData)
    {

        $scope.loading = true;
        $http.post('coupon/changeStatus', {
            id: userData.id,
            status: userData.coupon_status

        }).success(function (data, status, headers, config) {


            $scope.loading = false
            $scope.success_flash = data[1];
            $scope.init();
        });
    }

    $scope.getProduct = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getProduct', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.err = true;
                $scope.msg = data[1];
                $scope.products = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.products = data;
                $scope.err = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedItems = [];
    $scope.selectedItem = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedItems, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedItems.push(item);
            $scope.products = '';
            $scope.coupons.product = '';
            console.log($scope.selectedItems);
        }
    }

    $scope.removeItem = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedItems.splice(index, 1);

    }
    //explode product

    $scope.getexProduct = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getProduct', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.exerr = true;
                $scope.msg = data[1];
                $scope.exproducts = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.exproducts = data;
                console.log($scope.exproducts);
                $scope.exerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.exselectedItems = [];
    $scope.exselectedItem = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.exselectedItems, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.exselectedItems.push(item);
            $scope.exproducts = '';
            $scope.coupons.exproduct = '';
            console.log($scope.exselectedItems);
        }
    }

    $scope.exremoveItem = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.exselectedItems.splice(index, 1);

    }
    //category
    $scope.getCategory = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getCategory', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.cerr = true;
                $scope.msg = data[1];
                $scope.categories = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.categories = data;
                $scope.cerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedCats = [];
    $scope.selectedCat = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedCats, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedCats.push(item);
            $scope.categories = '';
            $scope.coupons.category = '';
            console.log($scope.selectedCats);
        }
    }
    $scope.removeCat = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedCats.splice(index, 1);

    }
    //exclude category
    $scope.getexCategory = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getCategory', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.excerr = true;
                $scope.msg = data[1];
                $scope.excategories = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.excategories = data;
                $scope.excerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.exselectedCats = [];
    $scope.exselectedCat = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.exselectedCats, function (eachmovie) { //For loop
            if (item.id == eachmovie.id) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.exselectedCats.push(item);
            $scope.excategories = '';
            $scope.coupons.excategory = '';
            console.log($scope.exselectedCats);
        }
    }
    $scope.exremoveCat = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.exselectedCats.splice(index, 1);

    }

    $scope.getUser = function (pData)
    {
        $scope.loading = true;
        $http.post('coupon/getUser', {
            keyWord: pData


        }).success(function (data, status, headers, config) {

            if (data[0] == 'error')
            {
                $scope.uerr = true;
                $scope.msg = data[1];
                $scope.users = '';
                console.log($scope.msg);
            } else
            {
                $scope.loading = false
                $scope.users = data;
                $scope.uerr = false;
                $scope.msg = '';
                //$scope.init();
            }
        });
    }
    $scope.selectedUsers = [];
    $scope.selectedUser = function (item)
    {
        oldmovies = '';
        angular.forEach($scope.selectedUsers, function (eachmovie) { //For loop
            if (item.email == eachmovie.email) { // this line will check whether the data is existing or not
                oldmovies = true;
            }
        });
        if (!oldmovies)
        {
            item.selected = true;
            $scope.selectedUsers.push(item);
            $scope.users = '';
            $scope.coupons.user = '';
            console.log($scope.selectedUsers);
        }
    }

    $scope.removeUser = function (index)
    {
        //var product = $scope.selectedItems[index];
        $scope.selectedUsers.splice(index, 1);

    }
    $scope.init();
});